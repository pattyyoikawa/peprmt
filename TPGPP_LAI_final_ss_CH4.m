function ss = TPGPP_LAI_final_ss_CH4(theta,data)

%time  = data.ydata(:,1);
ydata_m = data.ydata(:,2);%CH4 exchange obs daily integral
gapfill_error_m = data.ydata(:,3);%daily integral gapfilling error pre-loaded
random_error_m = data.ydata(:,4);%daily integral random error pre-loaded umol CH4 m-2 d-1

xdata  = data.xdata;

%run model
ymodel_m = TPGPP_LAI_final_sys_CH4(theta,xdata);
ymodel_m=ymodel_m';
% figure
% plot(ydata_m,'.')
% hold on
% plot(ymodel_m,'.r')
%With random error neg log likelihood fxn
%random_error_m=14.1811-(0.1329*ydata_m);%for CH4 at West Pond

 %With random error neg log likelihood fxn
% random_error=1.05-(0.043*ydata);%for NEE at West Pond
% sigma = 1/n * nansum(((ydata-ymodel)./(random_error)).^2);%with random error- using Flux to estimate it-see daily diff code--linear regression good for CH4 in West Pond 2012-2014
% ln_init1 = -(n/2)*log(2*pi) ;
% ln_init2 = -(n/2)*log(sigma);
% ln_init3 = - 1/(2*sigma);
% ln_init4 = nansum(((ydata-ymodel)./(random_error)).^2);
% ln_init=ln_init1+ln_init2+ln_init3*ln_init4;
%  ss = -ln_init;

%calculate sample size or # of non nan obs
nan_obs_m=sum(isnan(ymodel_m));
n_m = length(ymodel_m)-nan_obs_m;

%simple least squares optimization - following Keenan 2011 and 2012
 ss1 = ((ydata_m-ymodel_m)./(random_error_m+gapfill_error_m)).^2;
 ss = (nansum(ss1))/n_m;

 
 %OLD CODE
 %weight 2012 data higher than 2013 data for parameterization b/c 2013 was a
%strange year for CH4
% ss1_m = zeros(1,length(time));
% for i=1:length(time);
%    if i<108 %1:172 = 2012 high ch4 season
%  ss1_m(i) = (((ydata_m(i)-ymodel_m(i))./(random_error_m(i))).^2)*500;%5x as much weight
%    else
%  ss1_m(i) = ((ydata_m(i)-ymodel_m(i))./(random_error_m(i))).^2;
%    end
% end
% ss_m = (nansum(ss1_m))/(n_m);
 
end%negative log likelihood function based on Gaussian prob distribution
%%negative log likelihood--the more positive the better--so I change the sign so that mcmc run is minimizing this 


