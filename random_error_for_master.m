%random error for master data files

%new random error equations using new WP data 
%2015.05.29

%CO2
random_error_old=1.05-(0.043*wc);%for NEE at West Pond-OLD

random_error_pos=(0.067*wc)+0.68;%for NEE at West Pond-NEW
random_error_neg=(-0.051*wc)+0.74;%for NEE at West Pond-NEW

random_error_new=random_error_neg*0;
Sel=wc>0;
random_error_new(Sel)=random_error_pos(Sel);

Sel=wc<=0;
random_error_new(Sel)=random_error_neg(Sel);

figure
plot(decday,random_error_pos,'.',decday,random_error_neg,'.',decday,random_error_new,'.')
legend('pos','neg','new')
hold on
plot(decday,wc,'.')
figure
plot(random_error_old,'.')
hold on
plot(random_error_new,'.r')

%CH4
random_error_wm_old=14.1811+(0.1329*wm);%for CH4 at West Pond
random_error_wm_new=(0.14*wm)+12;%for CH4 at West Pond
figure
plot(random_error_wm_old,'.')
hold on
plot(random_error_wm_new,'.r')


%Data you need to run all codes-updated on 2015.06.10
%daily_wm is daily integral of wm_gf in umol m-2 d-1
%daily_wc is daily integral of wc_gf in umol m-2 d-1
data_master.daily=[daily_wm daily_wc daily_gapfilling_error_wm daily_gapfilling_error_wc daily_random_error_wm daily_random_error_wc daily_er_ANNnight daily_gpp_ANNnight];
data_master.halfhr=[DOY DOY_disc TA WT_gf PAR_gf LAI_gf GPP_min decday Season_drop];
data_master.halfhr_CI_gapfill=[wc_90CI wc_90CI_lower wc_90CI_upper wm_90CI wm_90CI_lower wm_90CI_upper];
data_master.halfhr_CI_random=[random_error_wc_prop_lower random_error_wc_prop_upper random_error_wm_prop_lower random_error_wm_prop_upper];
data_master.halfhr_obs=[wc wc_gf wm wm_gf gpp_ANNnight er_ANNnight];

daily_wm =data_master.daily(:,1);
daily_wc =data_master.daily(:,2);
daily_gapfilling_error_wm =data_master.daily(:,3);
daily_gapfilling_error_wc=data_master.daily(:,4);
daily_random_error_wm =data_master.daily(:,5);
daily_random_error_wc =data_master.daily(:,6);
daily_er_ANNnight =data_master.daily(:,7);
daily_gpp_ANNnight=data_master.daily(:,8);

DOY=data_master.halfhr(:,1);
DOY_disc=data_master.halfhr(:,2);
TA =data_master.halfhr(:,3);
WT_gf =data_master.halfhr(:,4);
PAR_gf =data_master.halfhr(:,5);
LAI_gf =data_master.halfhr(:,6);
GPP_min =data_master.halfhr(:,7);
decday =data_master.halfhr(:,8);
Season_drop=data_master.halfhr(:,9);

wc_90CI =data_master.halfhr_CI(:,1);
wc_90CI_lower =data_master.halfhr_CI(:,2);
wc_90CI_upper =data_master.halfhr_CI(:,3);
wm_90CI =data_master.halfhr_CI(:,4);
wm_90CI_lower =data_master.halfhr_CI(:,5);
wm_90CI_upper =data_master.halfhr_CI(:,6);

wc =data_master.halfhr_obs(:,1);
wc_gf=data_master.halfhr_obs(:,2);
wm =data_master.halfhr_obs(:,3);
wm_gf =data_master.halfhr_obs(:,4);
gpp_ANNnight =data_master.halfhr_obs(:,5);
er_ANNnight=data_master.halfhr_obs(:,6);

%loaded new L3 data with all of 2014
Sel=data.year<2015;
er_ANNnight=data.er_ANNnight(Sel);
gpp_ANNnight=data.gpp_ANNnight(Sel);
wc_gf=data.wc_gf(Sel);
decday=data.decday(Sel);
wm=data.wm(Sel);
wm_gf=data.wm_gf(Sel);
wc=data.wc(Sel);

Sel=Metdata.YEAR<2015;
PAR=Metdata.PAR(Sel);
DOY=Metdata.DOY(Sel);
WT=Metdata.WT(Sel);
TA=Metdata.TA(Sel);

%stitch together
WT_gf(40849:43295)=WT(40849:43295);
PAR_gf(40849:43295)=PAR(40849:43295);

figure
plot(Season_drop,'.')
hold on
plot(gpp_ANNnight,'.')

Season_drop(40849:43295)=1;
Season_drop(38497:40176)=5;
Season_drop(40177:41857)=6;
%1680=length of season 6

DOY_disc=DOY;%-365-366;
DOY=DOY_disc;
DOY(8256:end)=DOY(8256:end)+366;
DOY(25776:end)=DOY(25776:end)+365;

figure
plot(DOY,'.')
hold on
plot(decday,'.')
figure
plot(diff(DOY))

% need a Random error to add to the 90% CI gf error at 30min time step
%where RE is only RE in gaps and RE + NEEobs in non-gaps
random_error_wc_prop_lower=random_error_wc/2;%this is total random error at every 30min time step but goes to 0 when there is a gap in the data
random_error_wm_prop_lower=random_error_wm/2;
random_error_wc_prop_upper=random_error_wc/2;%this is total random error at every 30min time step but goes to 0 when there is a gap in the data
random_error_wm_prop_upper=random_error_wm/2;

figure
plot(random_error_wc,'.')

wm_0=wm;
wc_0=wc;
Sel=isnan(wm_0);
wm_0(Sel)=0;
Sel=isnan(wc_0);
wc_0(Sel)=0;

for i=1:length(wm_0)
    if wm_0(i)==0
        random_error_wm_prop_lower(i)=(random_error_wm(i)/2);%if there is a gap, then just have the pure random error
        random_error_wm_prop_upper(i)=(random_error_wm(i)/2);%if there is a gap, then just have the pure random error
    else
        random_error_wm_prop_lower(i)=wm(i)-(random_error_wm(i)/2);%if there is no gap, then add this random error to the obs wm
        random_error_wm_prop_upper(i)=wm(i)+(random_error_wm(i)/2);%if there is no gap, then add this random error to the obs wm
    end
    if wc_0(i)==0
        random_error_wc_prop_lower(i)=(random_error_wc(i)/2);%if there is no gap, then add this random error to the obs NEE
        random_error_wc_prop_upper(i)=(random_error_wc(i)/2);%if there is no gap, then add this random error to the obs NEE
    else
        random_error_wc_prop_lower(i)=wc(i)-(random_error_wc(i)/2);%if there is a gap in the data, then the gapfilling error counts
        random_error_wc_prop_upper(i)=wc(i)+(random_error_wc(i)/2);%if there is a gap in the data, then the gapfilling error counts
    end
end

figure
plot(decday,wm_gf,'.g',decday,wm_90CI_lower,'.r',decday,wm_90CI_upper,'.k')
hold on
plot(decday,wm,'.',decday,random_error_wm_prop_lower,'.',decday,random_error_wm_prop_upper,'.')
legend('wm gf','lower gf','upper gf','wm','lower re','upper re')

figure
plot(decday,wc_gf,'.g',decday,wc_90CI_lower,'.r',decday,wc_90CI_upper,'.k')
hold on
plot(decday,wc,'.',decday,random_error_wc_prop_lower,'.',decday,random_error_wc_prop_upper,'.')
legend('wc gf','lower gf','upper gf','wc','lower re','upper re')


