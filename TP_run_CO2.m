%Data you need to run all codes

%paramerterization data
%run using modeled GPP
%MAKE SURE daily data are in correct units!!!
%daily_Reco_obs_param + daily_GPP_obs_param data are already in g C m-2 d-1
daily_gapfilling_error_wc_param = daily_gapfilling_error_wc_param*10^-6*44.01*0.27*-1;
daily_random_error_wc_param = daily_random_error_wc_param*10^-6*44.01*0.27*-1;
Sel=daily_gapfilling_error_wc_param<0;
daily_gapfilling_error_wc_param(Sel)=0;
Sel=daily_random_error_wc_param<0;
daily_random_error_wc_param(Sel)=0;
figure
plot(daily_random_error_wc_param)
hold on
plot(daily_gapfilling_error_wc_param)

xdata=[DOY_param DOY_disc_param TA_param WT_gf_param PAR_gf_param LAI_gf_param GPP_min_param decday_param Season_drop_param wc_90CI_param site_param wetland_age_param];
ydata=[DOY_param wc_gf_param];%this has NEE umol m-2 s-1 and CH4 umol m-2 30min-1
zdata=[daily_gapfilling_error_wc_param daily_random_error_wc_param daily_Reco_obs_param' daily_GPP_obs_param'];

theta=[0 0];
theta=theta';
Reco_TP_param = TP_final_sys_CO2(theta,xdata);
%Reco
figure
plot(Reco_TP_param)
hold on
plot(er_ANNnight_param)
legend('mod','obs')



%% start MDF
%use for running TP_final_sys_CO2
model.ssfun = @TP_final_ss_CO2;
model.modelfun = @TP_final_sys_CO2;

data.xdata = xdata;
data.ydata = ydata;
data.zdata = zdata;

Rsum_obs = data.ydata(:,2);
nan_obs = sum(isnan(Rsum_obs));
n_obs = length(Rsum_obs) - nan_obs;
model.N     = n_obs;%total number of non nan obs
%model.S20 = [1 1 2];%has to do with error variance of prior
%model.N0  = [4 4 4];%has to do with error variance of prior

params = {
    {'rref',          0,     -2,         20}%entire range of observed values of Reco at 15C
    {'eo',            0,     -40,        150}%range based on Migliavacca 2011
%     name      start/theta/par  lBound   uBound    mean_prior   std_prior
    };

options.method = 'dram';
options.verbosity  = 0;%level of info printed
options.burnintime = 50000;% burn in before adaptation starts
%[results, chain, s2chain] = mcmcrun_FINAL(model,data,params,options,results);
% [RESULTS,CHAIN,S2CHAIN,SSCHAIN] = mcmcrun_FINAL(MODEL,DATA,PARAMS,OPTIONS)
%First generate an initial chain
options.nsimu = 50000;
[results, chain, s2chain, sschain]= mcmcrun_FINAL(model,data,params,options);

figure
plot(sschain,'.')%sum-of-squares chains
nanmin(sschain)
Sel=sschain<nanmin(sschain)+0.000000001;%442
figure
plot(Sel)
% cumsum(Sel)
find_min=chain(Sel,:);
find_min=find_min(end,:)';
find_min_Reco_TP=find_min;
Reco_TP_param = TP_final_sys_CO2(find_min,xdata);
%Reco
figure
plot(Reco_TP_param)
hold on
plot(er_ANNnight_param)
legend('mod','obs')

%Then re-run starting from the results of the previous run, this will take about 10 minutes.
params = {
    {'rref',         find_min(1),     -2,         20}%entire range of observed values of Reco at 15C
    {'eo',           find_min(2),     -40,        150}%range based on Migliavacca 2011
    };

options.method = 'dram';
options.verbosity  = 0;%level of info printed
options.burnintime = 0;% burn in before adaptation starts

options.nsimu = 150000;
[results, chain, s2chain, sschain] = mcmcrun_FINAL(model,data,params,options, results);

figure
mcmcplot(chain,[],results,'chainpanel');%plots chain for ea parameter and all the values it accepted
figure
mcmcplot(chain,[],results,'pairs');%tells you if parameters are correlated
%  figure
%  mcmcplot(sqrt(s2chain),[],[],'hist')%take square root of the s2chain to get the chain for error standard deviation
% title('Error std posterior')
figure
mcmcplot(chain,[],results,'denspanel',2);

chainstats(chain,results)

sschain_long=sschain;%save before you move on
chain_long=chain;
results_long=results;

%find the minimum
figure
plot(sschain_long,'.')%sum-of-squares chains
nanmin(sschain_long)
Sel=sschain_long<nanmin(sschain_long)+0.000001;%442
figure
plot(Sel)
% cumsum(Sel)
find_min=chain_long(Sel,:);
find_min_CO2_TP=find_min(end,:)';


%% Choose posterior
chain_TP_Reco=chain;
chain_long_TP_Reco=chain_long;
%First establish if chain stabilized
chainstats(chain_TP_Reco,results)
%Geweke test should be high ideally >0.9 for all parameters

%rejection rate of chain needs to be less than 50%
results_long.rejected*100

%Determine where means and sd of parameters stabilize in the chain
%this tells you what part of chain is stable for selection of posterior

%see where means and sd of parameters stabilize in the chain
%this tells you what part of chain is stable for selection of posterior
window_size = 5000;
simple = tsmovavg(chain_long_TP_Reco(:,1),'s',window_size,1);
simple_std = movingstd(chain_long_TP_Reco(:,1),5000);
figure
subplot(2,1,1)
plot(simple,'.')
title('5000 iteration moving average Rref')
subplot(2,1,2)
plot(simple_std,'.')
title('5000 iteration moving SD Rref')

window_size = 5000;
simple = tsmovavg(chain_long_TP_Reco(:,2),'s',window_size,1);
simple_std = movingstd(chain_long_TP_Reco(:,2),5000);
figure
subplot(2,1,1)
plot(simple,'.')
title('5000 iteration moving average Eo')
subplot(2,1,2)
plot(simple_std,'.')
title('5000 iteration moving SD Eo')

%identified visually that chain is good around 100,000 - 2015.06.11
%choose parammeters 10,000 parameter sets from that area
%e.g. 100,000-110,000

%find_min_10000_Reco_TP=chain_TP_Reco(100001:110000,:);
%using shorter chain for now
find_min_10000_Reco_TP=chain_TP_Reco(40001:50000,:);


%run 10000 parameter sets through validation data + and test chi square
% xdata=[DOY_valid DOY_disc_valid TA_valid WT_gf_valid PAR_gf_valid LAI_gf_valid GPP_min_valid decday_valid Season_drop_valid wc_90CI_valid site_valid wetland_age_valid];
% ydata=[DOY_valid wc_gf_valid];%this has NEE umol m-2 s-1 and CH4 umol m-2 30min-1
%zdata=[daily_gapfilling_error_wc_valid daily_random_error_wc_valid daily_Reco_obs_valid daily_GPP_obs_valid];

%run VALIDATION DATA in 2 parts b/c matrix is too big
%MB FIRST
xdata=[DOY_valid(1:17520) DOY_disc_valid(1:17520) TA_valid(1:17520) WT_gf_valid(1:17520)...
    PAR_gf_valid(1:17520) LAI_gf_valid(1:17520) GPP_min_valid(1:17520) decday_valid(1:17520)...
    Season_drop_valid(1:17520) wc_90CI_valid(1:17520) site_valid(1:17520) wetland_age_valid(1:17520)];
ydata=[DOY_valid(1:17520) wc_gf_valid(1:17520)];%this has NEE umol m-2 s-1 and CH4 umol m-2 30min-1

%make 1000 theta sets that will be used to estimate CI 
 model_pop_Reco=zeros(length(1:17520));%length of validation data
 model_pop_Reco=model_pop_Reco(:,1:10000);%update as needed 

 
for i=1:length(find_min_10000_Reco_TP)
    Reco=TP_final_sys_CO2(find_min_10000_Reco_TP(i,:)',xdata);
    model_pop_Reco(:,i)=Reco;
    
end

model_std_Reco_TP_MB=std(model_pop_Reco');

%WP 
xdata=[DOY_valid(17521:end) DOY_disc_valid(17521:end) TA_valid(17521:end) WT_gf_valid(17521:end)...
    PAR_gf_valid(17521:end) LAI_gf_valid(17521:end) GPP_min_valid(17521:end) decday_valid(17521:end)...
    Season_drop_valid(17521:end) wc_90CI_valid(17521:end) site_valid(17521:end) wetland_age_valid(17521:end)];
ydata=[DOY_valid(17521:end) wc_gf_valid(17521:end)];%this has NEE umol m-2 s-1 and CH4 umol m-2 30min-1

%make 1000 theta sets that will be used to estimate CI 
 model_pop_Reco=zeros(length(1:17520));%length of validation data
 model_pop_Reco=model_pop_Reco(:,1:10000);%update as needed 

 
for i=1:length(find_min_10000_Reco_TP)
    Reco=TP_final_sys_CO2(find_min_10000_Reco_TP(i,:)',xdata);
    model_pop_Reco(:,i)=Reco;
    
end

model_std_Reco_TP_WP=std(model_pop_Reco');
model_std_Reco_TP_valid=vertcat(model_std_Reco_TP_MB',model_std_Reco_TP_WP');


%% according to Zobitz 2008, model mean==model min(best para set)
 xdata=[DOY_valid DOY_disc_valid TA_valid WT_gf_valid PAR_gf_valid LAI_gf_valid GPP_min_valid decday_valid Season_drop_valid wc_90CI_valid site_valid wetland_age_valid];
 ydata=[DOY_valid wc_gf_valid];%this has NEE umol m-2 s-1 and CH4 umol m-2 30min-1

Reco_min  = TP_final_sys_CO2(find_min_Reco_TP,xdata);
 
figure
plot(decday_valid,er_ANNnight_valid,'.',decday_valid,Reco_min,'.')
legend('obs','mod min')
 
%RMSE is a better test of model performance than r2
RMSE_reco_min_valid_TP=nansum((Reco_min_valid_TP'-er_ANNnight_valid).^2);
RMSE_reco_min_valid_TP=sqrt(Reco_min_valid_TP/35040);

X = ones(length(Reco_min_valid_TP'),1);
X = [X Reco_min_valid_TP'];
[~,~,~,~,STATS] = regress(er_ANNnight_valid,X);

%calculate 90% CI
% 95% confidence intervals use 1.96
lower_Reco=Reco_min'-(1.645*(model_std_Reco_TP_valid./sqrt(50)));%use 1.645 for 90% CI
upper_Reco=Reco_min'+(1.645*(model_std_Reco_TP_valid./sqrt(50)));
 
figure
plot(lower_Reco)
hold on
plot(upper_Reco)

NEE_mod_min=Reco_min'+GPP_min_valid;
figure
plot(NEE_mod_min)
hold on
plot(wc_gf_valid)

%combine with GPP from PEPRMT
%lower and upper NEE
lower_NEE=lower_GPP_mod_min'+lower_Reco;
upper_NEE=upper_GPP_mod_min'+upper_Reco;
% lower_NEE=NEE_mod_min'-(1.645*(model_std_Reco./sqrt(5)));
% upper_NEE=NEE_mod_min'+(1.645*(model_std_Reco./sqrt(5)));

figure
plot(decday_valid,lower_NEE,'.',decday_valid,upper_NEE,'.',decday_valid,NEE_mod_min,'.',decday_valid,wc_gf_valid,'.')
legend('low','upper','min','obs')

%CAIC
p=2;
residual_SS_MB=sum((NEE_mod_min(1:17520)-wc_gf_valid(1:17520)).^2);
CAIC_valid_MB=-2*log(residual_SS_MB) + p*(log(17520)+1);
residual_SS_WP=sum((NEE_mod_min(17521:end)-wc_gf_valid(17521:end)).^2);
CAIC_valid_WP=-2*log(residual_SS_WP) + p*(log(17520)+1);

%% East End data
xdata_EE=[DOY_valid(1:17520) DOY_disc_valid(1:17520) TA_gf_EE_2015 WT_gf_EE_2015 PAR_gf_EE_2015 LAI_gf_EE_2015 GPP_min_EE_2015' decday_EE_2015 Season_drop_valid(1:17520) wc_90CI_valid(1:17520) site_EE wetland_age_EE];
Reco_min_EE = TP_final_sys_CO2(find_min_Reco_TP,xdata_EE);

figure
plot(decday_EE_2015,er_ANNnight_EE,'.',decday_EE_2015,Reco_min_EE,'.')
legend('obs','mod min')

NEE_mod_min_EE=GPP_min_EE_2015+Reco_min_EE;
figure
plot(decday_EE_2015,wc_gf_EE_2015,'.',decday_EE_2015,NEE_mod_min_EE,'.')
legend('obs','mod min')

% 90% confidence intervals (if you want 95% use 1.96)
%b/c the 10,000 parameters are not independent or may even be redundant, I
%reduce the sample size to 1000
lower_Reco_EE=Reco_min_EE-(1.645*(model_std_Reco(1:17520)./sqrt(50)));%use 1.645 for 90% CI
upper_Reco_EE=Reco_min_EE+(1.645*(model_std_Reco(1:17520)./sqrt(50)));
%     lower_Reco=quantile(model_pop_Reco',0.05);% 90% quantile
%     upper_Reco=quantile(model_pop_Reco',0.95);% 90% quantile
% 

figure
plot(lower_Reco_EE,'.')
hold on
plot(upper_Reco_EE,'.')


lower_GPP_mod_min_EE=GPP_min_EE_2015-(1.645*(model_std_Reco(1:17520)./sqrt(50)));
upper_GPP_mod_min_EE=GPP_min_EE_2015+(1.645*(model_std_Reco(1:17520)./sqrt(50)));
figure
plot(lower_GPP_mod_min_EE)%using Reco uncertainty
hold on
plot(upper_GPP_mod_min_EE)

%lower and upper NEE
lower_NEE_EE=lower_GPP_mod_min_EE+lower_Reco_EE;
upper_NEE_EE=upper_GPP_mod_min_EE+upper_Reco_EE;
% lower_NEE=NEE_mod_min'-(1.645*(model_std_Reco./sqrt(5)));
% upper_NEE=NEE_mod_min'+(1.645*(model_std_Reco./sqrt(5)));

figure
plot(decday_EE_2015,lower_NEE_EE,'.',decday_EE_2015,upper_NEE_EE,'.',decday_EE_2015,NEE_mod_min_EE,'.',decday_EE_2015,wc_gf_EE_2015,'.')
legend('low','upper','min','obs')

 %CAIC
p=2;
residual_SS_EE=sum((NEE_mod_min_EE-wc_gf_EE_2015').^2);
CAIC_valid_EE=-2*log(residual_SS_EE) + p*(log(17520)+1);


%% annual sums
%Validation MB stats
NEE_mod_min=NEE_mod_min';
lower_NEE=lower_NEE';
upper_NEE=upper_NEE';

NEE_mod_min_cum=NEE_mod_min*60*30;
lower_NEE_cum=lower_NEE*60*30;
upper_NEE_cum=upper_NEE*60*30;


cum_mod_NEE_valid_MB=cumsum(NEE_mod_min_cum(1:17520));%nmol m-2 yr-1
cum_mod_NEE_valid_sum_MB=cum_mod_NEE_valid_MB(end)*10^-6;%mol m-2 yr-1
cum_mod_NEE_valid_sum_MB=cum_mod_NEE_valid_sum_MB*44.01*0.27;%g C-Ch4 m-2 yr-1

cum_mod_NEE_valid_lower_MB=cumsum(lower_NEE_cum(1:17520));%nmol m-2 yr-1
cum_mod_NEE_valid_sum_lower_MB=cum_mod_NEE_valid_lower_MB(end)*10^-6;%mol m-2 yr-1
cum_mod_NEE_valid_sum_lower_MB=cum_mod_NEE_valid_sum_lower_MB*44.01*0.27;%g C-Ch4 m-2 yr-1
 
cum_mod_NEE_max_valid_upper_MB=cumsum(upper_NEE_cum(1:17520));%nmol m-2 yr-1
cum_mod_NEE_valid_sum_upper_MB=cum_mod_NEE_max_valid_upper_MB(end)*10^-6;%mol m-2 yr-1
cum_mod_NEE_valid_sum_upper_MB=cum_mod_NEE_valid_sum_upper_MB*44.01*0.27;%g C-Ch4 m-2 yr-1

cum_mod_NEE_valid_sum_upper_MB-cum_mod_NEE_valid_sum_MB

%% Validation WP 

cum_mod_NEE_valid_WP=cumsum(NEE_mod_min_cum(17521:end));%nmol m-2 yr-1
cum_mod_NEE_valid_sum_WP=cum_mod_NEE_valid_WP(end)*10^-6;%mol m-2 yr-1
cum_mod_NEE_valid_sum_WP=cum_mod_NEE_valid_sum_WP*44.01*0.27;%g C-Ch4 m-2 yr-1

cum_mod_NEE_valid_lower_WP=cumsum(lower_NEE_cum(17521:end));%nmol m-2 yr-1
cum_mod_NEE_valid_sum_lower_WP=cum_mod_NEE_valid_lower_WP(end)*10^-6;%mol m-2 yr-1
cum_mod_NEE_valid_sum_lower_WP=cum_mod_NEE_valid_sum_lower_WP*44.01*0.27;%g C-Ch4 m-2 yr-1
 
cum_mod_NEE_max_valid_upper_WP=cumsum(upper_NEE_cum(17521:end));%nmol m-2 yr-1
cum_mod_NEE_valid_sum_upper_WP=cum_mod_NEE_max_valid_upper_WP(end)*10^-6;%mol m-2 yr-1
cum_mod_NEE_valid_sum_upper_WP=cum_mod_NEE_valid_sum_upper_WP*44.01*0.27;%g C-Ch4 m-2 yr-1

cum_mod_NEE_valid_sum_upper_WP-cum_mod_NEE_valid_sum_WP


%% Validation EE 
NEE_mod_min_EE=NEE_mod_min_EE';
lower_NEE_EE=lower_NEE_EE';
upper_NEE_EE=upper_NEE_EE';


NEE_mod_min_EE_cum=NEE_mod_min_EE*60*30;
lower_NEE_EE_cum=lower_NEE_EE*60*30;
upper_NEE_EE_cum=upper_NEE_EE*60*30;

cum_mod_NEE_valid_EE=cumsum(NEE_mod_min_EE_cum);%nmol m-2 yr-1
cum_mod_NEE_valid_sum_EE=cum_mod_NEE_valid_EE(end)*10^-6;%mol m-2 yr-1
cum_mod_NEE_valid_sum_EE=cum_mod_NEE_valid_sum_EE*44.01*0.27;%g C-Ch4 m-2 yr-1

cum_mod_NEE_valid_lower_EE=cumsum(lower_NEE_EE_cum);%nmol m-2 yr-1
cum_mod_NEE_valid_sum_lower_EE=cum_mod_NEE_valid_lower_EE(end)*10^-6;%mol m-2 yr-1
cum_mod_NEE_valid_sum_lower_EE=cum_mod_NEE_valid_sum_lower_EE*44.01*0.27;%g C-Ch4 m-2 yr-1
 
cum_mod_NEE_max_valid_upper_EE=cumsum(upper_NEE_EE_cum);%nmol m-2 yr-1
cum_mod_NEE_valid_sum_upper_EE=cum_mod_NEE_max_valid_upper_EE(end)*10^-6;%mol m-2 yr-1
cum_mod_NEE_valid_sum_upper_EE=cum_mod_NEE_valid_sum_upper_EE*44.01*0.27;%g C-Ch4 m-2 yr-1

cum_mod_NEE_valid_sum_upper_EE-cum_mod_NEE_valid_sum_EE

%RMSE is a better test of model performance than r2
RMSE_NEE_min_EE_valid=nansum((NEE_mod_min_EE_cum-wc_gf_EE_2015_cum).^2);
RMSE_NEE_min_EE_valid=sqrt(RMSE_NEE_min_EE_valid/365);
RMSE_NEE_min_EE_valid=RMSE_NEE_min_EE_valid*10^-3*44.01*0.27;%mg C-Ch4 m-2 yr-1

%% Looking at Reco ratio in MB
%DON:T NEED GPP B/C ALREADY DID IT WITH PEPRMT
%Not re-naming cum variables so run and look at value but it won't be saved
Reco_min_cum=Reco_min*60*30;

%2015
cum_mod_ER_valid_MB=cumsum(Reco_min_cum(1:17520));%umol m-2 yr-1
cum_mod_ER_valid_sum_MB=cum_mod_ER_valid_MB(end)*10^-6;%mol ch4 m-2 yr-1
cum_mod_ER_valid_sum_MB=cum_mod_ER_valid_sum_MB*44.01*0.27;%g C-Ch4 m-2 yr-1


%% Looking at Reco to GPP ratio in WP
%Not re-naming cum variables so run and look at value but it won't be saved

%2015

cum_mod_ER_valid_WP=cumsum(Reco_min_cum(17521:end));%umol m-2 yr-1
cum_mod_ER_valid_sum_WP=cum_mod_ER_valid_WP(end)*10^-6;%mol ch4 m-2 yr-1
cum_mod_ER_valid_sum_WP=cum_mod_ER_valid_sum_WP*44.01*0.27;%g C-Ch4 m-2 yr-1


%% Looking at Reco to GPP ratio in EE
%Not re-naming cum variables so run and look at value but it won't be saved
Reco_min_EE_cum=Reco_min_EE*60*30;

%2015
cum_mod_ER_valid_EE=cumsum(Reco_min_EE_cum);%umol m-2 yr-1
cum_mod_ER_valid_sum_EE=cum_mod_ER_valid_EE(end)*10^-6;%mol ch4 m-2 yr-1
cum_mod_ER_valid_sum_EE=cum_mod_ER_valid_sum_EE*44.01*0.27;%g C-Ch4 m-2 yr-1

%% Get uncertainty in modeled param data MB WP

%First, I have to run param data in 3 parts b/c the dataset is too long
xdata_param_1=[DOY_param(1:28356) DOY_disc_param(1:28356) TA_param(1:28356)...
    WT_gf_param(1:28356) PAR_gf_param(1:28356) LAI_gf_param(1:28356)...
    GPP_min_param(1:28356) decday_param(1:28356) Season_drop_param(1:28356)...
    wc_90CI_param(1:28356) site_param(1:28356) wetland_age_param(1:28356)];
%make 1000 theta sets that will be used to estimate CI 
 model_pop_Reco=zeros(length(1:28356));%length of validation data
 model_pop_Reco=model_pop_Reco(:,1:10000);%update as needed 
 
for i=1:length(find_min_10000_Reco_TP)
    Reco= TP_final_sys_CO2(find_min_10000_Reco_TP(i,:)',xdata_param_1);
    model_pop_Reco(:,i)=Reco;
end

model_std_Reco_1=std(model_pop_Reco');

xdata_param_2=[DOY_param(28357:56712) DOY_disc_param(28357:56712) TA_param(28357:56712)...
    WT_gf_param(28357:56712) PAR_gf_param(28357:56712) LAI_gf_param(28357:56712)...
    GPP_min_param(28357:56712) decday_param(28357:56712) Season_drop_param(28357:56712)...
    wc_90CI_param(28357:56712) site_param(28357:56712) wetland_age_param(28357:56712)];
 
for i=1:length(find_min_10000_Reco_TP)
    Reco = TP_final_sys_CO2(find_min_10000_Reco_TP(i,:)',xdata_param_2);
    model_pop_Reco(:,i)=Reco;
end

model_std_Reco_2=std(model_pop_Reco');

xdata_param_3=[DOY_param(56713:85068) DOY_disc_param(56713:85068) TA_param(56713:85068)...
    WT_gf_param(56713:85068) PAR_gf_param(56713:85068) LAI_gf_param(56713:85068)...
    GPP_min_param(56713:85068) decday_param(56713:85068) Season_drop_param(56713:85068)...
    wc_90CI_param(56713:85068) site_param(56713:85068) wetland_age_param(56713:85068)];
 
for i=1:length(find_min_10000_Reco_TP)
    Reco = TP_final_sys_CO2(find_min_10000_Reco_TP(i,:)',xdata_param_3);
    model_pop_Reco(:,i)=Reco;
end

model_std_Reco_3=std(model_pop_Reco');

xdata_param_4=[DOY_param(85069:end) DOY_disc_param(85069:end) TA_param(85069:end)...
    WT_gf_param(85069:end) PAR_gf_param(85069:end) LAI_gf_param(85069:end)...
    GPP_min_param(85069:end) decday_param(85069:end) Season_drop_param(85069:end)...
    wc_90CI_param(85069:end) site_param(85069:end) wetland_age_param(85069:end)];
 
for i=1:length(find_min_10000_Reco_TP)
    Reco = TP_final_sys_CO2(find_min_10000_Reco_TP(i,:)',xdata_param_4);
    model_pop_Reco(:,i)=Reco;
end

model_std_Reco_4=std(model_pop_Reco');

%put 4 part together to get uncertainty
model_std_Reco_param=vertcat(model_std_Reco_1',model_std_Reco_2',model_std_Reco_3',model_std_Reco_4');
%% run param data MB WP
xdata_param=[DOY_param DOY_disc_param TA_param WT_gf_param PAR_gf_param LAI_gf_param...
    GPP_min_param decday_param Season_drop_param wc_90CI_param site_param wetland_age_param];
Reco_min_param  = TP_final_sys_CO2(find_min_Reco_TP,xdata_param);
NEE_mod_min_param=Reco_min_param'+GPP_min_param;
figure
plot(decday_param,er_ANNnight_param,'.',decday_param,Reco_min_param,'.')
legend('obs','mod min')
figure
plot(decday_param,wc_gf_param,'.',decday_param,NEE_mod_min_param,'.')
legend('obs','mod min')


% 90% confidence intervals (if you want 95% use 1.96)
%b/c the 10,000 parameters are not independent or may even be redundant, I
%reduce the sample size to 50
lower_Reco_param=Reco_min_param'-(1.645*(model_std_Reco_param./sqrt(50)));%use 1.645 for 90% CI
upper_Reco_param=Reco_min_param'+(1.645*(model_std_Reco_param./sqrt(50)));
figure
plot(lower_Reco_param,'.')
hold on
plot(upper_Reco_param,'.')
%assume GPP has same uncertainty as Reco--not sure if this is right
lower_GPP_param=GPP_min_param-(1.645*(model_std_Reco_param./sqrt(50)));
upper_GPP_param=GPP_min_param+(1.645*(model_std_Reco_param./sqrt(50)));
figure
plot(lower_GPP_param)%using GPP uncertainty quantile
hold on
plot(upper_GPP_param)

%lower and upper NEE
lower_NEE_param=lower_GPP_param+lower_Reco_param;
upper_NEE_param=upper_GPP_param+upper_Reco_param;

figure
plot(decday_param,lower_NEE_param,'.',decday_param,upper_NEE_param,'.',decday_param,NEE_mod_min_param,'.',decday_param,wc_gf_param,'.')
legend('low','upper','min','obs') 

%% Param stats MB 
NEE_mod_min_param=NEE_mod_min_param';
lower_NEE_param=lower_NEE_param';
upper_NEE_param=upper_NEE_param';

NEE_mod_min_param_cum=NEE_mod_min_param*60*30;
lower_NEE_param_cum=lower_NEE_param*60*30;
upper_NEE_param_cum=upper_NEE_param*60*30;


cum_mod_NEE_param_MB=cumsum(NEE_mod_min_param_cum(1:70128));%nmol m-2 yr-1
cum_mod_NEE_param_sum_MB=cum_mod_NEE_param_MB(end)*10^-6;%mol m-2 yr-1
cum_mod_NEE_param_sum_MB=cum_mod_NEE_param_sum_MB*44.01*0.27;%g C-Ch4 m-2 yr-1

cum_mod_NEE_param_lower_MB=cumsum(lower_NEE_param_cum(1:70128));%nmol m-2 yr-1
cum_mod_NEE_param_sum_lower_MB=cum_mod_NEE_param_lower_MB(end)*10^-6;%mol m-2 yr-1
cum_mod_NEE_param_sum_lower_MB=cum_mod_NEE_param_sum_lower_MB*44.01*0.27;%g C-Ch4 m-2 yr-1
 
cum_mod_NEE_max_param_upper_MB=cumsum(upper_NEE_param_cum(1:70128));%nmol m-2 yr-1
cum_mod_NEE_param_sum_upper_MB=cum_mod_NEE_max_param_upper_MB(end)*10^-6;%mol m-2 yr-1
cum_mod_NEE_param_sum_upper_MB=cum_mod_NEE_param_sum_upper_MB*44.01*0.27;%g C-Ch4 m-2 yr-1

cum_mod_NEE_param_sum_upper_MB-cum_mod_NEE_param_sum_MB

%% Annual sums for each param year MB n=4
%Not remaning cum variables so run and look at value
%2011

cum_mod_NEE_param_MB=cumsum(NEE_mod_min_param_cum(1:17520));%nmol m-2 yr-1
cum_mod_NEE_param_sum_MB=cum_mod_NEE_param_MB(end)*10^-6;%mol m-2 yr-1
cum_mod_NEE_param_sum_MB=cum_mod_NEE_param_sum_MB*44.01*0.27;%g C-Ch4 m-2 yr-1

cum_mod_NEE_param_lower_MB=cumsum(lower_NEE_param_cum(1:17520));%nmol m-2 yr-1
cum_mod_NEE_param_sum_lower_MB=cum_mod_NEE_param_lower_MB(end)*10^-6;%mol m-2 yr-1
cum_mod_NEE_param_sum_lower_MB=cum_mod_NEE_param_sum_lower_MB*44.01*0.27;%g C-Ch4 m-2 yr-1
 
cum_mod_NEE_max_param_upper_MB=cumsum(upper_NEE_param_cum(1:17520));%nmol m-2 yr-1
cum_mod_NEE_param_sum_upper_MB=cum_mod_NEE_max_param_upper_MB(end)*10^-6;%mol m-2 yr-1
cum_mod_NEE_param_sum_upper_MB=cum_mod_NEE_param_sum_upper_MB*44.01*0.27;%g C-Ch4 m-2 yr-1

(cum_mod_NEE_param_sum_upper_MB-cum_mod_NEE_param_sum_MB)*2

%2012

cum_mod_NEE_param_MB=cumsum(NEE_mod_min_param_cum(17521:35088));%nmol m-2 yr-1
cum_mod_NEE_param_sum_MB=cum_mod_NEE_param_MB(end)*10^-6;%mol m-2 yr-1
cum_mod_NEE_param_sum_MB=cum_mod_NEE_param_sum_MB*44.01*0.27;%g C-Ch4 m-2 yr-1

cum_mod_NEE_param_lower_MB=cumsum(lower_NEE_param_cum(17521:35088));%nmol m-2 yr-1
cum_mod_NEE_param_sum_lower_MB=cum_mod_NEE_param_lower_MB(end)*10^-6;%mol m-2 yr-1
cum_mod_NEE_param_sum_lower_MB=cum_mod_NEE_param_sum_lower_MB*44.01*0.27;%g C-Ch4 m-2 yr-1
 
cum_mod_NEE_max_param_upper_MB=cumsum(upper_NEE_param_cum(17521:35088));%nmol m-2 yr-1
cum_mod_NEE_param_sum_upper_MB=cum_mod_NEE_max_param_upper_MB(end)*10^-6;%mol m-2 yr-1
cum_mod_NEE_param_sum_upper_MB=cum_mod_NEE_param_sum_upper_MB*44.01*0.27;%g C-Ch4 m-2 yr-1

(cum_mod_NEE_param_sum_upper_MB-cum_mod_NEE_param_sum_MB)*2

%2013

cum_mod_NEE_param_MB=cumsum(NEE_mod_min_param_cum(35089:52608));%nmol m-2 yr-1
cum_mod_NEE_param_sum_MB=cum_mod_NEE_param_MB(end)*10^-6;%mol m-2 yr-1
cum_mod_NEE_param_sum_MB=cum_mod_NEE_param_sum_MB*44.01*0.27;%g C-Ch4 m-2 yr-1

cum_mod_NEE_param_lower_MB=cumsum(lower_NEE_param_cum(35089:52608));%nmol m-2 yr-1
cum_mod_NEE_param_sum_lower_MB=cum_mod_NEE_param_lower_MB(end)*10^-6;%mol m-2 yr-1
cum_mod_NEE_param_sum_lower_MB=cum_mod_NEE_param_sum_lower_MB*44.01*0.27;%g C-Ch4 m-2 yr-1
 
cum_mod_NEE_max_param_upper_MB=cumsum(upper_NEE_param_cum(35089:52608));%nmol m-2 yr-1
cum_mod_NEE_param_sum_upper_MB=cum_mod_NEE_max_param_upper_MB(end)*10^-6;%mol m-2 yr-1
cum_mod_NEE_param_sum_upper_MB=cum_mod_NEE_param_sum_upper_MB*44.01*0.27;%g C-Ch4 m-2 yr-1

(cum_mod_NEE_param_sum_upper_MB-cum_mod_NEE_param_sum_MB)*2

%2014
cum_mod_NEE_param_MB=cumsum(NEE_mod_min_param_cum(52609:70128));%nmol m-2 yr-1
cum_mod_NEE_param_sum_MB=cum_mod_NEE_param_MB(end)*10^-6;%mol m-2 yr-1
cum_mod_NEE_param_sum_MB=cum_mod_NEE_param_sum_MB*44.01*0.27;%g C-Ch4 m-2 yr-1

cum_mod_NEE_param_lower_MB=cumsum(lower_NEE_param_cum(52609:70128));%nmol m-2 yr-1
cum_mod_NEE_param_sum_lower_MB=cum_mod_NEE_param_lower_MB(end)*10^-6;%mol m-2 yr-1
cum_mod_NEE_param_sum_lower_MB=cum_mod_NEE_param_sum_lower_MB*44.01*0.27;%g C-Ch4 m-2 yr-1
 
cum_mod_NEE_max_param_upper_MB=cumsum(upper_NEE_param_cum(52609:70128));%nmol m-2 yr-1
cum_mod_NEE_param_sum_upper_MB=cum_mod_NEE_max_param_upper_MB(end)*10^-6;%mol m-2 yr-1
cum_mod_NEE_param_sum_upper_MB=cum_mod_NEE_param_sum_upper_MB*44.01*0.27;%g C-Ch4 m-2 yr-1

(cum_mod_NEE_param_sum_upper_MB-cum_mod_NEE_param_sum_MB)*2

%% Annual sums for each param year MB n=4--Looking at Reco to GPP ratio
%Not re-naming cum variables so run and look at value but it won't be saved
Reco_min_param_cum=Reco_min_param*60*30;

%all years
cum_mod_ER_param_MB=cumsum(Reco_min_param_cum(1:70128));%umol m-2 yr-1
cum_mod_ER_param_sum_MB=cum_mod_ER_param_MB(end)*10^-6;%mol ch4 m-2 yr-1
cum_mod_ER_param_sum_MB=cum_mod_ER_param_sum_MB*44.01*0.27;%g C-Ch4 m-2 yr-1

%2011
cum_mod_ER_param_MB=cumsum(Reco_min_param_cum(1:17520));%umol m-2 yr-1
cum_mod_ER_param_sum_MB=cum_mod_ER_param_MB(end)*10^-6;%mol ch4 m-2 yr-1
cum_mod_ER_param_sum_MB=cum_mod_ER_param_sum_MB*44.01*0.27;%g C-Ch4 m-2 yr-1

%2012
cum_mod_ER_param_MB=cumsum(Reco_min_param_cum(17521:35088));%umol m-2 yr-1
cum_mod_ER_param_sum_MB=cum_mod_ER_param_MB(end)*10^-6;%mol ch4 m-2 yr-1
cum_mod_ER_param_sum_MB=cum_mod_ER_param_sum_MB*44.01*0.27;%g C-Ch4 m-2 yr-1

%2013
cum_mod_ER_param_MB=cumsum(Reco_min_param_cum(35089:52608));%umol m-2 yr-1
cum_mod_ER_param_sum_MB=cum_mod_ER_param_MB(end)*10^-6;%mol ch4 m-2 yr-1
cum_mod_ER_param_sum_MB=cum_mod_ER_param_sum_MB*44.01*0.27;%g C-Ch4 m-2 yr-1

%2014
cum_mod_ER_param_MB=cumsum(Reco_min_param_cum(52609:70128));%umol m-2 yr-1
cum_mod_ER_param_sum_MB=cum_mod_ER_param_MB(end)*10^-6;%mol ch4 m-2 yr-1
cum_mod_ER_param_sum_MB=cum_mod_ER_param_sum_MB*44.01*0.27;%g C-Ch4 m-2 yr-1


%% Param WP --all years cum
cum_mod_NEE_param_WP=cumsum(NEE_mod_min_param_cum(70129:end));%nmol m-2 yr-1
cum_mod_NEE_param_sum_WP=cum_mod_NEE_param_WP(end)*10^-6;%mol m-2 yr-1
cum_mod_NEE_param_sum_WP=cum_mod_NEE_param_sum_WP*44.01*0.27;%g C-Ch4 m-2 yr-1

cum_mod_NEE_param_lower_WP=cumsum(lower_NEE_param_cum(70129:end));%nmol m-2 yr-1
cum_mod_NEE_param_sum_lower_WP=cum_mod_NEE_param_lower_WP(end)*10^-6;%mol m-2 yr-1
cum_mod_NEE_param_sum_lower_WP=cum_mod_NEE_param_sum_lower_WP*44.01*0.27;%g C-Ch4 m-2 yr-1
 
cum_mod_NEE_max_param_upper_WP=cumsum(upper_NEE_param_cum(70129:end));%nmol m-2 yr-1
cum_mod_NEE_param_sum_upper_WP=cum_mod_NEE_max_param_upper_WP(end)*10^-6;%mol m-2 yr-1
cum_mod_NEE_param_sum_upper_WP=cum_mod_NEE_param_sum_upper_WP*44.01*0.27;%g C-Ch4 m-2 yr-1

cum_mod_NEE_param_sum_upper_WP-cum_mod_NEE_param_sum_WP

%% By year Param WP 
%2012--Not a complete year!
cum_mod_NEE_param_WP=cumsum(NEE_mod_min_param_cum(70129:78384));%nmol m-2 yr-1
cum_mod_NEE_param_sum_WP=cum_mod_NEE_param_WP(end)*10^-6;%mol m-2 yr-1
cum_mod_NEE_param_sum_WP=cum_mod_NEE_param_sum_WP*44.01*0.27;%g C-Ch4 m-2 yr-1

cum_mod_NEE_param_lower_WP=cumsum(lower_NEE_param_cum(70129:78384));%nmol m-2 yr-1
cum_mod_NEE_param_sum_lower_WP=cum_mod_NEE_param_lower_WP(end)*10^-6;%mol m-2 yr-1
cum_mod_NEE_param_sum_lower_WP=cum_mod_NEE_param_sum_lower_WP*44.01*0.27;%g C-Ch4 m-2 yr-1
 
cum_mod_NEE_max_param_upper_WP=cumsum(upper_NEE_param_cum(70129:78384));%nmol m-2 yr-1
cum_mod_NEE_param_sum_upper_WP=cum_mod_NEE_max_param_upper_WP(end)*10^-6;%mol m-2 yr-1
cum_mod_NEE_param_sum_upper_WP=cum_mod_NEE_param_sum_upper_WP*44.01*0.27;%g C-Ch4 m-2 yr-1

cum_mod_NEE_param_sum_upper_WP-cum_mod_NEE_param_sum_WP
%2013
cum_mod_NEE_param_WP=cumsum(NEE_mod_min_param_cum(78385:95904));%nmol m-2 yr-1
cum_mod_NEE_param_sum_WP=cum_mod_NEE_param_WP(end)*10^-6;%mol m-2 yr-1
cum_mod_NEE_param_sum_WP=cum_mod_NEE_param_sum_WP*44.01*0.27;%g C-Ch4 m-2 yr-1

cum_mod_NEE_param_lower_WP=cumsum(lower_NEE_param_cum(78385:95904));%nmol m-2 yr-1
cum_mod_NEE_param_sum_lower_WP=cum_mod_NEE_param_lower_WP(end)*10^-6;%mol m-2 yr-1
cum_mod_NEE_param_sum_lower_WP=cum_mod_NEE_param_sum_lower_WP*44.01*0.27;%g C-Ch4 m-2 yr-1
 
cum_mod_NEE_max_param_upper_WP=cumsum(upper_NEE_param_cum(78385:95904));%nmol m-2 yr-1
cum_mod_NEE_param_sum_upper_WP=cum_mod_NEE_max_param_upper_WP(end)*10^-6;%mol m-2 yr-1
cum_mod_NEE_param_sum_upper_WP=cum_mod_NEE_param_sum_upper_WP*44.01*0.27;%g C-Ch4 m-2 yr-1

cum_mod_NEE_param_sum_upper_WP-cum_mod_NEE_param_sum_WP

%2014
cum_mod_NEE_param_WP=cumsum(NEE_mod_min_param_cum(95905:end));%nmol m-2 yr-1
cum_mod_NEE_param_sum_WP=cum_mod_NEE_param_WP(end)*10^-6;%mol m-2 yr-1
cum_mod_NEE_param_sum_WP=cum_mod_NEE_param_sum_WP*44.01*0.27;%g C-Ch4 m-2 yr-1

cum_mod_NEE_param_lower_WP=cumsum(lower_NEE_param_cum(95905:end));%nmol m-2 yr-1
cum_mod_NEE_param_sum_lower_WP=cum_mod_NEE_param_lower_WP(end)*10^-6;%mol m-2 yr-1
cum_mod_NEE_param_sum_lower_WP=cum_mod_NEE_param_sum_lower_WP*44.01*0.27;%g C-Ch4 m-2 yr-1
 
cum_mod_NEE_max_param_upper_WP=cumsum(upper_NEE_param_cum(95905:end));%nmol m-2 yr-1
cum_mod_NEE_param_sum_upper_WP=cum_mod_NEE_max_param_upper_WP(end)*10^-6;%mol m-2 yr-1
cum_mod_NEE_param_sum_upper_WP=cum_mod_NEE_param_sum_upper_WP*44.01*0.27;%g C-Ch4 m-2 yr-1

cum_mod_NEE_param_sum_upper_WP-cum_mod_NEE_param_sum_WP

%% Annual sums for each param year WP n=2--Looking at Reco to GPP ratio
%Not re-naming cum variables so run and look at value but it won't be saved
Reco_min_param_cum=Reco_min_param*60*30;

%all years
cum_mod_ER_param_WP=cumsum(Reco_min_param_cum(70129:end));%umol m-2 yr-1
cum_mod_ER_param_sum_WP=cum_mod_ER_param_WP(end)*10^-6;%mol ch4 m-2 yr-1
cum_mod_ER_param_sum_WP=cum_mod_ER_param_sum_WP*44.01*0.27;%g C-Ch4 m-2 yr-1

%2012-NOT A FULL YEAR!!
cum_mod_ER_param_WP=cumsum(Reco_min_param_cum(70129:78384));%umol m-2 yr-1
cum_mod_ER_param_sum_WP=cum_mod_ER_param_WP(end)*10^-6;%mol ch4 m-2 yr-1
cum_mod_ER_param_sum_WP=cum_mod_ER_param_sum_WP*44.01*0.27;%g C-Ch4 m-2 yr-1

%2013
cum_mod_ER_param_WP=cumsum(Reco_min_param_cum(78385:95904));%umol m-2 yr-1
cum_mod_ER_param_sum_WP=cum_mod_ER_param_WP(end)*10^-6;%mol ch4 m-2 yr-1
cum_mod_ER_param_sum_WP=cum_mod_ER_param_sum_WP*44.01*0.27;%g C-Ch4 m-2 yr-1

%2014
cum_mod_ER_param_WP=cumsum(Reco_min_param_cum(95905:end));%umol m-2 yr-1
cum_mod_ER_param_sum_WP=cum_mod_ER_param_WP(end)*10^-6;%mol ch4 m-2 yr-1
cum_mod_ER_param_sum_WP=cum_mod_ER_param_sum_WP*44.01*0.27;%g C-Ch4 m-2 yr-1

%% Daily integrals
%First do param results
days=1:1:2363;

daily_NEE_0=NEE_mod_min_param*60*30;%first turn NMB from umol m-2 s-1 into umol m-2 30min-1
[daily_NEE_min_param]=daily_sum(DOY_param,daily_NEE_0,days);
daily_NEE_0=lower_NEE_param*60*30;%first turn NMB from umol m-2 s-1 into umol m-2 30min-1
[daily_NEE_min_param_lower]=daily_sum(DOY_param,daily_NEE_0,days);
daily_NEE_0=upper_NEE_param*60*30;%first turn NMB from umol m-2 s-1 into umol m-2 30min-1
[daily_NEE_min_param_upper]=daily_sum(DOY_param,daily_NEE_0,days);

daily_Reco_0=Reco_min_param*60*30;%first turn NMB from umol m-2 s-1 into umol m-2 30min-1
[daily_Reco_min_param]=daily_sum(DOY_param,daily_Reco_0',days);
daily_Reco_0=lower_Reco_param*60*30;%first turn NMB from umol m-2 s-1 into umol m-2 30min-1
[daily_Reco_min_param_lower]=daily_sum(DOY_param,daily_Reco_0,days);
daily_Reco_0=upper_Reco_param*60*30;%first turn NMB from umol m-2 s-1 into umol m-2 30min-1
[daily_Reco_min_param_upper]=daily_sum(DOY_param,daily_Reco_0,days);

daily_NEE_min_param=daily_NEE_min_param*10^-6*11.88;%g CO_2-C m^-^2 d^-^1
daily_NEE_min_param_lower=daily_NEE_min_param_lower*10^-6*11.88;%g CO_2-C m^-^2 d^-^1
daily_NEE_min_param_upper=daily_NEE_min_param_upper*10^-6*11.88;%g CO_2-C m^-^2 d^-^1
daily_Reco_min_param=daily_Reco_min_param*10^-6*11.88;%g CO_2-C m^-^2 d^-^1
daily_Reco_min_param_lower=daily_Reco_min_param_lower*10^-6*11.88;%g CO_2-C m^-^2 d^-^1
daily_Reco_min_param_upper=daily_Reco_min_param_upper*10^-6*11.88;%g CO_2-C m^-^2 d^-^1


%Now do validation results
days=1:1:730;
daily_NEE_0=NEE_mod_min*60*30;%first turn NMB from umol m-2 s-1 into umol m-2 30min-1
[daily_NEE_min_valid]=daily_sum(DOY_param(1:35040),daily_NEE_0,days);
daily_NEE_0=lower_NEE*60*30;%first turn NMB from umol m-2 s-1 into umol m-2 30min-1
[daily_NEE_min_valid_lower]=daily_sum(DOY_param(1:35040),daily_NEE_0,days);
daily_NEE_0=upper_NEE*60*30;%first turn NMB from umol m-2 s-1 into umol m-2 30min-1
[daily_NEE_min_valid_upper]=daily_sum(DOY_param(1:35040),daily_NEE_0,days);

daily_Reco_0=Reco_min*60*30;%first turn NMB from umol m-2 s-1 into umol m-2 30min-1
[daily_Reco_min_valid]=daily_sum(DOY_param(1:35040),daily_Reco_0',days);
daily_Reco_0=lower_Reco*60*30;%first turn NMB from umol m-2 s-1 into umol m-2 30min-1
[daily_Reco_min_valid_lower]=daily_sum(DOY_param(1:35040),daily_Reco_0,days);
daily_Reco_0=upper_Reco*60*30;%first turn NMB from umol m-2 s-1 into umol m-2 30min-1
[daily_Reco_min_valid_upper]=daily_sum(DOY_param(1:35040),daily_Reco_0,days);

daily_NEE_min_valid=daily_NEE_min_valid*10^-6*11.88;%g CO_2-C m^-^2 d^-^1
daily_NEE_min_valid_lower=daily_NEE_min_valid_lower*10^-6*11.88;%g CO_2-C m^-^2 d^-^1
daily_NEE_min_valid_upper=daily_NEE_min_valid_upper*10^-6*11.88;%g CO_2-C m^-^2 d^-^1
daily_Reco_min_valid=daily_Reco_min_valid*10^-6*11.88;%g CO_2-C m^-^2 d^-^1
daily_Reco_min_valid_lower=daily_Reco_min_valid_lower*10^-6*11.88;%g CO_2-C m^-^2 d^-^1
daily_Reco_min_valid_upper=daily_Reco_min_valid_upper*10^-6*11.88;%g CO_2-C m^-^2 d^-^1

%EE data
days=1:1:365;

daily_NEE_0=NEE_mod_min_EE*60*30;%first turn NMB from umol m-2 s-1 into umol m-2 30min-1
[daily_NEE_min_EE]=daily_sum(DOY_EE_2015,daily_NEE_0',days);
daily_NEE_0=lower_NEE_EE*60*30;%first turn NMB from umol m-2 s-1 into umol m-2 30min-1
[daily_NEE_min_EE_lower]=daily_sum(DOY_EE_2015,daily_NEE_0',days);
daily_NEE_0=upper_NEE_EE*60*30;%first turn NMB from umol m-2 s-1 into umol m-2 30min-1
[daily_NEE_min_EE_upper]=daily_sum(DOY_EE_2015,daily_NEE_0',days);

daily_Reco_0=Reco_min_EE*60*30;%first turn NMB from umol m-2 s-1 into umol m-2 30min-1
[daily_Reco_min_EE]=daily_sum(DOY_EE_2015,daily_Reco_0',days);
daily_Reco_0=lower_Reco_EE*60*30;%first turn NMB from umol m-2 s-1 into umol m-2 30min-1
[daily_Reco_min_EE_lower]=daily_sum(DOY_EE_2015,daily_Reco_0',days);
daily_Reco_0=upper_Reco_EE*60*30;%first turn NMB from umol m-2 s-1 into umol m-2 30min-1
[daily_Reco_min_EE_upper]=daily_sum(DOY_EE_2015,daily_Reco_0',days);

daily_NEE_min_EE=daily_NEE_min_EE*10^-6*11.88;%g CO_2-C m^-^2 d^-^1
daily_NEE_min_EE_lower=daily_NEE_min_EE_lower*10^-6*11.88;%g CO_2-C m^-^2 d^-^1
daily_NEE_min_EE_upper=daily_NEE_min_EE_upper*10^-6*11.88;%g CO_2-C m^-^2 d^-^1
daily_Reco_min_EE=daily_Reco_min_EE*10^-6*11.88;%g CO_2-C m^-^2 d^-^1
daily_Reco_min_EE_lower=daily_Reco_min_EE_lower*10^-6*11.88;%g CO_2-C m^-^2 d^-^1
daily_Reco_min_EE_upper=daily_Reco_min_EE_upper*10^-6*11.88;%g CO_2-C m^-^2 d^-^1


days_EE=1:1:365;
days_valid=1:1:730;
days_param=1:1:2363;

figure
plot(days_EE,daily_NEE_obs_EE,'.',days_EE,daily_NEE_obs_EE_lower,'.')
hold on
plot(days_EE,daily_NEE_obs_EE_upper,'.',days_EE,daily_NEE_min_EE,'.')
hold on
plot(days_EE,daily_NEE_min_EE_lower,'.',days_EE,daily_NEE_min_EE_upper,'.')%,days,daily_NEE_mod_mean,'.')
title('EE 2015')
ylabel('NEE daily sum (g CO_2-C m^-^2 d^-^1)')
legend('obs','obs lower','obs upper','mod','mod lower','mod upper')

figure
plot(days_valid,daily_NEE_obs_valid,'.',days_valid,daily_NEE_obs_valid_lower,'.',...
    days_valid,daily_NEE_obs_valid_upper,'.',days_valid,daily_NEE_min_valid,'.',...
    days_valid,daily_NEE_min_valid_lower,'.',days_valid,daily_NEE_min_valid_upper,'.')%,days,daily_NEE_mod_mean,'.')
title('MB WP 2015')
ylabel('NEE daily sum (g CO_2-C m^-^2 d^-^1)')
legend('obs','obs lower','obs upper','mod','mod lower','mod upper')

figure
plot(days_param,daily_NEE_obs_param,'.',days_param,daily_NEE_obs_param_lower,'.',...
    days_param,daily_NEE_obs_param_upper,'.',days_param,daily_NEE_min_param,'.',...
    days_param,daily_NEE_min_param_lower,'.',days_param,daily_NEE_min_param_upper,'.')%,days,daily_NEE_mod_mean,'.')
title('MB WP PAram')
ylabel('NEE daily sum (g CO_2-C m^-^2 d^-^1)')
legend('obs','obs lower','obs upper','mod','mod lower','mod upper')


%Statistics Updated 10/10/2016
%RMSE is a better test of model performance than r2
%NEE
RMSE_nee_min_daily_MB_param=nansum((daily_NEE_min_param(1:1461)-daily_NEE_obs_param(1:1461)).^2);
RMSE_nee_min_daily_MB_param=sqrt(RMSE_nee_min_daily_MB_param/1461);
RMSE_nee_min_daily_WP_param=nansum((daily_NEE_min_param(1462:end)-daily_NEE_obs_param(1462:end)).^2);
RMSE_nee_min_daily_WP_param=sqrt(RMSE_nee_min_daily_WP_param/902);
 
RMSE_nee_min_daily_MB_valid=nansum((daily_NEE_min_valid(1:365)-daily_NEE_obs_valid(1:365)).^2);
RMSE_nee_min_daily_MB_valid=sqrt(RMSE_nee_min_daily_MB_valid/365);
RMSE_nee_min_daily_WP_valid=nansum((daily_NEE_min_valid(366:end)-daily_NEE_obs_valid(366:end)).^2);
RMSE_nee_min_daily_WP_valid=sqrt(RMSE_nee_min_daily_WP_valid/365);
RMSE_nee_min_daily_EE_valid=nansum((daily_NEE_min_EE(1:end)-daily_NEE_obs_EE(1:end)).^2);
RMSE_nee_min_daily_EE_valid=sqrt(RMSE_nee_min_daily_EE_valid/365);

%Mean absolute error (MAE)
%NEE
MAE_nee_min_daily_MB_param=nansum((daily_NEE_min_param(1:1461)-daily_NEE_obs_param(1:1461)));
MAE_nee_min_daily_MB_param=(MAE_nee_min_daily_MB_param/1461);
MAE_nee_min_daily_WP_param=nansum((daily_NEE_min_param(1462:end)-daily_NEE_obs_param(1462:end)));
MAE_nee_min_daily_WP_param=(MAE_nee_min_daily_WP_param/902);
 
MAE_nee_min_daily_MB_valid=nansum((daily_NEE_min_valid(1:365)-daily_NEE_obs_valid(1:365)));
MAE_nee_min_daily_MB_valid=(MAE_nee_min_daily_MB_valid/365);
MAE_nee_min_daily_WP_valid=nansum((daily_NEE_min_valid(366:end)-daily_NEE_obs_valid(366:end)));
MAE_nee_min_daily_WP_valid=(MAE_nee_min_daily_WP_valid/365);
MAE_nee_min_daily_EE_valid=nansum((daily_NEE_min_EE(1:end)-daily_NEE_obs_EE(1:end)));
MAE_nee_min_daily_EE_valid=(MAE_nee_min_daily_EE_valid/365);

%REco
% MAE_reco_min_daily=nansum(daily_Reco_mod_min(539:end)'-daily_Reco_obs(539:end));
% MAE_reco_min_daily=(MAE_reco_min_daily/365);

%D-index sensitive to systematic model bias
%NEE
D_index_NEE_min_daily_top_MB_param=nansum((daily_NEE_min_param(1:1461)-daily_NEE_obs_param(1:1461)).^2);
O_avg_MB_param=nanmean(daily_NEE_obs_param(1:1461));
D_index_NEE_min_daily_bottom_MB_param=nansum(((daily_NEE_min_param(1:1461)-O_avg_MB_param)+(daily_NEE_obs_param(1:1461)-O_avg_MB_param)).^2);
D_index_NEE_min_daily_MB_param=1-(D_index_NEE_min_daily_top_MB_param/D_index_NEE_min_daily_bottom_MB_param);

D_index_NEE_min_daily_top_WP_param=nansum((daily_NEE_min_param(1462:end)-daily_NEE_obs_param(1462:end)).^2);
O_avg_WP_param=nanmean(daily_NEE_obs_param(1462:end));
D_index_NEE_min_daily_bottom_WP_param=nansum(((daily_NEE_min_param(1462:end)-O_avg_WP_param)+(daily_NEE_obs_param(1462:end)-O_avg_WP_param)).^2);
D_index_NEE_min_daily_WP_param=1-(D_index_NEE_min_daily_top_WP_param/D_index_NEE_min_daily_bottom_WP_param);

D_index_NEE_min_daily_top_MB_valid=nansum((daily_NEE_min_valid(1:365)-daily_NEE_obs_valid(1:365)).^2);
O_avg_MB_valid=nanmean(daily_NEE_obs_valid(1:365));
D_index_NEE_min_daily_bottom_MB_valid=nansum(((daily_NEE_min_valid(1:365)-O_avg_MB_valid)+(daily_NEE_obs_valid(1:365)-O_avg_MB_valid)).^2);
D_index_NEE_min_daily_MB_valid=1-(D_index_NEE_min_daily_top_MB_valid/D_index_NEE_min_daily_bottom_MB_valid);

D_index_NEE_min_daily_top_WP_valid=nansum((daily_NEE_min_valid(366:end)-daily_NEE_obs_valid(366:end)).^2);
O_avg_WP_valid=nanmean(daily_NEE_obs_valid(366:end));
D_index_NEE_min_daily_bottom_WP_valid=nansum(((daily_NEE_min_valid(366:end)-O_avg_WP_valid)+(daily_NEE_obs_valid(366:end)-O_avg_WP_valid)).^2);
D_index_NEE_min_daily_WP_valid=1-(D_index_NEE_min_daily_top_WP_valid/D_index_NEE_min_daily_bottom_WP_valid);

D_index_NEE_min_daily_top_EE_valid=nansum((daily_NEE_min_EE(1:365)-daily_NEE_obs_EE(1:365)).^2);
O_avg_EE_valid=nanmean(daily_NEE_obs_EE(1:365));
D_index_NEE_min_daily_bottom_EE_valid=nansum(((daily_NEE_min_EE(1:365)-O_avg_EE_valid)+(daily_NEE_obs_EE(1:365)-O_avg_EE_valid)).^2);
D_index_NEE_min_daily_EE_valid=1-(D_index_NEE_min_daily_top_EE_valid/D_index_NEE_min_daily_bottom_EE_valid);

%REco
% D_index_reco_min_daily_top=nansum((daily_Reco_mod_min(539:end)'-daily_Reco_obs(539:end)).^2);
% O_avg=nanmean(daily_Reco_obs(539:end));
% D_index_reco_min_daily_bottom=nansum(((daily_Reco_mod_min(539:end)'-O_avg)+(daily_Reco_obs(539:end)-O_avg)).^2);
% D_index_reco_min_daily=1-(D_index_reco_min_daily_top/D_index_reco_min_daily_bottom);
 
  



%% plotting Reco param and valid data for MB AND WP AND EE 
%UPDATED ON 2016.10.10
days_plot_MB_partition=vertcat(daily_DOY_param(1:1461), daily_DOY_EE+1461);
days_plot_WP_partition=vertcat(daily_DOY_param(1462:end),daily_DOY_EE+2363);
days_plot_EE_partition=daily_DOY_EE;

daily_plot_MB_Reco_lower=vertcat(daily_Reco_min_param_lower(1:1461)', daily_Reco_min_valid_lower(1:365)');
daily_plot_WP_Reco_lower=vertcat(daily_Reco_min_param_lower(1462:end)', daily_Reco_min_valid_lower(366:end)');
daily_plot_MB_Reco_upper=vertcat(daily_Reco_min_param_upper(1:1461)', daily_Reco_min_valid_upper(1:365)');
daily_plot_WP_Reco_upper=vertcat(daily_Reco_min_param_upper(1462:end)', daily_Reco_min_valid_upper(366:end)');

daily_plot_MB_Reco_obs=vertcat(daily_Reco_obs_param(1:1461)', daily_Reco_obs_valid(1:365)');
daily_plot_WP_Reco_obs=vertcat(daily_Reco_obs_param(1462:end)', daily_Reco_obs_valid(366:end)');

startDate = datenum('01-01-2011');
endDate = datenum('12-31-2015');
xData1=linspace(startDate,endDate,1826);

startDate2 = datenum('07-12-2012');
endDate2 = datenum('12-31-2015');
xData2=linspace(startDate2,endDate2,1267);

startDate3 = datenum('01-01-2015');
endDate3 = datenum('12-31-2015');
xData3=linspace(startDate3,endDate3,365);

%for 1:1's
%MB
X = ones(length(daily_Reco_min_param(1:1461)),1);
X = [X daily_Reco_min_param(1:1461)'];
[~,~,~,~,STATS1] = regress(daily_Reco_obs_param(1:1461)',X);
rsq1=STATS1(1);
X = ones(length(daily_Reco_min_valid(1:365)),1);
X = [X daily_Reco_min_valid(1:365)'];
[~,~,~,~,STATS3] = regress(daily_Reco_obs_valid(1:365)',X);
rsq3=STATS3(1);

%WP
X = ones(length(daily_Reco_min_param(1462:end)),1);
X = [X daily_Reco_min_param(1462:end)'];
[~,~,~,~,STATS5] = regress(daily_Reco_obs_param(1462:end)',X);
rsq5=STATS5(1);
X = ones(length(daily_Reco_min_valid(366:end)),1);
X = [X daily_Reco_min_valid(366:end)'];
[~,~,~,~,STATS7] = regress(daily_Reco_obs_valid(366:end)',X);
rsq7=STATS7(1);

%EE
X = ones(length(daily_Reco_min_EE(1:365)),1);
X = [X daily_Reco_min_EE(1:365)'];
[~,~,~,~,STATS9] = regress(daily_Reco_obs_EE(1:365)',X);
rsq9=STATS9(1);


%% plot Reco GPP figure all sites all years
figure ('Units', 'pixels',...
    'Position', [100 100 500 375]);
%MB time series GPP and Reco
subplot(3,1,1)
ciplot(daily_plot_MB_Reco_lower,daily_plot_MB_Reco_upper,xData1,'k')
hold on
obs = plot(xData1,daily_plot_MB_Reco_obs,'.r');
ylim([0 15])
datetick('x','mm/yyyy')
box off
hYLabel = ylabel({'CO_2 exchange','(g CO_2-C m^-^2 d^-^1)' });
% legend('model R_e_c_o','model GPP','partitioned Reco','partitioned GPP')

 %WP time series GPP and Reco
 subplot(3,1,2)
ciplot(daily_plot_WP_Reco_lower,daily_plot_WP_Reco_upper,xData2,'k')
hold on
obs = plot(xData2,daily_plot_WP_Reco_obs,'.r');
ylim([0 15])
datetick('x','mm/yyyy','keeplimits')
box off
hYLabel = ylabel({'CO_2 exchange','(g CO_2-C m^-^2 d^-^1)' });
% legend('model R_e_c_o','model GPP','partitioned Reco','partitioned GPP')

 %EE time series GPP and Reco
 subplot(3,1,3)
ciplot(daily_Reco_min_EE_lower,daily_Reco_min_EE_upper,xData3,'k')
hold on
obs = plot(xData3,daily_Reco_obs_EE,'.r');
ylim([0 15])
datetick('x','mm/yyyy','keeplimits')
box off
hYLabel = ylabel({'CO_2 exchange','(g CO_2-C m^-^2 d^-^1)' });
 legend('R_e_c_o _m_o_d_e_l','R_e_c_o _p_a_r_t_i_t_i_o_n_e_d')
set( gca                       , ...
    'FontName'   , 'Helvetica' );
set([hXLabel, hYLabel, text]  , ...
    'FontSize'   , 12         );
 
set(gcf, 'PaperPositionMode', 'auto');
print('TP_Reco_all_data_MB_WP_EE','-dpng','-r600');

%% one to one's added in via powerpoint file
%MB 1:1 Reco
p1 = polyfit(daily_Reco_min_param(1:1461),daily_Reco_obs_param(1:1461),1);
p2 = polyfit(daily_Reco_min_valid(1:365),daily_Reco_obs_valid(1:365),1);
%WP
p5 = polyfit(daily_Reco_min_param(1462:end),daily_Reco_obs_param(1462:end),1);
p6 = polyfit(daily_Reco_min_valid(366:end),daily_Reco_obs_valid(366:end),1);
%EE
p9 = polyfit(daily_Reco_min_EE,daily_Reco_obs_EE,1);

figure
plot(daily_Reco_min_param(1:1461),daily_Reco_obs_param(1:1461),'.r');
h=lsline;
xlim([0,14])
ylim([0,14])
hold on
plot(daily_Reco_min_valid(1:365),daily_Reco_obs_valid(1:365),'.k');
m=lsline;
xlim([0,14])
ylim([0,14])
refline(1,0)
xlabel('R_e_c_o_ _m_o_d_e_l')
ylabel('R_e_c_o_ _p_a_r_t_i_t_i_o_n_e_d')
% legend('R_e_c_o_ _p_a_r_a_m','R_e_c_o_ _p_a_r_a_m','R_e_c_o_ _v_a_l_i_d','R_e_c_o_ _v_a_l_i_d')
box off
set(gcf, 'PaperPositionMode', 'auto');
print('TP_Reco_onetoone_param_validation_MB','-dpng','-r600');

figure
plot(daily_Reco_min_param(1462:end),daily_Reco_obs_param(1462:end),'.r');
h=lsline;
xlim([0,9])
ylim([0,9])
hold on
plot(daily_Reco_min_valid(366:end),daily_Reco_obs_valid(366:end),'.k');
m=lsline;
xlim([0,9])
ylim([0,9])
refline(1,0)
xlabel('R_e_c_o_ _m_o_d_e_l')
ylabel('R_e_c_o_ _p_a_r_t_i_t_i_o_n_e_d')
%legend('R_e_c_o_ _p_a_r_a_m','R_e_c_o_ _p_a_r_a_m','R_e_c_o_ _v_a_l_i_d','R_e_c_o_ _v_a_l_i_d')
box off
set(gcf, 'PaperPositionMode', 'auto');
print('TP_Reco_onetoone_param_validation_WP','-dpng','-r600');



figure
plot(daily_Reco_min_EE,daily_Reco_obs_EE,'.k');
h=lsline;
xlim([0,6])
ylim([0,6])
refline(1,0)
xlabel('R_e_c_o_ _m_o_d_e_l')
ylabel('R_e_c_o_ _p_a_r_t_i_t_i_o_n_e_d')
%legend('R_e_c_o_ _p_a_r_a_m','R_e_c_o_ _p_a_r_a_m','R_e_c_o_ _v_a_l_i_d','R_e_c_o_ _v_a_l_i_d')
box off
set(gcf, 'PaperPositionMode', 'auto');
print('TP_Reco_onetoone_EE','-dpng','-r600');



%% Plot posterior UPDATED 10/2016
 
Rref_posterior_TP_Reco=find_min_10000_Reco_TP(:,1)+2;%kJ mol-1
Eo_posterior_TP_Reco=find_min_10000_Reco_TP(:,2)+50;%umol m-2
figure
subplot(2,1,1)
hist(Rref_posterior_TP_Reco);%pdf for alpha 1
xlabel('Rref (umol m-2 s-1)')
subplot(2,1,2)
hist(Eo_posterior_TP_Reco);%pdf for ea 1
xlabel('Eo (kJ mol-1)')
 
set(gcf, 'PaperPositionMode', 'auto');
print('TP_posterior_20150616','-dpng','-r600');

%Final parameter estimates
Rref_FINAL=find_min(1)+2;%kJ mol-1
Eo_FINAL=find_min(2)+50;%umol m-2





%check for correlations between parameters
X = ones(length(Rref_posterior_TP_Reco),1);
X = [X Rref_posterior_TP_Reco];
[~,~,~,~,STATS] = regress(Eo_posterior_TP_Reco,X);










 
 %% EXTRA plotting OLD CODE

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%daily 
days_plot=days+194;
 
figure ('Units', 'pixels',...
    'Position', [100 100 500 375]);
ciplot(daily_lower_Reco,daily_upper_Reco,days_plot,'k')
hold on
obs = plot(days_plot,daily_Reco_obs','.');
 
set(obs                       , ...
  'LineWidth'       , 1           , ...
  'Marker'          , 'o'         , ...
  'MarkerSize'      , 3           , ...
  'MarkerEdgeColor' , [0.5 0.5 0.5]  , ...
  'MarkerFaceColor' , [1 1 1]  );
 
hXLabel = xlabel('Day of year'                     );
hYLabel = ylabel('Ecosystem respiration (g CO_2-C m^-^2 d^-^1)' );
 
 legend('model','obs')
set( gca                       , ...
    'FontName'   , 'Helvetica' );
set([hXLabel, hYLabel, text]  , ...
    'FontSize'   , 14         );
 
set(gca, ...
  'Box'         , 'off'     , ...
  'TickDir'     , 'out'     , ...
  'TickLength'  , [.02 .02] , ...
  'XMinorTick'  , 'off'      , ...
  'YMinorTick'  , 'off'      , ...
  'YGrid'       , 'off'      , ...
  'XColor'      , [0 0 0], ...
  'YColor'      , [0 0 0], ...
  'Xtick'       , 200:100:1100,...
  'YTick'       , -2:1:6, ...
  'LineWidth'   , 1         );
 
set(gcf, 'PaperPositionMode', 'auto');
print('TP_alldata_20150615_validation','-dpng','-r600');
%print -depsc2 finalPlot1.eps
close;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Plot one to one
figure ('Units', 'pixels',...
    'Position', [100 100 500 375]);
hold on
obs = plot(daily_Reco_mod_min,daily_Reco_obs,'.');
set(obs                           , ...
  'LineStyle'       , 'none'      , ...
  'Marker'          , '.'         , ...
  'Color'           , [0 0 0]  );
set(obs                       , ...
  'LineWidth'       , 1           , ...
  'Marker'          , '.'         , ...
  'MarkerSize'      , 15          , ...
  'MarkerEdgeColor' , [.2 .2 .2]  , ...
  'MarkerFaceColor' , [.7 .7 .7]  );
hXLabel = xlabel('TP Model (g C m^-^2 d^-^1)');
hYLabel = ylabel('Obs (g C m^-^2 d^-^1)');
set([hXLabel, hYLabel, text]  , ...
    'FontSize'   , 14         );
 
set(gca, ...
  'Box'         , 'off'     , ...
  'TickDir'     , 'out'     , ...
  'TickLength'  , [.02 .02] , ...
  'XMinorTick'  , 'off'      , ...
  'YMinorTick'  , 'off'      , ...
  'YGrid'       , 'off'      , ...
  'XColor'      , [0 0 0], ...
  'YColor'      , [0 0 0], ...
  'YTick'       , 1:2:5, ...
  'XTick'       , 1:2:5, ...
  'LineWidth'   , 1         );
 
set(gcf, 'PaperPositionMode', 'auto');
print('TP_alldata_onetoone_20150615','-dpng','-r600');
 
%print -depsc2 finalPlot1_inset.eps
close;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%VALIDATION DATA ONLY FIGURE
X = ones(length(daily_Reco_mod_min(539:end)'),1);
X = [X daily_Reco_mod_min(539:end)'];
[~,~,~,~,STATS] = regress(daily_Reco_obs(539:end),X);
rsq=STATS(1);

figure ('Units', 'pixels',...
    'Position', [100 100 500 375])
obs=plot(daily_Reco_mod_min(539:end),daily_Reco_obs(539:end),'.');
hXLabel = xlabel('Model (g CO_2-C m^-^2 d^-^1)');
hYLabel=ylabel('Obs (g CO_2-C m^-^2 d^-^1)') ;
box off
xlim([0,5])
ylim([0,5])
set(obs                       , ...
  'LineWidth'       , 1           , ...
  'Marker'          , 'o'         , ...
  'MarkerSize'      , 3           , ...
  'MarkerEdgeColor' , [0.25 0.25 0.25]  , ...
  'MarkerFaceColor' , [0.25 0.25 0.25]  );
h=lsline;
p2 = polyfit(get(h,'xdata'),get(h,'ydata'),1);
theString = sprintf('y = %.3f x + %.3f', p2(1), p2(2));
place_x=min(daily_Reco_mod_min(539:end));
place_y=max(daily_Reco_obs(539:end));
t=text(place_x,place_y,theString);%;t=text(2,-7,'p2');%puts letter in upper L corner
theString = sprintf('r^2 = %.2f', rsq);
t=text(1,5,theString);%puts letter in upper L corner
refline(1,0)
 set( gca                       , ...
    'FontName'   , 'Helvetica' );
set([hXLabel, hYLabel, text]  , ...
    'FontSize'   , 12         );
 
set(gcf, 'PaperPositionMode', 'auto');
print('Reco_TP_onetoone_20160220','-dpng','-r300');
%print -depsc2 finalPlot1.eps
close;
%% calculate final model NEE with model uncertainty
daily_lower_plot_NEE=daily_lower_GPP_plot+daily_lower_plot_Reco;
daily_upper_plot_NEE=daily_upper_GPP_plot+daily_upper_plot_Reco;
daily_NEE_mod_plot=daily_Reco_mod_min_plot+daily_GPP_mod_min_plot;

daily_NEE_obs_plot= daily_GPP_obs_plot'+daily_Reco_obs_plot;

daily_NEE_obs_plot_test=daily_NEE_obs*10^-6*11.88;

X = ones(length(daily_NEE_mod_plot'),1);
X = [X daily_NEE_mod_plot'];
[B,BINT,R,RINT,STATS] = regress(daily_NEE_obs_plot,X);

 
figure ('Units', 'pixels',...
    'Position', [100 100 500 375]);
ciplot(daily_lower_plot_NEE,daily_upper_plot_NEE,days_plot,'k')
hold on
obs = plot(days_plot,daily_NEE_obs_plot','.');
 
set(obs                       , ...
  'LineWidth'       , 1           , ...
  'Marker'          , 'o'         , ...
  'MarkerSize'      , 3           , ...
  'MarkerEdgeColor' , [0.5 0.5 0.5]  , ...
  'MarkerFaceColor' , [1 1 1]  );
 
hXLabel = xlabel('Day of year'                     );
hYLabel = ylabel('Net ecosystem exchange (g CO_2-C m^-^2 d^-^1)' );
 
 legend('model','obs')
% hLegend = legend( ...
%   [data1, obs], ...
%   'model' , ...
%   'obs'      );
 
set( gca                       , ...
    'FontName'   , 'Helvetica' );
 
% set([legend, gca]             , ...
%     'FontSize'   , 14           );
set([hXLabel, hYLabel, text]  , ...
    'FontSize'   , 14         );
 
set(gca, ...
  'Box'         , 'off'     , ...
  'TickDir'     , 'out'     , ...
  'TickLength'  , [.02 .02] , ...
  'XMinorTick'  , 'off'      , ...
  'YMinorTick'  , 'off'      , ...
  'YGrid'       , 'off'      , ...
  'XColor'      , [0 0 0], ...
  'YColor'      , [0 0 0], ...
  'Xtick'       , 200:100:1100,...
  'YTick'       , -10:2:4, ...
  'LineWidth'   , 1         );
 
set(gcf, 'PaperPositionMode', 'auto');
print('NEE_alldata_20150519_validation','-dpng','-r600');
%print -depsc2 finalPlot1.eps
close;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure ('Units', 'pixels',...
    'Position', [100 100 500 375]);
hold on
obs = plot(daily_NEE_mod_plot',daily_NEE_obs_plot,'.');
set(obs                           , ...
  'LineStyle'       , 'none'      , ...
  'Marker'          , '.'         , ...
  'Color'           , [0 0 0]  );
set(obs                       , ...
  'LineWidth'       , 1           , ...
  'Marker'          , '.'         , ...
  'MarkerSize'      , 15          , ...
  'MarkerEdgeColor' , [.2 .2 .2]  , ...
  'MarkerFaceColor' , [.7 .7 .7]  );
hXLabel = xlabel('Model (g C m^-^2 d^-^1)');
hYLabel = ylabel('Obs (g C m^-^2 d^-^1)');
%set(gca, 'xticklabel', {'\pi'}) 
%legend('y = 0.78*x + 6.6')
% 
% set([legend, gca]             , ...
%     'FontSize'   , 14           );
set([hXLabel, hYLabel, text]  , ...
    'FontSize'   , 12         );
 
set(gca, ...
  'Box'         , 'off'     , ...
  'TickDir'     , 'out'     , ...
  'TickLength'  , [.02 .02] , ...
  'XMinorTick'  , 'off'      , ...
  'YMinorTick'  , 'off'      , ...
  'YGrid'       , 'off'      , ...
  'XColor'      , [0 0 0], ...
  'YColor'      , [0 0 0], ...
  'YTick'       , -10:2:4, ...
  'XTick'       , -10:2:4, ...
  'LineWidth'   , 1         );
 
set(gcf, 'PaperPositionMode', 'auto');
print('NEE_alldata_onetoone_20150519','-dpng','-r600');
 
%print -depsc2 finalPlot1_inset.eps
close;
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%plotting theta_pop
theta_pop_plot=theta_pop;
theta_pop_plot(:,1)=theta_pop(:,1)+2.6e13;
theta_pop_plot(:,2)=theta_pop(:,2)+75;
theta_pop_plot(:,3)=theta_pop(:,3)+5.7e6;
theta_pop_plot(:,4)=theta_pop(:,4)+1e17;
theta_pop_plot(:,5)=theta_pop(:,5)+94;
theta_pop_plot(:,6)=theta_pop(:,6)+1e7;
 
figure ('Units', 'pixels',...
    'Position', [100 100 500 375]);
hold on
subplot(2,3,1);
hist(theta_pop_plot(:,1))
%axis([1e13 3e13 0 500])
set(get(gca,'child'),'FaceColor',[0.5,0.5,0.5],'EdgeColor','k');
hXLabel = xlabel('Alpha SOC (umol m^-^2 s^-^1)');
hYLabel = ylabel('Count');
set([hXLabel, hYLabel, text]  , ...
    'FontSize'   , 10         );
subplot(2,3,2);
hist(theta_pop_plot(:,2))
set(get(gca,'child'),'FaceColor',[0.5,0.5,0.5],'EdgeColor','k');
hXLabel = xlabel('Ea SOC (J mol^-^1)');
hYLabel = ylabel('Count');
set([hXLabel, hYLabel, text]  , ...
    'FontSize'   , 10         );
 
subplot(2,3,3);
hist(theta_pop_plot(:,3))
%axis([1e7 2e7 0 200])
set(get(gca,'child'),'FaceColor',[0.5,0.5,0.5],'EdgeColor','k');
hXLabel = xlabel('Km SOC (umol m^-^2)');
hYLabel = ylabel('Count');
set([hXLabel, hYLabel, text]  , ...
    'FontSize'   , 10         );
 
subplot(2,3,4);
hist(theta_pop_plot(:,4))
axis([1e17 1.01e17 0 200])
set(get(gca,'child'),'FaceColor',[0.5,0.5,0.5],'EdgeColor','k');
hXLabel = xlabel('Alpha Ps-C (umol m^-^2 s^-^1)');
hYLabel = ylabel('Count');
set([hXLabel, hYLabel, text]  , ...
    'FontSize'   , 10         );
 
subplot(2,3,5);
hist(theta_pop_plot(:,5))
%axis([0 200 0 200])
set(get(gca,'child'),'FaceColor',[0.5,0.5,0.5],'EdgeColor','k');
hXLabel = xlabel('Ea Ps-C (J mol^-^1)');
hYLabel = ylabel('Count');
set([hXLabel, hYLabel, text]  , ...
    'FontSize'   , 10         );
 
subplot(2,3,6);
hist(theta_pop_plot(:,6))
%axis([1e7 2e7 0 200])
set(get(gca,'child'),'FaceColor',[0.5,0.5,0.5],'EdgeColor','k');
hXLabel = xlabel('Km SOC (umol m^-^2)');
hYLabel = ylabel('Count');
set([hXLabel, hYLabel, text]  , ...
    'FontSize'   , 10         );
 
 
 
set(gca(gca,'child', 'Box'         , 'off' ))
, ...
  'TickDir'     , 'out'     , ...
  'TickLength'  , [.02 .02] , ...
  'XMinorTick'  , 'off'      , ...
  'YMinorTick'  , 'off'      , ...
  'YGrid'       , 'off'      , ...
  'XColor'      , [0 0 0], ...
  'YColor'      ; [0 0 0], ...
  'YTick'       , 0:50:300, ...
  'LineWidth'   , 1         ));
 
set(gcf, 'PaperPositionMode', 'auto');
print('finalPlot_Reco_hist','-dpng','-r600');
close;
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure ('Units', 'pixels',...
    'Position', [100 100 500 375]);
hold on
obs = plot(time,WT,'.');
%axis([0 350 0 200])
set(obs                           , ...
  'LineStyle'       , 'none'      , ...
  'Marker'          , '.'         , ...
  'Color'           , [0 0 0]  );
set(obs                       , ...
  'LineWidth'       , 1           , ...
  'Marker'          , '.'         , ...
  'MarkerSize'      , 15          , ...
  'MarkerEdgeColor' , [.2 .2 .2]  , ...
  'MarkerFaceColor' , [.7 .7 .7]  );
hXLabel = xlabel('Day of year');
hYLabel = ylabel('Water table height (cm)');
 
%legend('y = 0.78*x + 6.6')
% 
% set([legend, gca]             , ...
%     'FontSize'   , 14           );
set([hXLabel, hYLabel, text]  , ...
    'FontSize'   , 14         );
 
set(gca, ...
  'Box'         , 'off'     , ...
  'TickDir'     , 'out'     , ...
  'TickLength'  , [.02 .02] , ...
  'XMinorTick'  , 'off'      , ...
  'YMinorTick'  , 'off'      , ...
  'YGrid'       , 'off'      , ...
  'XColor'      , [0 0 0], ...
  'YColor'      , [0 0 0], ...
  'Xtick'       , 0:50:366,...
  'YTick'       , -50:10:50, ...
  'LineWidth'   , 1         );
 
set(gcf, 'PaperPositionMode', 'auto');
print('finalPlot_ch4_2013_WT','-dpng','-r600');
%print -depsc2 finalPlot1_inset.eps
close;
 
%for sigmaplot
    [y_fig6,x_fig6]=density(chain(:,6));%(:,inds(i)),[],varargin{:});
    figure
    plot(x_fig6,y_fig6)
 
sigmaplot=[x_fig1',y_fig1',x_fig2',y_fig2',x_fig3',y_fig3',x_fig4',y_fig4',x_fig5',y_fig5',x_fig6',y_fig6'];
 
 

