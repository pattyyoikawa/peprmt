%TP model alternative Reco model structure

function  Reco = TP_final_sys_CO2(theta,xdata)
%INPUTS
Time_2 =xdata(:,1);
%DOY_disc_2=xdata(:,2);
TA_2 = xdata(:,3);
%WT_2 = xdata(:,4);%water table height cm
% PAR_2 = xdata(:,5);
% LAI_2 = xdata(:,6);
%GPP_2 = xdata(:,7);%gpp modeled--umol m-2 30min-1
%decday_2=xdata(:,8);
%Season_drop_2=xdata(:,9);
%wc_90CI_2=xdata(:,10);
%site_2=xdata(:,11);
wetland_age_2=xdata(:,12);

AirT_K=TA_2+274.15;
AirT_K=AirT_K';

%COMPUTE GPP%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Reco PARAMETERS
Rref=theta(1)+2;
Eo=theta(2)+50;

Tref=288.15;%Ref temp = 15C
To=227.13;

%preallocating space
R2 = zeros(1,length(Time_2));
Reco=zeros(1,length(Time_2));
percent_reduction=zeros(1,length(Time_2));
percent_enhancement=zeros(1,length(Time_2));

for t = 1:length(Time_2);%at t=1
    
 %following Migliavacca 2011, Raich 2002, Reichstein 2003
    R2(t) = Rref*exp(Eo*((1/(Tref-To))-(1/(AirT_K(t)-To))));

    %Empirical fxn, Reco increases when WT drops
  percent_reduction(t)=(a1*WT_2(t).^2)-(a2*WT_2(t))+a3;
if WT_2(t)>5
    percent_reduction(t)=0.75;
end
    
if percent_reduction(t)>1.25
   percent_reduction(t)=1.25;
end
if percent_reduction(t)<0.75
   percent_reduction(t)=0.75;
end

    
    R2(t) = R2(t)*percent_reduction(t); %umol m2 sec
    
%Empirical factor for elevated Reco during the first 3 yrs following restoration
   if wetland_age_2(t)<4
    percent_enhancement(t)=1.2;
   else
    percent_enhancement(t)=1;
   end
    R2(t) = R2(t)*percent_enhancement(t); %umol m2 sec   

   Reco(t) = R2(t); %umol CO2 m-2 s-1
 
end

end