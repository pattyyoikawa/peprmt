%Code for parameterizing PEPRMT CO2 GPP model 
%On data from WP 2012-2013 and MB 2011-2013; validate on 2014-2015 data

load WP_MB_base_data

gpp_ANNnight_cum=cumsum(gpp_ANNnight);
gpp_ANNnight_cum_param=cumsum(gpp_ANNnight_param);
gpp_ANNnight_cum_valid=cumsum(gpp_ANNnight_valid);
figure
plot(gpp_ANNnight_cum,'.')


%paramerterization data
xdata=[DOY_param DOY_disc_param TA_param WT_gf_param PAR_gf_param LAI_gf_param gpp_ANNnight_cum_param decday_param Season_drop_param wc_90CI_param site_param wetland_age_param];
ydata=[DOY_param gpp_ANNnight_cum_param];

daily_gapfilling_error_wc_param=daily_gapfilling_error_wc_param*-1;%b/c model has GPP as negative
daily_random_error_wc_param=daily_random_error_wc_param*-1;
% daily_GPP_obs=daily_GPP_obs*;%daily cum
zdata=[daily_gapfilling_error_wc_param daily_random_error_wc_param daily_Reco_obs_param daily_GPP_obs_param];
 
theta=[0 0 0];
theta=theta';
GPP = PEPRMT_final_sys_CO2_GPP(theta,xdata);
%GPP = PEPRMT_final_sys_CO2_GPP(find_min_GPP,xdata);
 
figure
plot(gpp_ANNnight_param,'.')
hold on
plot(GPP,'.')
legend('obs','mod')
 
X = ones(length(GPP'),1);
X = [X GPP'];
[~,~,~,~,STATS] = regress(gpp_ANNnight_param,X);

%all data
xdata=[DOY DOY_disc TA WT_gf PAR_gf LAI_gf gpp_ANNnight_cum decday Season_drop wc_90CI site wetland_age];
GPP_mod_min = PEPRMT_final_sys_CO2_GPP(find_min,xdata);
figure
plot(decday,gpp_ANNnight,'.',decday,GPP_mod_min,'.')
legend('obs','mod')

%% start MDF
%use for running PEPRMT_final_sys_CO2
model.ssfun = @PEPRMT_final_ss_CO2_GPP;
model.modelfun = @PEPRMT_final_sys_CO2_GPP;

data.xdata = xdata;
data.ydata = ydata;
data.zdata = zdata;
 
 Rsum_obs = data.ydata(:,2);
 nan_obs = sum(isnan(Rsum_obs));
 n_obs = length(Rsum_obs) - nan_obs;
model.N     = n_obs;%total number of non nan obs
%model.S20 = [1 1 2];%has to do with error variance of prior
%model.N0  = [4 4 4];%has to do with error variance of prior
 
params = {
    {'k',             0,     -0.6,        0.2}%light extinction coefficient ranges from 0.2-1
    {'ha',            0,     -20,        120}%Ha range=10-150
    {'hd',            0,     -50,        150}%Hd range = 50-200
%    {'age_param',     1,     0,        1}%wetland age corr = 0-1
%     name      start/theta/par  lBound   uBound    mean_prior   std_prior
    };
  
options.method = 'dram';
options.verbosity  = 0;%level of info printed
options.burnintime = 50000;% burn in before adaptation starts
%[results, chain, s2chain] = mcmcrun(model,data,params,options,results);
% [RESULTS,CHAIN,S2CHAIN,SSCHAIN] = MCMCRUN(MODEL,DATA,PARAMS,OPTIONS)
%First generate an initial chain
options.nsimu = 50000;
[results, chain, s2chain, sschain]= mcmcrun_FINAL(model,data,params,options);

%find min
figure
plot(sschain,'.')%sum-of-squares chains
nanmin(sschain)
Sel=sschain<nanmin(sschain)+0.00001;%442
figure
plot(Sel)
% cumsum(Sel)
find_min=chain(Sel,:);
find_min=find_min(end,:)';

GPP = PEPRMT_final_sys_CO2_GPP(find_min,xdata);
figure
plot(gpp_ANNnight_param,'.')
hold on
plot(GPP,'.')
legend('obs','mod')


%Then re-run starting from the results of the previous run, this will take about 10 minutes.
params = {
    {'k',            find_min(1),     -0.6,        0.2}
    {'ha',           find_min(2),     -20,        120}
    {'hd',           find_min(3),     -50,        150}
%    {'age_param',    find_min(4),     0,        1}%wetland age corr = 0-1
%     name      start/theta/par  lBound   uBound    mean_prior   std_prior
    };
 
options.method = 'dram';
options.verbosity  = 0;%level of info printed
options.burnintime = 0;% burn in before adaptation starts
 
options.nsimu = 150000;
[results, chain, s2chain, sschain] = mcmcrun_FINAL(model,data,params,options, results);
 
figure
mcmcplot(chain,[],results,'chainpanel');%plots chain for ea parameter and all the values it accepted
figure
mcmcplot(chain,[],results,'pairs');%tells you if parameters are correlated
%  figure
%  mcmcplot(sqrt(s2chain),[],[],'hist')%take square root of the s2chain to get the chain for error standard deviation
% title('Error std posterior')
figure
mcmcplot(chain,[],results,'denspanel',2);
 
 
sschain_long=sschain;%save before you move on
chain_long=chain;
results_long=results;

figure
plot(chain_long,'.')%sum-of-squares chains
figure
plot(sschain_long,'.')%sum-of-squares chains
nanmin(sschain_long)
Sel=sschain<nanmin(sschain_long)+0.000001;%442
figure
plot(Sel)
% cumsum(Sel)
find_min=chain_long(Sel,:);
find_min=find_min(end,:)';

GPP_mod_min = PEPRMT_final_sys_CO2_GPP(find_min_GPP,xdata);
figure
plot(decday_param,gpp_ANNnight_param,'.')
hold on
plot(decday_param,GPP_mod_min,'.')
legend('obs','mod')

%% Choose posterior

%First establish if chain stabilized
chainstats(chain,results)
%Geweke test should be high ideally >0.9 for all parameters

%rejection rate of chain needs to be less than 50%
results_long.rejected*100

%Determine where means and sd of parameters stabilize in the chain
%this tells you what part of chain is stable for selection of posterior

window_size = 5000;
simple = tsmovavg(chain_long(:,1),'s',window_size,1);
simple_std = movingstd(chain_long(:,1),5000);
figure
subplot(2,1,1)
plot(simple,'.')
title('5000 iteration moving average k')
subplot(2,1,2)
plot(simple_std,'.')
title('5000 iteration moving SD k')

window_size = 5000;
simple = tsmovavg(chain_long(:,2),'s',window_size,1);
simple_std = movingstd(chain_long(:,2),5000);
figure
subplot(2,1,1)
plot(simple,'.')
title('5000 iteration moving average Ha')
subplot(2,1,2)
plot(simple_std,'.')
title('5000 iteration moving SD Ha')

window_size = 5000;
simple = tsmovavg(chain_long(:,3),'s',window_size,1);
simple_std = movingstd(chain_long(:,3),5000);
figure
subplot(2,1,1)
plot(simple,'.')
title('5000 iteration moving average Hd')
subplot(2,1,2)
plot(simple_std,'.')
title('5000 iteration moving SD Hd')


%identified visually that chain is good around 100,000 - 2015.06.11
%choose parammeters 10,000 parameter sets from that area
%e.g. 100,000-110,000

find_min_10000=chain(100000:110000,:);

%run 10000 parameter sets through validation data + and test chi square
xdata=[DOY_valid DOY_disc_valid TA_valid WT_gf_valid PAR_gf_valid LAI_gf_valid GPP_min_valid decday_valid Season_drop_valid wc_90CI_valid site_valid wetland_age_valid];
ydata=[DOY_valid wc_gf_valid];%this has NEE umol m-2 s-1 and CH4 umol m-2 30min-1
zdata=[daily_gapfilling_error_wc_valid daily_random_error_wc_valid daily_Reco_obs_valid daily_GPP_obs_valid];

model_pop_GPP=zeros(length(1:35040));%length of validation data
model_pop_GPP=model_pop_GPP(:,1:10000);%update as needed 
 
for i=1:length(find_min_10000)
    GPP = PEPRMT_final_sys_CO2_GPP(find_min_10000(i,:)',xdata);
    model_pop_GPP(:,i)=GPP;
end
 
model_std_GPP=std(model_pop_GPP');
mean_theta_final_GPP=nanmean(find_min_10000);
mean_theta_final_GPP=mean_theta_final_GPP';
%%  Evaluate posterior and confidence intervals

%according to Zobitz 2008, model mean==model min(best para set)
GPP_mean  = PEPRMT_final_sys_CO2_GPP(mean_theta_final_GPP,xdata);
GPP_min  = PEPRMT_final_sys_CO2_GPP(find_min_GPP,xdata);
 
figure
plot(decday_valid,gpp_ANNnight_valid,'.',decday_valid,GPP_min,'.')
legend('obs','mod min')
figure
plot(decday_valid,gpp_ANNnight_valid,'.',decday_valid,GPP_mean,'.')
legend('obs','mod mean')

figure
plot(TA,gpp_ANNnight,'.',TA,GPP_min,'.')
xlabel('Tair')
legend('obs','mod')

%for supplemental figure
figure
plot(APAR_2(20000:end),gpp_ANNnight(20000:end),'.k')
xlabel('APAR (MJ m^-^2 s^-^1)')
ylabel('GPP partitioned (\mumol m^-^2 s^-^1)')
X = ones(length(gpp_ANNnight(20000:end)),1);
X = [X gpp_ANNnight(20000:end)];
[~,~,~,~,STATS] = regress(APAR_2(20000:end),X);
set(gcf, 'PaperPositionMode', 'auto');
print('Supple_fig_APAR_GPP_20160118','-dpng','-r600');


figure
plot(model_std_GPP,'.')
% 90% confidence intervals
%b/c the 10,000 parameters are not independent or may even be redundant, I
%reduce the sample size to 1000
% lower_GPP=GPP_min-(1.645*(model_std_GPP./sqrt(50)));
% upper_GPP=GPP_min+(1.645*(model_std_GPP./sqrt(50)));
     lower_GPP=quantile(model_pop_GPP',0.30);% 90% quantile
     upper_GPP=quantile(model_pop_GPP',0.70);% 90% quantile
% 
figure
plot(decday_valid,GPP_min,'.',decday_valid,lower_GPP,'.',decday_valid,upper_GPP,'.')
legend('mean','lower','upper')
 
%RMSE is a better test of model performance than r2
RMSE_GPP_min=nansum((GPP_min'-gpp_ANNnight).^2);
RMSE_GPP_min=sqrt(RMSE_GPP_min/43296);


%For validation stats
%annual sum test
gpp_dailysum=gpp_ANNnight(25777:end)*60*30;%convert from nmol m-2 s-1 to nmol m-2 day-1
cum_obs_valid=cumsum(gpp_dailysum);%umol m-2 yr-1
cum_obs_valid_sum=cum_obs_valid(end)*10^-6;%mol CO2 m-2 yr-1
cum_obs_valid_sum=cum_obs_valid_sum*44.01*0.27;%g C-CO2 m-2 yr-1
%model_mean=model_mean';
mod_dailysum=GPP_min(25777:end)*60*30;%convert from nmol m-2 s-1 to nmol m-2 day-1
cum_mod_valid=cumsum(mod_dailysum);%nmol m-2 yr-1
cum_mod_valid_sum=cum_mod_valid(end)*10^-6;%mol m-2 yr-1
cum_mod_valid_sum=cum_mod_valid_sum*44.01*0.27;%g C-CO2 m-2 yr-1


mod_dailysum_min=lower_GPP(25777:end)*60*30;%convert from nmol m-2 s-1 to nmol m-2 day-1
cum_mod_min_valid=cumsum(mod_dailysum_min);%nmol m-2 yr-1
cum_mod_min_valid_sum=cum_mod_min_valid(end)*10^-6;%mol m-2 yr-1
cum_mod_min_valid_sum=cum_mod_min_valid_sum*44.01*0.27;%g C-CO2 m-2 yr-1
 
cum_mod_min_valid_sum-cum_mod_valid_sum

mod_dailysum_max=upper_GPP(25777:end)*60*30;%convert from nmol m-2 s-1 to nmol m-2 day-1
cum_mod_max_valid=cumsum(mod_dailysum_max);%nmol m-2 yr-1
cum_mod_max_valid_sum=cum_mod_max_valid(end)*10^-6;%mol m-2 yr-1
cum_mod_max_valid_sum=cum_mod_max_valid_sum*44.01*0.27;%g C-CO2 m-2 yr-1

cum_mod_max_valid_sum-cum_mod_valid_sum

figure
plot(decday(25777:end),cum_obs_valid,'.',decday(25777:end),cum_mod_valid,'.')
legend('obs','mod')
%For param stats
%annual sum test
gpp_dailysum=gpp_ANNnight(1:25776)*60*30;%convert from nmol m-2 s-1 to nmol m-2 day-1
cum_obs_param=cumsum(gpp_dailysum);%umol m-2 yr-1
cum_obs_param_sum=cum_obs_param(end)*10^-6;%mol CO2 m-2 yr-1
cum_obs_param_sum=cum_obs_param_sum*44.01*0.27;%g C-CO2 m-2 yr-1
%model_mean=model_mean';
mod_dailysum=GPP_min(1:25776)*60*30;%convert from nmol m-2 s-1 to nmol m-2 day-1
cum_mod_param=cumsum(mod_dailysum);%nmol m-2 yr-1
cum_mod_param_sum=cum_mod_param(end)*10^-6;%mol m-2 yr-1
cum_mod_param_sum=cum_mod_param_sum*44.01*0.27;%g C-CO2 m-2 yr-1

cum_mod_param=cum_mod_param';
cum_mod_param=cum_mod_param*10^-6*44.01*0.27;
cum_obs_param=cum_obs_param*10^-6*44.01*0.27;
figure
plot(decday(1:25776),cum_obs_param,'.',decday(1:25776),cum_mod_param,'.')
legend('obs','mod')

mod_dailysum_min=lower_GPP(1:25776)*60*30;%convert from nmol m-2 s-1 to nmol m-2 day-1
cum_mod_min_param=cumsum(mod_dailysum_min);%nmol m-2 yr-1
cum_mod_min_param_sum=cum_mod_min_param(end)*10^-6;%mol m-2 yr-1
cum_mod_min_param_sum=cum_mod_min_param_sum*44.01*0.27;%g C-CO2 m-2 yr-1
 
cum_mod_min_param_sum-cum_mod_param_sum

mod_dailysum_max=upper_GPP(1:25776)*60*30;%convert from nmol m-2 s-1 to nmol m-2 day-1
cum_mod_max_param=cumsum(mod_dailysum_max);%nmol m-2 yr-1
cum_mod_max_param_sum=cum_mod_max_param(end)*10^-6;%mol m-2 yr-1
cum_mod_max_param_sum=cum_mod_max_param_sum*44.01*0.27;%g C-CO2 m-2 yr-1


  
%% daily sums 
days = 195:1:1097;
days=days';
%b/c my code likes to start from 1, shift data temporarily so they start at
%1
days=days-194;
DOY_daily=DOY-194;
 
%DOY_daily=DOY_daily(1:25776);
  
daily_GPP_0=GPP_min*60*30;%first turn gpp from umol m-2 s-1 into umol m-2 30min-1
daily_GPP_0=daily_GPP_0';
[daily_GPP_mod_min]=daily_sum(DOY_daily,daily_GPP_0,days);
 
daily_GPP_0=GPP_mean*60*30;%first turn gpp from umol m-2 s-1 into umol m-2 30min-1
daily_GPP_0=daily_GPP_0';
[daily_GPP_mod_mean]=daily_sum(DOY_daily,daily_GPP_0,days);
 
daily_GPP_0=gpp_ANNnight*60*30;%first turn gpp from umol m-2 s-1 into umol m-2 30min-1
%daily_GPP_0=gpp_ANNnight(1:25776)*-1*60*30;%first turn gpp from umol m-2 s-1 into umol m-2 30min-1
[daily_GPP_obs]=daily_sum(DOY_daily,daily_GPP_0,days);
figure
plot(days,daily_GPP_obs,'.',days,daily_GPP_mod_min,'.')%,days,daily_GPP_mod_mean,'.')
ylabel('Reco daily sum umol m-2 d-1')
legend('obs','mod')
figure
plot(days,daily_GPP_obs,'.',days,daily_GPP_mod_mean,'.')%,days,daily_GPP_mod_mean,'.')
ylabel('Reco daily sum umol m-2 d-1')
legend('obs','mod mean')
 
figure
plot(daily_GPP_mod_min,daily_GPP_obs,'.')
 
%RMSE is a better test of model performance than r2
RMSE_gpp_min_daily=nansum((daily_GPP_mod_min(539:end)-daily_GPP_obs(539:end)).^2);
RMSE_gpp_min_daily=sqrt(RMSE_gpp_min_daily/365);

X = ones(length(daily_GPP_mod_min(1:538)),1);
X = [X daily_GPP_mod_min(1:538)];
[~,~,~,~,STATS] = regress(daily_GPP_obs(1:538),X);
 
 
daily_lower_0=lower_GPP*60*30;%first turn gpp from umol m-2 s-1 into umol m-2 30min-1
daily_lower_0=daily_lower_0';
[daily_lower_GPP]=daily_sum(DOY_daily,daily_lower_0,days);
 
daily_upper_0=upper_GPP*60*30;%first turn gpp from umol m-2 s-1 into umol m-2 30min-1
daily_upper_0=daily_upper_0';
[daily_upper_GPP]=daily_sum(DOY_daily,daily_upper_0,days);
figure
plot(days,daily_lower_GPP,'.',days,daily_upper_GPP,'.',days,daily_GPP_mod_min,'.')
legend('lower','upper','mod')
 
daily_lower_GPP=daily_lower_GPP';
daily_upper_GPP=daily_upper_GPP';
daily_GPP_obs=daily_GPP_obs';
daily_GPP_mod_min=daily_GPP_mod_min';
%exclude strange outlier
daily_lower_GPP(49)=daily_lower_GPP(48);
daily_upper_GPP(49)=daily_upper_GPP(48);
daily_GPP_mod_min(49)=daily_GPP_mod_min(48);
figure
plot(days,daily_lower_GPP,'.',days,daily_upper_GPP,'.',days,daily_GPP_mod_min,'.')
legend('lower','upper','mod')


 %% Posteriors plotted updated 10/10/2016
k_posterior=find_min_10000_GPP(:,1)+0.8;
Ha_posterior=find_min_10000_GPP(:,2)+30;
Hd_posterior=find_min_10000_GPP(:,3)+100;

figure
subplot(3,1,1)
hist(k_posterior,10);%pdf for alpha 1
xlabel('k')
subplot(3,1,2)
hist(Ha_posterior,10);%pdf for alpha 1
xlabel('H_a (kJ mol^-^1)')
subplot(3,1,3)
hist(Hd_posterior,10);%pdf for alpha 1
xlabel('H_d (kJ mol^-^1)') 

set(gcf, 'PaperPositionMode', 'auto');
print('GPP_posterior_distr_20161010','-dpng','-r600');


 %Final parameter estimates
k_FINAL=find_min(1)+0.8;%kJ mol-1
Ha_FINAL=find_min(2)+30;%umol m-2
Hd_FINAL=find_min(3)+100;
%% Correlation matrix GPP parameters 2016.10.10
figure
subplot(3,3,1)
plot(k_posterior,k_posterior,'.');%pdf for alpha 1
subplot(3,3,4)
plot(Ha_posterior,k_posterior,'.');%pdf for alpha 1
subplot(3,3,5)
plot(Ha_posterior,Ha_posterior,'.');%pdf for alpha 1
subplot(3,3,7)
plot(Hd_posterior,k_posterior,'.');%pdf for alpha 1
subplot(3,3,8)
plot(Hd_posterior,Ha_posterior,'.');%pdf for alpha 1
subplot(3,3,9)
plot(Hd_posterior,Hd_posterior,'.');%pdf for alpha 1

set(gcf, 'PaperPositionMode', 'auto');
print('GPP_posterior_correlation_matrix_20161010','-dpng','-r600');


%% plotting
%30min 


%daily 
days_plot=days+194;
daily_lower_GPP_plot=daily_lower_GPP*10^-6*11.88;
daily_upper_GPP_plot=daily_upper_GPP*10^-6*11.88;
daily_GPP_obs_plot=daily_GPP_obs*10^-6*11.88;
daily_GPP_mod_min_plot=daily_GPP_mod_min*10^-6*11.88;

X = ones(length(daily_GPP_mod_min_plot),1);
X = [X daily_GPP_mod_min_plot];
[~,~,~,~,STATS] = regress(daily_GPP_obs_plot,X);
 
%RMSE is a better test of model performance than r2
RMSE_GPP_min=nansum((daily_GPP_mod_min_plot(1:538)-daily_GPP_obs_plot(1:538)).^2);
RMSE_GPP_min=sqrt(RMSE_GPP_min/538);

startDate = datenum('07-12-2012');
endDate = datenum('12-31-2014');
xData1=linspace(startDate,endDate,903);
figure ('Units', 'pixels',...
    'Position', [100 100 500 375]);
ciplot(daily_lower_GPP_plot,daily_upper_GPP_plot,xData1,'k')
datetick('x','mm/yyyy')
hold on
obs = plot(date1,daily_GPP_obs_plot','.');
 box off
set(obs                       , ...
  'LineWidth'       , 1           , ...
  'Marker'          , 'o'         , ...
  'MarkerSize'      , 3           , ...
  'MarkerEdgeColor' , [0 1 0]  , ...
  'MarkerFaceColor' , [0 1 0]  );
 
hXLabel = xlabel('Day of year'                     );
hYLabel = ylabel('GPP (g C-CO_2 m^-^2 d^-^1)' );
 
 legend('model','obs')
 
set( gca                       , ...
    'FontName'   , 'Helvetica' );
set([hXLabel, hYLabel, text]  , ...
    'FontSize'   , 12         );


set(gcf, 'PaperPositionMode', 'auto');
print('GPP_alldata_20150622_validation','-dpng','-r600');
%print -depsc2 finalPlot1.eps
close;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure ('Units', 'pixels',...
    'Position', [100 100 500 375]);
hold on
obs = plot(daily_GPP_mod_min_plot(1:538)',daily_GPP_obs_plot(1:538),'.',daily_GPP_mod_min_plot(539:end)',daily_GPP_obs_plot(539:end),'.');
set(obs                       , ...
  'LineWidth'       , 1           , ...
  'Marker'          , 'o'         , ...
  'MarkerSize'      , 3          , ...
  'MarkerEdgeColor' , [0 0 0]  , ...
  'MarkerFaceColor' , [0 0 0]  );
hXLabel = xlabel('Model (g C m^-^2 d^-^1)');
hYLabel = ylabel('Obs (g C m^-^2 d^-^1)');
set([hXLabel, hYLabel, text]  , ...
    'FontSize'   , 12         );
lsline
legend('param','valid')
 
set(gca, ...
  'Box'         , 'off'     , ...
  'TickDir'     , 'in'     , ...
  'TickLength'  , [.02 .02] , ...
  'XMinorTick'  , 'off'      , ...
  'YMinorTick'  , 'off'      , ...
  'YGrid'       , 'off'      , ...
  'XColor'      , [0 0 0], ...
  'YColor'      , [0 0 0], ...
  'YTick'       , -14:2:4, ...
  'XTick'       , -14:2:4, ...
  'LineWidth'   , 1         );
 
set(gcf, 'PaperPositionMode', 'auto');
print('GPP_alldata_onetoone_20150831','-dpng','-r600');
 
%print -depsc2 finalPlot1_inset.eps
close;
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%plotting theta_pop
theta_pop_plot=theta_pop;
theta_pop_plot(:,1)=theta_pop(:,1)+2.6e13;
theta_pop_plot(:,2)=theta_pop(:,2)+75;
theta_pop_plot(:,3)=theta_pop(:,3)+5.7e6;
theta_pop_plot(:,4)=theta_pop(:,4)+1e17;
theta_pop_plot(:,5)=theta_pop(:,5)+94;
theta_pop_plot(:,6)=theta_pop(:,6)+1e7;
 
figure ('Units', 'pixels',...
    'Position', [100 100 500 375]);
hold on
subplot(2,3,1);
hist(theta_pop_plot(:,1))
%axis([1e13 3e13 0 500])
set(get(gca,'child'),'FaceColor',[0.5,0.5,0.5],'EdgeColor','k');
hXLabel = xlabel('Alpha SOC (umol m^-^2 s^-^1)');
hYLabel = ylabel('Count');
set([hXLabel, hYLabel, text]  , ...
    'FontSize'   , 10         );
subplot(2,3,2);
hist(theta_pop_plot(:,2))
set(get(gca,'child'),'FaceColor',[0.5,0.5,0.5],'EdgeColor','k');
hXLabel = xlabel('Ea SOC (J mol^-^1)');
hYLabel = ylabel('Count');
set([hXLabel, hYLabel, text]  , ...
    'FontSize'   , 10         );
 
subplot(2,3,3);
hist(theta_pop_plot(:,3))
%axis([1e7 2e7 0 200])
set(get(gca,'child'),'FaceColor',[0.5,0.5,0.5],'EdgeColor','k');
hXLabel = xlabel('Km SOC (umol m^-^2)');
hYLabel = ylabel('Count');
set([hXLabel, hYLabel, text]  , ...
    'FontSize'   , 10         );
 
subplot(2,3,4);
hist(theta_pop_plot(:,4))
axis([1e17 1.01e17 0 200])
set(get(gca,'child'),'FaceColor',[0.5,0.5,0.5],'EdgeColor','k');
hXLabel = xlabel('Alpha Ps-C (umol m^-^2 s^-^1)');
hYLabel = ylabel('Count');
set([hXLabel, hYLabel, text]  , ...
    'FontSize'   , 10         );
 
subplot(2,3,5);
hist(theta_pop_plot(:,5))
%axis([0 200 0 200])
set(get(gca,'child'),'FaceColor',[0.5,0.5,0.5],'EdgeColor','k');
hXLabel = xlabel('Ea Ps-C (J mol^-^1)');
hYLabel = ylabel('Count');
set([hXLabel, hYLabel, text]  , ...
    'FontSize'   , 10         );
 
subplot(2,3,6);
hist(theta_pop_plot(:,6))
%axis([1e7 2e7 0 200])
set(get(gca,'child'),'FaceColor',[0.5,0.5,0.5],'EdgeColor','k');
hXLabel = xlabel('Km SOC (umol m^-^2)');
hYLabel = ylabel('Count');
set([hXLabel, hYLabel, text]  , ...
    'FontSize'   , 10         );
 
 
 
set(gca(gca,'child', 'Box'         , 'off' ))
, ...
  'TickDir'     , 'out'     , ...
  'TickLength'  , [.02 .02] , ...
  'XMinorTick'  , 'off'      , ...
  'YMinorTick'  , 'off'      , ...
  'YGrid'       , 'off'      , ...
  'XColor'      , [0 0 0], ...
  'YColor'      ; [0 0 0], ...
  'YTick'       , 0:50:300, ...
  'LineWidth'   , 1         ));
 
set(gcf, 'PaperPositionMode', 'auto');
print('finalPlot_Reco_hist','-dpng','-r600');
close;
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure ('Units', 'pixels',...
    'Position', [100 100 500 375]);
hold on
obs = plot(time,WT,'.');
%axis([0 350 0 200])
set(obs                           , ...
  'LineStyle'       , 'none'      , ...
  'Marker'          , '.'         , ...
  'Color'           , [0 0 0]  );
set(obs                       , ...
  'LineWidth'       , 1           , ...
  'Marker'          , '.'         , ...
  'MarkerSize'      , 15          , ...
  'MarkerEdgeColor' , [.2 .2 .2]  , ...
  'MarkerFaceColor' , [.7 .7 .7]  );
hXLabel = xlabel('Day of year');
hYLabel = ylabel('Water table height (cm)');
 
%legend('y = 0.78*x + 6.6')
% 
% set([legend, gca]             , ...
%     'FontSize'   , 14           );
set([hXLabel, hYLabel, text]  , ...
    'FontSize'   , 14         );
 
set(gca, ...
  'Box'         , 'off'     , ...
  'TickDir'     , 'out'     , ...
  'TickLength'  , [.02 .02] , ...
  'XMinorTick'  , 'off'      , ...
  'YMinorTick'  , 'off'      , ...
  'YGrid'       , 'off'      , ...
  'XColor'      , [0 0 0], ...
  'YColor'      , [0 0 0], ...
  'Xtick'       , 0:50:366,...
  'YTick'       , -50:10:50, ...
  'LineWidth'   , 1         );
 
set(gcf, 'PaperPositionMode', 'auto');
print('finalPlot_ch4_2013_WT','-dpng','-r600');
%print -depsc2 finalPlot1_inset.eps
close;
 
%for sigmaplot
    [y_fig6,x_fig6]=density(chain(:,6));%(:,inds(i)),[],varargin{:});
    figure
    plot(x_fig6,y_fig6)
 
sigmaplot=[x_fig1',y_fig1',x_fig2',y_fig2',x_fig3',y_fig3',x_fig4',y_fig4',x_fig5',y_fig5',x_fig6',y_fig6'];
 
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%APAR vs GPP
%APAR_2=fPAR_2.*PAR_2_MJ;%MJ m-2 s-1
APAR=APAR_2*60*60*24*365*1e-3;%GJ m-2 yr-1
cum_APAR=cumsum(APAR_2*60*30);

daily_APAR_0=APAR_2*60*30;%first turn APAR from MJ m-2 s-1 into MJ m-2 30min-1
daily_APAR_0=daily_APAR_0';
[daily_APAR]=daily_sum(DOY_daily,daily_APAR_0',days);
%daily_APAR MJ m-2 d-1

X = ones(length(daily_APAR'),1);
X = [X daily_APAR'];
[~,~,~,~,STATS] = regress(daily_GPP_obs_plot,X);
rsq=STATS(1);

figure ('Units', 'pixels',...
    'Position', [100 100 500 375])
obs=plot(daily_APAR,daily_GPP_obs_plot,'.');
hXLabel = xlabel('APAR (MJ m^-^2 d^-^1)');
hYLabel=ylabel('GPP partitioned (g C m^-^2 d^-^1)') ;
box off
%xlim([0,5])
%ylim([0,5])
set(obs                       , ...
  'LineWidth'       , 1           , ...
  'Marker'          , 'o'         , ...
  'MarkerSize'      , 3           , ...
  'MarkerEdgeColor' , [0.25 0.25 0.25]  , ...
  'MarkerFaceColor' , [0.25 0.25 0.25]  );
h=lsline;
p2 = polyfit(get(h,'xdata'),get(h,'ydata'),1);
theString = sprintf('y = %.3f x + %.3f', p2(1), p2(2));
place_x=min(daily_APAR);
place_y=max(daily_GPP_obs_plot);
t=text(place_x,place_y,theString);%;t=text(2,-7,'p2');%puts letter in upper L corner
theString = sprintf('r^2 = %.2f', rsq);
t=text(5,0,theString);%puts letter in upper L corner
 set( gca                       , ...
    'FontName'   , 'Helvetica' );
set([hXLabel, hYLabel, text]  , ...
    'FontSize'   , 12         );
 
set(gcf, 'PaperPositionMode', 'auto');
print('APAR_daily_vs_GPP_20160223','-dpng','-r300');
