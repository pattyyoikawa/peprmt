function ss = PEPRMT_final_ss_CO2_GPP(theta,data)

time   = data.ydata(:,1);
ydata  = data.ydata(:,2);%gpp
xdata  = data.xdata;

% y0=0;
%ymodel = DAMMCH4fun(time,theta,y0,xdata);
[GPP] = PEPRMT_final_sys_CO2_GPP(theta,xdata);%umol m-2 s-1

%errors pre-loaded now

%convert to daily sums for data-model mismatch comparison
%this allows variance to be normally distributed and allows you to use a
%simple cost function here (Richardson 2010 Oecologia)
time_max=nanmax(time);
time_min=nanmin(time);
days = time_min:1:time_max;%NEEDS TO BE UPDATED EVERY TIME!!
days=days';
%b/c my code likes to start from 1, shift data temporarily so they start at
%1
days=days-time_min+1;
DOY_daily=time-time_min+1;

zdata=data.zdata;
daily_gapfilling_error = zdata(:,1);
daily_random_error = zdata(:,2);
%daily_Reco_obs = zdata(:,3);
daily_GPP_obs = zdata(:,4);%daily cum
%OLD METHOD
% daily_ymodel_0=ymodel*60*30;%convert NEE from umol m-2 s-1 into umol m-2 30min-1
% daily_ymodel_0=daily_ymodel_0';
% [daily_ymodel]=daily_sum(DOY_daily,daily_ymodel_0,days);
% daily_ymodel=daily_ymodel';
%NEW METHOD
daily_GPP_0=GPP*60*30;%convert NEE from umol m-2 s-1 into umol m-2 30min-1
daily_GPP_0=daily_GPP_0';
[daily_GPP]=daily_sum(DOY_daily,daily_GPP_0,days);
daily_GPP=daily_GPP';
% figure
% plot(daily_GPP,'.')
% hold on
% plot(daily_GPP_obs,'r.')
% hold on
% plot(daily_random_error,'k')
% hold on
% plot(daily_gapfilling_error,'m')
% 
%calculate sample size
nan_obs=sum(isnan(daily_GPP));
n = length(daily_GPP)-nan_obs;

%simple least squares optimization - following Keenan 2011 and 2012
 ss1 = ((daily_GPP_obs-daily_GPP)./(daily_random_error+daily_gapfilling_error)).^2;
 ss = (nansum(ss1))/n;
 
 
end%negative log likelihood function based on Gaussian prob distribution


