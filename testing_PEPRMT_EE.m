%testing PEPRMT model in East End
%2016.07.29

%% Prep data
%take WP season_drop variable and pull out 1 year that can be applied to
%other sites
figure
plot(Season_drop_WP,'.')
Season_drop_one_year=Season_drop_WP(6818:24337);


%Load EE L3 data
%First calculate GPP
gpp_ANNnight_EE=data.gpp_ANNnight(19297:36816);
er_ANNnight_EE=data.er_ANNnight(19297:36816);


WT_gf_EE_2015=Metdata.WT(19297:36816);
figure
plot(WT_gf_EE_2015,'.')

PAR_gf_EE_2015=Metdata.PAR(19297:36816);
figure
plot(PAR_gf_EE_2015,'.')

EE_gpp_ANNnight_cum_2015=data.gpp_ANNnight(19297:36816);
EE_gpp_ANNnight_cum_2015=cumsum(EE_gpp_ANNnight_cum_2015);
figure
plot(EE_gpp_ANNnight_cum_2015,'.')
%compute LAI for EE
%first make a smooth GCC
[gcc_2015,Yearsmooth,DOYsmooth,gccsmooth_2015] = smoothgcc_Oikawa(Metdata.GCC(19297:36816),data.decday(19297:36816)-365*2,...
    data.year(19297:36816),data.DOY(19297:36816),0,3,'90percentile',1);
figure
plot(data.decday(19297:36816)-365*2, gcc_2015,'.','MarkerSize',14);
    hold on
    plot(DOYsmooth, gccsmooth_2015,'.','MarkerSize',20);
    legend('gcc','gccsmooth')
%convert gcc smooth into longer time frame
DOYrunning=data.DOY(19297:36816);
GCCsmooth=DOYrunning*0;
Sel=GCCsmooth==0;
GCCsmooth(Sel)=NaN;

    for i=1:length(DOYsmooth)
    Sel=find(DOYsmooth(i)==DOYrunning);
        GCCsmooth(Sel)=gccsmooth_2015(i);
    end
     Sel=GCCsmooth<0.33;
     GCCsmooth(Sel)=NaN;
    [GCCsmooth] = FillSmallGaps(GCCsmooth,10000);
figure
plot(data.decday(19297:36816)-365*2,GCCsmooth,'.')
hold on
plot(data.decday(19297:36816)-365*2,Metdata.GCC(19297:36816),'.r')

LAI_EE=0.0037*exp(GCCsmooth*17.06);
LAI_gf_EE_2015=FillSmallGaps(LAI_EE,10000);
LAI_gf_EE_2015(1:911)=1.5041;
LAI_gf_EE_2015(16369:end)=LAI_gf_EE_2015(16368);

figure
plot(LAI_gf_EE_2015,'.b')
hold on
plot(LAI_EE,'.r')

TA_EE_2015=data.TA(19297:36816);
TA_gf_EE_2015=FillSmallGaps(TA_EE_2015,10000);
figure
plot(TA_gf_EE_2015,'.')
hold on
plot(TA_EE_2015,'r.')

DOY_EE_2015=data.DOY(19297:36816);
decday_EE_2015=data.decday(19297:36816);
wm_gf_EE_2015=data.wm_gf(19297:36816);
wc_gf_EE_2015=data.wc_gf(19297:36816);

%create fake 90CI for EE--don't need it b/c not using this data in MDF
wc_90CI_EE= decday_EE_2015*0;
%create site variable--just 1's b/c only 1 site
site_EE =decday_EE_2015*0+1;
%create wetland age variable--only 1 year of data and 2 years old
wetland_age_EE=decday_EE_2015*0+1;

%% calculate 90%CI for ANN
%create 20 datasets for GPP ANN--apparently not in data structure

for i=1:length(data.decday)
    
gpp_ANNext(i,:)=data.wc_gf(i)-data.er_ANNext(i,:);
er_ANN_mean(i)=nanmean(data.er_ANNext(i,:));
er_ANN_SD(i)=nanstd(data.er_ANNext(i,:));
gpp_ANN_mean(i)=nanmean(gpp_ANNext(i,:));
gpp_ANN_SD(i)=nanstd(gpp_ANNext(i,:));
wm_ANN_mean(i)=nanmean(data.wm_ANNext(i,:));
wm_ANN_SD(i)=nanstd(data.wm_ANNext(i,:));

end
gf_error_er=1.645*(er_ANN_SD./sqrt(20));
gf_error_gpp=1.645*(gpp_ANN_SD./sqrt(20));
gf_error_wm=1.645*(wm_ANN_SD./sqrt(20));

lower_er_ANN=er_ANN_mean-(1.645*(er_ANN_SD./sqrt(20)));%use 1.645 for 90% CI
upper_er_ANN=er_ANN_mean+(1.645*(er_ANN_SD./sqrt(20)));
lower_gpp_ANN=gpp_ANN_mean-(1.645*(gpp_ANN_SD./sqrt(20)));%use 1.645 for 90% CI
upper_gpp_ANN=gpp_ANN_mean+(1.645*(gpp_ANN_SD./sqrt(20)));

figure
plot(lower_er_ANN)
hold on
plot(upper_er_ANN)
figure
plot(lower_gpp_ANN)
hold on
plot(upper_gpp_ANN)

%select only 2015
lower_er_ANN=lower_er_ANN(19297:36816);
upper_er_ANN=upper_er_ANN(19297:36816);
lower_gpp_ANN=lower_gpp_ANN(19297:36816);
upper_gpp_ANN=upper_gpp_ANN(19297:36816);
gf_error_er=gf_error_er(19297:36816);
gf_error_gpp=gf_error_gpp(19297:36816);
gf_error_wm=gf_error_wm(19297:36816);
%gf error same for er and gpp
figure
plot(gf_error_er,'.')
hold on
plot(gf_error_gpp,'g.')
figure
plot(gf_error_wm,'.r')
%Random error 
envir_var=[data.ubar(19297:36816) PAR_gf_EE_2015 TA_EE_2015];
var_num=3;
range=[1 75 3];
[wc_RE,~,~,~,wc_p_pos,wc_p_neg]=daily_diff(DOY_EE_2015,data.time(19297:36816),wc_gf_EE_2015,var_num,envir_var,range);
figure
plot(wc_RE,'.')
wc_RE_high=wc_gf_EE_2015+wc_RE';
wc_RE_low=wc_gf_EE_2015-wc_RE';
figure
ciplot(wc_RE_low,wc_RE_high,decday_EE_2015)

envir_var=[data.ubar(19297:36816) PAR_gf_EE_2015 TA_EE_2015];
var_num=3;
range=[1 75 3];
[wm_RE,~,~,~,wm_p_pos,wm_p_neg]=daily_diff(DOY_EE_2015,data.time(19297:36816),wm_gf_EE_2015,var_num,envir_var,range);
figure
plot(wm_RE,'.')
wm_RE_high=wm_gf_EE_2015+wm_RE';
wm_RE_low=wm_gf_EE_2015-wm_RE';
figure
ciplot(wm_RE_low,wm_RE_high,decday_EE_2015)

% need to add Random error to the 90% CI gf error at 30min time step
%where during gaps, have gapfilling error+random error
%in non-gaps, only random error

wm_0=data.wm(19297:36816);
wc_0=data.wc(19297:36816);
Sel=isnan(wm_0);
wm_0(Sel)=0;
Sel=isnan(wc_0);
wc_0(Sel)=0;

for i=1:length(wm_0)
    if wm_0(i)==0%if there is a gap, gf error and random error applies
        total_error_wm_EE(i)=wm_RE(i)+gf_error_wm(i);
    else %without gap, only Random error applies
        total_error_wm_EE(i)=wm_RE(i);
    end
    if wc_0(i)==0%if there is a gap, gf error and random error applies
        total_error_wc_EE(i)=wc_RE(i)+gf_error_er(i);
    else %without gap, only Random error applies
        total_error_wc_EE(i)=wc_RE(i);
    end
end
figure
plot(total_error_wc_EE)
figure
plot(decday_EE_2015,wm_gf_EE_2015,'g',decday_EE_2015,wm_gf_EE_2015-total_error_wm_EE','r',decday_EE_2015,wm_gf_EE_2015+total_error_wm_EE','k')
hold on
plot(decday_EE_2015,wm_0)
legend('wm gf','lower gf','upper gf','wm')

figure
plot(decday_EE_2015,wc_gf_EE_2015,'g',decday_EE_2015,wc_gf_EE_2015-total_error_wc_EE','r',decday_EE_2015,wc_gf_EE_2015+total_error_wc_EE','k')
hold on
plot(decday_EE_2015,wc_0)
legend('wc gf','lower gf','upper gf','wm')

wm_gf_EE_lower=wm_gf_EE_2015-total_error_wm_EE';
wm_gf_EE_upper=wm_gf_EE_2015+total_error_wm_EE';
wc_gf_EE_lower=wc_gf_EE_2015-total_error_wc_EE';
wc_gf_EE_upper=wc_gf_EE_2015+total_error_wc_EE';
%% run GPP model
xdata=[DOY_EE_2015 DOY_EE_2015 TA_gf_EE_2015 WT_gf_EE_2015...
    PAR_gf_EE_2015 LAI_gf_EE_2015 EE_gpp_ANNnight_cum_2015 decday_EE_2015...
    Season_drop_one_year];
GPP_min_EE_2015  = PEPRMT_final_sys_CO2_GPP(find_min_GPP,xdata);

figure
plot(data.decday(19297:36816),data.gpp_ANNnight(19297:36816),'.b')
hold on
plot(data.decday(19297:36816),GPP_min_EE_2015,'.r')
legend('obs','mod')
%% run Reco model
xdata=[DOY_EE_2015 DOY_EE_2015 TA_gf_EE_2015 WT_gf_EE_2015...
    PAR_gf_EE_2015 LAI_gf_EE_2015 GPP_min_EE_2015' decday_EE_2015...
    Season_drop_one_year wc_90CI_EE site_EE wetland_age_EE];
[NEE_min_EE_2015, S1_EE, S2_EE, Reco_min_EE_2015]  = PEPRMT_final_sys_CO2_Reco(find_min_Reco,xdata);

figure
plot(data.decday(19297:36816),data.er_ANNnight(19297:36816),'.b')
hold on
plot(data.decday(19297:36816),Reco_min_EE_2015,'.r')
legend('obs','mod')

figure
plot(data.decday(19297:36816),data.wc_gf(19297:36816),'.b')
hold on
plot(data.decday(19297:36816),NEE_min_EE_2015,'.r')
legend('obs','mod')

figure
plot(S1_EE,'.')
hold on
plot(S2_EE,'.')

%% convert to daily variables
%First take output from CO2 model and convert to daily averages
days = 1:1:365;%NEEDS TO BE UPDATED!!--all data

days=days';%b/c my code likes to start from 1, shift data temporarily so they start at
%1
%first do daily averages
[daily_DOY,daily_DOY_disc]=daily_ave(DOY_EE_2015,DOY_EE_2015,days);%time is 30min time scale, days is DOY
% figure
% plot(daily_DOY,daily_DOY_disc,'.')

[daily_DOY,daily_TA]=daily_ave(DOY_EE_2015,TA_gf_EE_2015,days);%time is 30min time scale, days is DOY
% figure
% plot(DOY_EE_2015,TA_gf_EE_2015,'.r')
% hold on
% plot(daily_DOY,daily_TA,'.')

[daily_DOY,daily_WT_gf]=daily_ave(DOY_EE_2015,WT_gf_EE_2015,days);%time is 30min time scale, days is DOY
% figure
% plot(DOY_EE_2015,WT_gf_EE_2015,'.')
% hold on
% plot(daily_DOY,daily_WT_gf,'.r')

[daily_DOY,daily_PAR_gf]=daily_ave(DOY_EE_2015,PAR_gf_EE_2015,days);%time is 30min time scale, days is DOY
% figure
% plot(DOY_EE_2015,PAR_gf_EE_2015,'.')
% hold on
% plot(daily_DOY,daily_PAR_gf,'.r')
[daily_DOY,daily_wetland_age_EE]=daily_ave(DOY_EE_2015,wetland_age_EE,days);%time is 30min time scale, days is DOY

daily_S1_0=S1_EE';%already in cummulative umol m-2
[daily_S1]=daily_sum(DOY_EE_2015,daily_S1_0,days);%time is 30min time scale, days is DOY
% figure
% plot(DOY_EE_2015,S1_EE,'.')
% hold on
% plot(daily_DOY,daily_S1,'.r')
% 
% test=cumsum(S1_EE);
% test(end)
% test2=cumsum(daily_S1);
% test2(end)

daily_S2_0=S2_EE';%already in cummulative umol m-2 
[daily_S2]=daily_sum(DOY_EE_2015,daily_S2_0,days);%time is 30min time scale, days is DOY
% figure
% plot(DOY_EE_2015,S2_EE,'.')
% hold on
% plot(daily_DOY,daily_S2,'.r')


%now do daily integrals
daily_gpp_min_0=GPP_min_EE_2015*-1*60*30;%first turn gpp from umol m-2 s-1 into umol m-2 30min-1
[daily_gpp_min]=daily_sum(DOY_EE_2015,daily_gpp_min_0',days);
figure
plot(daily_gpp_min,'.')

wm_gf_EE_2015=data.wm_gf(19297:36816);
daily_wm=wm_gf_EE_2015*60*30*0.001;%nmol to umol m-2 30min-1
[daily_wm_sum_umol_gf]=daily_sum(DOY_EE_2015,daily_wm,days);%time is 30min time scale, days is DOY
% figure
% plot(DOY_EE_2015,daily_wm,'.')
% hold on
% plot(daily_DOY,daily_wm_sum_umol_gf,'.r')
% 

daily_NEE_0=NEE_min_EE_2015*60*30;%first turn NEE from umol m-2 s-1 into umol m-2 30min-1
[daily_NEE_min]=daily_sum(DOY_EE_2015,daily_NEE_0',days);

daily_Reco_0=Reco_min_EE_2015*60*30;%first turn NEE from umol m-2 s-1 into umol m-2 30min-1
[daily_Reco_min]=daily_sum(DOY_EE_2015,daily_Reco_0',days);

daily_NEE_obs=data.wc_gf(19297:36816)*60*30;%first turn NEE from umol m-2 s-1 into umol m-2 30min-1
[daily_NEE_obs]=daily_sum(DOY_EE_2015,daily_NEE_obs,days);

daily_GPP_obs=data.gpp_ANNnight(19297:36816)*60*30;%first turn NEE from umol m-2 s-1 into umol m-2 30min-1
[daily_GPP_obs]=daily_sum(DOY_EE_2015,daily_GPP_obs,days);

daily_Reco_obs=data.er_ANNnight(19297:36816)*60*30;%first turn NEE from umol m-2 s-1 into umol m-2 30min-1
[daily_Reco_obs]=daily_sum(DOY_EE_2015,daily_Reco_obs,days);

figure
plot(daily_DOY,daily_NEE_min,'.',daily_DOY,daily_NEE_obs,'.')
ylabel('NEE daily sum umol m-2 d-1')
legend('mod','obs')
figure
plot(daily_DOY,daily_Reco_min,'.',daily_DOY,daily_Reco_obs,'.')
ylabel('Reco daily sum umol m-2 d-1')
legend('mod','obs')
figure
plot(daily_DOY,daily_gpp_min,'.',daily_DOY,daily_GPP_obs,'.')
ylabel('GPP daily sum umol m-2 d-1')
legend('mod','obs')

daily_gf_error_wc_EE=gf_error_er'*60*30;%first turn NWP from umol m-2 s-1 into umol m-2 30min-1
[daily_gf_error_wc_EE]=daily_sum(DOY_EE_2015,daily_gf_error_wc_EE,days);

daily_random_error_wc_EE=wc_RE'*60*30;%first turn NWP from umol m-2 s-1 into umol m-2 30min-1
[daily_random_error_wc_EE]=daily_sum(DOY_EE_2015,daily_random_error_wc_EE,days);

daily_gf_error_wm_EE=gf_error_wm'*60*30*0.001;%first turn NWP from umol m-2 s-1 into umol m-2 30min-1
[daily_gf_error_wm_EE]=daily_sum(DOY_EE_2015,daily_gf_error_wm_EE,days);

daily_random_error_wm_EE=wm_RE'*60*30*0.001;%first turn NWP from umol m-2 s-1 into umol m-2 30min-1
[daily_random_error_wm_EE]=daily_sum(DOY_EE_2015,daily_random_error_wm_EE,days);

daily_wm_gf_EE_lower=wm_gf_EE_lower*60*30*0.001;%first turn NMB from umol m-2 s-1 into umol m-2 30min-1
[daily_wm_gf_EE_lower]=daily_sum(DOY_EE_2015,daily_wm_gf_EE_lower,days);

daily_wm_gf_EE_upper=wm_gf_EE_upper*60*30*0.001;%first turn NMB from umol m-2 s-1 into umol m-2 30min-1
[daily_wm_gf_EE_upper]=daily_sum(DOY_EE_2015,daily_wm_gf_EE_upper,days);

daily_wc_gf_EE_lower=wc_gf_EE_lower*60*30;%first turn NMB from umol m-2 s-1 into umol m-2 30min-1
[daily_wc_gf_EE_lower]=daily_sum(DOY_EE_2015,daily_wc_gf_EE_lower,days);

daily_wc_gf_EE_upper=wc_gf_EE_upper*60*30;%first turn NMB from umol m-2 s-1 into umol m-2 30min-1
[daily_wc_gf_EE_upper]=daily_sum(DOY_EE_2015,daily_wc_gf_EE_upper,days);

figure
ciplot(daily_wm_gf_EE_lower,daily_wm_gf_EE_upper,daily_DOY_EE)
figure
ciplot(daily_wc_gf_EE_lower,daily_wc_gf_EE_upper,daily_DOY_EE)

figure
subplot(2,1,1)
plot(daily_DOY_EE,daily_gf_error_wc_EE,'.',daily_DOY_EE,daily_random_error_wc_EE,'.')
legend('gf','re')
subplot(2,1,2)
plot(daily_DOY_EE,daily_gf_error_wm_EE,'.',daily_DOY_EE,daily_random_error_wm_EE,'.')
legend('gf','re')



%% run CH4 model
%paramerterization data
daily_DOY=daily_DOY';
daily_DOY_disc =daily_DOY_disc';
daily_TA =daily_TA';
daily_WT_gf =daily_WT_gf';
daily_PAR_gf =daily_PAR_gf';
daily_gpp_min =daily_gpp_min';
daily_S1 =daily_S1';
daily_S2=daily_S2';
daily_wm_sum_umol_gf=daily_wm_sum_umol_gf';
daily_wetland_age_EE=daily_wetland_age_EE';

xdata=[daily_DOY daily_DOY_disc daily_TA daily_WT_gf daily_PAR_gf daily_gpp_min daily_S1 daily_S2 daily_wetland_age_EE];
ch4_EE_2015 = PEPRMT_final_sys_CH4(find_min_CH4,xdata);
figure
subplot(2,1,1)
plot(daily_DOY,ch4_EE_2015,'.',daily_DOY,daily_wm_sum_umol_gf,'.')
ylabel('CH4 daily sum umol m-2 d-1')
legend('mod','obs')
subplot(2,1,2)
plot(daily_DOY,daily_WT_gf,'.')
figure
plot(gccsmooth_2015,'g')

%prep data for running though CH4 run
daily_DOY_EE=daily_DOY;
daily_DOY_disc_EE=daily_DOY_disc ;
daily_TA_EE =daily_TA;
daily_WT_gf_EE=daily_WT_gf;
daily_PAR_gf_EE =daily_PAR_gf;
daily_gpp_min_EE=daily_gpp_min;
daily_S1_EE=daily_S1;
daily_S2_EE=daily_S2;
daily_wm_sum_umol_gf_EE=daily_wm_sum_umol_gf;

xdata_EE=[daily_DOY_EE daily_DOY_disc_EE daily_TA_EE daily_WT_gf_EE daily_PAR_gf_EE daily_gpp_min_EE daily_S1_EE daily_S2_EE daily_wetland_age_EE];

%% annual sums
cum_obs_valid=cumsum(daily_wm_sum_umol_gf);%umol m-2 yr-1
cum_obs_valid_sum=cum_obs_valid(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_valid_sum=cum_obs_valid_sum*16.04*0.75;%g C-Ch4 m-2 yr-1

cum_mod_valid=cumsum(ch4_EE_2015);%nmol m-2 yr-1
cum_mod_valid_sum=cum_mod_valid(end)*10^-6;%mol m-2 yr-1
cum_mod_valid_sum=cum_mod_valid_sum*16.04*0.75;%g C-Ch4 m-2 yr-1

obs_dailysum_NEE=daily_NEE_obs*60*30;%convert from nmol m-2 s-1 to nmol m-2 day-1
cum_obs_valid_NEE=cumsum(obs_dailysum_NEE);%nmol m-2 yr-1
cum_obs_valid_sum_NEE=cum_obs_valid_NEE(end)*10^-6;%mol m-2 yr-1
cum_obs_valid_sum_NEE=cum_obs_valid_sum_NEE*44.01*0.27;%g C-CO2 m-2 yr-1

mod_dailysum_NEE=daily_NEE_min*60*30;%convert from nmol m-2 s-1 to nmol m-2 day-1
cum_mod_valid_NEE=cumsum(mod_dailysum_NEE);%nmol m-2 yr-1
cum_mod_valid_sum_NEE=cum_mod_valid_NEE(end)*10^-6;%mol m-2 yr-1
cum_mod_valid_sum_NEE=cum_mod_valid_sum_NEE*44.01*0.27;%g C-CO2 m-2 yr-1

obs_dailysum_Reco=daily_Reco_obs*60*30;%convert from nmol m-2 s-1 to nmol m-2 day-1
cum_obs_valid_Reco=cumsum(obs_dailysum_Reco);%nmol m-2 yr-1
cum_obs_valid_sum_Reco=cum_obs_valid_Reco(end)*10^-6;%mol m-2 yr-1
cum_obs_valid_sum_Reco=cum_obs_valid_sum_Reco*44.01*0.27;%g C-CO2 m-2 yr-1

mod_dailysum_Reco=daily_Reco_min*60*30;%convert from nmol m-2 s-1 to nmol m-2 day-1
cum_mod_valid_Reco=cumsum(mod_dailysum_Reco);%nmol m-2 yr-1
cum_mod_valid_sum_Reco=cum_mod_valid_Reco(end)*10^-6;%mol m-2 yr-1
cum_mod_valid_sum_Reco=cum_mod_valid_sum_Reco*44.01*0.27;%g C-CO2 m-2 yr-1

obs_dailysum_GPP=daily_GPP_obs*60*30;%convert from nmol m-2 s-1 to nmol m-2 day-1
cum_obs_valid_GPP=cumsum(obs_dailysum_GPP);%nmol m-2 yr-1
cum_obs_valid_sum_GPP=cum_obs_valid_GPP(end)*10^-6;%mol m-2 yr-1
cum_obs_valid_sum_GPP=cum_obs_valid_sum_GPP*44.01*0.27;%g C-CO2 m-2 yr-1

mod_dailysum_GPP=daily_gpp_min*60*30;%convert from nmol m-2 s-1 to nmol m-2 day-1
cum_mod_valid_GPP=cumsum(mod_dailysum_GPP);%nmol m-2 yr-1
cum_mod_valid_sum_GPP=cum_mod_valid_GPP(end)*10^-6;%mol m-2 yr-1
cum_mod_valid_sum_GPP=cum_mod_valid_sum_GPP*44.01*0.27;%g C-CO2 m-2 yr-1








