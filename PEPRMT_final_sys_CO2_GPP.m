function  [GPP] = PEPRMT_final_sys_CO2_GPP(theta,xdata)
%[NEE_mod, S1, S2, GPP] 
%Constants

Time_2 =xdata(:,1);
DOY_disc_2=xdata(:,2);
%Exogenous Variables
TA_2 = xdata(:,3);
%WT_2 = xdata(:,4);%water table height cm
PAR_2 = xdata(:,5);
LAI_2 = xdata(:,6);
GPP_2 = xdata(:,7);%gpp ann night--umol m-2 30min-1
%decday_2=xdata(:,8);
%Season_drop_2=xdata(:,9);
%wc_90CI_2=xdata(:,10);
%site_2=xdata(:,11);
%wetland_age_2=xdata(:,12);

%WT_2_adj=(WT_2/100)+1;%when wt=1 wt at soil surface
% figure
% plot(WT_2_adj)

%FIRST COMPUTE GPP%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%need a springtime inhibition 
%First, divide year into seasons, b/c model uses different parameter values
%depending on time of year
Season=zeros(1,length(GPP_2))';
Sel=DOY_disc_2<=88;%115
Season(Sel)=1;%WINTER
Sel=DOY_disc_2<=175&DOY_disc_2>88;
Season(Sel)=2;%Pre-SPRING--inhibit by 0.55
Sel=DOY_disc_2<=205&DOY_disc_2>175;
Season(Sel)=3;%SPRING--inhibit by 0.75
Sel=DOY_disc_2<=265&DOY_disc_2>205;
Season(Sel)=4;%SUMMER--inhibit none
Sel=DOY_disc_2<=335&DOY_disc_2>265;
Season(Sel)=5;%FALL--inhibit by 0.73
Sel=DOY_disc_2<=365&DOY_disc_2>335;
Season(Sel)=1;%WINTER AGAIN
% figure
% plot(decday,Season)
% figure
% plot(decday,DOY_disc)


GI_fix=zeros(1,length(Time_2));
%     figure
%     plotyy(Time_2,GPP_2,Time_2,Season)
for t = 1:length(Time_2);
    if Season(t)>1&&Season(t)<3 %it's pre-spring, ihhibit
        GI_fix(t)=0.6;%0.5
    else
         GI_fix(t)=1;
    end
    if Season(t)>2&&Season(t)<4%it's spring, inhibit
        GI_fix(t)=0.8;
    end
     if Season(t)>4
         GI_fix(t)=0.73;
     end
end

%    figure
%    plot(decday,GI_fix)
%PARAMETERS
k=theta(1)+0.8;%0.8 range=0-1
Ha =theta(2)+30;%30;%activation energy for general crop plant
Hd=theta(3)+100;%100;
%age_param=theta(4);

%CONSTANTS
LUE_mean=0.9;%computed a mean across each growing season (g C m-2 MJ-1)
%Running 2000-closed shrubland epsilon=0.9; crop=0.6; deci broadleaf forest=1.0
vcopt = 1.0;%1.05
R_t=0.00831;
T_opt = 25 + 274.15;%our Temp opt for Ps is 25C

%EQUATIONS
PAR_2_MJ=(PAR_2*0.0002186)*0.001;%convert PAR umol m-2 s-1 to MJ m-2 s-1
fPAR_2=0.95*(1-exp(-k*LAI_2));%for an LAI=4.9, fpar=0.87--Yuan 2007
%b/c we have so much dead veg at WP that is not included in LAI, our k is
%high b/c not much light reaches the floor (k=0.8)
APAR_2=fPAR_2.*PAR_2_MJ;%MJ m-2

AirT_K =TA_2+ 274.15;%C to Kelvin
AirT_K=AirT_K';

max_time = length(TA_2);
vct=zeros(1,length(Time_2));
NPP_FPAR_T=zeros(1,length(Time_2));
%wetland_age_corr=zeros(1,length(Time_2));

for t = 1:max_time
%wetland_age_corr(t)=age_param/wetland_age_2(t)*wetland_age_2(t);

exponent1=(Ha*(AirT_K(t)-T_opt))./(AirT_K(t)*R_t*T_opt);
exponent2=(Hd*(AirT_K(t)-T_opt))./(AirT_K(t)*R_t*T_opt);
top = Hd*exp(exponent1);   
bottom=Hd-(Ha*(1-exp(exponent2)));
vct(t) = vcopt*(top./bottom);
  if Season(t)<2%fPAR_2(t)<0.43%winter threshold, when GI drops to winter levels turn off Ps
      vct(t)=0.15;
  end
NPP_FPAR_T(t)=(GI_fix(t)*(vct(t)*(APAR_2(t)*LUE_mean)));%g C m-2 s-1
end
GPP=(NPP_FPAR_T/12)*10^6*-1;%go back to umol m-2 s-1
%  NPP_FPAR_T_test=NPP_FPAR_T_test/12*10^6;
%  figure
%  plot(Time_2,NPP_FPAR_T_test,'.',Time_2,GPP_2/60/30,'.')
%  legend('without gi fix','with fix')
    
end














