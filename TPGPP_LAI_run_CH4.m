%use for running DAMMCH4 example

%
%% 

model.ssfun = @TPGPP_LAI_final_ss_CH4;
model.modelfun = @TPGPP_LAI_final_sys_CH4;

xdata=[daily_DOY_param daily_DOY_disc_param daily_TA_param daily_WT_gf_param daily_PAR_gf_param daily_gpp_min_param daily_wetland_age_param daily_wetland_age_param daily_wetland_age_param];
ydata=[daily_DOY_param daily_wm_sum_umol_gf_param daily_gapfilling_error_wm_param daily_random_error_wm_param];%this has CH4 umol m-2 d-1
theta=[0 0 1 0 1];
theta=theta';
mod_ch4 = TPGPP_LAI_final_sys_CH4(theta,xdata);
 figure
 plot(mod_ch4,'.')
 hold on
 plot(daily_wm_sum_umol_gf_param,'.r')

%% START MDF
data.xdata =xdata ;
data.ydata =ydata;

Rsum_obs = data.ydata(:,2);
nan_obs = sum(isnan(Rsum_obs));
n_obs = length(Rsum_obs) - nan_obs;
model.N     = n_obs;%total number of non nan obs
%model.S20 = [1 1 2];%has to do with error variance of prior
%model.N0  = [4 4 4];%has to do with error variance of prior


params = {
    {'m_ref',        0,     -0.15,        1.3}
    {'Eo',           0,     -490,        500}
    {'k2',           1,     1e-12,        1}
    {'M_ea3',        0,     -10,         10}
    {'M_km3',        1,     1e-3,        1e3}
%     name      start/theta/par  lBound   uBound    mean_prior   std_prior
    };

options.method = 'dram';
options.verbosity  = 0;%level of info printed
options.burnintime = 50000;% burn in before adaptation starts

%First generate an initial chain
options.nsimu = 50000;
[results, chain, s2chain, sschain]= mcmcrun_FINAL(model,data,params,options);

%find the minimum
figure
plot(sschain,'.')%sum-of-squares chains
nanmin(sschain)
Sel=sschain<nanmin(sschain)+0.00001;%442
figure
plot(Sel)
% cumsum(Sel)
find_min=chain(Sel,:);
find_min=find_min(end,:)';

mod_ch4 = TPGPP_LAI_final_sys_CH4(find_min,xdata);
 figure
 plot(mod_ch4,'.')
 hold on
 plot(daily_wm_sum_umol_gf_param,'.r')

%Then re-run starting from the results of the previous run, this will take about 10 minutes.
params = {
    {'m_ref',        find_min(1),     -0.15,        1.3}
    {'Eo',           find_min(2),     -490,        500}
    {'k2',           find_min(3),     1e-12,        1}
    {'M_ea3',        find_min(4),     -10,         10}
    {'M_km3',        find_min(5),     1e-3,        1e3}
%     name      start/theta/par  lBound   uBound    mean_prior   std_prior
    };
 
options.method = 'dram';
options.verbosity  = 0;%level of info printed
options.burnintime = 0;% burn in before adaptation starts
 
options.nsimu = 150000;
[results, chain, s2chain, sschain] = mcmcrun_FINAL(model,data,params,options, results);
 
figure
mcmcplot(chain,[],results,'chainpanel');%plots chain for ea parameter and all the values it accepted
figure
mcmcplot(chain,[],results,'pairs');%tells you if parameters are correlated
%  figure
%  mcmcplot(sqrt(s2chain),[],[],'hist')%take square root of the s2chain to get the chain for error standard deviation
% title('Error std posterior')
figure
mcmcplot(chain,[],results,'denspanel',2);
 
 
chainstats(chain,results)
sschain_long=sschain;%save before you move on
chain_long=chain;
results_long=results;

%find the minimum
figure
plot(sschain_long,'.')%sum-of-squares chains
nanmin(sschain_long)
Sel=sschain_long<nanmin(sschain_long)+0.00001;%442
figure
plot(Sel)
% cumsum(Sel)
find_min=chain_long(Sel,:);
find_min=find_min(end,:)';

%% Choose posterior

%First establish if chain stabilized
chainstats(chain,results)
%Geweke test should be high ideally >0.9 for all parameters

%rejection rate of chain needs to be less than 50%
results_long.rejected*100

%Determine where means and sd of parameters stabilize in the chain
%this tells you what part of chain is stable for selection of posterior

%see where means and sd of parameters stabilize in the chain
%this tells you what part of chain is stable for selection of posterior
window_size = 5000;
simple = tsmovavg(chain_long(:,1),'s',window_size,1);
simple_std = movingstd(chain_long(:,1),5000);
figure
subplot(2,1,1)
plot(simple,'.')
title('5000 iteration moving average Mref SOM')
subplot(2,1,2)
plot(simple_std,'.')
title('5000 iteration moving SD Mref SOM')

window_size = 5000;
simple = tsmovavg(chain_long(:,2),'s',window_size,1);
simple_std = movingstd(chain_long(:,2),5000);
figure
subplot(2,1,1)
plot(simple,'.')
title('5000 iteration moving average Eo SOM')
subplot(2,1,2)
plot(simple_std,'.')
title('5000 iteration moving SD Eo SOM')

window_size = 5000;
simple = tsmovavg(chain_long(:,3),'s',window_size,1);
simple_std = movingstd(chain_long(:,3),5000);
figure
subplot(2,1,1)
plot(simple,'.')
title('5000 iteration moving average k2')
subplot(2,1,2)
plot(simple_std,'.')
title('5000 iteration moving SD k2')

window_size = 5000;
simple = tsmovavg(chain_long(:,4),'s',window_size,1);
simple_std = movingstd(chain_long(:,4),5000);
figure
subplot(2,1,1)
plot(simple,'.')
title('5000 iteration moving average EA oxi')
subplot(2,1,2)
plot(simple_std,'.')
title('5000 iteration moving SD EA oxi')


window_size = 5000;
simple = tsmovavg(chain_long(:,5),'s',window_size,1);
simple_std = movingstd(chain_long(:,5),5000);
figure
subplot(2,1,1)
plot(simple,'.')
title('5000 iteration moving average km oxi')
subplot(2,1,2)
plot(simple_std,'.')
title('5000 iteration moving SD km oxi')

%identified visually that chain is good around 100,000
%choose parammeters 10,000 parameter sets from that area
%e.g. 100,000-101,000

find_min_10000=chain(100001:110000,:);%update everytime
%now select 1000 unique parameter sets from these 10,000, that are
%distributed across the range
figure
hist(find_min_10000(:,1))
%bin data and sample evenly across bins


%run 10000 parameter sets through all data+ and test chi square
xdata=[daily_DOY_param daily_DOY_disc_param daily_TA_param daily_WT_gf_param daily_PAR_gf_param daily_gpp_min_param daily_wetland_age_param daily_wetland_age_param daily_wetland_age_param];
ydata=[daily_DOY_param daily_wm_sum_umol_gf_param daily_gapfilling_error_wm_param daily_random_error_wm_param];%this has CH4 umol m-2 d-1

%make 1000 theta sets that will be used to estimate CI 
model_pop_ch4=zeros(length(1:10000));%length of validation data
model_pop_ch4=model_pop_ch4(1:2363,:);%update as needed 
 
for i=1:length(find_min_10000)
   ch4mod = TPGPP_LAI_final_sys_CH4(find_min_10000(i,:)',xdata);
    model_pop_ch4(:,i)=ch4mod;
    
end

model_std_ch4=std(model_pop_ch4');


%% according to Zobitz 2008, model mean==model min(best para set)
CH4_mod_min_param = TPGPP_LAI_final_sys_CH4(find_min,xdata);

figure
plot(daily_DOY_param,daily_wm_sum_umol_gf_param,'.',daily_DOY_param,CH4_mod_min_param,'.')
legend('obs','modmin')

%Determine actual sample size for posterior
figure
subplot(4,1,1)
hist(find_min_10000(:,1),25)
subplot(4,1,2)
hist(find_min_10000(:,1),50)
subplot(4,1,3)
hist(find_min_10000(:,1),100)
subplot(4,1,4)
hist(find_min_10000(:,1),500)


% 90% confidence intervals
CH4_mod_min_param_lower=CH4_mod_min_param-(1.645*(model_std_ch4));
CH4_mod_min_param_upper=CH4_mod_min_param+(1.645*(model_std_ch4));
%      lower_ch4=quantile(model_pop_ch4',0.05);% 90% quantile
%     upper_ch4=quantile(model_pop_ch4',0.95);% 90% quantile

figure
plot(CH4_mod_min_param_lower)
hold on
plot(CH4_mod_min_param_upper)
hold on
plot(CH4_mod_min_param,'r')

%% Run validation data MB WP
%run 10000 parameter sets through all data+ and test chi square
xdata_valid=[daily_DOY_valid daily_DOY_disc_valid daily_TA_valid daily_WT_gf_valid daily_PAR_gf_valid daily_gpp_min_valid daily_wetland_age_valid daily_wetland_age_valid daily_wetland_age_valid];
ydata_valid=[daily_DOY_valid daily_wm_sum_umol_gf_valid daily_gapfilling_error_wm_valid daily_random_error_wm_valid];%this has CH4 umol m-2 d-1

%make 1000 theta sets that will be used to estimate CI 
model_pop_ch4=zeros(length(1:10000));%length of validation data
model_pop_ch4=model_pop_ch4(1:730,:);%update as needed 
 
for i=1:length(find_min_10000)
   ch4mod = TPGPP_LAI_final_sys_CH4(find_min_10000(i,:)',xdata_valid);
    model_pop_ch4(:,i)=ch4mod;
    
end

model_std_ch4_valid=std(model_pop_ch4');


%% according to Zobitz 2008, model mean==model min(best para set)
CH4_mod_min_valid = TPGPP_LAI_final_sys_CH4(find_min,xdata_valid);

figure
plot(daily_DOY_valid,daily_wm_sum_umol_gf_valid,'.',daily_DOY_valid,CH4_mod_min_valid,'.')
legend('obs','modmin')

% 90% confidence intervals
%CH4_mod_min=CH4_mod_initial;
CH4_mod_min_valid_lower=CH4_mod_min_valid-(1.645*(model_std_ch4_valid));
CH4_mod_min_valid_upper=CH4_mod_min_valid+(1.645*(model_std_ch4_valid));
%      lower_ch4=quantile(model_pop_ch4',0.05);% 90% quantile
%     upper_ch4=quantile(model_pop_ch4',0.95);% 90% quantile

figure
plot(CH4_mod_min_valid_lower)
hold on
plot(CH4_mod_min_valid_upper)
hold on
plot(CH4_mod_min_valid,'r')

%CAIC
p=5;
residual_SS_MB=sum((CH4_mod_min_valid(1:365)-daily_wm_sum_umol_gf_valid(1:365)').^2);
CAIC_valid_MB=-2*log(residual_SS_MB) + p*(log(365)+1);
residual_SS_WP=sum((CH4_mod_min_valid(366:end)-daily_wm_sum_umol_gf_valid(366:end)').^2);
CAIC_valid_WP=-2*log(residual_SS_WP) + p*(log(365)+1);

%% run EE data through
xdata_EE=[daily_DOY_EE daily_DOY_disc_EE daily_TA_EE daily_WT_gf_EE daily_PAR_gf_EE...
    daily_gpp_min_EE daily_wetland_age_EE daily_wetland_age_EE daily_wetland_age_EE];

model_pop_ch4_EE=zeros(length(1:10000));%length of validation data
model_pop_ch4_EE=model_pop_ch4_EE(1:365,:);%update as needed 
 
for i=1:length(find_min_10000)
   ch4mod = TPGPP_LAI_final_sys_CH4(find_min_10000(i,:)',xdata_EE);
    model_pop_ch4_EE(:,i)=ch4mod';
    
end

model_std_ch4_EE=std(model_pop_ch4_EE');



%% according to Zobitz 2008, model mean==model min(best para set)
ch4_min_EE = TPGPP_LAI_final_sys_CH4(find_min,xdata_EE);

figure
plot(daily_DOY_EE,daily_wm_sum_umol_gf_EE,'.',daily_DOY_EE,ch4_min_EE,'.')
legend('obs','mod min')
 
figure
plot(model_std_ch4_EE,'.')

% 90% confidence intervals (if you want 95% use 1.96)
%b/c the 10,000 parameters are not independent or may even be redundant, I
%reduce the sample size to 1000
lower_ch4_EE=ch4_min_EE-(1.645*(model_std_ch4_EE));%use 1.645 for 90% CI
upper_ch4_EE=ch4_min_EE+(1.645*(model_std_ch4_EE));
%     lower_ch4_EE=quantile(model_pop_ch4_EE',0.05);% 90% quantile
%     upper_ch4_EE=quantile(model_pop_ch4_EE',0.95);% 90% quantile

figure
plot(lower_ch4_EE,'.')
hold on
plot(upper_ch4_EE,'.')
hold on
plot(ch4_min_EE,'.r')

%CAIC
p=5;
residual_SS_EE=sum((ch4_min_EE(1:365)-daily_wm_sum_umol_gf_EE(1:365)').^2);
CAIC_valid_EE=-2*log(residual_SS_EE) + p*(log(365)+1);


%% annual sums
%Validation MB stats

cum_mod_valid_MB=cumsum(CH4_mod_min_valid(1:365));%nmol m-2 yr-1
cum_mod_valid_sum_MB=cum_mod_valid_MB(end)*10^-6;%mol m-2 yr-1
cum_mod_valid_sum_MB=cum_mod_valid_sum_MB*16.04*0.75;%g C-Ch4 m-2 yr-1

cum_mod_valid_lower_MB=cumsum(CH4_mod_min_valid_lower(1:365));%nmol m-2 yr-1
cum_mod_valid_sum_lower_MB=cum_mod_valid_lower_MB(end)*10^-6;%mol m-2 yr-1
cum_mod_valid_sum_lower_MB=cum_mod_valid_sum_lower_MB*16.04*0.75;%g C-Ch4 m-2 yr-1
 
cum_mod_max_valid_upper_MB=cumsum(CH4_mod_min_valid_upper(1:365));%nmol m-2 yr-1
cum_mod_valid_sum_upper_MB=cum_mod_max_valid_upper_MB(end)*10^-6;%mol m-2 yr-1
cum_mod_valid_sum_upper_MB=cum_mod_valid_sum_upper_MB*16.04*0.75;%g C-Ch4 m-2 yr-1

(cum_mod_valid_sum_upper_MB-cum_mod_valid_sum_MB)*10

%% Validation WP 
cum_mod_valid_WP=cumsum(CH4_mod_min_valid(366:end));%nmol m-2 yr-1
cum_mod_valid_sum_WP=cum_mod_valid_WP(end)*10^-6;%mol m-2 yr-1
cum_mod_valid_sum_WP=cum_mod_valid_sum_WP*16.04*0.75;%g C-Ch4 m-2 yr-1

cum_mod_valid_lower_WP=cumsum(CH4_mod_min_valid_lower(366:end));%nmol m-2 yr-1
cum_mod_valid_sum_lower_WP=cum_mod_valid_lower_WP(end)*10^-6;%mol m-2 yr-1
cum_mod_valid_sum_lower_WP=cum_mod_valid_sum_lower_WP*16.04*0.75;%g C-Ch4 m-2 yr-1
 
cum_mod_max_valid_upper_WP=cumsum(CH4_mod_min_valid_upper(366:end));%nmol m-2 yr-1
cum_mod_valid_sum_upper_WP=cum_mod_max_valid_upper_WP(end)*10^-6;%mol m-2 yr-1
cum_mod_valid_sum_upper_WP=cum_mod_valid_sum_upper_WP*16.04*0.75;%g C-Ch4 m-2 yr-1

(cum_mod_valid_sum_upper_WP-cum_mod_valid_sum_WP)*10


%% Validation EE 

cum_mod_valid_EE=cumsum(ch4_min_EE);%nmol m-2 yr-1
cum_mod_valid_sum_EE=cum_mod_valid_EE(end)*10^-6;%mol m-2 yr-1
cum_mod_valid_sum_EE=cum_mod_valid_sum_EE*16.04*0.75;%g C-Ch4 m-2 yr-1

cum_mod_valid_lower_EE=cumsum(lower_ch4_EE);%nmol m-2 yr-1
cum_mod_valid_sum_lower_EE=cum_mod_valid_lower_EE(end)*10^-6;%mol m-2 yr-1
cum_mod_valid_sum_lower_EE=cum_mod_valid_sum_lower_EE*16.04*0.75;%g C-Ch4 m-2 yr-1
 
cum_mod_max_valid_upper_EE=cumsum(upper_ch4_EE);%nmol m-2 yr-1
cum_mod_valid_sum_upper_EE=cum_mod_max_valid_upper_EE(end)*10^-6;%mol m-2 yr-1
cum_mod_valid_sum_upper_EE=cum_mod_valid_sum_upper_EE*16.04*0.75;%g C-Ch4 m-2 yr-1

(cum_mod_valid_sum_upper_EE-cum_mod_valid_sum_EE)*10


%% Param stats MB 
%ALL years

cum_mod_param_MB=cumsum(CH4_mod_min_param(1:1461));%nmol m-2 yr-1
cum_mod_param_sum_MB=cum_mod_param_MB(end)*10^-6;%mol m-2 yr-1
cum_mod_param_sum_MB=cum_mod_param_sum_MB*16.04*0.75;%g C-Ch4 m-2 yr-1

cum_mod_param_lower_MB=cumsum(CH4_mod_min_param_lower(1:1461));%nmol m-2 yr-1
cum_mod_param_sum_lower_MB=cum_mod_param_lower_MB(end)*10^-6;%mol m-2 yr-1
cum_mod_param_sum_lower_MB=cum_mod_param_sum_lower_MB*16.04*0.75;%g C-Ch4 m-2 yr-1
 
cum_mod_max_param_upper_MB=cumsum(CH4_mod_min_param_upper(1:1461));%nmol m-2 yr-1
cum_mod_param_sum_upper_MB=cum_mod_max_param_upper_MB(end)*10^-6;%mol m-2 yr-1
cum_mod_param_sum_upper_MB=cum_mod_param_sum_upper_MB*16.04*0.75;%g C-Ch4 m-2 yr-1

(cum_mod_param_sum_upper_MB-cum_mod_param_sum_MB)*10

%By year
%2011


cum_mod_param_MB=cumsum(CH4_mod_min_param(1:365));%nmol m-2 yr-1
cum_mod_param_sum_MB=cum_mod_param_MB(end)*10^-6;%mol m-2 yr-1
cum_mod_param_sum_MB=cum_mod_param_sum_MB*16.04*0.75;%g C-Ch4 m-2 yr-1

cum_mod_param_lower_MB=cumsum(CH4_mod_min_param_lower(1:365));%nmol m-2 yr-1
cum_mod_param_sum_lower_MB=cum_mod_param_lower_MB(end)*10^-6;%mol m-2 yr-1
cum_mod_param_sum_lower_MB=cum_mod_param_sum_lower_MB*16.04*0.75;%g C-Ch4 m-2 yr-1
 
cum_mod_max_param_upper_MB=cumsum(CH4_mod_min_param_upper(1:365));%nmol m-2 yr-1
cum_mod_param_sum_upper_MB=cum_mod_max_param_upper_MB(end)*10^-6;%mol m-2 yr-1
cum_mod_param_sum_upper_MB=cum_mod_param_sum_upper_MB*16.04*0.75;%g C-Ch4 m-2 yr-1

(cum_mod_param_sum_upper_MB-cum_mod_param_sum_MB)*10

%2012

cum_mod_param_MB=cumsum(CH4_mod_min_param(366:731));%nmol m-2 yr-1
cum_mod_param_sum_MB=cum_mod_param_MB(end)*10^-6;%mol m-2 yr-1
cum_mod_param_sum_MB=cum_mod_param_sum_MB*16.04*0.75;%g C-Ch4 m-2 yr-1

cum_mod_param_lower_MB=cumsum(CH4_mod_min_param_lower(366:731));%nmol m-2 yr-1
cum_mod_param_sum_lower_MB=cum_mod_param_lower_MB(end)*10^-6;%mol m-2 yr-1
cum_mod_param_sum_lower_MB=cum_mod_param_sum_lower_MB*16.04*0.75;%g C-Ch4 m-2 yr-1
 
cum_mod_max_param_upper_MB=cumsum(CH4_mod_min_param_upper(366:731));%nmol m-2 yr-1
cum_mod_param_sum_upper_MB=cum_mod_max_param_upper_MB(end)*10^-6;%mol m-2 yr-1
cum_mod_param_sum_upper_MB=cum_mod_param_sum_upper_MB*16.04*0.75;%g C-Ch4 m-2 yr-1

(cum_mod_param_sum_upper_MB-cum_mod_param_sum_MB)*10

%2013
cum_mod_param_MB=cumsum(CH4_mod_min_param(732:1096));%nmol m-2 yr-1
cum_mod_param_sum_MB=cum_mod_param_MB(end)*10^-6;%mol m-2 yr-1
cum_mod_param_sum_MB=cum_mod_param_sum_MB*16.04*0.75;%g C-Ch4 m-2 yr-1

cum_mod_param_lower_MB=cumsum(CH4_mod_min_param_lower(732:1096));%nmol m-2 yr-1
cum_mod_param_sum_lower_MB=cum_mod_param_lower_MB(end)*10^-6;%mol m-2 yr-1
cum_mod_param_sum_lower_MB=cum_mod_param_sum_lower_MB*16.04*0.75;%g C-Ch4 m-2 yr-1
 
cum_mod_max_param_upper_MB=cumsum(CH4_mod_min_param_upper(732:1096));%nmol m-2 yr-1
cum_mod_param_sum_upper_MB=cum_mod_max_param_upper_MB(end)*10^-6;%mol m-2 yr-1
cum_mod_param_sum_upper_MB=cum_mod_param_sum_upper_MB*16.04*0.75;%g C-Ch4 m-2 yr-1

(cum_mod_param_sum_upper_MB-cum_mod_param_sum_MB)*10

%2014

cum_mod_param_MB=cumsum(CH4_mod_min_param(1097:1461));%nmol m-2 yr-1
cum_mod_param_sum_MB=cum_mod_param_MB(end)*10^-6;%mol m-2 yr-1
cum_mod_param_sum_MB=cum_mod_param_sum_MB*16.04*0.75;%g C-Ch4 m-2 yr-1

cum_mod_param_lower_MB=cumsum(CH4_mod_min_param_lower(1097:1461));%nmol m-2 yr-1
cum_mod_param_sum_lower_MB=cum_mod_param_lower_MB(end)*10^-6;%mol m-2 yr-1
cum_mod_param_sum_lower_MB=cum_mod_param_sum_lower_MB*16.04*0.75;%g C-Ch4 m-2 yr-1
 
cum_mod_max_param_upper_MB=cumsum(CH4_mod_min_param_upper(1097:1461));%nmol m-2 yr-1
cum_mod_param_sum_upper_MB=cum_mod_max_param_upper_MB(end)*10^-6;%mol m-2 yr-1
cum_mod_param_sum_upper_MB=cum_mod_param_sum_upper_MB*16.04*0.75;%g C-Ch4 m-2 yr-1

(cum_mod_param_sum_upper_MB-cum_mod_param_sum_MB)*10

%% PAram WP 
%ALL YEARS

cum_mod_param_WP=cumsum(CH4_mod_min_param(1462:end));%nmol m-2 yr-1
cum_mod_param_sum_WP=cum_mod_param_WP(end)*10^-6;%mol m-2 yr-1
cum_mod_param_sum_WP=cum_mod_param_sum_WP*16.04*0.75;%g C-Ch4 m-2 yr-1

cum_mod_param_lower_WP=cumsum(CH4_mod_min_param_lower(1462:end));%nmol m-2 yr-1
cum_mod_param_sum_lower_WP=cum_mod_param_lower_WP(end)*10^-6;%mol m-2 yr-1
cum_mod_param_sum_lower_WP=cum_mod_param_sum_lower_WP*16.04*0.75;%g C-Ch4 m-2 yr-1
 
cum_mod_max_param_upper_WP=cumsum(CH4_mod_min_param_upper(1462:end));%nmol m-2 yr-1
cum_mod_param_sum_upper_WP=cum_mod_max_param_upper_WP(end)*10^-6;%mol m-2 yr-1
cum_mod_param_sum_upper_WP=cum_mod_param_sum_upper_WP*16.04*0.75;%g C-Ch4 m-2 yr-1

(cum_mod_param_sum_upper_WP-cum_mod_param_sum_WP)*10
 
%By year
%2012**NOT A FULL YEAR

cum_mod_param_WP=cumsum(CH4_mod_min_param(1462:1633));%nmol m-2 yr-1
cum_mod_param_sum_WP=cum_mod_param_WP(end)*10^-6;%mol m-2 yr-1
cum_mod_param_sum_WP=cum_mod_param_sum_WP*16.04*0.75;%g C-Ch4 m-2 yr-1

cum_mod_param_lower_WP=cumsum(CH4_mod_min_param_lower(1462:1633));%nmol m-2 yr-1
cum_mod_param_sum_lower_WP=cum_mod_param_lower_WP(end)*10^-6;%mol m-2 yr-1
cum_mod_param_sum_lower_WP=cum_mod_param_sum_lower_WP*16.04*0.75;%g C-Ch4 m-2 yr-1
 
cum_mod_max_param_upper_WP=cumsum(CH4_mod_min_param_upper(1462:1633));%nmol m-2 yr-1
cum_mod_param_sum_upper_WP=cum_mod_max_param_upper_WP(end)*10^-6;%mol m-2 yr-1
cum_mod_param_sum_upper_WP=cum_mod_param_sum_upper_WP*16.04*0.75;%g C-Ch4 m-2 yr-1

(cum_mod_param_sum_upper_WP-cum_mod_param_sum_WP)*10

%2013

cum_mod_param_WP=cumsum(CH4_mod_min_param(1634:1998));%nmol m-2 yr-1
cum_mod_param_sum_WP=cum_mod_param_WP(end)*10^-6;%mol m-2 yr-1
cum_mod_param_sum_WP=cum_mod_param_sum_WP*16.04*0.75;%g C-Ch4 m-2 yr-1

cum_mod_param_lower_WP=cumsum(CH4_mod_min_param_lower(1634:1998));%nmol m-2 yr-1
cum_mod_param_sum_lower_WP=cum_mod_param_lower_WP(end)*10^-6;%mol m-2 yr-1
cum_mod_param_sum_lower_WP=cum_mod_param_sum_lower_WP*16.04*0.75;%g C-Ch4 m-2 yr-1
 
cum_mod_max_param_upper_WP=cumsum(CH4_mod_min_param_upper(1634:1998));%nmol m-2 yr-1
cum_mod_param_sum_upper_WP=cum_mod_max_param_upper_WP(end)*10^-6;%mol m-2 yr-1
cum_mod_param_sum_upper_WP=cum_mod_param_sum_upper_WP*16.04*0.75;%g C-Ch4 m-2 yr-1

(cum_mod_param_sum_upper_WP-cum_mod_param_sum_WP)*10

%2014

cum_mod_param_WP=cumsum(CH4_mod_min_param(1999:end));%nmol m-2 yr-1
cum_mod_param_sum_WP=cum_mod_param_WP(end)*10^-6;%mol m-2 yr-1
cum_mod_param_sum_WP=cum_mod_param_sum_WP*16.04*0.75;%g C-Ch4 m-2 yr-1

cum_mod_param_lower_WP=cumsum(CH4_mod_min_param_lower(1999:end));%nmol m-2 yr-1
cum_mod_param_sum_lower_WP=cum_mod_param_lower_WP(end)*10^-6;%mol m-2 yr-1
cum_mod_param_sum_lower_WP=cum_mod_param_sum_lower_WP*16.04*0.75;%g C-Ch4 m-2 yr-1
 
cum_mod_max_param_upper_WP=cumsum(CH4_mod_min_param_upper(1999:end));%nmol m-2 yr-1
cum_mod_param_sum_upper_WP=cum_mod_max_param_upper_WP(end)*10^-6;%mol m-2 yr-1
cum_mod_param_sum_upper_WP=cum_mod_param_sum_upper_WP*16.04*0.75;%g C-Ch4 m-2 yr-1

(cum_mod_param_sum_upper_WP-cum_mod_param_sum_WP)*10


%% Statistics Updated 10/10/2016
%RMSE is a better test of model performance than r2
%convert units before computing stats
CH4_mod_min_param_gC_m2_day=CH4_mod_min_param*10^-6*16.04*0.75;
CH4_mod_min_valid_gC_m2_day=CH4_mod_min_valid*10^-6*16.04*0.75;
ch4_min_EE_gC_m2_day=ch4_min_EE*10^-6*16.04*0.75;
daily_wm_sum_gC_gf_param=daily_wm_sum_umol_gf_param*10^-6*16.04*0.75;
daily_wm_sum_gC_gf_valid=daily_wm_sum_umol_gf_valid*10^-6*16.04*0.75;
daily_wm_sum_gC_gf_EE=daily_wm_sum_umol_gf_EE*10^-6*16.04*0.75;

%ch4
RMSE_ch4_min_daily_MB_param=nansum((CH4_mod_min_param_gC_m2_day(1:1461)-daily_wm_sum_gC_gf_param(1:1461)').^2);
RMSE_ch4_min_daily_MB_param=sqrt(RMSE_ch4_min_daily_MB_param/1461);
RMSE_ch4_min_daily_WP_param=nansum((CH4_mod_min_param_gC_m2_day(1462:end)-daily_wm_sum_gC_gf_param(1462:end)').^2);
RMSE_ch4_min_daily_WP_param=sqrt(RMSE_ch4_min_daily_WP_param/902);
 
RMSE_ch4_min_daily_MB_valid=nansum((CH4_mod_min_valid_gC_m2_day(1:365)-daily_wm_sum_gC_gf_valid(1:365)').^2);
RMSE_ch4_min_daily_MB_valid=sqrt(RMSE_ch4_min_daily_MB_valid/365);
RMSE_ch4_min_daily_WP_valid=nansum((CH4_mod_min_valid_gC_m2_day(366:end)-daily_wm_sum_gC_gf_valid(366:end)').^2);
RMSE_ch4_min_daily_WP_valid=sqrt(RMSE_ch4_min_daily_WP_valid/365);
RMSE_ch4_min_daily_EE_valid=nansum((ch4_min_EE_gC_m2_day(1:end)-daily_wm_sum_gC_gf_EE(1:end)').^2);
RMSE_ch4_min_daily_EE_valid=sqrt(RMSE_ch4_min_daily_EE_valid/365);

%Mean absolute error (MAE)
%ch4
MAE_ch4_min_daily_MB_param=nansum((CH4_mod_min_param_gC_m2_day(1:1461)-daily_wm_sum_gC_gf_param(1:1461)'));
MAE_ch4_min_daily_MB_param=(MAE_ch4_min_daily_MB_param/1461);
MAE_ch4_min_daily_WP_param=nansum((CH4_mod_min_param_gC_m2_day(1462:end)-daily_wm_sum_gC_gf_param(1462:end)'));
MAE_ch4_min_daily_WP_param=(MAE_ch4_min_daily_WP_param/902);
 
MAE_ch4_min_daily_MB_valid=nansum((CH4_mod_min_valid_gC_m2_day(1:365)-daily_wm_sum_gC_gf_valid(1:365)'));
MAE_ch4_min_daily_MB_valid=(MAE_ch4_min_daily_MB_valid/365);
MAE_ch4_min_daily_WP_valid=nansum((CH4_mod_min_valid_gC_m2_day(366:end)-daily_wm_sum_gC_gf_valid(366:end)'));
MAE_ch4_min_daily_WP_valid=(MAE_ch4_min_daily_WP_valid/365);
MAE_ch4_min_daily_EE_valid=nansum((ch4_min_EE_gC_m2_day(1:end)-daily_wm_sum_gC_gf_EE(1:end)'));
MAE_ch4_min_daily_EE_valid=(MAE_ch4_min_daily_EE_valid/365);


%D-index sensitive to systematic model bias
%ch4
D_index_ch4_min_daily_top_MB_param=nansum((CH4_mod_min_param_gC_m2_day(1:1461)-daily_wm_sum_gC_gf_param(1:1461)').^2);
O_avg_MB_param=nanmean(daily_wm_sum_gC_gf_param(1:1461));
D_index_ch4_min_daily_bottom_MB_param=nansum(((CH4_mod_min_param_gC_m2_day(1:1461)-O_avg_MB_param)+(daily_wm_sum_gC_gf_param(1:1461)'-O_avg_MB_param)).^2);
D_index_ch4_min_daily_MB_param=1-(D_index_ch4_min_daily_top_MB_param/D_index_ch4_min_daily_bottom_MB_param);

D_index_ch4_min_daily_top_WP_param=nansum((CH4_mod_min_param_gC_m2_day(1462:end)-daily_wm_sum_gC_gf_param(1462:end)').^2);
O_avg_WP_param=nanmean(daily_wm_sum_gC_gf_param(1462:end));
D_index_ch4_min_daily_bottom_WP_param=nansum(((CH4_mod_min_param_gC_m2_day(1462:end)-O_avg_WP_param)+(daily_wm_sum_gC_gf_param(1462:end)'-O_avg_WP_param)).^2);
D_index_ch4_min_daily_WP_param=1-(D_index_ch4_min_daily_top_WP_param/D_index_ch4_min_daily_bottom_WP_param);

D_index_ch4_min_daily_top_MB_valid=nansum((CH4_mod_min_valid_gC_m2_day(1:365)-daily_wm_sum_gC_gf_valid(1:365)').^2);
O_avg_MB_valid=nanmean(daily_wm_sum_gC_gf_valid(1:365));
D_index_ch4_min_daily_bottom_MB_valid=nansum(((CH4_mod_min_valid_gC_m2_day(1:365)-O_avg_MB_valid)+(daily_wm_sum_gC_gf_valid(1:365)'-O_avg_MB_valid)).^2);
D_index_ch4_min_daily_MB_valid=1-(D_index_ch4_min_daily_top_MB_valid/D_index_ch4_min_daily_bottom_MB_valid);

D_index_ch4_min_daily_top_WP_valid=nansum((CH4_mod_min_valid_gC_m2_day(366:end)-daily_wm_sum_gC_gf_valid(366:end)').^2);
O_avg_WP_valid=nanmean(daily_wm_sum_gC_gf_valid(366:end));
D_index_ch4_min_daily_bottom_WP_valid=nansum(((CH4_mod_min_valid_gC_m2_day(366:end)-O_avg_WP_valid)+(daily_wm_sum_gC_gf_valid(366:end)'-O_avg_WP_valid)).^2);
D_index_ch4_min_daily_WP_valid=1-(D_index_ch4_min_daily_top_WP_valid/D_index_ch4_min_daily_bottom_WP_valid);

D_index_ch4_min_daily_top_EE_valid=nansum((ch4_min_EE_gC_m2_day(1:365)-daily_wm_sum_gC_gf_EE(1:365)').^2);
O_avg_EE_valid=nanmean(daily_wm_sum_gC_gf_EE(1:365));
D_index_ch4_min_daily_bottom_EE_valid=nansum(((ch4_min_EE_gC_m2_day(1:365)-O_avg_EE_valid)+(daily_wm_sum_gC_gf_EE(1:365)'-O_avg_EE_valid)).^2);
D_index_ch4_min_daily_EE_valid=1-(D_index_ch4_min_daily_top_EE_valid/D_index_ch4_min_daily_bottom_EE_valid);


%% plotting Reco and GPP param and valid data for MB AND WP AND EE 
%UPDATED ON 2016.10.10
days_plot_MB_partition=vertcat(daily_DOY_param(1:1461), daily_DOY_EE+1461);
days_plot_WP_partition=vertcat(daily_DOY_param(1462:end),daily_DOY_EE+2363);
days_plot_EE_partition=daily_DOY_EE;

daily_plot_MB_CH4_lower=vertcat(CH4_mod_min_param_lower(1:1461)'*10^-6*16.04*0.75, CH4_mod_min_valid_lower(1:365)'*10^-6*16.04*0.75);
daily_plot_WP_CH4_lower=vertcat(CH4_mod_min_param_lower(1462:end)'*10^-6*16.04*0.75, CH4_mod_min_valid_lower(366:end)'*10^-6*16.04*0.75);
daily_plot_MB_CH4_upper=vertcat(CH4_mod_min_param_upper(1:1461)'*10^-6*16.04*0.75, CH4_mod_min_valid_upper(1:365)'*10^-6*16.04*0.75);
daily_plot_WP_CH4_upper=vertcat(CH4_mod_min_param_upper(1462:end)'*10^-6*16.04*0.75, CH4_mod_min_valid_upper(366:end)'*10^-6*16.04*0.75);

daily_plot_MB_CH4_obs=vertcat(daily_wm_sum_umol_gf_param(1:1461)*10^-6*16.04*0.75, daily_wm_sum_umol_gf_valid(1:365)*10^-6*16.04*0.75);
daily_plot_WP_CH4_obs=vertcat(daily_wm_sum_umol_gf_param(1462:end)*10^-6*16.04*0.75, daily_wm_sum_umol_gf_valid(366:end)*10^-6*16.04*0.75);

startDate = datenum('01-01-2011');
endDate = datenum('12-31-2015');
xData1=linspace(startDate,endDate,1826);

startDate2 = datenum('07-12-2012');
endDate2 = datenum('12-31-2015');
xData2=linspace(startDate2,endDate2,1267);

startDate3 = datenum('01-01-2015');
endDate3 = datenum('12-31-2015');
xData3=linspace(startDate3,endDate3,365);

%for 1:1's
%MB
X = ones(length(CH4_mod_min_param(1:1461)'),1);
X = [X CH4_mod_min_param(1:1461)'];
[~,~,~,~,STATS1] = regress(daily_wm_sum_umol_gf_param(1:1461),X);
rsq1=STATS1(1);
X = ones(length(CH4_mod_min_valid(1:365)'),1);
X = [X CH4_mod_min_valid(1:365)'];
[~,~,~,~,STATS3] = regress(daily_wm_sum_umol_gf_valid(1:365),X);
rsq3=STATS3(1);

%WP
X = ones(length(CH4_mod_min_param(1462:end)'),1);
X = [X CH4_mod_min_param(1462:end)'];
[~,~,~,~,STATS5] = regress(daily_wm_sum_umol_gf_param(1462:end),X);
rsq5=STATS5(1);
X = ones(length(CH4_mod_min_valid(366:end)'),1);
X = [X CH4_mod_min_valid(366:end)'];
[~,~,~,~,STATS7] = regress(daily_wm_sum_umol_gf_valid(366:end),X);
rsq7=STATS7(1);
%EE
X = ones(length(ch4_min_EE(1:365)'),1);
X = [X ch4_min_EE(1:365)'];
[~,~,~,~,STATS9] = regress(daily_wm_sum_umol_gf_EE(1:365),X);
rsq9=STATS9(1);


%% plot CH4 GPP figure all sites all years
figure ('Units', 'pixels',...
    'Position', [100 100 500 375]);
%MB time series GPP and CH4
subplot(3,1,1)
ciplot(daily_plot_MB_CH4_lower*1e3,daily_plot_MB_CH4_upper*1e3,xData1,'k')
hold on
obs = plot(xData1,daily_plot_MB_CH4_obs*1e3,'.r');
%ylim([-5 15])
datetick('x','mm/yyyy')
box off
hYLabel = ylabel({'CH_4 exchange','(mg CH_4-C m^-^2 d^-^1)' });
% legend('model R_e_c_o','model GPP','partitioned CH4','partitioned GPP')

 %WP time series GPP and CH4
 subplot(3,1,2)
ciplot(daily_plot_WP_CH4_lower*1e3,daily_plot_WP_CH4_upper*1e3,xData2,'k')
hold on
obs = plot(xData2,daily_plot_WP_CH4_obs*1e3,'.r');
%ylim([-5 15])
datetick('x','mm/yyyy','keeplimits')
box off
hYLabel = ylabel({'CH_4 exchange','(mg CH_4-C m^-^2 d^-^1)' });
% legend('model R_e_c_o','model GPP','partitioned CH4','partitioned GPP')

 %EE time series GPP and CH4
 subplot(3,1,3)
ciplot(lower_ch4_EE*10^-3*16.04*0.75,upper_ch4_EE*10^-3*16.04*0.75,xData3,'k')
hold on
obs = plot(xData3,daily_wm_sum_umol_gf_EE*10^-3*16.04*0.75,'.r');
%ylim([-5 15])
datetick('x','mm/yyyy','keeplimits')
box off
hYLabel = ylabel({'CH_4 exchange','(mg CH_4-C m^-^2 d^-^1)' });
 legend('CH_4 _m_o_d_e_l','CH_4 _o_b_s')
set( gca                       , ...
    'FontName'   , 'Helvetica' );
set([hXLabel, hYLabel, text]  , ...
    'FontSize'   , 12         );
 
set(gcf, 'PaperPositionMode', 'auto');
print('TPGPP_CH4_all_data_MB_WP_EE','-dpng','-r600');

%% one to one's added in via powerpoint file
%MB 1:1 CH4 convert to mg c m-2 d-1
p1 = polyfit(CH4_mod_min_param(1:1461)*10^-3*16.04*0.75,daily_wm_sum_umol_gf_param(1:1461)'*10^-3*16.04*0.75,1);
p2 = polyfit(CH4_mod_min_valid(1:365)*10^-3*16.04*0.75,daily_wm_sum_umol_gf_valid(1:365)'*10^-3*16.04*0.75,1);
%WP
p5 = polyfit(CH4_mod_min_param(1462:end)*10^-3*16.04*0.75,daily_wm_sum_umol_gf_param(1462:end)'*10^-3*16.04*0.75,1);
p6 = polyfit(CH4_mod_min_valid(366:end)*10^-3*16.04*0.75,daily_wm_sum_umol_gf_valid(366:end)'*10^-3*16.04*0.75,1);
%EE
p9 = polyfit(ch4_min_EE*10^-3*16.04*0.75,daily_wm_sum_umol_gf_EE'*10^-3*16.04*0.75,1);

figure
plot(CH4_mod_min_param(1:1461)*10^-3*16.04*0.75,daily_wm_sum_umol_gf_param(1:1461)*10^-3*16.04*0.75,'.r');
h=lsline;
xlim([0,700])
ylim([0,700])
hold on
plot(CH4_mod_min_valid(1:365)*10^-3*16.04*0.75,daily_wm_sum_umol_gf_valid(1:365)*10^-3*16.04*0.75,'.k');
m=lsline;
refline(1,0)
xlim([0,700])
ylim([0,700])
xlabel('CH_4 _m_o_d_e_l')
ylabel('CH_4 _o_b_s')
box off
set(gcf, 'PaperPositionMode', 'auto');
print('TPGPP_CH4_onetoone_param_validation_MB','-dpng','-r600');


figure
plot(CH4_mod_min_param(1462:end)*10^-3*16.04*0.75,daily_wm_sum_umol_gf_param(1462:end)*10^-3*16.04*0.75,'.r');
h=lsline;
xlim([0,700])
ylim([0,700])
hold on
plot(CH4_mod_min_valid(366:end)*10^-3*16.04*0.75,daily_wm_sum_umol_gf_valid(366:end)*10^-3*16.04*0.75,'.k');
m=lsline;
xlim([0,700])
ylim([0,700])
refline(1,0)
xlabel('CH_4 _m_o_d_e_l')
ylabel('CH_4 _o_b_s')
box off
set(gcf, 'PaperPositionMode', 'auto');
print('TPGPP_CH4_onetoone_param_validation_WP','-dpng','-r600');

figure
plot(ch4_min_EE*10^-3*16.04*0.75,daily_wm_sum_umol_gf_EE*10^-3*16.04*0.75,'.k');
h=lsline;
xlim([0,300])
ylim([0,300])
refline(1,0)
xlabel('CH_4 _m_o_d_e_l')
ylabel('CH_4 _o_b_s')
box off
set(gcf, 'PaperPositionMode', 'auto');
print('TPGPP_CH4_onetoone_EE','-dpng','-r600');



%% UPDATED 10/2016
%Plot posteriors
Mref_posterior_TPGPP=find_min_10000(:,1)+0.2+0.28;%kJ mol-1
Eo_posterior_TPGPP=find_min_10000(:,2)+500+400;%umol m-2
k2_posterior_TPGPP=find_min_10000(:,3)*1e-7;
Ea_oxy_posterior_TPGPP=find_min_10000(:,4)+70;
km_oxy_posterior_TPGPP=find_min_10000(:,5)*3e3;

%check for correlations between parameters
X = ones(length(Eo_posterior_TPGPP),1);
X = [X Eo_posterior_TPGPP];
[~,~,~,~,STATS] = regress(k2_posterior_TPGPP,X);

X = ones(length(Mref_posterior_TPGPP),1);
X = [X Mref_posterior_TPGPP];
[~,~,~,~,STATS] = regress(km_oxy_posterior_TPGPP,X);

X = ones(length(Mref_posterior_TPGPP),1);
X = [X Mref_posterior_TPGPP];
[~,~,~,~,STATS] = regress(Ea_oxy_posterior_TPGPP,X);

X = ones(length(Eo_posterior_TPGPP),1);
X = [X Eo_posterior_TPGPP];
[~,~,~,~,STATS] = regress(km_oxy_posterior_TPGPP,X);

X = ones(length(Eo_posterior_TPGPP),1);
X = [X Eo_posterior_TPGPP];
[~,~,~,~,STATS] = regress(Ea_oxy_posterior_TPGPP,X);

X = ones(length(Ea_oxy_posterior_TPGPP),1);
X = [X Ea_oxy_posterior_TPGPP];
[~,~,~,~,STATS] = regress(km_oxy_posterior_TPGPP,X);


figure
subplot(3,2,1)
hist(Mref_posterior_TPGPP);%pdf for alpha 1
ylabel('Mref ')
subplot(3,2,2)
hist(Eo_posterior_TPGPP);%pdf for ea 1
ylabel('Eo ')
subplot(3,2,3)
hist(k2_posterior_TPGPP);%pdf for km 1
ylabel('k2 ')
subplot(3,2,4)
hist(Ea_oxy_posterior_TPGPP);%pdf for km 1
ylabel('Ea oxy (kJ mol-1)')
subplot(3,2,5)
hist(km_oxy_posterior_TPGPP);%pdf for alpha 2
ylabel('km oxy (umol C m-3 soil)')
 
set(gcf, 'PaperPositionMode', 'auto');
print('TPGPP_LAI_CH4_posterior_20150617','-dpng','-r600');

Mref_posterior_final=(find_min(1)+0.2);%kJ mol-1
Eo_posterior_final=find_min(2)+500;%umol m-2
k2_posterior_final=find_min(3)*1e-7;
Ea_oxy_posterior_final=find_min(4)+70;
km_oxy_posterior_final=find_min(5)*3e3;

 %% plotting

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%daily 
days_plot=days+194;
 
figure ('Units', 'pixels',...
    'Position', [100 100 500 375]);
ciplot(daily_ch4_mod_lower,daily_ch4_mod_upper,days_plot,'k')
hold on
obs = plot(days_plot,daily_ch4_obs,'.');
 
set(obs                       , ...
  'LineWidth'       , 1           , ...
  'Marker'          , 'o'         , ...
  'MarkerSize'      , 3           , ...
  'MarkerEdgeColor' , [0.5 0.5 0.5]  , ...
  'MarkerFaceColor' , [1 1 1]  );
 
hXLabel = xlabel('Day of year'                     );
hYLabel = ylabel('CH_4 exchange(mg C-CH_4 m^-^2 d^-^1)' );
 
 legend('TPGPP model','obs')
set( gca                       , ...
    'FontName'   , 'Helvetica' );
set([hXLabel, hYLabel, text]  , ...
    'FontSize'   , 14         );
 
set(gca, ...
  'Box'         , 'off'     , ...
  'TickDir'     , 'out'     , ...
  'TickLength'  , [.02 .02] , ...
  'XMinorTick'  , 'off'      , ...
  'YMinorTick'  , 'off'      , ...
  'YGrid'       , 'off'      , ...
  'XColor'      , [0 0 0], ...
  'YColor'      , [0 0 0], ...
  'Xtick'       , 200:100:1100,...
  'YTick'       , 0:100:500, ...
  'LineWidth'   , 1         );
 
set(gcf, 'PaperPositionMode', 'auto');
print('TPGPP_LAI_ch4_alldata_20150617_validation','-dpng','-r600');
%print -depsc2 finalPlot1.eps
close;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plot one to one line


figure ('Units', 'pixels',...
    'Position', [100 100 500 375]);
hold on
obs = plot(daily_ch4_mod_min,daily_ch4_obs,'.');
set(obs                           , ...
  'LineStyle'       , 'none'      , ...
  'Marker'          , '.'         , ...
  'Color'           , [0 0 0]  );
set(obs                       , ...
  'LineWidth'       , 1           , ...
  'Marker'          , '.'         , ...
  'MarkerSize'      , 15          , ...
  'MarkerEdgeColor' , [.2 .2 .2]  , ...
  'MarkerFaceColor' , [.7 .7 .7]  );
hXLabel = xlabel('Model (mg C-CH_4 m^-^2 d^-^1)');
hYLabel = ylabel('Obs (mg C-CH_4 m^-^2 d^-^1)');
%set(gca, 'xticklabel', {'\pi'}) 
%legend('y = 0.78*x + 6.6')
% 
% set([legend, gca]             , ...
%     'FontSize'   , 14           );
set([hXLabel, hYLabel, text]  , ...
    'FontSize'   , 14         );
 
set(gca, ...
  'Box'         , 'off'     , ...
  'TickDir'     , 'out'     , ...
  'TickLength'  , [.02 .02] , ...
  'XMinorTick'  , 'off'      , ...
  'YMinorTick'  , 'off'      , ...
  'YGrid'       , 'off'      , ...
  'XColor'      , [0 0 0], ...
  'YColor'      , [0 0 0], ...
  'YTick'       , 0:100:400, ...
  'XTick'       , 0:100:400, ...
  'LineWidth'   , 1         );
 
set(gcf, 'PaperPositionMode', 'auto');
print('TPGPP_LAI_CH4_alldata_onetoone_20150617','-dpng','-r600');
 
%print -depsc2 finalPlot1_inset.eps
close;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%VALIDATION DATA ONLY FIGURE
X = ones(length(daily_ch4_mod_min(539:end)),1);
X = [X daily_ch4_mod_min(539:end)];
[~,~,~,~,STATS] = regress(daily_ch4_obs(539:end),X);
rsq=STATS(1);

figure ('Units', 'pixels',...
    'Position', [100 100 500 375])
obs=plot(daily_ch4_mod_min(539:end),daily_ch4_obs(539:end),'.');
hXLabel = xlabel('Model (mg C-CH_4 m^-^2 d^-^1)');
hYLabel = ylabel('Obs (mg C-CH_4 m^-^2 d^-^1)');
box off
xlim([0,450])
ylim([0,450])
set(obs                       , ...
  'LineWidth'       , 1           , ...
  'Marker'          , 'o'         , ...
  'MarkerSize'      , 3           , ...
  'MarkerEdgeColor' , [0.25 0.25 0.25]  , ...
  'MarkerFaceColor' , [0.25 0.25 0.25]  );
h=lsline;
p2 = polyfit(get(h,'xdata'),get(h,'ydata'),1);
theString = sprintf('y = %.3f x + %.3f', p2(1), p2(2));
place_x=min(daily_ch4_mod_min(539:end));
place_y=max(daily_ch4_obs(539:end));
t=text(place_x,place_y,theString);%;t=text(2,-7,'p2');%puts letter in upper L corner
theString = sprintf('r^2 = %.2f', rsq);
t=text(1,400,theString);%puts letter in upper L corner
refline(1,0)
 set( gca                       , ...
    'FontName'   , 'Helvetica' );
set([hXLabel, hYLabel, text]  , ...
    'FontSize'   , 12         );
 
set(gcf, 'PaperPositionMode', 'auto');
print('CH4_TPGPP_onetoone_20160220','-dpng','-r300');
%print -depsc2 finalPlot1.eps
close;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  residual=daily_ch4_obs-daily_ch4_mod_min;
 
 figure
 obs=plot(daily_ch4_mod_min,residual,'.');
 xlabel('TP-GPP model (mg C-CH_4 m^-^2 d^-^1)');
 ylabel('Residual (obs-mod)')
 box off
 set(obs                       , ...
  'LineWidth'       , 1           , ...
  'Marker'          , 'o'         , ...
  'MarkerSize'      , 3          , ...
  'MarkerEdgeColor' , [.4 .4 .4]  , ...
  'MarkerFaceColor' , [1 1 1]  );

set(gcf, 'PaperPositionMode', 'auto');
print('TPP_GPP_CH4_residual_20150623','-dpng','-r600');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure ('Units', 'pixels',...
    'Position', [100 100 500 375]);
hold on
obs = plot(days_plot,daily_WT_gf,'.');
%axis([0 350 0 200])
set(obs                           , ...
  'LineStyle'       , 'none'      , ...
  'Marker'          , '.'         , ...
  'Color'           , [0 0 0]  );
set(obs                       , ...
  'LineWidth'       , 1           , ...
  'Marker'          , '.'         , ...
  'MarkerSize'      , 15          , ...
  'MarkerEdgeColor' , [.2 .2 .2]  , ...
  'MarkerFaceColor' , [.7 .7 .7]  );
hXLabel = xlabel('Day of year');
hYLabel = ylabel('Water table height (cm)');
 
%legend('y = 0.78*x + 6.6')
% 
% set([legend, gca]             , ...
%     'FontSize'   , 14           );
set([hXLabel, hYLabel, text]  , ...
    'FontSize'   , 14         );
 
set(gca, ...
  'Box'         , 'off'     , ...
  'TickDir'     , 'out'     , ...
  'TickLength'  , [.02 .02] , ...
  'XMinorTick'  , 'off'      , ...
  'YMinorTick'  , 'off'      , ...
  'YGrid'       , 'off'      , ...
  'XColor'      , [0 0 0], ...
  'YColor'      , [0 0 0], ...
  'Xtick'       , 0:100:1100,...
  'YTick'       , -50:10:50, ...
  'LineWidth'   , 1         );
 
set(gcf, 'PaperPositionMode', 'auto');
print('WT_20150617','-dpng','-r600');
%print -depsc2 finalPlot1_inset.eps
close;
 
 

