function [yfill] = FillSmallGaps(y,gapmax)
% Usage: [yfill] = FillSmallGaps(y,gapmax)
% Fills NaN values in a time series with linear interpolation up to a 
% maximum gap size, beyond which gaps will remain NaN. Note: NaNs
% existing at the beginning and end of the time series will remain NaN.
% 
% ------- Inputs -------
% y = the time series to fill. 
% gapmax = largest gaps size (in data points) to fill. MUST BE EVEN.
%          Gaps larger than this size will remain NaN.
%
% ------- Outputs -------
% yfill = the gap-filled time series. 
%
% -----------------------
% Cove Sturtevant
% March 2014


% Find NaNs and non-NaNs
ni = find(isnan(y));
nni = find(~isnan(y));

% Fill NaNs with linear interpolation
x = 1:length(y);
yf = interp1(x(nni),y(nni),ni);
yfill = y;
yfill(ni) = yf;

% Remove fill from gaps larger than specified size
gapmax = gapmax+1;
g = isnan(y);
for i = ceil(gapmax/2):length(y)-ceil(gapmax/2)+1
    ieval = (1:gapmax)+i-ceil(gapmax/2);
    if sum(g(ieval)) == gapmax
        yfill(ieval) = NaN;
    end
end
    
