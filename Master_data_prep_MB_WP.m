%creating master data WP and MB

%% Prep data
%Load MB L3 data
%pull only 2011-2015
figure
plot(data.decday(3793:91440),data.wc_gf(3793:91440),'.')
decday_MB=data.decday(3793:91440);
wc_gf_MB=data.wc_gf(3793:91440);
wc_MB=data.wc(3793:91440);
er_ANNnight_MB=data.er_ANNnight(3793:91440);
gpp_ANNnight_MB=data.gpp_ANNnight(3793:91440);
wm_gf_MB=data.wm_gf(3793:91440);
wm_MB=data.wm(3793:91440);

DOY_disc_MB=data.DOY(3793:91440);

%make wetland age parameter
wetland_age_MB=DOY_disc_MB*0+1;
wetland_age_MB(17521:end)=wetland_age_MB(17521:end)+1;
wetland_age_MB(35089:end)=wetland_age_MB(35089:end)+1;
wetland_age_MB(70129:end)=wetland_age_MB(70129:end)+1;
wetland_age_MB(70129:end)=wetland_age_MB(70129:end)+1;
figure
plot(wetland_age_MB,'.')

%need a DOY continuous
DOY_MB=DOY_disc_MB;
DOY_MB(17521:end)=DOY_MB(17521:end)+365;
DOY_MB(35089:end)=DOY_MB(35089:end)+366;
DOY_MB(70129:end)=DOY_MB(70129:end)+365;
DOY_MB(70129:end)=DOY_MB(70129:end)+365;
figure
plot(diff(DOY_MB))
% figure
% plot(data.DOY,'.')%2012 had 366 days
leap_year=Season_drop_one_year(17400:17447);
Season_drop_MB=vertcat(Season_drop_one_year ,Season_drop_one_year,leap_year,Season_drop_one_year,...
    Season_drop_one_year, Season_drop_one_year);

WT_gf_MB=Metdata.WT(3793:91440);
figure
plot(WT_gf_MB,'.')

PAR_gf_MB=Metdata.PAR(3793:91440);
figure
plot(PAR_gf_MB,'.')

MB_gpp_ANNnight_cum=data.gpp_ANNnight(3793:91440);
MB_gpp_ANNnight_cum=cumsum(MB_gpp_ANNnight_cum);
figure
plot(MB_gpp_ANNnight_cum,'.')
%compute LAI for MB
GCCsmooth_MB=Metdata.GCCsmooth(3793:91440);
figure
plot(GCCsmooth_MB,'.g')

LAI_MB=0.0037*exp(GCCsmooth_MB*17.06);
LAI_gf_MB=FillSmallGaps(LAI_MB,10000);

figure
plot(LAI_gf_MB,'.b')
hold on
plot(LAI_MB,'.r')

TA_MB=data.TA(3793:91440);
% figure
% plot(TA_MB,'r.')
%% calculate 90%CI for ANN
%create 20 datasets for GPP ANN--apparently not in data structure

for i=1:length(data.decday)
    
gpp_ANNext(i,:)=data.wc_gf(i)-data.er_ANNext(i,:);
er_ANN_mean(i)=nanmean(data.er_ANNext(i,:));
er_ANN_SD(i)=nanstd(data.er_ANNext(i,:));
gpp_ANN_mean(i)=nanmean(gpp_ANNext(i,:));
gpp_ANN_SD(i)=nanstd(gpp_ANNext(i,:));
wm_ANN_mean(i)=nanmean(data.wm_ANNext(i,:));
wm_ANN_SD(i)=nanstd(data.wm_ANNext(i,:));

end
gf_error_er=1.645*(er_ANN_SD./sqrt(20));
gf_error_gpp=1.645*(gpp_ANN_SD./sqrt(20));
gf_error_wm=1.645*(wm_ANN_SD./sqrt(20));

lower_er_ANN=er_ANN_mean-(1.645*(er_ANN_SD./sqrt(20)));%use 1.645 for 90% CI
upper_er_ANN=er_ANN_mean+(1.645*(er_ANN_SD./sqrt(20)));
lower_gpp_ANN=gpp_ANN_mean-(1.645*(gpp_ANN_SD./sqrt(20)));%use 1.645 for 90% CI
upper_gpp_ANN=gpp_ANN_mean+(1.645*(gpp_ANN_SD./sqrt(20)));

figure
ciplot(lower_er_ANN,upper_er_ANN,data.decday,'r')

figure
ciplot(lower_gpp_ANN,upper_gpp_ANN,data.decday,'k')
%select only 2011-2016
lower_er_ANN=lower_er_ANN(3793:91440);
upper_er_ANN=upper_er_ANN(3793:91440);
lower_gpp_ANN=lower_gpp_ANN(3793:91440);
upper_gpp_ANN=upper_gpp_ANN(3793:91440);
gf_error_er=gf_error_er(3793:91440);
gf_error_gpp=gf_error_gpp(3793:91440);
gf_error_wm=gf_error_wm(3793:91440);
%gf error same for er and gpp
figure
plot(gf_error_er,'.')
hold on
plot(gf_error_gpp,'g.')
figure
plot(gf_error_wm,'.r')
figure
ciplot(wm_gf_MB-gf_error_wm,wm_gf_MB+gf_error_wm,decday_MB)
%Random error 
envir_var=[data.ubar(3793:91440) PAR_gf_MB TA_MB];
var_num=3;
range=[1 75 3];
[wc_RE,~,~,~,wc_p_pos,wc_p_neg]=daily_diff(DOY_MB,data.time(3793:91440),wc_gf_MB,var_num,envir_var,range);
figure
plot(wc_RE,'.')
wc_RE_high=wc_gf_MB+wc_RE';
wc_RE_low=wc_gf_MB-wc_RE';
figure
ciplot(wc_RE_low,wc_RE_high,decday_MB)

envir_var=[data.ubar(3793:91440) PAR_gf_MB TA_MB];
var_num=3;
range=[1 75 3];
[wm_RE,~,~,~,wm_p_pos,wm_p_neg]=daily_diff(DOY_MB,data.time(3793:91440),wm_gf_MB,var_num,envir_var,range);
figure
plot(wm_RE,'.')
wm_RE_high=wm_gf_MB+wm_RE';
wm_RE_low=wm_gf_MB-wm_RE';
figure
ciplot(wm_RE_low,wm_RE_high,decday_MB)

% need to add Random error to the 90% CI gf error at 30min time step
%where during gaps, have gapfilling error+random error
%in non-gaps, only random error

wm_0=data.wm(3793:91440);
wc_0=data.wc(3793:91440);
Sel=isnan(wm_0);
wm_0(Sel)=0;
Sel=isnan(wc_0);
wc_0(Sel)=0;

for i=1:length(wm_0)
    if wm_0(i)==0%if there is a gap, gf error and random error applies
        total_error_wm_MB(i)=wm_RE(i)+gf_error_wm(i);
    else %without gap, only Random error applies
        total_error_wm_MB(i)=wm_RE(i);
    end
    if wc_0(i)==0%if there is a gap, gf error and random error applies
        total_error_wc_MB(i)=wc_RE(i)+gf_error_er(i);
    else %without gap, only Random error applies
        total_error_wc_MB(i)=wc_RE(i);
    end
end

figure
plot(decday_MB,wm_gf_MB,'g',decday_MB,wm_gf_MB-total_error_wm_MB','r',decday_MB,wm_gf_MB+total_error_wm_MB','k')
hold on
plot(decday_MB,wm_0)
legend('wm gf','lower gf','upper gf','wm')

figure
plot(decday_MB,wc_gf_MB,'g',decday_MB,wc_gf_MB-total_error_wc_MB','r',decday_MB,wc_gf_MB+total_error_wc_MB','k')
hold on
plot(decday_MB,wc_0)
legend('wc gf','lower gf','upper gf','wm')

wm_gf_MB_lower=wm_gf_MB-total_error_wm_MB';
wm_gf_MB_upper=wm_gf_MB+total_error_wm_MB';
wc_gf_MB_lower=wc_gf_MB-total_error_wc_MB';
wc_gf_MB_upper=wc_gf_MB+total_error_wc_MB';

%% convert to daily variables
%First take output from CO2 model and convert to daily averages
days_MB = 1:1:1826;%NMBDS TO BE UPDATED!!--all data

days_MB=days_MB';%b/c my code likes to start from 1, shift data temporarily so they start at
%1
%first do daily averages
[daily_DOY_MB,daily_DOY_disc_MB]=daily_ave(DOY_MB,DOY_disc_MB,days_MB);%time is 30min time scale, days is DOY
figure
plot(daily_DOY_MB,daily_DOY_disc_MB,'.')

[daily_DOY_MB,daily_TA_MB]=daily_ave(DOY_MB,TA_MB,days_MB);%time is 30min time scale, days is DOY
figure
plot(DOY_MB,TA_MB,'.r')
hold on
plot(daily_DOY_MB,daily_TA_MB,'.')

[daily_DOY_MB,daily_WT_gf_MB]=daily_ave(DOY_MB,WT_gf_MB,days_MB);%time is 30min time scale, days is DOY
figure
plot(DOY_MB,WT_gf_MB,'.')
hold on
plot(daily_DOY_MB,daily_WT_gf_MB,'.r')

[daily_DOY_MB,daily_PAR_gf_MB]=daily_ave(DOY_MB,PAR_gf_MB,days_MB);%time is 30min time scale, days is DOY
figure
plot(DOY_MB,PAR_gf_MB,'.')
hold on
plot(daily_DOY_MB,daily_PAR_gf_MB,'.r')

%Wetland age parameter
daily_wetland_age_MB=daily_PAR_gf_MB*0+1;
daily_wetland_age_MB(366:end)=daily_wetland_age_MB(366:end)+1;
daily_wetland_age_MB(732:end)=daily_wetland_age_MB(732:end)+1;
daily_wetland_age_MB(1462:end)=daily_wetland_age_MB(1462:end)+1;
daily_wetland_age_MB(1462:end)=daily_wetland_age_MB(1462:end)+1;
figure
plot(daily_DOY_MB,daily_wetland_age_MB,'.')

%now do integrals
daily_wm=wm_gf_MB*60*30*0.001;%nmol to umol m-2 30min-1
[daily_wm_sum_umol_gf_MB]=daily_sum(DOY_MB,daily_wm,days_MB);%time is 30min time scale, days is DOY
figure
plot(DOY_MB,daily_wm,'.')
hold on
plot(daily_DOY_MB,daily_wm_sum_umol_gf_MB,'.r')


daily_NEE_obs_MB=data.wc_gf(3793:91440)*60*30;%first turn NMB from umol m-2 s-1 into umol m-2 30min-1
[daily_NEE_obs_MB]=daily_sum(DOY_MB,daily_NEE_obs_MB,days_MB);

daily_GPP_obs_MB=data.gpp_ANNnight(3793:91440)*60*30;%first turn NMB from umol m-2 s-1 into umol m-2 30min-1
[daily_GPP_obs_MB]=daily_sum(DOY_MB,daily_GPP_obs_MB,days_MB);

daily_Reco_obs_MB=data.er_ANNnight(3793:91440)*60*30;%first turn NMB from umol m-2 s-1 into umol m-2 30min-1
[daily_Reco_obs_MB]=daily_sum(DOY_MB,daily_Reco_obs_MB,days_MB);

daily_gf_error_wc_MB=gf_error_er'*60*30;%first turn NMB from umol m-2 s-1 into umol m-2 30min-1
[daily_gf_error_wc_MB]=daily_sum(DOY_MB,daily_gf_error_wc_MB,days_MB);

daily_random_error_wc_MB=wc_RE'*60*30;%first turn NMB from umol m-2 s-1 into umol m-2 30min-1
[daily_random_error_wc_MB]=daily_sum(DOY_MB,daily_random_error_wc_MB,days_MB);

daily_gf_error_wm_MB=gf_error_wm'*60*30*0.001;%first turn NMB from umol m-2 s-1 into umol m-2 30min-1
[daily_gf_error_wm_MB]=daily_sum(DOY_MB,daily_gf_error_wm_MB,days_MB);

daily_random_error_wm_MB=wm_RE'*60*30*0.001;%first turn NMB from umol m-2 s-1 into umol m-2 30min-1
[daily_random_error_wm_MB]=daily_sum(DOY_MB,daily_random_error_wm_MB,days_MB);

daily_wm_gf_MB_lower=wm_gf_MB_lower*60*30*0.001;%first turn NMB from umol m-2 s-1 into umol m-2 30min-1
[daily_wm_gf_MB_lower]=daily_sum(DOY_MB,daily_wm_gf_MB_lower,days_MB);

daily_wm_gf_MB_upper=wm_gf_MB_upper*60*30*0.001;%first turn NMB from umol m-2 s-1 into umol m-2 30min-1
[daily_wm_gf_MB_upper]=daily_sum(DOY_MB,daily_wm_gf_MB_upper,days_MB);

daily_wc_gf_MB_lower=wc_gf_MB_lower*60*30;%first turn NMB from umol m-2 s-1 into umol m-2 30min-1
[daily_wc_gf_MB_lower]=daily_sum(DOY_MB,daily_wc_gf_MB_lower,days_MB);

daily_wc_gf_MB_upper=wc_gf_MB_upper*60*30;%first turn NMB from umol m-2 s-1 into umol m-2 30min-1
[daily_wc_gf_MB_upper]=daily_sum(DOY_MB,daily_wc_gf_MB_upper,days_MB);

figure
plot(daily_DOY_MB,daily_gf_error_wm_MB)
hold on
plot(daily_DOY_MB,daily_random_error_wm_MB)
legend('gf','re')

figure
ciplot(daily_wm_gf_MB_lower,daily_wm_gf_MB_upper,daily_DOY_MB)
figure
ciplot(daily_wc_gf_MB_lower,daily_wc_gf_MB_upper,daily_DOY_MB)


figure
plot(daily_DOY_MB,daily_NEE_obs,'.',daily_DOY_MB,daily_GPP_obs,'.',daily_DOY_MB,daily_Reco_obs,'.')
legend('nee','gpp','reco')
figure
subplot(2,1,1)
plot(daily_DOY_MB,daily_gf_error_wc,'.',daily_DOY_MB,daily_random_error_wc,'.')
legend('gf','re')
subplot(2,1,2)
plot(daily_DOY_MB,daily_gf_error_wm,'.',daily_DOY_MB,daily_random_error_wm,'.')
legend('gf','re')



%% Prep data
%Load WP L3 data
%pull only 2012-2015
figure
plot(data.decday(1:60816),data.wc_gf(1:60816),'.')
decday_WP=data.decday(1:60816);
wc_gf_WP=data.wc_gf(1:60816);
wc_WP=data.wc(1:60816);
er_ANNnight_WP=data.er_ANNnight(1:60816);
gpp_ANNnight_WP=data.gpp_ANNnight(1:60816);
wm_gf_WP=data.wm_gf(1:60816);
wm_WP=data.wm(1:60816);

DOY_disc_WP=data.DOY(1:60816);

%make wetland age parameter
wetland_age_WP=DOY_disc_WP*0+15;
wetland_age_WP(8257:end)=wetland_age_WP(8257:end)+1;
wetland_age_WP(43297:end)=wetland_age_WP(43297:end)+1;
wetland_age_WP(43297:end)=wetland_age_WP(43297:end)+1;
figure
plot(wetland_age_WP,'.')

%need a DOY continuous
DOY_WP=DOY_disc_WP;
DOY_WP(8257:end)=DOY_WP(8257:end)+366;
DOY_WP(43297:end)=DOY_WP(43297:end)+365;
DOY_WP(43297:end)=DOY_WP(43297:end)+365;
figure
plot(diff(DOY_WP))
% figure
% plot(data.DOY,'.')%2012 had 366 days
partial_year=Season_drop_one_year(9360:end);
leap_year=Season_drop_one_year(17400:17447);
Season_drop_WP=vertcat(partial_year ,leap_year,leap_year,Season_drop_one_year,Season_drop_one_year,...
    Season_drop_one_year);
Season_drop_WP=Season_drop_WP(2:end);

WT_WP=Metdata.WT(1:60816);
WT_gf_WP=FillSmallGaps(WT_WP,10000);
figure
plot(WT_gf_WP,'.')
hold on
plot(WT_WP,'.r')

PAR_gf_WP=Metdata.PAR(1:60816);
figure
plot(PAR_gf_WP,'.')

WP_gpp_ANNnight_cum=data.gpp_ANNnight(1:60816);
WP_gpp_ANNnight_cum=cumsum(WP_gpp_ANNnight_cum);
figure
plot(WP_gpp_ANNnight_cum,'.')
%compute LAI for WP
GCC_WP=Metdata.GCC(1:60816);
figure
plot(GCC_WP,'.g')
%first make a smooth GCC
[GCC_WP,Yearsmooth,DOYsmooth,GCCsmooth_WP_1] = smoothgcc_Oikawa(GCC_WP,decday_WP,...
    data.year(1:60816),DOY_WP,0,3,'90percentile',1);
figure
plot(decday_WP, GCC_WP,'.','MarkerSize',14);
    hold on
    plot(DOYsmooth, GCCsmooth_WP_1,'.','MarkerSize',20);
    legend('gcc','gccsmooth')
%convert gcc smooth into longer time frame

GCCsmooth_WP=DOY_WP*0;
Sel=GCCsmooth_WP==0;
GCCsmooth_WP(Sel)=NaN;

    for i=1:length(DOYsmooth)
    Sel=find(DOYsmooth(i)==DOY_WP);
        GCCsmooth_WP(Sel)=GCCsmooth_WP_1(i);
    end
     Sel=GCCsmooth_WP<0.3;
     GCCsmooth_WP(Sel)=NaN;
    [GCCsmooth_WP] = FillSmallGaps(GCCsmooth_WP,100000);
    GCCsmooth_WP(60721:end)=0.3308;
    GCCsmooth_WP(1:48)=0.4363;

figure
plot(decday_WP,GCCsmooth_WP,'.')
hold on
plot(decday_WP,GCC_WP,'.r')


LAI_WP=0.0037*exp(GCCsmooth_WP*17.06);
LAI_gf_WP=FillSmallGaps(LAI_WP,10000);

figure
plot(LAI_gf_WP,'.b')
hold on
plot(LAI_WP,'.r')

TA_WP=data.TA(1:60816);
% figure
% plot(TA_WP,'r.')
%% calculate 90%CI for ANN
%create 20 datasets for GPP ANN--apparently not in data structure

for i=1:length(data.decday)
    
gpp_ANNext_WP(i,:)=data.wc_gf(i)-data.er_ANNext(i,:);
er_ANN_mean_WP(i)=nanmean(data.er_ANNext(i,:));
er_ANN_SD_WP(i)=nanstd(data.er_ANNext(i,:));
gpp_ANN_mean_WP(i)=nanmean(gpp_ANNext_WP(i,:));
gpp_ANN_SD_WP(i)=nanstd(gpp_ANNext_WP(i,:));
wm_ANN_mean_WP(i)=nanmean(data.wm_ANNext(i,:));
wm_ANN_SD_WP(i)=nanstd(data.wm_ANNext(i,:));

end
gf_error_er_WP=1.645*(er_ANN_SD_WP./sqrt(20));
gf_error_gpp_WP=1.645*(gpp_ANN_SD_WP./sqrt(20));
gf_error_wm_WP=1.645*(wm_ANN_SD_WP./sqrt(20));
lower_er_ANN_WP=er_ANN_mean_WP-(1.645*(er_ANN_SD_WP./sqrt(20)));%use 1.645 for 90% CI
upper_er_ANN_WP=er_ANN_mean_WP+(1.645*(er_ANN_SD_WP./sqrt(20)));
lower_gpp_ANN_WP=gpp_ANN_mean_WP-(1.645*(gpp_ANN_SD_WP./sqrt(20)));%use 1.645 for 90% CI
upper_gpp_ANN_WP=gpp_ANN_mean_WP+(1.645*(gpp_ANN_SD_WP./sqrt(20)));

figure
ciplot(lower_er_ANN_WP,upper_er_ANN_WP,data.decday,'r')
figure
ciplot(lower_gpp_ANN_WP,upper_gpp_ANN_WP,data.decday,'k')

%select only 2012-2015
lower_er_ANN_WP=lower_er_ANN_WP(1:60816);
upper_er_ANN_WP=upper_er_ANN_WP(1:60816);
lower_gpp_ANN_WP=lower_gpp_ANN_WP(1:60816);
upper_gpp_ANN_WP=upper_gpp_ANN_WP(1:60816);
gf_error_er_WP=gf_error_er_WP(1:60816);
gf_error_gpp_WP=gf_error_gpp_WP(1:60816);
gf_error_wm_WP=gf_error_wm_WP(1:60816);
%gf error same for er and gpp
figure
plot(gf_error_er_WP,'.')
hold on
plot(gf_error_gpp_WP,'g.')
figure
plot(gf_error_wm_WP,'.r')
%Random error 
envir_var=[data.ubar(1:60816) PAR_gf_WP TA_WP];
var_num=3;
range=[1 75 3];
[wc_RE_WP,~,~,~,wc_p_pos_WP,wc_p_neg_WP]=daily_diff(DOY_WP,data.time(1:60816),wc_gf_WP,var_num,envir_var,range);
figure
plot(wc_RE_WP,'.')
wc_RE_high_WP=wc_gf_WP+wc_RE_WP';
wc_RE_low_WP=wc_gf_WP-wc_RE_WP';
figure
ciplot(wc_RE_low_WP,wc_RE_high_WP,decday_WP)

envir_var=[data.ubar(1:60816) PAR_gf_WP TA_WP];
var_num=3;
range=[1 75 3];
[wm_RE_WP,~,~,~,wm_p_pos_WP,wm_p_neg_WP]=daily_diff(DOY_WP,data.time(1:60816),wm_gf_WP,var_num,envir_var,range);
figure
plot(wm_RE_WP,'.')
wm_RE_high_WP=wm_gf_WP+wm_RE_WP';
wm_RE_low_WP=wm_gf_WP-wm_RE_WP';
figure
ciplot(wm_RE_low_WP,wm_RE_high_WP,decday_WP)

% need to add Random error to the 90% CI gf error at 30min time step
%where during gaps, have gapfilling error+random error
%in non-gaps, only random error

wm_0=data.wm(1:60816);
wc_0=data.wc(1:60816);
Sel=isnan(wm_0);
wm_0(Sel)=0;
Sel=isnan(wc_0);
wc_0(Sel)=0;

for i=1:length(wm_0)
    if wm_0(i)==0%if there is a gap, gf error and random error applies
        total_error_wm_WP(i)=wm_RE_WP(i)+gf_error_wm_WP(i);
    else %without gap, only Random error applies
        total_error_wm_WP(i)=wm_RE_WP(i);
    end
    if wc_0(i)==0%if there is a gap, gf error and random error applies
        total_error_wc_WP(i)=wc_RE_WP(i)+gf_error_er_WP(i);
    else %without gap, only Random error applies
        total_error_wc_WP(i)=wc_RE_WP(i);
    end
end

figure
plot(decday_WP,wm_gf_WP,'g',decday_WP,wm_gf_WP-total_error_wm_WP','r',decday_WP,wm_gf_WP+total_error_wm_WP','k')
hold on
plot(decday_WP,wm_0)
legend('wm gf','lower gf','upper gf','wm')

figure
plot(decday_WP,wc_gf_WP,'g',decday_WP,wc_gf_WP-total_error_wc_WP','r',decday_WP,wc_gf_WP+total_error_wc_WP','k')
hold on
plot(decday_WP,wc_0)
legend('wc gf','lower gf','upper gf','wc')

wm_gf_WP_lower=wm_gf_WP-total_error_wm_WP';
wm_gf_WP_upper=wm_gf_WP+total_error_wm_WP';
wc_gf_WP_lower=wc_gf_WP-total_error_wc_WP';
wc_gf_WP_upper=wc_gf_WP+total_error_wc_WP';

%% convert to daily variables
%First take output from CO2 model and convert to daily averages
days_WP = 195:1:1461;%NWPDS TO BE UPDATED!!--all data

days_WP=days_WP-194';%b/c my code likes to start from 1, shift data temporarily so they start at
DOY_WP=DOY_WP-194;
%first do daily averages
[daily_DOY_WP,daily_DOY_disc_WP]=daily_ave(DOY_WP,DOY_disc_WP,days_WP);%time is 30min time scale, days is DOY
figure
plot(daily_DOY_WP,daily_DOY_disc_WP,'.')

[daily_DOY_WP,daily_TA_WP]=daily_ave(DOY_WP,TA_WP,days_WP);%time is 30min time scale, days is DOY
figure
plot(DOY_WP,TA_WP,'.r')
hold on
plot(daily_DOY_WP,daily_TA_WP,'.')

[daily_DOY_WP,daily_WT_gf_WP]=daily_ave(DOY_WP,WT_gf_WP,days_WP);%time is 30min time scale, days is DOY
figure
plot(DOY_WP,WT_gf_WP,'.')
hold on
plot(daily_DOY_WP,daily_WT_gf_WP,'.r')

[daily_DOY_WP,daily_PAR_gf_WP]=daily_ave(DOY_WP,PAR_gf_WP,days_WP);%time is 30min time scale, days is DOY
figure
plot(DOY_WP,PAR_gf_WP,'.')
hold on
plot(daily_DOY_WP,daily_PAR_gf_WP,'.r')

%Wetland age parameter
daily_wetland_age_WP=daily_PAR_gf_WP*0+15;
daily_wetland_age_WP(173:end)=daily_wetland_age_WP(173:end)+1;
daily_wetland_age_WP(903:end)=daily_wetland_age_WP(903:end)+1;
daily_wetland_age_WP(903:end)=daily_wetland_age_WP(903:end)+1;
figure
plot(daily_DOY_WP,daily_wetland_age_WP,'.')

%now do integrals
daily_wm_WP=wm_gf_WP*60*30*0.001;%nmol to umol m-2 30min-1
[daily_wm_sum_umol_gf_WP]=daily_sum(DOY_WP,daily_wm_WP,days_WP);%time is 30min time scale, days is DOY
figure
plot(DOY_WP,daily_wm_WP,'.')
hold on
plot(daily_DOY_WP,daily_wm_sum_umol_gf_WP,'.r')


daily_NEE_obs_WP=data.wc_gf(1:60816)*60*30;%first turn NWP from umol m-2 s-1 into umol m-2 30min-1
[daily_NEE_obs_WP]=daily_sum(DOY_WP,daily_NEE_obs_WP,days_WP);

daily_GPP_obs_WP=data.gpp_ANNnight(1:60816)*60*30;%first turn NWP from umol m-2 s-1 into umol m-2 30min-1
[daily_GPP_obs_WP]=daily_sum(DOY_WP,daily_GPP_obs_WP,days_WP);

daily_Reco_obs_WP=data.er_ANNnight(1:60816)*60*30;%first turn NWP from umol m-2 s-1 into umol m-2 30min-1
[daily_Reco_obs_WP]=daily_sum(DOY_WP,daily_Reco_obs_WP,days_WP);

daily_gf_error_wc_WP=gf_error_er_WP'*60*30;%first turn NWP from umol m-2 s-1 into umol m-2 30min-1
[daily_gf_error_wc_WP]=daily_sum(DOY_WP,daily_gf_error_wc_WP,days_WP);

daily_random_error_wc_WP=wc_RE_WP'*60*30;%first turn NWP from umol m-2 s-1 into umol m-2 30min-1
[daily_random_error_wc_WP]=daily_sum(DOY_WP,daily_random_error_wc_WP,days_WP);

daily_gf_error_wm_WP=gf_error_wm_WP'*60*30*0.001;%first turn NWP from umol m-2 s-1 into umol m-2 30min-1
[daily_gf_error_wm_WP]=daily_sum(DOY_WP,daily_gf_error_wm_WP,days_WP);

daily_random_error_wm_WP=wm_RE_WP'*60*30*0.001;%first turn NWP from umol m-2 s-1 into umol m-2 30min-1
[daily_random_error_wm_WP]=daily_sum(DOY_WP,daily_random_error_wm_WP,days_WP);

daily_wm_gf_WP_lower=wm_gf_WP_lower*60*30*0.001;%first turn NMB from umol m-2 s-1 into umol m-2 30min-1
[daily_wm_gf_WP_lower]=daily_sum(DOY_WP,daily_wm_gf_WP_lower,days_WP);

daily_wm_gf_WP_upper=wm_gf_WP_upper*60*30*0.001;%first turn NMB from umol m-2 s-1 into umol m-2 30min-1
[daily_wm_gf_WP_upper]=daily_sum(DOY_WP,daily_wm_gf_WP_upper,days_WP);

daily_wc_gf_WP_lower=wc_gf_WP_lower*60*30;%first turn NMB from umol m-2 s-1 into umol m-2 30min-1
[daily_wc_gf_WP_lower]=daily_sum(DOY_WP,daily_wc_gf_WP_lower,days_WP);

daily_wc_gf_WP_upper=wc_gf_WP_upper*60*30;%first turn NMB from umol m-2 s-1 into umol m-2 30min-1
[daily_wc_gf_WP_upper]=daily_sum(DOY_WP,daily_wc_gf_WP_upper,days_WP);

figure
ciplot(daily_wm_gf_WP_lower,daily_wm_gf_WP_upper,daily_DOY_WP)
figure
ciplot(daily_wc_gf_WP_lower,daily_wc_gf_WP_upper,daily_DOY_WP)

figure
plot(daily_DOY_WP,daily_NEE_obs_WP,'.',daily_DOY_WP,daily_GPP_obs_WP,'.',daily_DOY_WP,daily_Reco_obs_WP,'.')
legend('nee','gpp','reco')
figure
subplot(2,1,1)
plot(daily_DOY_WP,daily_gf_error_wc_WP,'.',daily_DOY_WP,daily_random_error_wc_WP,'.')
legend('gf','re')
subplot(2,1,2)
plot(daily_DOY_WP,daily_gf_error_wm_WP,'.',daily_DOY_WP,daily_random_error_wm_WP,'.')
legend('gf','re')

DOY_WP=DOY_WP+194;



%% combine data MB + WP
%param is MB 2011+2012+2013+2014 WP 2012, 2013, 2014
%valid is MB 2015 and WP 2015
%save MB 2011+2012 for test on early years

%half hourly variables
DOY_WP=DOY_WP-194;
DOY = vertcat(DOY_MB,DOY_WP);
DOY(87649:end) = DOY(87649:end)+1826;
DOY_param = vertcat(DOY_MB(1:70128),DOY_WP(1:43296)+(1461));%MB 2011-2013 WP 2012-2013
DOY_valid = vertcat(DOY_MB(70129:end)-1096,DOY_WP(43297:end)-(1097-731));%MB 2014-2015 WP 2014-2015

decday =vertcat(decday_MB,decday_WP);
decday(87649:end) = decday(87649:end)+(2192-195);
decday_param =vertcat(decday_MB(1:70128),decday_WP(1:43296)+(1827-195));
decday_valid =vertcat(decday_MB(70129:end),decday_WP(43297:end)+(2192-1097));

DOY_disc = vertcat(DOY_disc_MB,DOY_disc_WP);
DOY_disc_param = vertcat(DOY_disc_MB(1:70128),DOY_disc_WP(1:43296));
DOY_disc_valid = vertcat(DOY_disc_MB(70129:end),DOY_disc_WP(43297:end));

TA =vertcat(TA_MB,TA_WP);
TA_param =vertcat(TA_MB(1:70128),TA_WP(1:43296));
TA_valid =vertcat(TA_MB(70129:end),TA_WP(43297:end));

WT_gf =vertcat(WT_gf_MB,WT_gf_WP);
WT_gf_param =vertcat(WT_gf_MB(1:70128),WT_gf_WP(1:43296));
WT_gf_valid =vertcat(WT_gf_MB(70129:end),WT_gf_WP(43297:end));

PAR_gf =vertcat(PAR_gf_MB,PAR_gf_WP);
PAR_gf_param =vertcat(PAR_gf_MB(1:70128),PAR_gf_WP(1:43296));
PAR_gf_valid =vertcat(PAR_gf_MB(70129:end),PAR_gf_WP(43297:end));

LAI_gf =vertcat(LAI_gf_MB,LAI_gf_WP);
LAI_gf_param =vertcat(LAI_gf_MB(1:70128),LAI_gf_WP(1:43296));
LAI_gf_valid =vertcat(LAI_gf_MB(70129:end),LAI_gf_WP(43297:end));

Season_drop =vertcat(Season_drop_MB,Season_drop_WP);
Season_drop_param =vertcat(Season_drop_MB(1:70128),Season_drop_WP(1:43296));
Season_drop_valid =vertcat(Season_drop_MB(70129:end),Season_drop_WP(43297:end));

wc_90CI =vertcat(total_error_wc_MB',total_error_wc_WP');
wc_90CI_param =vertcat(total_error_wc_MB(1:70128)',total_error_wc_WP(1:43296)');
wc_90CI_valid =vertcat(total_error_wc_MB(70129:end)',total_error_wc_WP(43297:end)');

wc_90CI_lower =vertcat(wc_gf_MB_lower,wc_gf_WP_lower);
wc_90CI_lower_param =vertcat(wc_gf_MB_lower(1:70128),wc_gf_WP_lower(1:43296));
wc_90CI_lower_valid =vertcat(wc_gf_MB_lower(70129:end),wc_gf_WP_lower(43297:end));

wc_90CI_upper =vertcat(wc_gf_MB_upper,wc_gf_WP_upper);
wc_90CI_upper_param =vertcat(wc_gf_MB_upper(1:70128),wc_gf_WP_upper(1:43296));
wc_90CI_upper_valid =vertcat(wc_gf_MB_upper(70129:end),wc_gf_WP_upper(43297:end));

wm_90CI =vertcat(total_error_wm_MB',total_error_wm_WP');
wm_90CI_param =vertcat(total_error_wm_MB(1:70128)',total_error_wm_WP(1:43296)');
wm_90CI_valid =vertcat(total_error_wm_MB(70129:end)',total_error_wm_WP(43297:end)');

wm_90CI_lower =vertcat(wm_gf_MB_lower,wm_gf_WP_lower);
wm_90CI_lower_param =vertcat(wm_gf_MB_lower(1:70128),wm_gf_WP_lower(1:43296));
wm_90CI_lower_valid =vertcat(wm_gf_MB_lower(70129:end),wm_gf_WP_lower(43297:end));

wm_90CI_upper =vertcat(wm_gf_MB_upper,wm_gf_WP_upper);
wm_90CI_upper_param =vertcat(wm_gf_MB_upper(1:70128),wm_gf_WP_upper(1:43296));
wm_90CI_upper_valid =vertcat(wm_gf_MB_upper(70129:end),wm_gf_WP_upper(43297:end));

wc =vertcat(wc_MB,wc_WP);
wc_param =vertcat(wc_MB(1:70128),wc_WP(1:43296));
wc_valid =vertcat(wc_MB(70129:end),wc_WP(43297:end));

wc_gf =vertcat(wc_gf_MB,wc_gf_WP);
wc_gf_param =vertcat(wc_gf_MB(1:70128),wc_gf_WP(1:43296));
wc_gf_valid =vertcat(wc_gf_MB(70129:end),wc_gf_WP(43297:end));

wc_gf_lower =vertcat(wc_gf_MB_lower,wc_gf_WP_lower);
wc_gf_lower_param =vertcat(wc_gf_MB_lower(1:70128),wc_gf_WP_lower(1:43296));
wc_gf_lower_valid =vertcat(wc_gf_MB_lower(70129:end),wc_gf_WP_lower(43297:end));

wc_gf_upper =vertcat(wc_gf_MB_upper,wc_gf_WP_upper);
wc_gf_upper_param =vertcat(wc_gf_MB_upper(1:70128),wc_gf_WP_upper(1:43296));
wc_gf_upper_valid =vertcat(wc_gf_MB_upper(70129:end),wc_gf_WP_upper(43297:end));


wm =vertcat(wm_MB,wm_WP);
wm_param =vertcat(wm_MB(1:70128),wm_WP(1:43296));
wm_valid =vertcat(wm_MB(70129:end),wm_WP(43297:end));

wm_gf =vertcat(wm_gf_MB,wm_gf_WP);
wm_gf_param =vertcat(wm_gf_MB(1:70128),wm_gf_WP(1:43296));
wm_gf_valid =vertcat(wm_gf_MB(70129:end),wm_gf_WP(43297:end));

gpp_ANNnight =vertcat(gpp_ANNnight_MB,gpp_ANNnight_WP);
gpp_ANNnight_param =vertcat(gpp_ANNnight_MB(1:70128),gpp_ANNnight_WP(1:43296));
gpp_ANNnight_valid =vertcat(gpp_ANNnight_MB(70129:end),gpp_ANNnight_WP(43297:end));

er_ANNnight =vertcat(er_ANNnight_MB,er_ANNnight_WP);
er_ANNnight_param =vertcat(er_ANNnight_MB(1:70128),er_ANNnight_WP(1:43296));
er_ANNnight_valid =vertcat(er_ANNnight_MB(70129:end),er_ANNnight_WP(43297:end));

wetland_age =vertcat(wetland_age_MB,wetland_age_WP);
wetland_age_param =vertcat(wetland_age_MB(1:70128),wetland_age_WP(1:43296));
wetland_age_valid =vertcat(wetland_age_MB(70129:end),wetland_age_WP(43297:end));

GCCsmooth =vertcat(GCCsmooth_MB,GCCsmooth_WP);
GCCsmooth_param =vertcat(GCCsmooth_MB(1:70128),GCCsmooth_WP(1:43296));
GCCsmooth_valid =vertcat(GCCsmooth_MB(70129:end),GCCsmooth_WP(43297:end));

%create site variable - indicates to model when switching to new site
site_MB=er_ANNnight_MB*0+1;
site_WP=er_ANNnight_WP*0+2;
site =vertcat(site_MB,site_WP);
site_param =vertcat(site_MB(1:70128),site_WP(1:43296));
site_valid =vertcat(site_MB(70129:end),site_WP(43297:end));

%daily variables
daily_wm_sum_umol_gf =vertcat(daily_wm_sum_umol_gf_MB',daily_wm_sum_umol_gf_WP');
daily_wm_sum_umol_gf_param =vertcat(daily_wm_sum_umol_gf_MB(1:1461)',daily_wm_sum_umol_gf_WP(1:902)');
daily_wm_sum_umol_gf_valid =vertcat(daily_wm_sum_umol_gf_MB(1462:end)',daily_wm_sum_umol_gf_WP(903:end)');

%daily_gpp_min =vertcat(daily_gppmin);
daily_gapfilling_error_wm =vertcat(daily_gf_error_wm_MB',daily_gf_error_wm_WP');
daily_gapfilling_error_wm_param =vertcat(daily_gf_error_wm_MB(1:1461)',daily_gf_error_wm_WP(1:902)');
daily_gapfilling_error_wm_valid =vertcat(daily_gf_error_wm_MB(1462:end)',daily_gf_error_wm_WP(903:end)');

daily_random_error_wm =vertcat(daily_random_error_wm_MB',daily_random_error_wm_WP');
daily_random_error_wm_param =vertcat(daily_random_error_wm_MB(1:1461)',daily_random_error_wm_WP(1:902)');
daily_random_error_wm_valid =vertcat(daily_random_error_wm_MB(1462:end)',daily_random_error_wm_WP(903:end)');

daily_gapfilling_error_wc =vertcat(daily_gf_error_wc_MB',daily_gf_error_wc_WP');
daily_gapfilling_error_wc_param =vertcat(daily_gf_error_wc_MB(1:1461)',daily_gf_error_wc_WP(1:902)');
daily_gapfilling_error_wc_valid =vertcat(daily_gf_error_wc_MB(1462:end)',daily_gf_error_wc_WP(903:end)');

daily_random_error_wc =vertcat(daily_random_error_wc_MB',daily_random_error_wc_WP');
daily_random_error_wc_param =vertcat(daily_random_error_wc_MB(1:1461)',daily_random_error_wc_WP(1:902)');
daily_random_error_wc_valid =vertcat(daily_random_error_wc_MB(1462:end)',daily_random_error_wc_WP(903:end)');

daily_wc_gf_lower =vertcat(daily_wc_gf_MB_lower',daily_wc_gf_WP_lower');
daily_wc_gf_lower_param =vertcat(daily_wc_gf_MB_lower(1:1461)',daily_wc_gf_WP_lower(1:902)');
daily_wc_gf_lower_valid =vertcat(daily_wc_gf_MB_lower(1462:end)',daily_wc_gf_WP_lower(903:end)');

daily_wc_gf_upper =vertcat(daily_wc_gf_MB_upper',daily_wc_gf_WP_upper');
daily_wc_gf_upper_param =vertcat(daily_wc_gf_MB_upper(1:1461)',daily_wc_gf_WP_upper(1:902)');
daily_wc_gf_upper_valid =vertcat(daily_wc_gf_MB_upper(1462:end)',daily_wc_gf_WP_upper(903:end)');

daily_wm_gf_lower =vertcat(daily_wm_gf_MB_lower',daily_wm_gf_WP_lower');
daily_wm_gf_lower_param =vertcat(daily_wm_gf_MB_lower(1:1461)',daily_wm_gf_WP_lower(1:902)');
daily_wm_gf_lower_valid =vertcat(daily_wm_gf_MB_lower(1462:end)',daily_wm_gf_WP_lower(903:end)');

daily_wm_gf_upper =vertcat(daily_wm_gf_MB_upper',daily_wm_gf_WP_upper');
daily_wm_gf_upper_param =vertcat(daily_wm_gf_MB_upper(1:1461)',daily_wm_gf_WP_upper(1:902)');
daily_wm_gf_upper_valid =vertcat(daily_wm_gf_MB_upper(1462:end)',daily_wm_gf_WP_upper(903:end)');


daily_DOY =vertcat(daily_DOY_MB',daily_DOY_WP');
daily_DOY(1827:end)=daily_DOY(1827:end)+1826;
daily_DOY_param =vertcat(daily_DOY_MB(1:1461)',daily_DOY_WP(1:902)');
daily_DOY_param(1462:end)=daily_DOY_param(1462:end)+1461;
daily_DOY_valid =vertcat(daily_DOY_MB(1462:end)',daily_DOY_WP(903:end)');
daily_DOY_valid(366:end)=daily_DOY_valid(366:end)+(1826-902);

daily_DOY_disc =vertcat(daily_DOY_disc_MB',daily_DOY_disc_WP');
daily_DOY_disc_param =vertcat(daily_DOY_disc_MB(1:1461)',daily_DOY_disc_WP(1:902)');
daily_DOY_disc_valid =vertcat(daily_DOY_disc_MB(1462:end)',daily_DOY_disc_WP(903:end)');

daily_TA =vertcat(daily_TA_MB',daily_TA_WP');
daily_TA_param =vertcat(daily_TA_MB(1:1461)',daily_TA_WP(1:902)');
daily_TA_valid =vertcat(daily_TA_MB(1462:end)',daily_TA_WP(903:end)');

daily_WT_gf =vertcat(daily_WT_gf_MB',daily_WT_gf_WP');
daily_WT_gf_param =vertcat(daily_WT_gf_MB(1:1461)',daily_WT_gf_WP(1:902)');
daily_WT_gf_valid =vertcat(daily_WT_gf_MB(1462:end)',daily_WT_gf_WP(903:end)');

daily_PAR_gf =vertcat(daily_PAR_gf_MB',daily_PAR_gf_WP');
daily_PAR_gf_param =vertcat(daily_PAR_gf_MB(1:1461)',daily_PAR_gf_WP(1:902)');
daily_PAR_gf_valid =vertcat(daily_PAR_gf_MB(1462:end)',daily_PAR_gf_WP(903:end)');

daily_wetland_age =vertcat(daily_wetland_age_MB',daily_wetland_age_WP');
daily_wetland_age_param =vertcat(daily_wetland_age_MB(1:1461)',daily_wetland_age_WP(1:902)');
daily_wetland_age_valid =vertcat(daily_wetland_age_MB(1462:end)',daily_wetland_age_WP(903:end)');

daily_GPP_obs =vertcat(daily_GPP_obs_MB',daily_GPP_obs_WP');
daily_GPP_obs_param =vertcat(daily_GPP_obs_MB(1:1461)',daily_GPP_obs_WP(1:902)');
daily_GPP_obs_valid =vertcat(daily_GPP_obs_MB(1462:end)',daily_GPP_obs_WP(903:end)');

daily_Reco_obs =vertcat(daily_Reco_obs_MB',daily_Reco_obs_WP');
daily_Reco_obs_param =vertcat(daily_Reco_obs_MB(1:1461)',daily_Reco_obs_WP(1:902)');
daily_Reco_obs_valid =vertcat(daily_Reco_obs_MB(1462:end)',daily_Reco_obs_WP(903:end)');


%when you have modeled data--run remaining to add to base 

%use GPP_mod_min from output of GPP code where it is for all data
GPP_mod_min=GPP_mod_min';
 GPP_min_MB=GPP_mod_min(1:87648);
 GPP_min_WP=GPP_mod_min(87649:end);
GPP_min_param =vertcat(GPP_min_MB(1:70128),GPP_min_WP(1:43296));
GPP_min_valid =vertcat(GPP_min_MB(70129:end),GPP_min_WP(43297:end));

days=1:1:3093;
daily_gpp_min_0=GPP_mod_min*-1*60*30;%first turn gpp from umol m-2 s-1 into umol m-2 30min-1
[daily_gpp_min]=daily_sum(DOY,daily_gpp_min_0,days);
daily_gpp_min=daily_gpp_min';
daily_gpp_min_MB=daily_gpp_min(1:1826);
daily_gpp_min_WP=daily_gpp_min(1827:end);
daily_gpp_min_param =vertcat(daily_gpp_min_MB(1:1461),daily_gpp_min_WP(1:902));
daily_gpp_min_valid =vertcat(daily_gpp_min_MB(1462:end),daily_gpp_min_WP(903:end));

days=1:1:3093;
%using Reco and CH4 modeling results...
NEE_mod_min=NEE_mod_min';
 NEE_min_MB=NEE_mod_min(1:87648);
 NEE_min_WP=NEE_mod_min(87649:end);
NEE_min_param =vertcat(NEE_min_MB(1:70128),NEE_min_WP(1:43296));
NEE_min_valid =vertcat(NEE_min_MB(70129:end),NEE_min_WP(43297:end));

Reco_mod_min=Reco_mod_min';
 Reco_min_MB=Reco_mod_min(1:87648);
 Reco_min_WP=Reco_mod_min(87649:end);
Reco_min_param =vertcat(Reco_min_MB(1:70128),Reco_min_WP(1:43296));
Reco_min_valid =vertcat(Reco_min_MB(70129:end),Reco_min_WP(43297:end));

S1=S1';
 S1_min_MB=S1(1:87648);
 S1_min_WP=S1(87649:end);
S1_min_param =vertcat(S1_min_MB(1:70128),S1_min_WP(1:43296));
S1_min_valid =vertcat(S1_min_MB(70129:end),S1_min_WP(43297:end));

S2=S2';
 S2_min_MB=S2(1:87648);
 S2_min_WP=S2(87649:end);
S2_min_param =vertcat(S2_min_MB(1:70128),S2_min_WP(1:43296));
S2_min_valid =vertcat(S2_min_MB(70129:end),S2_min_WP(43297:end));



daily_S1_0=S1';%already in cummulative umol m-2
[daily_S1]=daily_sum(DOY,daily_S1_0,days);%time is 30min time scale, days is DOY
figure
plot(DOY,S1')
hold on
plot(days,daily_S1)
daily_S2_0=S2';%already in cummulative umol m-2 
[daily_S2]=daily_sum(DOY,daily_S2_0,days);%time is 30min time scale, days is DOY

daily_NEE_0=NEE_mod_min*60*30;%first turn NMB from umol m-2 s-1 into umol m-2 30min-1
[daily_NEE_min]=daily_sum(DOY,daily_NEE_0',days);

daily_Reco_0=Reco_mod_min*60*30;%first turn NMB from umol m-2 s-1 into umol m-2 30min-1
[daily_Reco_min]=daily_sum(DOY,daily_Reco_0',days);

daily_S1_MB=daily_S1(1:1826);
daily_S1_WP=daily_S1(1827:end);
daily_S1_param =vertcat(daily_S1_MB(1:1461)',daily_S1_WP(1:902)');
daily_S1_valid =vertcat(daily_S1_MB(1462:end)',daily_S1_WP(903:end)');

daily_S2_MB=daily_S2(1:1826);
daily_S2_WP=daily_S2(1827:end);
daily_S2_param =vertcat(daily_S2_MB(1:1461)',daily_S2_WP(1:902)');
daily_S2_valid =vertcat(daily_S2_MB(1462:end)',daily_S2_WP(903:end)');

daily_Reco_min_MB=daily_Reco_min(1:1826);
daily_Reco_min_WP=daily_Reco_min(1827:end);
daily_Reco_min_param =vertcat(daily_Reco_min_MB(1:1461)',daily_Reco_min_WP(1:902)');
daily_Reco_min_valid =vertcat(daily_Reco_min_MB(1462:end)',daily_Reco_min_WP(903:end)');

daily_NEE_min_MB=daily_NEE_min(1:1826);
daily_NEE_min_WP=daily_NEE_min(1827:end);
daily_NEE_min_param =vertcat(daily_NEE_min_MB(1:1461)',daily_NEE_min_WP(1:902)');
daily_NEE_min_valid =vertcat(daily_NEE_min_MB(1462:end)',daily_NEE_min_WP(903:end)');

CH4_mod_min=ch4_min';
CH4_mod_min_MB=CH4_mod_min(1:1826);
CH4_mod_min_WP=CH4_mod_min(1827:end);
CH4_mod_min_param =vertcat(CH4_mod_min_MB(1:1461),CH4_mod_min_WP(1:902));
CH4_mod_min_valid =vertcat(CH4_mod_min_MB(1462:end),CH4_mod_min_WP(903:end));

CH4_mod_min_lower=lower_ch4';
CH4_mod_min_MB_lower=CH4_mod_min_lower(1:1826);
CH4_mod_min_WP_lower=CH4_mod_min_lower(1827:end);
CH4_mod_min_param_lower =vertcat(CH4_mod_min_MB_lower(1:1461),CH4_mod_min_WP_lower(1:902));
CH4_mod_min_valid_lower =vertcat(CH4_mod_min_MB_lower(1462:end),CH4_mod_min_WP_lower(903:end));

CH4_mod_min_upper=upper_ch4';
CH4_mod_min_MB_upper=CH4_mod_min_upper(1:1826);
CH4_mod_min_WP_upper=CH4_mod_min_upper(1827:end);
CH4_mod_min_param_upper =vertcat(CH4_mod_min_MB_upper(1:1461),CH4_mod_min_WP_upper(1:902));
CH4_mod_min_valid_upper =vertcat(CH4_mod_min_MB_upper(1462:end),CH4_mod_min_WP_upper(903:end));

%when you have modeled data--run this to get daily integrals


%Data you need to run all codes
data_master.daily=[daily_wm_sum_umol_gf daily_gapfilling_error_wm daily_random_error_wm];
data_master.halfhr=[DOY DOY_disc TA WT_gf PAR_gf LAI_gf GPP_min decday Season_drop site];
data_master.halfhr_CI=[wc_90CI wc_90CI_lower wc_90CI_upper wm_90CI wm_90CI_lower wm_90CI_upper];
data_master.halfhr_obs=[wc wc_gf wm wm_gf gpp_ANNnight er_ANNnight];



