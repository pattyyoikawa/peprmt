function ss = PEPRMT_final_ss_CO2_Reco(theta,data)
% algae sum-of-squares function
%xdata= [DOY diel_t diel_wt diel_sum_gpp];
%GPPsum=diel_sum_gpp;
%ydata = [DOY diel_wm];%state variable
% data.ydata=ydata;
% data.xdata=xdata;
time   = data.ydata(:,1);
ydata  = data.ydata(:,2);%NEE
%ydata_m = data.ydata(:,3);%CH4 exchange
xdata  = data.xdata;

% y0=0;
%ymodel = DAMMCH4fun(time,theta,y0,xdata);
[ymodel, S1, S2, Reco] = PEPRMT_final_sys_CO2_Reco(theta,xdata);

%errors pre-loaded now
 %random_error=1.05-(0.043*ydata);%for NEE at West Pond
 %gapfilling_error=xdata(:,10);
 
 %looking at total error
%  total_error=random_error+gapfilling_error;
%  lower=ydata-total_error;
%  upper=ydata+total_error;
% figure
% plot(time,ydata,'.',time,upper,'.',time,lower,'.')
% legend('data','upper','lower')

%convert to daily sums for data-model mismatch comparison
%this allows variance to be normally distributed and allows you to use a
%simple cost function here (Richardson 2010 Oecologia)
time_max=nanmax(time);
time_min=nanmin(time);
days = time_min:1:time_max;%NEEDS TO BE UPDATED EVERY TIME!!
days=days';
%b/c my code likes to start from 1, shift data temporarily so they start at
%1
days=days-time_min+1;
DOY_daily=time-time_min+1;

zdata=data.zdata;
daily_gapfilling_error = zdata(:,1);
daily_random_error = zdata(:,2);
daily_Reco_obs = zdata(:,3);
daily_GPP_obs = zdata(:,4);

%OLD METHOD
% daily_ymodel_0=ymodel*60*30;%convert NEE from umol m-2 s-1 into umol m-2 30min-1
% daily_ymodel_0=daily_ymodel_0';
% [daily_ymodel]=daily_sum(DOY_daily,daily_ymodel_0,days);
% daily_ymodel=daily_ymodel';
%NEW METHOD

daily_Reco_0=Reco*60*30;%convert NEE from umol m-2 s-1 into umol m-2 30min-1
daily_Reco_0=daily_Reco_0';
[daily_Reco]=daily_sum(DOY_daily,daily_Reco_0,days);
daily_Reco=daily_Reco';
% figure
% plot(daily_Reco)
% hold on
% plot(daily_GPP_obs,'r')
%calculate sample size
nan_obs=sum(isnan(daily_Reco_obs));
n = length(daily_Reco_obs)-nan_obs;

%simple least squares optimization - following Keenan 2011 and 2012
 ss1 = ((daily_Reco_obs-daily_Reco)./(daily_random_error+daily_gapfilling_error)).^2;
 ss = (nansum(ss1))/n;
 
 
end%negative log likelihood function based on Gaussian prob distribution


