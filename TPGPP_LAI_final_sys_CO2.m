% TPGPP-LAImodel alternative Reco model structure

function  Reco = TPGPP_LAI_final_sys_CO2(theta,xdata)

%INPUTS
Time_2 =xdata(:,1);
%DOY_disc_2=xdata(:,2);
%Exogenous Variables
TA_2 = xdata(:,3);
%WT_2 = xdata(:,4);%water table height cm
%PAR_2 = xdata(:,5);
%LAI_2 = xdata(:,6);
GPP_2 = xdata(:,7);%gpp modeled--umol m-2 s-1
%wc_90CI_2=xdata(:,10);
%site_2=xdata(:,11);
wetland_age_2=xdata(:,12);


AirT_K =TA_2+ 274.15;%C to Kelvin
AirT_K=AirT_K';


%Static C allocation theme
GPPsum_avail_2 = (GPP_2*-1*60*30);%umol m-2 30min-1 --give Reco access to all GPP

% figure
% plot((GPP_2*0.5))
% hold on
% plot(GPPsum_avail_2,'r')
%Reco PARAMETERS
Rref=theta(1)+2;%Reco at T=15C when LAI=0
Eo=theta(2)+100;%100
k2=theta(3)+0.00001;%+0.00001

Tref=288.15;%Ref temp = 15C
To=227.13;


%preallocating space
S2sol = zeros(1,length(Time_2));
R2 = zeros(1,length(Time_2));
S2=zeros(1,length(Time_2));
Reco=zeros(1,length(Time_2));
Reco_full=zeros(1,length(Time_2));
C2in=zeros(1,length(Time_2));
percent_reduction=zeros(1,length(Time_2));
percent_enhancement=zeros(1,length(Time_2));



for t = 1:length(Time_2);%at t=1
    
%C allocation
    C2in(t)= GPPsum_avail_2(t);

    if (t == 1)
        S2(t) = C2in(t) ; % Ps C pool-- some initial Ps C lingering in soil + day 1 GPPavail
    else
        S2(t) = S2sol(t-1);%substrate availability based on Ps on time step previous
    end
 %following Migliavacca 2011, Raich 2002, Reichstein 2003
    R2(t) = (Rref+k2*C2in(t))*exp(Eo*((1/(Tref-To))-(1/(AirT_K(t)-To))));
     %Empirical fxn, Reco increases when WT drops
  percent_reduction(t)=(a1*WT_2(t).^2)-(a2*WT_2(t))+a3;
if WT_2(t)>5
    percent_reduction(t)=0.75;
end
    
if percent_reduction(t)>1.25
   percent_reduction(t)=1.25;
end
if percent_reduction(t)<0.75
   percent_reduction(t)=0.75;
end

    
    R2(t) = R2(t)*percent_reduction(t); %umol m2 sec
    
%Empirical factor for elevated Reco during the first 3 yrs following restoration
   if wetland_age_2(t)<4
    percent_enhancement(t)=1.2;
   else
    percent_enhancement(t)=1;
   end
    R2(t) = R2(t)*percent_enhancement(t); %umol m2 sec   


    if (t==1)
        S2sol(t) = (C2in(t)) - (R2(t)*60*30);
    else
        S2sol(t) = (S2sol(t-1)+C2in(t))- (R2(t)*60*30);
    end

    
   Reco(t) = R2(t); 
   Reco_full(t) = R2(t)*60*30; %umol m2 30min
   %GPP_1(t)=GPP_2(t)*-1;%umol m-2 s-1
   %GPP_full(t)= GPP_2(t)*-1*60*30;%umol m-2 30min-1
end
%NEE_mod=(GPP_2*-1)+Reco_1;%umol m-2 s-1

%GPP=GPP_2*-1;%umol m-2 s-1

end