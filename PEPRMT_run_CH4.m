%use for running PEPRMT DAMMCH4 

%investigating how wetland age influences CH4
%control fluxes for WT, GCC, Temp
% daily_wm_sum_umol_gf_control=daily_wm_sum_umol_gf./(daily_WT_gf+daily_GPP_obs+daily_TA);
% figure
% plot(daily_wm_sum_umol_gf_control)

%use daily_gpp_min_param for input to CH4 model
%% 

model.ssfun = @PEPRMT_final_ss_CH4;
model.modelfun = @PEPRMT_final_sys_CH4;

%load paramerterization data

xdata=[daily_DOY_param daily_DOY_disc_param daily_TA_param daily_WT_gf_param daily_PAR_gf_param daily_gpp_min_param daily_S1_param daily_S2_param daily_wetland_age_param];
ydata=[daily_DOY_param daily_wm_sum_umol_gf_param daily_gapfilling_error_wm_param daily_random_error_wm_param];%this has CH4 umol m-2 d-1
theta=[0 1 0 1 0 1];
theta=theta';
mod_ch4 = PEPRMT_final_sys_CH4(theta,xdata);
 figure
 plot(mod_ch4,'.')
 hold on
 plot(daily_wm_sum_umol_gf_param,'.r')

figure
plot(daily_DOY_param,mod_ch4,'.',daily_DOY_param,daily_wm_sum_umol_gf_param,'.')
legend('mod','obs')
%all data
% xdata=[daily_DOY daily_DOY_disc daily_TA daily_WT_gf daily_PAR_gf daily_gpp_min daily_S1' daily_S2' daily_wetland_age];
% mod_ch4 = PEPRMT_final_sys_CH4(find_min,xdata);
%  figure
%  plot(mod_ch4,'.')
%  hold on
%  plot(daily_wm_sum_umol_gf,'.r')
 %valid
% xdata=[daily_DOY_valid daily_DOY_disc_valid daily_TA_valid daily_WT_gf_valid daily_PAR_gf_valid daily_gpp_min_valid daily_S1_valid daily_S2_valid daily_wetland_age_valid];
% mod_ch4_valid = PEPRMT_final_sys_CH4(find_min,xdata);
%  figure
%  plot(mod_ch4_valid,'.')
%  hold on
%  plot(daily_wm_sum_umol_gf_valid,'.r')
% 
%  figure
%  plot(WT_gf_valid)
% 
%% START MDF
data.xdata =xdata;
data.ydata =ydata;

Rsum_obs = data.ydata(:,2);
nan_obs = sum(isnan(Rsum_obs));
n_obs = length(Rsum_obs) - nan_obs;
model.N     = n_obs;%total number of non nan obs
%model.S20 = [1 1 2];%has to do with error variance of prior
%model.N0  = [4 4 4];%has to do with error variance of prior


params = {
    {'M_ea1',        -2,     -15,         -2}
    {'M_km1',        1,     1e-3,        1e3}
    {'M_ea2',        -2,     -15,         -2}
    {'M_km2',        1,     1e-3,        1e3}
    {'M_ea3',        0,     -15,         10}
    {'M_km3',        1,     1e-3,        1e3}
%     name      start/theta/par  lBound   uBound    mean_prior   std_prior
    };

% params = {
%     {'M_ea1',        0,     -10,         10}
%     {'M_ea2',        0,     -10,         10}
%     {'M_ea3',        0,     -10,         10}
%     {'V_trans',      0.24,   0.01,       0.9}
%     {'Oxi_Factor',   0.72,   0.1,        0.95}
% %     name      start/theta/par  lBound   uBound    mean_prior   std_prior
%     };


options.method = 'dram';
options.verbosity  = 0;%level of info printed
options.burnintime = 50000;% burn in before adaptation starts
%[results, chain, s2chain] = mcmcrun(model,data,params,options,results);
% [RESULTS,CHAIN,S2CHAIN,SSCHAIN] = MCMCRUN(MODEL,DATA,PARAMS,OPTIONS)
%First generate an initial chain
options.nsimu = 50000;
[results, chain, s2chain, sschain]= mcmcrun_FINAL(model,data,params,options);

%find the minimum
figure
plot(sschain,'.')%sum-of-squares chains
nanmin(sschain)
Sel=sschain<nanmin(sschain)+0.0000001;%442
figure
plot(Sel)
% cumsum(Sel)
find_min=chain(Sel,:);
find_min=find_min(end,:)';

figure
subplot(3,1,1)
plot(chain(:,1))
subplot(3,1,2)
plot(chain(:,3))
subplot(3,1,3)
plot(chain(:,5))

mod_ch4 = PEPRMT_final_sys_CH4(find_min,xdata);
figure
subplot(2,1,1)
plot(daily_DOY_param,mod_ch4,'.',daily_DOY_param,daily_wm_sum_umol_gf_param,'.')
legend('mod','obs')
subplot(2,1,2)
plot(daily_DOY_param,daily_WT_gf_param)
     figure
     plot(Time_2,WT_2_adj,'.')
     hold on
     plot(Time_2,M_percent_reduction,'.r')

%Then re-run starting from the results of the previous run, this will take about 10 minutes.
params = {
    {'M_ea1',        find_min(1),     -15,         -2}
    {'M_km1',        find_min(2),     1e-3,        1e3}
    {'M_ea2',        find_min(3),     -15,          -2}
    {'M_km2',        find_min(4),     1e-3,        1e3}
    {'M_ea3',        find_min(5),     -15,         10}
    {'M_km3',        find_min(6),     1e-3,        1e3}
%     name      start/theta/par  lBound   uBound    mean_prior   std_prior
    };
 
options.method = 'dram';
options.verbosity  = 0;%level of info printed
options.burnintime = 0;% burn in before adaptation starts
 
options.nsimu = 150000;
[results, chain, s2chain, sschain] = mcmcrun_FINAL(model,data,params,options, results);
 
figure
mcmcplot(chain,[],results,'chainpanel');%plots chain for ea parameter and all the values it accepted
figure
mcmcplot(chain,[],results,'pairs');%tells you if parameters are correlated
%  figure
%  mcmcplot(sqrt(s2chain),[],[],'hist')%take square root of the s2chain to get the chain for error standard deviation
% title('Error std posterior')
figure
mcmcplot(chain,[],results,'denspanel',2);
 
 
chainstats(chain,results)
sschain_long=sschain;%save before you move on
chain_long=chain;
results_long=results;

%find the minimum
figure
plot(sschain_long,'.')%sum-of-squares chains
nanmin(sschain_long)
Sel=sschain_long<nanmin(sschain_long)+0.0001;%442
figure
plot(Sel)
% cumsum(Sel)
find_min=chain_long(Sel,:);
find_min=find_min(end,:)';

CH4_mod_min = PEPRMT_final_sys_CH4(find_min,xdata);
figure
plot(CH4_mod_min)
hold on
plot(daily_wm_sum_umol_gf_param,'r')

%% Choose posterior

%First establish if chain stabilized
chainstats(chain,results)
%Geweke test should be high ideally >0.9 for all parameters

%rejection rate of chain needs to be less than 50%
results_long.rejected*100

%Determine where means and sd of parameters stabilize in the chain
%this tells you what part of chain is stable for selection of posterior

%see where means and sd of parameters stabilize in the chain
%this tells you what part of chain is stable for selection of posterior
window_size = 5000;
simple = tsmovavg(chain_long(:,1),'s',window_size,1);
simple_std = movingstd(chain_long(:,1),5000);
figure
subplot(2,1,1)
plot(simple,'.')
title('5000 iteration moving average EA SOM')
subplot(2,1,2)
plot(simple_std,'.')
title('5000 iteration moving SD EA SOM')

window_size = 5000;
simple = tsmovavg(chain_long(:,2),'s',window_size,1);
simple_std = movingstd(chain_long(:,2),5000);
figure
subplot(2,1,1)
plot(simple,'.')
title('5000 iteration moving average km SOM')
subplot(2,1,2)
plot(simple_std,'.')
title('5000 iteration moving SD km SOM')

window_size = 5000;
simple = tsmovavg(chain_long(:,3),'s',window_size,1);
simple_std = movingstd(chain_long(:,3),5000);
figure
subplot(2,1,1)
plot(simple,'.')
title('5000 iteration moving average EA labile')
subplot(2,1,2)
plot(simple_std,'.')
title('5000 iteration moving SD EA labile')

window_size = 5000;
simple = tsmovavg(chain_long(:,4),'s',window_size,1);
simple_std = movingstd(chain_long(:,4),5000);
figure
subplot(2,1,1)
plot(simple,'.')
title('5000 iteration moving average km labile')
subplot(2,1,2)
plot(simple_std,'.')
title('5000 iteration moving SD km labile')

window_size = 5000;
simple = tsmovavg(chain_long(:,5),'s',window_size,1);
simple_std = movingstd(chain_long(:,5),5000);
figure
subplot(2,1,1)
plot(simple,'.')
title('5000 iteration moving average EA oxi')
subplot(2,1,2)
plot(simple_std,'.')
title('5000 iteration moving SD EA oxi')

window_size = 5000;
simple = tsmovavg(chain_long(:,6),'s',window_size,1);
simple_std = movingstd(chain_long(:,6),5000);
figure
subplot(2,1,1)
plot(simple,'.')
title('5000 iteration moving average km oxi')
subplot(2,1,2)
plot(simple_std,'.')
title('5000 iteration moving SD km oxi')

%identified visually that chain is good around 100,000
%choose parammeters 10,000 parameter sets from that area
%e.g. 100,000-101,000

find_min_10000=chain(140001:150000,:);%update everytime
%now select 1000 unique parameter sets from these 10,000, that are
%distributed across the range
figure
hist(find_min_10000(:,1))
%bin data and sample evenly across bins


%run 10000 parameter sets through all data+ and test chi square
xdata=[daily_DOY_param daily_DOY_disc_param daily_TA_param daily_WT_gf_param daily_PAR_gf_param daily_gpp_min_param daily_wetland_age_param daily_wetland_age_param daily_wetland_age_param];
ydata=[daily_DOY_param daily_wm_sum_umol_gf_param daily_gapfilling_error_wm_param daily_random_error_wm_param];%this has CH4 umol m-2 d-1

%make 1000 theta sets that will be used to estimate CI 
model_pop_ch4=zeros(length(1:10000));%length of validation data
model_pop_ch4=model_pop_ch4(1:2363,:);%update as needed 
 
for i=1:length(find_min_10000)
   ch4mod = PEPRMT_final_sys_CH4(find_min_10000(i,:)',xdata);
    model_pop_ch4(:,i)=ch4mod';
    
end

model_std_ch4=std(model_pop_ch4');

%% according to Zobitz 2008, model mean==model min(best para set)
CH4_mod_min_param = PEPRMT_final_sys_CH4(find_min,xdata);

figure
plot(daily_DOY_param,daily_wm_sum_umol_gf_param,'.',daily_DOY_param,CH4_mod_min_param,'.')
legend('obs','modmin')
 

% 90% confidence intervals (if you want 95% use 1.96)
%b/c the 10,000 parameters are not independent or may even be redundant, I
%reduce the sample size to 1000
CH4_mod_min_param_lower=CH4_mod_min_param-(1.645*(model_std_ch4));
CH4_mod_min_param_upper=CH4_mod_min_param+(1.645*(model_std_ch4));
%      lower_ch4=quantile(model_pop_ch4',0.05);% 90% quantile
%     upper_ch4=quantile(model_pop_ch4',0.95);% 90% quantile

figure
plot(CH4_mod_min_param_lower,'.')
hold on
plot(CH4_mod_min_param_upper,'.')
hold on
plot(CH4_mod_min_param,'r')
%% Run validation data MB WP
%run 10000 parameter sets through all data+ and test chi square
xdata_valid=[daily_DOY_valid daily_DOY_disc_valid daily_TA_valid daily_WT_gf_valid daily_PAR_gf_valid daily_gpp_min_valid daily_wetland_age_valid daily_wetland_age_valid daily_wetland_age_valid];
ydata_valid=[daily_DOY_valid daily_wm_sum_umol_gf_valid daily_gapfilling_error_wm_valid daily_random_error_wm_valid];%this has CH4 umol m-2 d-1

%make 1000 theta sets that will be used to estimate CI 
model_pop_ch4=zeros(length(1:10000));%length of validation data
model_pop_ch4=model_pop_ch4(1:730,:);%update as needed 
 
for i=1:length(find_min_10000)
   ch4mod = PEPRMT_final_sys_CH4(find_min_10000(i,:)',xdata_valid);
    model_pop_ch4(:,i)=ch4mod;
    
end

model_std_ch4_valid=std(model_pop_ch4');


%% according to Zobitz 2008, model mean==model min(best para set)
CH4_mod_min_valid = PEPRMT_final_sys_CH4(find_min,xdata_valid);

figure
plot(daily_DOY_valid,daily_wm_sum_umol_gf_valid,'.',daily_DOY_valid,CH4_mod_min_valid,'.')
legend('obs','modmin')

% 90% confidence intervals
%CH4_mod_min=CH4_mod_initial;
CH4_mod_min_valid_lower=CH4_mod_min_valid-(1.645*(model_std_ch4_valid));
CH4_mod_min_valid_upper=CH4_mod_min_valid+(1.645*(model_std_ch4_valid));
%      lower_ch4=quantile(model_pop_ch4',0.05);% 90% quantile
%     upper_ch4=quantile(model_pop_ch4',0.95);% 90% quantile

figure
plot(CH4_mod_min_valid_lower)
hold on
plot(CH4_mod_min_valid_upper)
hold on
plot(CH4_mod_min_valid,'r')

%CAIC
p=6;
residual_SS_MB=sum((CH4_mod_min_valid(1:365)'-daily_wm_sum_umol_gf_valid(1:365)').^2);
CAIC_valid_MB=-2*log(residual_SS_MB) + p*(log(365)+1);
residual_SS_WP=sum((CH4_mod_min_valid(366:end)'-daily_wm_sum_umol_gf_valid(366:end)').^2);
CAIC_valid_WP=-2*log(residual_SS_WP) + p*(log(365)+1);


%% run EE data through
model_pop_ch4_EE=zeros(length(1:10000));%length of validation data
model_pop_ch4_EE=model_pop_ch4_EE(1:365,:);%update as needed 
 
for i=1:length(find_min_10000)
   ch4mod = PEPRMT_final_sys_CH4(find_min_10000(i,:)',xdata_EE);
    model_pop_ch4_EE(:,i)=ch4mod';
    
end

model_std_ch4_EE=std(model_pop_ch4_EE');

%% according to Zobitz 2008, model mean==model min(best para set)
ch4_min_EE = PEPRMT_final_sys_CH4(find_min,xdata_EE);

figure
plot(daily_DOY_EE,daily_wm_sum_umol_gf_EE,'.',daily_DOY_EE,ch4_min_EE,'.')
legend('obs','mod min')
 

% 90% confidence intervals (if you want 95% use 1.96)
%b/c the 10,000 parameters are not independent or may even be redundant, I
%reduce the sample size to 1000
lower_ch4_EE=ch4_min_EE-(1.645*(model_std_ch4_EE));%use 1.645 for 90% CI
upper_ch4_EE=ch4_min_EE+(1.645*(model_std_ch4_EE));
%     lower_ch4_EE=quantile(model_pop_ch4_EE',0.05);% 90% quantile
%     upper_ch4_EE=quantile(model_pop_ch4_EE',0.95);% 90% quantile

figure
plot(lower_ch4_EE,'.')
hold on
plot(upper_ch4_EE,'.')
hold on
plot(ch4_min_EE,'.r')

%CAIC
p=6;
residual_SS_EE=sum((ch4_min_EE(1:365)-daily_wm_sum_umol_gf_EE(1:365)').^2);
CAIC_valid_EE=-2*log(residual_SS_EE) + p*(log(365)+1);

%% annual sums
%Validation MB stats
cum_obs_valid_MB=cumsum(daily_wm_sum_umol_gf_valid(1:365));%umol m-2 yr-1
cum_obs_valid_sum_MB=cum_obs_valid_MB(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_valid_sum_MB=cum_obs_valid_sum_MB*16.04*0.75;%g C-Ch4 m-2 yr-1

cum_obs_valid_lower_MB=cumsum(daily_wm_gf_lower_valid(1:365));%umol m-2 yr-1
cum_obs_valid_sum_lower_MB=cum_obs_valid_lower_MB(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_valid_sum_lower_MB=cum_obs_valid_sum_lower_MB*16.04*0.75;%g C-Ch4 m-2 yr-1

cum_obs_valid_upper_MB=cumsum(daily_wm_gf_upper_valid(1:365));%umol m-2 yr-1
cum_obs_valid_sum_upper_MB=cum_obs_valid_upper_MB(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_valid_sum_upper_MB=cum_obs_valid_sum_upper_MB*16.04*0.75;%g C-Ch4 m-2 yr-1

cum_obs_valid_sum_lower_MB-cum_obs_valid_sum_MB

cum_mod_valid_MB=cumsum(CH4_mod_min_valid(1:365));%nmol m-2 yr-1
cum_mod_valid_sum_MB=cum_mod_valid_MB(end)*10^-6;%mol m-2 yr-1
cum_mod_valid_sum_MB=cum_mod_valid_sum_MB*16.04*0.75;%g C-Ch4 m-2 yr-1

cum_mod_valid_lower_MB=cumsum(CH4_mod_min_valid_lower(1:365));%nmol m-2 yr-1
cum_mod_valid_sum_lower_MB=cum_mod_valid_lower_MB(end)*10^-6;%mol m-2 yr-1
cum_mod_valid_sum_lower_MB=cum_mod_valid_sum_lower_MB*16.04*0.75;%g C-Ch4 m-2 yr-1
 
cum_mod_max_valid_upper_MB=cumsum(CH4_mod_min_valid_upper(1:365));%nmol m-2 yr-1
cum_mod_valid_sum_upper_MB=cum_mod_max_valid_upper_MB(end)*10^-6;%mol m-2 yr-1
cum_mod_valid_sum_upper_MB=cum_mod_valid_sum_upper_MB*16.04*0.75;%g C-Ch4 m-2 yr-1

(cum_mod_valid_sum_upper_MB-cum_mod_valid_sum_MB)*5
(cum_mod_valid_sum_MB*100)/cum_obs_valid_sum_MB

%RMSE is a better test of model performance than r2
RMSE_ch4_min_MB_valid=nansum((CH4_mod_min_valid(1:365)-daily_wm_sum_umol_gf_valid(1:365)).^2);
RMSE_ch4_min_MB_valid=sqrt(RMSE_ch4_min_MB_valid/365);
RMSE_ch4_min_MB_valid=RMSE_ch4_min_MB_valid*10^-3*16.04*0.75;%mg C-Ch4 m-2 yr-1
%% Validation WP 
cum_obs_valid_WP=cumsum(daily_wm_sum_umol_gf_valid(366:end));%umol m-2 yr-1
cum_obs_valid_sum_WP=cum_obs_valid_WP(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_valid_sum_WP=cum_obs_valid_sum_WP*16.04*0.75;%g C-Ch4 m-2 yr-1

cum_obs_valid_lower_WP=cumsum(daily_wm_gf_lower_valid(366:end));%umol m-2 yr-1
cum_obs_valid_sum_lower_WP=cum_obs_valid_lower_WP(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_valid_sum_lower_WP=cum_obs_valid_sum_lower_WP*16.04*0.75;%g C-Ch4 m-2 yr-1

cum_obs_valid_upper_WP=cumsum(daily_wm_gf_upper_valid(366:end));%umol m-2 yr-1
cum_obs_valid_sum_upper_WP=cum_obs_valid_upper_WP(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_valid_sum_upper_WP=cum_obs_valid_sum_upper_WP*16.04*0.75;%g C-Ch4 m-2 yr-1

cum_obs_valid_sum_upper_WP-cum_obs_valid_sum_WP

cum_mod_valid_WP=cumsum(CH4_mod_min_valid(366:end));%nmol m-2 yr-1
cum_mod_valid_sum_WP=cum_mod_valid_WP(end)*10^-6;%mol m-2 yr-1
cum_mod_valid_sum_WP=cum_mod_valid_sum_WP*16.04*0.75;%g C-Ch4 m-2 yr-1

cum_mod_valid_lower_WP=cumsum(CH4_mod_min_valid_lower(366:end));%nmol m-2 yr-1
cum_mod_valid_sum_lower_WP=cum_mod_valid_lower_WP(end)*10^-6;%mol m-2 yr-1
cum_mod_valid_sum_lower_WP=cum_mod_valid_sum_lower_WP*16.04*0.75;%g C-Ch4 m-2 yr-1
 
cum_mod_max_valid_upper_WP=cumsum(CH4_mod_min_valid_upper(366:end));%nmol m-2 yr-1
cum_mod_valid_sum_upper_WP=cum_mod_max_valid_upper_WP(end)*10^-6;%mol m-2 yr-1
cum_mod_valid_sum_upper_WP=cum_mod_valid_sum_upper_WP*16.04*0.75;%g C-Ch4 m-2 yr-1

(cum_mod_valid_sum_upper_WP-cum_mod_valid_sum_WP)*5
(cum_mod_valid_sum_WP*100)/cum_obs_valid_sum_WP

%RMSE is a better test of model performance than r2
RMSE_ch4_min_WP_valid=nansum((CH4_mod_min_valid(366:end)-daily_wm_sum_umol_gf_valid(366:end)).^2);
RMSE_ch4_min_WP_valid=sqrt(RMSE_ch4_min_WP_valid/365);
RMSE_ch4_min_WP_valid=RMSE_ch4_min_WP_valid*10^-3*16.04*0.75;%mg C-Ch4 m-2 yr-1

%% Validation EE 
cum_obs_valid_EE=cumsum(daily_wm_sum_umol_gf_EE);%umol m-2 yr-1
cum_obs_valid_sum_EE=cum_obs_valid_EE(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_valid_sum_EE=cum_obs_valid_sum_EE*16.04*0.75;%g C-Ch4 m-2 yr-1

cum_obs_valid_lower_EE=cumsum(daily_wm_gf_EE_lower);%umol m-2 yr-1
cum_obs_valid_sum_lower_EE=cum_obs_valid_lower_EE(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_valid_sum_lower_EE=cum_obs_valid_sum_lower_EE*16.04*0.75;%g C-Ch4 m-2 yr-1

cum_obs_valid_upper_EE=cumsum(daily_wm_gf_EE_upper);%umol m-2 yr-1
cum_obs_valid_sum_upper_EE=cum_obs_valid_upper_EE(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_valid_sum_upper_EE=cum_obs_valid_sum_upper_EE*16.04*0.75;%g C-Ch4 m-2 yr-1

cum_obs_valid_sum_upper_EE-cum_obs_valid_sum_EE

cum_mod_valid_EE=cumsum(ch4_min_EE);%nmol m-2 yr-1
cum_mod_valid_sum_EE=cum_mod_valid_EE(end)*10^-6;%mol m-2 yr-1
cum_mod_valid_sum_EE=cum_mod_valid_sum_EE*16.04*0.75;%g C-Ch4 m-2 yr-1

cum_mod_valid_lower_EE=cumsum(lower_ch4_EE);%nmol m-2 yr-1
cum_mod_valid_sum_lower_EE=cum_mod_valid_lower_EE(end)*10^-6;%mol m-2 yr-1
cum_mod_valid_sum_lower_EE=cum_mod_valid_sum_lower_EE*16.04*0.75;%g C-Ch4 m-2 yr-1
 
cum_mod_max_valid_upper_EE=cumsum(upper_ch4_EE);%nmol m-2 yr-1
cum_mod_valid_sum_upper_EE=cum_mod_max_valid_upper_EE(end)*10^-6;%mol m-2 yr-1
cum_mod_valid_sum_upper_EE=cum_mod_valid_sum_upper_EE*16.04*0.75;%g C-Ch4 m-2 yr-1

(cum_mod_valid_sum_upper_EE-cum_mod_valid_sum_EE)*5

(cum_mod_valid_sum_EE*100)/cum_obs_valid_sum_EE
%RMSE is a better test of model performance than r2
RMSE_ch4_min_EE_valid=nansum((ch4_min_EE'-daily_wm_sum_umol_gf_EE).^2);
RMSE_ch4_min_EE_valid=sqrt(RMSE_ch4_min_EE_valid/365);
RMSE_ch4_min_EE_valid=RMSE_ch4_min_EE_valid*10^-3*16.04*0.75;%mg C-Ch4 m-2 yr-1

%% Param stats MB 
%ALL years
cum_obs_param_MB=cumsum(daily_wm_sum_umol_gf_param(1:1461));%umol m-2 yr-1
cum_obs_param_sum_MB=cum_obs_param_MB(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_param_sum_MB=cum_obs_param_sum_MB*16.04*0.75;%g C-Ch4 m-2 yr-1

cum_obs_param_lower_MB=cumsum(daily_wm_gf_lower_param(1:1461));%umol m-2 yr-1
cum_obs_param_sum_lower_MB=cum_obs_param_lower_MB(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_param_sum_lower_MB=cum_obs_param_sum_lower_MB*16.04*0.75;%g C-Ch4 m-2 yr-1

cum_obs_param_upper_MB=cumsum(daily_wm_gf_upper_param(1:1461));%umol m-2 yr-1
cum_obs_param_sum_upper_MB=cum_obs_param_upper_MB(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_param_sum_upper_MB=cum_obs_param_sum_upper_MB*16.04*0.75;%g C-Ch4 m-2 yr-1

cum_obs_param_sum_lower_MB-cum_obs_param_sum_MB

cum_mod_param_MB=cumsum(CH4_mod_min_param(1:1461));%nmol m-2 yr-1
cum_mod_param_sum_MB=cum_mod_param_MB(end)*10^-6;%mol m-2 yr-1
cum_mod_param_sum_MB=cum_mod_param_sum_MB*16.04*0.75;%g C-Ch4 m-2 yr-1

cum_mod_param_lower_MB=cumsum(CH4_mod_min_param_lower(1:1461));%nmol m-2 yr-1
cum_mod_param_sum_lower_MB=cum_mod_param_lower_MB(end)*10^-6;%mol m-2 yr-1
cum_mod_param_sum_lower_MB=cum_mod_param_sum_lower_MB*16.04*0.75;%g C-Ch4 m-2 yr-1
 
cum_mod_max_param_upper_MB=cumsum(CH4_mod_min_param_upper(1:1461));%nmol m-2 yr-1
cum_mod_param_sum_upper_MB=cum_mod_max_param_upper_MB(end)*10^-6;%mol m-2 yr-1
cum_mod_param_sum_upper_MB=cum_mod_param_sum_upper_MB*16.04*0.75;%g C-Ch4 m-2 yr-1

(cum_mod_param_sum_upper_MB-cum_mod_param_sum_MB)*5
(cum_mod_param_sum_MB*100)/cum_obs_param_sum_MB

%By year
%2011
cum_obs_param_MB=cumsum(daily_wm_sum_umol_gf_param(1:365));%umol m-2 yr-1
cum_obs_param_sum_MB=cum_obs_param_MB(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_param_sum_MB=cum_obs_param_sum_MB*16.04*0.75;%g C-Ch4 m-2 yr-1

cum_obs_param_lower_MB=cumsum(daily_wm_gf_lower_param(1:365));%umol m-2 yr-1
cum_obs_param_sum_lower_MB=cum_obs_param_lower_MB(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_param_sum_lower_MB=cum_obs_param_sum_lower_MB*16.04*0.75;%g C-Ch4 m-2 yr-1

cum_obs_param_upper_MB=cumsum(daily_wm_gf_upper_param(1:365));%umol m-2 yr-1
cum_obs_param_sum_upper_MB=cum_obs_param_upper_MB(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_param_sum_upper_MB=cum_obs_param_sum_upper_MB*16.04*0.75;%g C-Ch4 m-2 yr-1

cum_obs_param_sum_lower_MB-cum_obs_param_sum_MB

cum_mod_param_MB=cumsum(CH4_mod_min_param(1:365));%nmol m-2 yr-1
cum_mod_param_sum_MB=cum_mod_param_MB(end)*10^-6;%mol m-2 yr-1
cum_mod_param_sum_MB=cum_mod_param_sum_MB*16.04*0.75;%g C-Ch4 m-2 yr-1

cum_mod_param_lower_MB=cumsum(CH4_mod_min_param_lower(1:365));%nmol m-2 yr-1
cum_mod_param_sum_lower_MB=cum_mod_param_lower_MB(end)*10^-6;%mol m-2 yr-1
cum_mod_param_sum_lower_MB=cum_mod_param_sum_lower_MB*16.04*0.75;%g C-Ch4 m-2 yr-1
 
cum_mod_max_param_upper_MB=cumsum(CH4_mod_min_param_upper(1:365));%nmol m-2 yr-1
cum_mod_param_sum_upper_MB=cum_mod_max_param_upper_MB(end)*10^-6;%mol m-2 yr-1
cum_mod_param_sum_upper_MB=cum_mod_param_sum_upper_MB*16.04*0.75;%g C-Ch4 m-2 yr-1

(cum_mod_param_sum_upper_MB-cum_mod_param_sum_MB)*5
(cum_mod_param_sum_MB*100)/cum_obs_param_sum_MB

%2012
cum_obs_param_MB=cumsum(daily_wm_sum_umol_gf_param(366:731));%umol m-2 yr-1
cum_obs_param_sum_MB=cum_obs_param_MB(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_param_sum_MB=cum_obs_param_sum_MB*16.04*0.75;%g C-Ch4 m-2 yr-1

cum_obs_param_lower_MB=cumsum(daily_wm_gf_lower_param(366:731));%umol m-2 yr-1
cum_obs_param_sum_lower_MB=cum_obs_param_lower_MB(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_param_sum_lower_MB=cum_obs_param_sum_lower_MB*16.04*0.75;%g C-Ch4 m-2 yr-1

cum_obs_param_upper_MB=cumsum(daily_wm_gf_upper_param(366:731));%umol m-2 yr-1
cum_obs_param_sum_upper_MB=cum_obs_param_upper_MB(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_param_sum_upper_MB=cum_obs_param_sum_upper_MB*16.04*0.75;%g C-Ch4 m-2 yr-1

cum_obs_param_sum_lower_MB-cum_obs_param_sum_MB

cum_mod_param_MB=cumsum(CH4_mod_min_param(366:731));%nmol m-2 yr-1
cum_mod_param_sum_MB=cum_mod_param_MB(end)*10^-6;%mol m-2 yr-1
cum_mod_param_sum_MB=cum_mod_param_sum_MB*16.04*0.75;%g C-Ch4 m-2 yr-1

cum_mod_param_lower_MB=cumsum(CH4_mod_min_param_lower(366:731));%nmol m-2 yr-1
cum_mod_param_sum_lower_MB=cum_mod_param_lower_MB(end)*10^-6;%mol m-2 yr-1
cum_mod_param_sum_lower_MB=cum_mod_param_sum_lower_MB*16.04*0.75;%g C-Ch4 m-2 yr-1
 
cum_mod_max_param_upper_MB=cumsum(CH4_mod_min_param_upper(366:731));%nmol m-2 yr-1
cum_mod_param_sum_upper_MB=cum_mod_max_param_upper_MB(end)*10^-6;%mol m-2 yr-1
cum_mod_param_sum_upper_MB=cum_mod_param_sum_upper_MB*16.04*0.75;%g C-Ch4 m-2 yr-1

(cum_mod_param_sum_upper_MB-cum_mod_param_sum_MB)*5
(cum_mod_param_sum_MB*100)/cum_obs_param_sum_MB

%2013
cum_obs_param_MB=cumsum(daily_wm_sum_umol_gf_param(732:1096));%umol m-2 yr-1
cum_obs_param_sum_MB=cum_obs_param_MB(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_param_sum_MB=cum_obs_param_sum_MB*16.04*0.75;%g C-Ch4 m-2 yr-1

cum_obs_param_lower_MB=cumsum(daily_wm_gf_lower_param(732:1096));%umol m-2 yr-1
cum_obs_param_sum_lower_MB=cum_obs_param_lower_MB(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_param_sum_lower_MB=cum_obs_param_sum_lower_MB*16.04*0.75;%g C-Ch4 m-2 yr-1

cum_obs_param_upper_MB=cumsum(daily_wm_gf_upper_param(732:1096));%umol m-2 yr-1
cum_obs_param_sum_upper_MB=cum_obs_param_upper_MB(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_param_sum_upper_MB=cum_obs_param_sum_upper_MB*16.04*0.75;%g C-Ch4 m-2 yr-1

cum_obs_param_sum_lower_MB-cum_obs_param_sum_MB

cum_mod_param_MB=cumsum(CH4_mod_min_param(732:1096));%nmol m-2 yr-1
cum_mod_param_sum_MB=cum_mod_param_MB(end)*10^-6;%mol m-2 yr-1
cum_mod_param_sum_MB=cum_mod_param_sum_MB*16.04*0.75;%g C-Ch4 m-2 yr-1

cum_mod_param_lower_MB=cumsum(CH4_mod_min_param_lower(732:1096));%nmol m-2 yr-1
cum_mod_param_sum_lower_MB=cum_mod_param_lower_MB(end)*10^-6;%mol m-2 yr-1
cum_mod_param_sum_lower_MB=cum_mod_param_sum_lower_MB*16.04*0.75;%g C-Ch4 m-2 yr-1
 
cum_mod_max_param_upper_MB=cumsum(CH4_mod_min_param_upper(732:1096));%nmol m-2 yr-1
cum_mod_param_sum_upper_MB=cum_mod_max_param_upper_MB(end)*10^-6;%mol m-2 yr-1
cum_mod_param_sum_upper_MB=cum_mod_param_sum_upper_MB*16.04*0.75;%g C-Ch4 m-2 yr-1

(cum_mod_param_sum_upper_MB-cum_mod_param_sum_MB)*5
(cum_mod_param_sum_MB*100)/cum_obs_param_sum_MB

%2014
cum_obs_param_MB=cumsum(daily_wm_sum_umol_gf_param(1097:1461));%umol m-2 yr-1
cum_obs_param_sum_MB=cum_obs_param_MB(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_param_sum_MB=cum_obs_param_sum_MB*16.04*0.75;%g C-Ch4 m-2 yr-1

cum_obs_param_lower_MB=cumsum(daily_wm_gf_lower_param(1097:1461));%umol m-2 yr-1
cum_obs_param_sum_lower_MB=cum_obs_param_lower_MB(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_param_sum_lower_MB=cum_obs_param_sum_lower_MB*16.04*0.75;%g C-Ch4 m-2 yr-1

cum_obs_param_upper_MB=cumsum(daily_wm_gf_upper_param(1097:1461));%umol m-2 yr-1
cum_obs_param_sum_upper_MB=cum_obs_param_upper_MB(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_param_sum_upper_MB=cum_obs_param_sum_upper_MB*16.04*0.75;%g C-Ch4 m-2 yr-1

cum_obs_param_sum_lower_MB-cum_obs_param_sum_MB

cum_mod_param_MB=cumsum(CH4_mod_min_param(1097:1461));%nmol m-2 yr-1
cum_mod_param_sum_MB=cum_mod_param_MB(end)*10^-6;%mol m-2 yr-1
cum_mod_param_sum_MB=cum_mod_param_sum_MB*16.04*0.75;%g C-Ch4 m-2 yr-1

cum_mod_param_lower_MB=cumsum(CH4_mod_min_param_lower(1097:1461));%nmol m-2 yr-1
cum_mod_param_sum_lower_MB=cum_mod_param_lower_MB(end)*10^-6;%mol m-2 yr-1
cum_mod_param_sum_lower_MB=cum_mod_param_sum_lower_MB*16.04*0.75;%g C-Ch4 m-2 yr-1
 
cum_mod_max_param_upper_MB=cumsum(CH4_mod_min_param_upper(1097:1461));%nmol m-2 yr-1
cum_mod_param_sum_upper_MB=cum_mod_max_param_upper_MB(end)*10^-6;%mol m-2 yr-1
cum_mod_param_sum_upper_MB=cum_mod_param_sum_upper_MB*16.04*0.75;%g C-Ch4 m-2 yr-1

(cum_mod_param_sum_upper_MB-cum_mod_param_sum_MB)*5
(cum_mod_param_sum_MB*100)/cum_obs_param_sum_MB

%% PAram WP 
%ALL YEARS
cum_obs_param_WP=cumsum(daily_wm_sum_umol_gf_param(1462:end));%umol m-2 yr-1
cum_obs_param_sum_WP=cum_obs_param_WP(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_param_sum_WP=cum_obs_param_sum_WP*16.04*0.75;%g C-Ch4 m-2 yr-1

cum_obs_param_lower_WP=cumsum(daily_wm_gf_lower_param(1462:end));%umol m-2 yr-1
cum_obs_param_sum_lower_WP=cum_obs_param_lower_WP(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_param_sum_lower_WP=cum_obs_param_sum_lower_WP*16.04*0.75;%g C-Ch4 m-2 yr-1

cum_obs_param_upper_WP=cumsum(daily_wm_gf_upper_param(1462:end));%umol m-2 yr-1
cum_obs_param_sum_upper_WP=cum_obs_param_upper_WP(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_param_sum_upper_WP=cum_obs_param_sum_upper_WP*16.04*0.75;%g C-Ch4 m-2 yr-1

cum_obs_param_sum_upper_WP-cum_obs_param_sum_WP

cum_mod_param_WP=cumsum(CH4_mod_min_param(1462:end));%nmol m-2 yr-1
cum_mod_param_sum_WP=cum_mod_param_WP(end)*10^-6;%mol m-2 yr-1
cum_mod_param_sum_WP=cum_mod_param_sum_WP*16.04*0.75;%g C-Ch4 m-2 yr-1

cum_mod_param_lower_WP=cumsum(CH4_mod_min_param_lower(1462:end));%nmol m-2 yr-1
cum_mod_param_sum_lower_WP=cum_mod_param_lower_WP(end)*10^-6;%mol m-2 yr-1
cum_mod_param_sum_lower_WP=cum_mod_param_sum_lower_WP*16.04*0.75;%g C-Ch4 m-2 yr-1
 
cum_mod_max_param_upper_WP=cumsum(CH4_mod_min_param_upper(1462:end));%nmol m-2 yr-1
cum_mod_param_sum_upper_WP=cum_mod_max_param_upper_WP(end)*10^-6;%mol m-2 yr-1
cum_mod_param_sum_upper_WP=cum_mod_param_sum_upper_WP*16.04*0.75;%g C-Ch4 m-2 yr-1

(cum_mod_param_sum_upper_WP-cum_mod_param_sum_WP)*5
(cum_mod_param_sum_WP*100)/cum_obs_param_sum_WP
 
%By year
%2012**NOT A FULL YEAR
cum_obs_param_WP=cumsum(daily_wm_sum_umol_gf_param(1462:1633));%umol m-2 yr-1
cum_obs_param_sum_WP=cum_obs_param_WP(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_param_sum_WP=cum_obs_param_sum_WP*16.04*0.75;%g C-Ch4 m-2 yr-1

cum_obs_param_lower_WP=cumsum(daily_wm_gf_lower_param(1462:1633));%umol m-2 yr-1
cum_obs_param_sum_lower_WP=cum_obs_param_lower_WP(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_param_sum_lower_WP=cum_obs_param_sum_lower_WP*16.04*0.75;%g C-Ch4 m-2 yr-1

cum_obs_param_upper_WP=cumsum(daily_wm_gf_upper_param(1462:1633));%umol m-2 yr-1
cum_obs_param_sum_upper_WP=cum_obs_param_upper_WP(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_param_sum_upper_WP=cum_obs_param_sum_upper_WP*16.04*0.75;%g C-Ch4 m-2 yr-1

cum_obs_param_sum_upper_WP-cum_obs_param_sum_WP

cum_mod_param_WP=cumsum(CH4_mod_min_param(1462:1633));%nmol m-2 yr-1
cum_mod_param_sum_WP=cum_mod_param_WP(end)*10^-6;%mol m-2 yr-1
cum_mod_param_sum_WP=cum_mod_param_sum_WP*16.04*0.75;%g C-Ch4 m-2 yr-1

cum_mod_param_lower_WP=cumsum(CH4_mod_min_param_lower(1462:1633));%nmol m-2 yr-1
cum_mod_param_sum_lower_WP=cum_mod_param_lower_WP(end)*10^-6;%mol m-2 yr-1
cum_mod_param_sum_lower_WP=cum_mod_param_sum_lower_WP*16.04*0.75;%g C-Ch4 m-2 yr-1
 
cum_mod_max_param_upper_WP=cumsum(CH4_mod_min_param_upper(1462:1633));%nmol m-2 yr-1
cum_mod_param_sum_upper_WP=cum_mod_max_param_upper_WP(end)*10^-6;%mol m-2 yr-1
cum_mod_param_sum_upper_WP=cum_mod_param_sum_upper_WP*16.04*0.75;%g C-Ch4 m-2 yr-1

(cum_mod_param_sum_upper_WP-cum_mod_param_sum_WP)*5
(cum_mod_param_sum_WP*100)/cum_obs_param_sum_WP

%2013
cum_obs_param_WP=cumsum(daily_wm_sum_umol_gf_param(1634:1998));%umol m-2 yr-1
cum_obs_param_sum_WP=cum_obs_param_WP(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_param_sum_WP=cum_obs_param_sum_WP*16.04*0.75;%g C-Ch4 m-2 yr-1

cum_obs_param_lower_WP=cumsum(daily_wm_gf_lower_param(1634:1998));%umol m-2 yr-1
cum_obs_param_sum_lower_WP=cum_obs_param_lower_WP(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_param_sum_lower_WP=cum_obs_param_sum_lower_WP*16.04*0.75;%g C-Ch4 m-2 yr-1

cum_obs_param_upper_WP=cumsum(daily_wm_gf_upper_param(1634:1998));%umol m-2 yr-1
cum_obs_param_sum_upper_WP=cum_obs_param_upper_WP(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_param_sum_upper_WP=cum_obs_param_sum_upper_WP*16.04*0.75;%g C-Ch4 m-2 yr-1

cum_obs_param_sum_upper_WP-cum_obs_param_sum_WP

cum_mod_param_WP=cumsum(CH4_mod_min_param(1634:1998));%nmol m-2 yr-1
cum_mod_param_sum_WP=cum_mod_param_WP(end)*10^-6;%mol m-2 yr-1
cum_mod_param_sum_WP=cum_mod_param_sum_WP*16.04*0.75;%g C-Ch4 m-2 yr-1

cum_mod_param_lower_WP=cumsum(CH4_mod_min_param_lower(1634:1998));%nmol m-2 yr-1
cum_mod_param_sum_lower_WP=cum_mod_param_lower_WP(end)*10^-6;%mol m-2 yr-1
cum_mod_param_sum_lower_WP=cum_mod_param_sum_lower_WP*16.04*0.75;%g C-Ch4 m-2 yr-1
 
cum_mod_max_param_upper_WP=cumsum(CH4_mod_min_param_upper(1634:1998));%nmol m-2 yr-1
cum_mod_param_sum_upper_WP=cum_mod_max_param_upper_WP(end)*10^-6;%mol m-2 yr-1
cum_mod_param_sum_upper_WP=cum_mod_param_sum_upper_WP*16.04*0.75;%g C-Ch4 m-2 yr-1

(cum_mod_param_sum_upper_WP-cum_mod_param_sum_WP)*5
(cum_mod_param_sum_WP*100)/cum_obs_param_sum_WP

%2014
cum_obs_param_WP=cumsum(daily_wm_sum_umol_gf_param(1999:end));%umol m-2 yr-1
cum_obs_param_sum_WP=cum_obs_param_WP(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_param_sum_WP=cum_obs_param_sum_WP*16.04*0.75;%g C-Ch4 m-2 yr-1

cum_obs_param_lower_WP=cumsum(daily_wm_gf_lower_param(1999:end));%umol m-2 yr-1
cum_obs_param_sum_lower_WP=cum_obs_param_lower_WP(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_param_sum_lower_WP=cum_obs_param_sum_lower_WP*16.04*0.75;%g C-Ch4 m-2 yr-1

cum_obs_param_upper_WP=cumsum(daily_wm_gf_upper_param(1999:end));%umol m-2 yr-1
cum_obs_param_sum_upper_WP=cum_obs_param_upper_WP(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_param_sum_upper_WP=cum_obs_param_sum_upper_WP*16.04*0.75;%g C-Ch4 m-2 yr-1

cum_obs_param_sum_upper_WP-cum_obs_param_sum_WP

cum_mod_param_WP=cumsum(CH4_mod_min_param(1999:end));%nmol m-2 yr-1
cum_mod_param_sum_WP=cum_mod_param_WP(end)*10^-6;%mol m-2 yr-1
cum_mod_param_sum_WP=cum_mod_param_sum_WP*16.04*0.75;%g C-Ch4 m-2 yr-1

cum_mod_param_lower_WP=cumsum(CH4_mod_min_param_lower(1999:end));%nmol m-2 yr-1
cum_mod_param_sum_lower_WP=cum_mod_param_lower_WP(end)*10^-6;%mol m-2 yr-1
cum_mod_param_sum_lower_WP=cum_mod_param_sum_lower_WP*16.04*0.75;%g C-Ch4 m-2 yr-1
 
cum_mod_max_param_upper_WP=cumsum(CH4_mod_min_param_upper(1999:end));%nmol m-2 yr-1
cum_mod_param_sum_upper_WP=cum_mod_max_param_upper_WP(end)*10^-6;%mol m-2 yr-1
cum_mod_param_sum_upper_WP=cum_mod_param_sum_upper_WP*16.04*0.75;%g C-Ch4 m-2 yr-1

(cum_mod_param_sum_upper_WP-cum_mod_param_sum_WP)*5
(cum_mod_param_sum_WP*100)/cum_obs_param_sum_WP


%% Statistics Updated 10/10/2016
%RMSE is a better test of model performance than r2
%convert units before computing stats
CH4_mod_min_param_gC_m2_day=CH4_mod_min_param*10^-6*16.04*0.75;
CH4_mod_min_valid_gC_m2_day=CH4_mod_min_valid*10^-6*16.04*0.75;
ch4_min_EE_gC_m2_day=ch4_min_EE*10^-6*16.04*0.75;
daily_wm_sum_gC_gf_param=daily_wm_sum_umol_gf_param*10^-6*16.04*0.75;
daily_wm_sum_gC_gf_valid=daily_wm_sum_umol_gf_valid*10^-6*16.04*0.75;
daily_wm_sum_gC_gf_EE=daily_wm_sum_umol_gf_EE*10^-6*16.04*0.75;

%ch4
RMSE_ch4_min_daily_MB_param=nansum((CH4_mod_min_param_gC_m2_day(1:1461)-daily_wm_sum_gC_gf_param(1:1461)).^2);
RMSE_ch4_min_daily_MB_param=sqrt(RMSE_ch4_min_daily_MB_param/1461);
RMSE_ch4_min_daily_WP_param=nansum((CH4_mod_min_param_gC_m2_day(1462:end)-daily_wm_sum_gC_gf_param(1462:end)).^2);
RMSE_ch4_min_daily_WP_param=sqrt(RMSE_ch4_min_daily_WP_param/902);
 
RMSE_ch4_min_daily_MB_valid=nansum((CH4_mod_min_valid_gC_m2_day(1:365)-daily_wm_sum_gC_gf_valid(1:365)).^2);
RMSE_ch4_min_daily_MB_valid=sqrt(RMSE_ch4_min_daily_MB_valid/365);
RMSE_ch4_min_daily_WP_valid=nansum((CH4_mod_min_valid_gC_m2_day(366:end)-daily_wm_sum_gC_gf_valid(366:end)).^2);
RMSE_ch4_min_daily_WP_valid=sqrt(RMSE_ch4_min_daily_WP_valid/365);
RMSE_ch4_min_daily_EE_valid=nansum((ch4_min_EE_gC_m2_day(1:end)-daily_wm_sum_gC_gf_EE(1:end)').^2);
RMSE_ch4_min_daily_EE_valid=sqrt(RMSE_ch4_min_daily_EE_valid/365);

%Mean absolute error (MAE)
%ch4
MAE_ch4_min_daily_MB_param=nansum((CH4_mod_min_param_gC_m2_day(1:1461)-daily_wm_sum_gC_gf_param(1:1461)));
MAE_ch4_min_daily_MB_param=(MAE_ch4_min_daily_MB_param/1461);
MAE_ch4_min_daily_WP_param=nansum((CH4_mod_min_param_gC_m2_day(1462:end)-daily_wm_sum_gC_gf_param(1462:end)));
MAE_ch4_min_daily_WP_param=(MAE_ch4_min_daily_WP_param/902);
 
MAE_ch4_min_daily_MB_valid=nansum((CH4_mod_min_valid_gC_m2_day(1:365)-daily_wm_sum_gC_gf_valid(1:365)));
MAE_ch4_min_daily_MB_valid=(MAE_ch4_min_daily_MB_valid/365);
MAE_ch4_min_daily_WP_valid=nansum((CH4_mod_min_valid_gC_m2_day(366:end)-daily_wm_sum_gC_gf_valid(366:end)));
MAE_ch4_min_daily_WP_valid=(MAE_ch4_min_daily_WP_valid/365);
MAE_ch4_min_daily_EE_valid=nansum((ch4_min_EE_gC_m2_day(1:end)-daily_wm_sum_gC_gf_EE(1:end)'));
MAE_ch4_min_daily_EE_valid=(MAE_ch4_min_daily_EE_valid/365);


%D-index sensitive to systematic model bias
%ch4
D_index_ch4_min_daily_top_MB_param=nansum((CH4_mod_min_param_gC_m2_day(1:1461)-daily_wm_sum_gC_gf_param(1:1461)).^2);
O_avg_MB_param=nanmean(daily_wm_sum_gC_gf_param(1:1461));
D_index_ch4_min_daily_bottom_MB_param=nansum(((CH4_mod_min_param_gC_m2_day(1:1461)-O_avg_MB_param)+(daily_wm_sum_gC_gf_param(1:1461)-O_avg_MB_param)).^2);
D_index_ch4_min_daily_MB_param=1-(D_index_ch4_min_daily_top_MB_param/D_index_ch4_min_daily_bottom_MB_param);

D_index_ch4_min_daily_top_WP_param=nansum((CH4_mod_min_param_gC_m2_day(1462:end)-daily_wm_sum_gC_gf_param(1462:end)).^2);
O_avg_WP_param=nanmean(daily_wm_sum_gC_gf_param(1462:end));
D_index_ch4_min_daily_bottom_WP_param=nansum(((CH4_mod_min_param_gC_m2_day(1462:end)-O_avg_WP_param)+(daily_wm_sum_gC_gf_param(1462:end)-O_avg_WP_param)).^2);
D_index_ch4_min_daily_WP_param=1-(D_index_ch4_min_daily_top_WP_param/D_index_ch4_min_daily_bottom_WP_param);

D_index_ch4_min_daily_top_MB_valid=nansum((CH4_mod_min_valid_gC_m2_day(1:365)-daily_wm_sum_gC_gf_valid(1:365)).^2);
O_avg_MB_valid=nanmean(daily_wm_sum_gC_gf_valid(1:365));
D_index_ch4_min_daily_bottom_MB_valid=nansum(((CH4_mod_min_valid_gC_m2_day(1:365)-O_avg_MB_valid)+(daily_wm_sum_gC_gf_valid(1:365)-O_avg_MB_valid)).^2);
D_index_ch4_min_daily_MB_valid=1-(D_index_ch4_min_daily_top_MB_valid/D_index_ch4_min_daily_bottom_MB_valid);

D_index_ch4_min_daily_top_WP_valid=nansum((CH4_mod_min_valid_gC_m2_day(366:end)-daily_wm_sum_gC_gf_valid(366:end)).^2);
O_avg_WP_valid=nanmean(daily_wm_sum_gC_gf_valid(366:end));
D_index_ch4_min_daily_bottom_WP_valid=nansum(((CH4_mod_min_valid_gC_m2_day(366:end)-O_avg_WP_valid)+(daily_wm_sum_gC_gf_valid(366:end)-O_avg_WP_valid)).^2);
D_index_ch4_min_daily_WP_valid=1-(D_index_ch4_min_daily_top_WP_valid/D_index_ch4_min_daily_bottom_WP_valid);

D_index_ch4_min_daily_top_EE_valid=nansum((ch4_min_EE_gC_m2_day(1:365)-daily_wm_sum_gC_gf_EE(1:365)').^2);
O_avg_EE_valid=nanmean(daily_wm_sum_gC_gf_EE(1:365));
D_index_ch4_min_daily_bottom_EE_valid=nansum(((ch4_min_EE_gC_m2_day(1:365)-O_avg_EE_valid)+(daily_wm_sum_gC_gf_EE(1:365)'-O_avg_EE_valid)).^2);
D_index_ch4_min_daily_EE_valid=1-(D_index_ch4_min_daily_top_EE_valid/D_index_ch4_min_daily_bottom_EE_valid);

 
  



%% plotting param data for both sites
days_plot_MB=daily_DOY_param(1:1461);
days_plot_WP=daily_DOY_param(1462:end);

startDate = datenum('01-01-2011');
endDate = datenum('12-31-2014');
xData1=linspace(startDate,endDate,1461);

startDate2 = datenum('07-12-2012');
endDate2 = datenum('12-31-2014');
xData2=linspace(startDate2,endDate2,902);

figure ('Units', 'pixels',...
    'Position', [100 100 500 375]);
subplot(2,1,1)
ciplot(CH4_mod_min_param_lower(1:1461)*0.0120,CH4_mod_min_param_upper(1:1461)*0.0120,xData1,'k')
hold on
obs = plot(xData1,daily_wm_sum_umol_gf_param(1:1461)*0.0120,'.');
ylim([-10 625])
%set(gca, 'XTick', [startDate:150:endDate] );
datetick('x','mm/yyyy','keeplimits')
box off
hYLabel = ylabel({'CH_4 exchange','(mg C-CH_4 m^-^2 d^-^1)' });
 legend('model','obs')
t=text(xData1(20),575,'(a)');%;t=text(2,-7,'p2');%puts letter in upper L corner

subplot(2,1,2)
ciplot(CH4_mod_min_param_lower(1462:end)*0.0120,CH4_mod_min_param_upper(1462:end)*0.0120,xData2,'k')
hold on
obs = plot(xData2,daily_wm_sum_umol_gf_param(1462:end)*0.0120,'.');
%set(gca, 'XTick', [735062:150:735964] );
%set(gca, 'XTick', [startDate2:150:endDate2] );
datetick('x','mm/yyyy','keeplimits')
box off
hXLabel = xlabel('Day of year'                     );
hYLabel = ylabel({'CH_4 exchange','(mg C-CH_4 m^-^2 d^-^1)'} );
ylim([-10 500])
t=text(xData2(5),475,'(d)');%;t=text(2,-7,'p2');%puts letter in upper L corner

set( gca                       , ...
    'FontName'   , 'Helvetica' );
set([hXLabel, hYLabel, text]  , ...
    'FontSize'   , 12         );
 
set(gcf, 'PaperPositionMode', 'auto');
print('PEPRMT_ch4_param_MB_WP','-dpng','-r600');

%% one to one's for param data MB and WP

X = ones(length(CH4_mod_min_param(1:1461)),1);
X = [X CH4_mod_min_param(1:1461)];
[~,~,~,~,STATS] = regress(daily_wm_sum_umol_gf_param(1:1461),X);
rsq=STATS(1);

X = ones(length(CH4_mod_min_param(1462:end)),1);
X = [X CH4_mod_min_param(1462:end)];
[~,~,~,~,STATS2] = regress(daily_wm_sum_umol_gf_param(1462:end),X);
rsq2=STATS2(1);


figure ('Units', 'pixels',...
    'Position', [100 100 500 375])
obs=plot(daily_wm_sum_umol_gf_param(1:1461)*0.0120,CH4_mod_min_param(1:1461)*0.0120,'.');
hXLabel = xlabel('Model (mg C-CH_4 m^-^2 d^-^1)');
hYLabel = ylabel('Obs (mg C-CH_4 m^-^2 d^-^1)');
box off
% xlim([0,450])
% ylim([0,450])
set(obs                       , ...
  'LineWidth'       , 1           , ...
  'Marker'          , 'o'         , ...
  'MarkerSize'      , 3           , ...
  'MarkerEdgeColor' , [0.25 0.25 0.25]  , ...
  'MarkerFaceColor' , [0.25 0.25 0.25]  );
h=lsline;
p2 = polyfit(get(h,'xdata'),get(h,'ydata'),1);
theString = sprintf('y = %.2f x + %.2f', p2(1), p2(2));
place_x=min(CH4_mod_min_param(1:1461)*0.0120);
place_y=max(daily_wm_sum_umol_gf_param(1:1461)*0.0120);
t=text(place_x,place_y,theString);%;t=text(2,-7,'p2');%puts letter in upper L corner
theString = sprintf('r^2 = %.2f', rsq);
t=text(1,400,theString);%puts letter in upper L corner
refline(1,0)
 set( gca                       , ...
    'FontName'   , 'Helvetica' );
set([hXLabel, hYLabel, text]  , ...
    'FontSize'   , 12         );
 
set(gcf, 'PaperPositionMode', 'auto');
print('CH4_PEPRMT_onetoone_param_MB','-dpng','-r600');


figure ('Units', 'pixels',...
    'Position', [100 100 500 375])
obs=plot(daily_wm_sum_umol_gf_param(1462:end)*0.0120,CH4_mod_min_param(1462:end)*0.0120,'.');
hXLabel = xlabel('Model (mg C-CH_4 m^-^2 d^-^1)');
hYLabel = ylabel('Obs (mg C-CH_4 m^-^2 d^-^1)');
box off
% xlim([0,450])
% ylim([0,450])
set(obs                       , ...
  'LineWidth'       , 1           , ...
  'Marker'          , 'o'         , ...
  'MarkerSize'      , 3           , ...
  'MarkerEdgeColor' , [0.25 0.25 0.25]  , ...
  'MarkerFaceColor' , [0.25 0.25 0.25]  );
h=lsline;
p2 = polyfit(get(h,'xdata'),get(h,'ydata'),1);
theString = sprintf('y = %.2f x + %.2f', p2(1), p2(2));
place_x=min(CH4_mod_min_param(1462:end)*0.0120);
place_y=max(daily_wm_sum_umol_gf_param(1462:end)*0.0120);
t=text(place_x,place_y,theString);%;t=text(2,-7,'p2');%puts letter in upper L corner
theString = sprintf('r^2 = %.2f', rsq2);
t=text(1,400,theString);%puts letter in upper L corner
refline(1,0)
 set( gca                       , ...
    'FontName'   , 'Helvetica' );
set([hXLabel, hYLabel, text]  , ...
    'FontSize'   , 12         );
 
set(gcf, 'PaperPositionMode', 'auto');
print('CH4_PEPRMT_onetoone_param_WP','-dpng','-r600');

%% Cumm plots for param data MB and WP
cum_obs_param_MB_plot=cum_obs_param_MB*10^-6*16.04*0.75;
cum_obs_param_upper_MB_plot=cum_obs_param_upper_MB*10^-6*16.04*0.75;
cum_obs_param_lower_MB_plot=cum_obs_param_lower_MB*10^-6*16.04*0.75;

cum_obs_param_MB_plot=[cum_obs_param_MB_plot(365) cum_obs_param_MB_plot(731) cum_obs_param_MB_plot(1096) cum_obs_param_MB_plot(1461)];
cum_obs_param_upper_MB_plot=[cum_obs_param_upper_MB_plot(365) cum_obs_param_upper_MB_plot(731) cum_obs_param_upper_MB_plot(1096) cum_obs_param_upper_MB_plot(1461)];
cum_obs_param_lower_MB_plot=[cum_obs_param_lower_MB_plot(365) cum_obs_param_lower_MB_plot(731) cum_obs_param_lower_MB_plot(1096) cum_obs_param_lower_MB_plot(1461)];

startDate = datenum('01-01-2011');
endDate = datenum('12-31-2014');
xData1=linspace(startDate,endDate,1461);
date1_plot=[xData1(365) xData1(731) xData1(1096) xData1(1461)];

cum_obs_param_MB_plot_error=cum_obs_param_upper_MB_plot-cum_obs_param_MB_plot;
cum_obs_param_MB_plot_error=cum_obs_param_MB_plot_error/2;

figure
ciplot(cum_mod_max_param_upper_MB*10^-6*16.04*0.75,cum_mod_param_lower_MB*10^-6*16.04*0.75,xData1)
datetick('x','mm/yyyy')
hold on
errorbar(date1_plot,cum_obs_param_MB_plot,cum_obs_param_MB_plot_error)
datetick('x','mm/yyyy')
hold on
legend('mod','obs')
xlabel('DOY')
ylabel('Cummulative CH_4 (g C-CH_4 m^-^2)')
 box off

set(gcf, 'PaperPositionMode', 'auto');
print('CH4_PEPRMT_cumm_param_MB','-dpng','-r600');

cum_obs_param_WP_plot=cum_obs_param_WP*10^-6*16.04*0.75;
cum_obs_param_upper_WP_plot=cum_obs_param_upper_WP*10^-6*16.04*0.75;
cum_obs_param_lower_WP_plot=cum_obs_param_lower_WP*10^-6*16.04*0.75;

cum_obs_param_WP_plot=[cum_obs_param_WP_plot(172) cum_obs_param_WP_plot(537) cum_obs_param_WP_plot(902) ];
cum_obs_param_upper_WP_plot=[cum_obs_param_upper_WP_plot(172) cum_obs_param_upper_WP_plot(537) cum_obs_param_upper_WP_plot(902)];
cum_obs_param_lower_WP_plot=[cum_obs_param_lower_WP_plot(172) cum_obs_param_lower_WP_plot(537) cum_obs_param_lower_WP_plot(902)];

startDate2 = datenum('07-12-2012');
endDate2 = datenum('12-31-2014');
xData2=linspace(startDate2,endDate2,902);
date2_plot=[xData2(172) xData2(537) xData2(902)];

cum_obs_param_WP_plot_error=cum_obs_param_upper_WP_plot-cum_obs_param_WP_plot;
cum_obs_param_WP_plot_error=cum_obs_param_WP_plot_error/2;


figure
ciplot(cum_mod_max_param_upper_WP*10^-6*16.04*0.75,cum_mod_param_lower_WP*10^-6*16.04*0.75,xData2)
hold on
errorbar(date2_plot,cum_obs_param_WP_plot,cum_obs_param_WP_plot_error)
datetick('x','mm/yyyy','keeplimits')
hold on
legend('mod','obs')
xlabel('DOY')
ylabel('Cummulative CH_4 (g C-CH_4 m^-^2)')
 box off

set(gcf, 'PaperPositionMode', 'auto');
print('CH4_PEPRMT_cumm_param_WP','-dpng','-r600');



%% Plot posteriors Section updated 10/10/2016
find_min_10000_CH4=find_min_10000;
Ea_SOM_posterior=find_min_10000_CH4(:,1)+70;%kJ mol-1
km_SOM_posterior=find_min_10000_CH4(:,2)*3e3*1e-12*12;%umol m-2
Ea_labile_posterior=find_min_10000_CH4(:,3)+70;
km_labile_posterior=find_min_10000_CH4(:,4)*3e3*1e-12*12;
Ea_oxy_posterior=find_min_10000_CH4(:,5)+70;
km_oxy_posterior=find_min_10000_CH4(:,6)*3e3*1e-12*12;

% M_km1*1e-12*12%umol m-3 to gC cm_3
% M_km2*1e-12*12%umol m-3 to gC cm_3

figure
subplot(3,2,1)
hist(Ea_SOM_posterior);%pdf for alpha 1
ylabel('Ea SOM (kJ mol^-^1)')
subplot(3,2,2)
hist(km_SOM_posterior);%pdf for ea 1
ylabel('km SOM (g C cm^-^3 soil)')
subplot(3,2,3)
hist(Ea_labile_posterior);%pdf for km 1
ylabel('Ea labile (kJ mol^-^1)')
subplot(3,2,4)
hist(km_labile_posterior);%pdf for alpha 2
ylabel('km labile (g C cm^-^3 soil)')
subplot(3,2,5)
hist(Ea_oxy_posterior);%pdf for km 1
ylabel('Ea labile (kJ mol^-^1)')
subplot(3,2,6)
hist(km_oxy_posterior);%pdf for alpha 2
ylabel('km labile (g C cm^-^3 soil)')
 
set(gcf, 'PaperPositionMode', 'auto');
print('PEPRMT_CH4_posterior_20161010','-dpng','-r600');

%looking at correlations between parameters

X = ones(length(Ea_SOM_posterior),1);
X = [X Ea_SOM_posterior];
[~,~,~,~,STATS_EaSOM_kmlab] = regress(km_labile_posterior,X);

X = ones(length(Ea_SOM_posterior),1);
X = [X Ea_SOM_posterior];
[~,~,~,~,STATS_EaSOM_Eal] = regress(Ea_labile_posterior,X);

X = ones(length(Ea_labile_posterior),1);
X = [X Ea_labile_posterior];
[~,~,~,~,STATS_Eal_kml] = regress(km_labile_posterior,X);
%% 
figure
subplot(6,6,1)
plot(Ea_SOM_posterior,Ea_SOM_posterior,'.');%pdf for alpha 1
% xlabel({'Ea SOM','(kJ mol^-^1)'})
% ylabel({'Ea SOM','(kJ mol^-^1)'})
subplot(6,6,7)
plot(km_SOM_posterior,Ea_SOM_posterior,'.');%pdf for alpha 1
% xlabel({'km SOM','(g C cm^-^3 soil)'})
% ylabel({'Ea SOM','(kJ mol^-^1)'})

subplot(6,6,8)
plot(km_SOM_posterior,km_SOM_posterior,'.');%pdf for alpha 1
% xlabel({'km SOM','(g C cm^-^3 soil)'})
% ylabel({'km SOM','(g C cm^-^3 soil)'})
subplot(6,6,13)
plot(Ea_labile_posterior,Ea_SOM_posterior,'.');%pdf for km 1
% xlabel({'Ea labile','(kJ mol^-^1)'})
% ylabel({'Ea SOM','(kJ mol^-^1)'})
subplot(6,6,14)
plot(Ea_labile_posterior,km_SOM_posterior,'.');%pdf for km 1
% xlabel({'Ea labile','(kJ mol^-^1)'})
% ylabel({'km SOM','(g C cm^-^3 soil)'})
subplot(6,6,15)
plot(Ea_labile_posterior,Ea_labile_posterior,'.');%pdf for km 1
% xlabel({'Ea labile','(kJ mol^-^1)'})
% ylabel({'Ea labile','(kJ mol^-^1)'})
subplot(6,6,19)
plot(km_labile_posterior,Ea_SOM_posterior,'.');%pdf for km 1
% xlabel('km labile (g C cm^-^3 soil)')
% ylabel({'Ea SOM','(kJ mol^-^1)'})
subplot(6,6,20)
plot(km_labile_posterior,km_SOM_posterior,'.');%pdf for km 1
% xlabel({'km labile','(g C cm^-^3 soil)'})
% ylabel({'km SOM','(g C cm^-^3 soil)'})
subplot(6,6,21)
plot(km_labile_posterior,Ea_labile_posterior,'.');%pdf for km 1
% xlabel({'km labile','(g C cm^-^3 soil)'})
% ylabel({'Ea labile','(kJ mol^-^1)'})
subplot(6,6,22)
plot(km_labile_posterior,km_labile_posterior,'.');%pdf for km 1
% xlabel({'km labile','(g C cm^-^3 soil)'})
% ylabel({'km labile','(g C cm^-^3 soil)'})
subplot(6,6,25)
plot(Ea_oxy_posterior,Ea_SOM_posterior,'.');%pdf for km 1
% xlabel({'Ea oxidation','(kJ mol^-^1)'})
% ylabel({'Ea SOM','(kJ mol^-^1)'})
subplot(6,6,26)
plot(Ea_oxy_posterior,km_SOM_posterior,'.');%pdf for km 1
% xlabel({'Ea oxidation','(kJ mol^-^1)'})
% ylabel({'km SOM','(g C cm^-^3 soil)'})
subplot(6,6,27)
plot(Ea_oxy_posterior,Ea_labile_posterior,'.');%pdf for km 1
% xlabel({'Ea oxidation','(kJ mol^-^1)'})
% ylabel({'Ea labile','(kJ mol^-^1)'})
subplot(6,6,28)
plot(Ea_oxy_posterior,km_labile_posterior,'.');%pdf for km 1
% xlabel({'Ea oxidation','(kJ mol^-^1)'})
% ylabel({'km labile','(g C cm^-^3 soil)'})
subplot(6,6,29)
plot(Ea_oxy_posterior,Ea_oxy_posterior,'.');%pdf for km 1
% xlabel({'Ea oxidation','(kJ mol^-^1)'})
% ylabel({'Ea oxidation','(kJ mol^-^1)'})
subplot(6,6,31)
plot(km_oxy_posterior,Ea_SOM_posterior,'.');%pdf for km 1
% xlabel({'km oxidation','(g C cm^-^3 soil)'})
% ylabel({'Ea SOM','(kJ mol^-^1)'})
subplot(6,6,32)
plot(km_oxy_posterior,km_SOM_posterior,'.');%pdf for km 1
% xlabel({'km oxidation','(g C cm^-^3 soil)'})
% ylabel({'km SOM','(g C cm^-^3 soil)'})
subplot(6,6,33)
plot(km_oxy_posterior,Ea_labile_posterior,'.');%pdf for km 1
% xlabel({'km oxidation','(g C cm^-^3 soil)'})
% ylabel({'Ea labile','(kJ mol^-^1)'})
subplot(6,6,34)
plot(km_oxy_posterior,km_labile_posterior,'.');%pdf for km 1
% xlabel({'km oxidation','(g C cm^-^3 soil)'})
% ylabel({'km labile','(g C cm^-^3 soil)'})
subplot(6,6,35)
plot(km_oxy_posterior,Ea_oxy_posterior,'.');%pdf for km 1
% xlabel({'km oxidation','(g C cm^-^3 soil)'})
% ylabel({'Ea oxidation','(kJ mol^-^1)'})
subplot(6,6,36)
plot(km_oxy_posterior,km_oxy_posterior,'.');%pdf for km 1
% xlabel({'km oxidation','(g C cm^-^3 soil)'})
% ylabel({'km oxidation','(g C cm^-^3 soil)'})
set( gca                       , ...
    'FontName'   , 'Helvetica' );
set([hXLabel, hYLabel, text]  , ...
    'FontSize'   , 8         );

set(gcf, 'PaperPositionMode', 'auto');
print('PEPRMT_CH4_posterior_correlation_matrix_20161010','-dpng','-r600');
%%


Ea_SOM_FINAL=find_min(1)+70;%kJ mol-1
km_SOM_FINAL=find_min(2)*3e3*1e-9*12/100/15;%umol m-2
Ea_labile_FINAL=find_min(3)+70;
km_labile_FINAL=find_min(4)*3e3*1e-9*12/100/15;
Ea_oxy_FINAL=find_min(5)+70;
km_oxy_FINAL=find_min(6)*3e3*1e-9*12/100/15;

%% plot posteriors from all models
%UPDATED 10/2016
figure
subplot(3,4,1)
hist(Ea_SOM_posterior);%pdf for alpha 1
xlabel('E_a _S_O_C (kJ mol^-^1)')
subplot(3,4,2)
hist(km_SOM_posterior);%pdf for ea 1
xlabel('kM_S_O_C (g C cm^-^3 soil)')
subplot(3,4,3)
hist(Ea_labile_posterior);%pdf for km 1
xlabel('E_a _l_a_b_i_l_e (kJ mol^-^1)')
subplot(3,4,4)
hist(km_labile_posterior);%pdf for alpha 2
xlabel('kM_l_a_b_i_l_e (g C cm^-^3 soil)')

subplot(3,4,5)
hist(Mref_posterior_TPGPP);%g C m-2 d-1
xlabel('M_r_e_f (g C m^-^2 d^-^1)')
subplot(3,4,6)
hist(Eo_posterior_TPGPP);%pdf for ea 1
xlabel('E_o (K)')
subplot(3,4,7)
hist(k2_posterior_TPGPP*60*60*24);%pdf for km 1
xlabel('k2')

subplot(3,4,9)
hist(Mref_posterior_TP);%pdf for alpha 1
xlabel('M_r_e_f (g C m^-^2 d^-^1)')
subplot(3,4,10)
hist(Eo_posterior_TP);%pdf for ea 1
xlabel('E_o (K)')
set(gcf, 'PaperPositionMode', 'auto');
print('CH4_ALL_posteriors_20161017','-dpng','-r600');


%% plotting different fluxes and pools in final model simulation UPDATED 10/2016
%MB
%fluxes
pw_MB=nansum(Plant_flux(1:1826));
hw_MB=nansum(Hydro_flux(1:1826));
%fraction released by plant
pw_MB/(pw_MB+hw_MB)
hw_MB/(pw_MB+hw_MB)
%wintertime only
Plant_flux_MB=Plant_flux(1:1826);
Hydro_flux_MB=Hydro_flux(1:1826);
DOY_disc_2_MB=DOY_disc_2(1:1826);
Plant_flux_MB_winter=Plant_flux_MB(DOY_disc_2_MB<30 | DOY_disc_2_MB>335);
Hydro_flux_MB_winter=Hydro_flux_MB(DOY_disc_2_MB<30 | DOY_disc_2_MB>335);
pw_MB_winter=nansum(Plant_flux_MB_winter);
hw_MB_winter=nansum(Hydro_flux_MB_winter);
%fraction released by plant in winter
pw_MB_winter/(pw_MB_winter+hw_MB_winter)
hw_MB_winter/(pw_MB_winter+hw_MB_winter)

%C pools
SOC_MB=nansum(M1(1:1826));
labile_MB=nansum(M2(1:1826));
%fraction released by plant
SOC_MB/(SOC_MB+labile_MB)
labile_MB/(SOC_MB+labile_MB)
%wintertime only
SOC_MB=M1(1:1826);
labile_MB=M2(1:1826);
DOY_disc_2_MB=DOY_disc_2(1:1826);
SOC_MB_winter=SOC_MB(DOY_disc_2_MB<30 | DOY_disc_2_MB>335);
labile_MB_winter=labile_MB(DOY_disc_2_MB<30 | DOY_disc_2_MB>335);
SOC_MB_winter=nansum(SOC_MB_winter);
labile_MB_winter=nansum(labile_MB_winter);
%fraction released by plant in winter
SOC_MB_winter/(SOC_MB_winter+labile_MB_winter)
labile_MB_winter/(SOC_MB_winter+labile_MB_winter)

%WP
pw_WP=nansum(Plant_flux(1827:end));
hw_WP=nansum(Hydro_flux(1827:end));
%fraction released by plant
pw_WP/(pw_WP+hw_WP)
hw_WP/(pw_WP+hw_WP)

%wintertime only
Plant_flux_WP=Plant_flux(1827:end);
Hydro_flux_WP=Hydro_flux(1827:end);
DOY_disc_2_WP=DOY_disc_2(1827:end);
Plant_flux_WP_winter=Plant_flux_WP(DOY_disc_2_WP<30 | DOY_disc_2_WP>335);
Hydro_flux_WP_winter=Hydro_flux_WP(DOY_disc_2_WP<30 | DOY_disc_2_WP>335);
pw_WP_winter=nansum(Plant_flux_WP_winter);
hw_WP_winter=nansum(Hydro_flux_WP_winter);
%fraction released by plant in winter
pw_WP_winter/(pw_WP_winter+hw_WP_winter)
hw_WP_winter/(pw_WP_winter+hw_WP_winter)

%C pools
SOC_WP=nansum(M1(1:1826));
labile_WP=nansum(M2(1:1826));
%fraction released by plant
SOC_WP/(SOC_WP+labile_WP)
labile_WP/(SOC_WP+labile_WP)
%wintertime only
SOC_WP=M1(1:1826);
labile_WP=M2(1:1826);
DOY_disc_2_WP=DOY_disc_2(1:1826);
SOC_WP_winter=SOC_WP(DOY_disc_2_WP<30 | DOY_disc_2_WP>335);
labile_WP_winter=labile_WP(DOY_disc_2_WP<30 | DOY_disc_2_WP>335);
SOC_WP_winter=nansum(SOC_WP_winter);
labile_WP_winter=nansum(labile_WP_winter);
%fraction released by plant in winter
SOC_WP_winter/(SOC_WP_winter+labile_WP_winter)
labile_WP_winter/(SOC_WP_winter+labile_WP_winter)


 %% plotting validation data UPDATED 10/2016
 %9 panel figure
days_plot_MB_valid=daily_DOY_valid(1:365);
days_plot_WP_valid=daily_DOY_valid(366:end);

startDate = datenum('01-01-2015');
endDate = datenum('12-31-2015');
xData1=linspace(startDate,endDate,365);

X = ones(length(CH4_mod_min_valid(1:365)),1);
X = [X CH4_mod_min_valid(1:365)];
[~,~,~,~,STATS] = regress(daily_wm_sum_umol_gf_valid(1:365),X);
rsq=STATS(1);

X = ones(length(CH4_mod_min_valid(366:end)),1);
X = [X CH4_mod_min_valid(366:end)];
[~,~,~,~,STATS2] = regress(daily_wm_sum_umol_gf_valid(366:end),X);
rsq2=STATS2(1);

X = ones(length(ch4_min_EE'),1);
X = [X ch4_min_EE'];
[~,~,~,~,STATS3] = regress(daily_wm_sum_umol_gf_EE,X);
rsq3=STATS3(1);

%for cum plots
date1_plot=xData1(365);

cum_obs_valid_MB_plot=cum_obs_valid_MB*10^-6*16.04*0.75;
cum_obs_valid_upper_MB_plot=cum_obs_valid_upper_MB*10^-6*16.04*0.75;
cum_obs_valid_lower_MB_plot=cum_obs_valid_lower_MB*10^-6*16.04*0.75;

cum_obs_valid_MB_plot=cum_obs_valid_MB_plot(365);
cum_obs_valid_upper_MB_plot=cum_obs_valid_upper_MB_plot(365);
cum_obs_valid_lower_MB_plot=cum_obs_valid_lower_MB_plot(365);

cum_obs_valid_MB_plot_error=cum_obs_valid_upper_MB_plot-cum_obs_valid_MB_plot;
cum_obs_valid_MB_plot_error=cum_obs_valid_MB_plot_error/2;

cum_obs_valid_WP_plot=cum_obs_valid_WP*10^-6*16.04*0.75;
cum_obs_valid_upper_WP_plot=cum_obs_valid_upper_WP*10^-6*16.04*0.75;
cum_obs_valid_lower_WP_plot=cum_obs_valid_lower_WP*10^-6*16.04*0.75;

cum_obs_valid_WP_plot=cum_obs_valid_WP_plot(365);
cum_obs_valid_upper_WP_plot=cum_obs_valid_upper_WP_plot(365);
cum_obs_valid_lower_WP_plot=cum_obs_valid_lower_WP_plot(365);

cum_obs_valid_WP_plot_error=cum_obs_valid_upper_WP_plot-cum_obs_valid_WP_plot;
cum_obs_valid_WP_plot_error=cum_obs_valid_WP_plot_error/2;

cum_obs_valid_EE_plot=cum_obs_valid_EE*10^-6*16.04*0.75;
cum_obs_valid_upper_EE_plot=cum_obs_valid_upper_EE*10^-6*16.04*0.75;
cum_obs_valid_lower_EE_plot=cum_obs_valid_lower_EE*10^-6*16.04*0.75;

cum_obs_valid_EE_plot=cum_obs_valid_EE_plot(365);
cum_obs_valid_upper_EE_plot=cum_obs_valid_upper_EE_plot(365);
cum_obs_valid_lower_EE_plot=cum_obs_valid_lower_EE_plot(365);

cum_obs_valid_EE_plot_error=cum_obs_valid_upper_EE_plot-cum_obs_valid_EE_plot;
cum_obs_valid_EE_plot_error=cum_obs_valid_EE_plot_error/2;
%% 
figure ('Units', 'pixels',...
    'Position', [100 100 500 375]);
subplot(3,3,1)
ciplot(CH4_mod_min_valid_lower(1:365)*0.0120,CH4_mod_min_valid_upper(1:365)*0.0120,xData1,'k')
hold on
obs = plot(xData1,daily_wm_sum_umol_gf_valid(1:365)*0.0120,'.');
%set(gca, 'XTick', [startDate:150:endDate] );
datetick('x','mm/yyyy','keeplimits')
box off
hYLabel = ylabel({'CH_4 exchange','(mg C-CH_4 m^-^2 d^-^1)' });
 legend('model','obs')

subplot(3,3,4)
ciplot(CH4_mod_min_valid_lower(366:end)*0.0120,CH4_mod_min_valid_upper(366:end)*0.0120,xData1,'k')
hold on
obs = plot(xData1,daily_wm_sum_umol_gf_valid(366:end)*0.0120,'.');
%set(gca, 'XTick', [735062:150:735964] );
%set(gca, 'XTick', [startDate2:150:endDate2] );
datetick('x','mm/yyyy','keeplimits')
box off
hXLabel = xlabel('Day of year'                     );
hYLabel = ylabel({'CH_4 exchange','(mg C-CH_4 m^-^2 d^-^1)' });

subplot(3,3,7)
ciplot(lower_ch4_EE*0.0120,upper_ch4_EE*0.0120,xData1,'k')
hold on
obs = plot(xData1,daily_wm_sum_umol_gf_EE*0.0120,'.');
%set(gca, 'XTick', [735062:150:735964] );
%set(gca, 'XTick', [startDate2:150:endDate2] );
datetick('x','mm/yyyy','keeplimits')
box off
hXLabel = xlabel('Day of year'                     );
hYLabel = ylabel({'CH_4 exchange','(mg C-CH_4 m^-^2 d^-^1)' });

subplot(3,3,2)
obs=plot(daily_wm_sum_umol_gf_valid(1:365)*0.0120,CH4_mod_min_valid(1:365)*0.0120,'.');
hXLabel = xlabel('Model (mg C-CH_4 m^-^2 d^-^1)');
hYLabel = ylabel('Obs (mg C-CH_4 m^-^2 d^-^1)');
box off
% xlim([0,450])
% ylim([0,450])
set(obs                       , ...
  'LineWidth'       , 1           , ...
  'Marker'          , 'o'         , ...
  'MarkerSize'      , 3           , ...
  'MarkerEdgeColor' , [0.25 0.25 0.25]  , ...
  'MarkerFaceColor' , [0.25 0.25 0.25]  );
h=lsline;
p2 = polyfit(get(h,'xdata'),get(h,'ydata'),1);
theString = sprintf('y = %.2f x + %.2f', p2(1), p2(2));
place_x=min(CH4_mod_min_valid(1:365)*0.0120);
place_y=max(daily_wm_sum_umol_gf_valid(1:365)*0.0120);
t=text(place_x,place_y,theString);%;t=text(2,-7,'p2');%puts letter in upper L corner
theString = sprintf('r^2 = %.2f', rsq);
t=text(1,400,theString);%puts letter in upper L corner
refline(1,0)
 
subplot(3,3,5)
obs=plot(daily_wm_sum_umol_gf_valid(366:end)*0.0120,CH4_mod_min_valid(366:end)*0.0120,'.');
hXLabel = xlabel('Model (mg C-CH_4 m^-^2 d^-^1)');
hYLabel = ylabel('Obs (mg C-CH_4 m^-^2 d^-^1)');
box off
% xlim([0,450])
% ylim([0,450])
set(obs                       , ...
  'LineWidth'       , 1           , ...
  'Marker'          , 'o'         , ...
  'MarkerSize'      , 3           , ...
  'MarkerEdgeColor' , [0.25 0.25 0.25]  , ...
  'MarkerFaceColor' , [0.25 0.25 0.25]  );
h=lsline;
p2 = polyfit(get(h,'xdata'),get(h,'ydata'),1);
theString = sprintf('y = %.2f x + %.2f', p2(1), p2(2));
place_x=min(CH4_mod_min_valid(366:end)*0.0120);
place_y=max(daily_wm_sum_umol_gf_valid(366:end)*0.0120);
t=text(place_x,place_y,theString);%;t=text(2,-7,'p2');%puts letter in upper L corner
theString = sprintf('r^2 = %.2f', rsq2);
t=text(1,400,theString);%puts letter in upper L corner
refline(1,0)

subplot(3,3,8)
obs=plot(daily_wm_sum_umol_gf_EE*0.0120,ch4_min_EE*0.0120,'.');
hXLabel = xlabel('Model (mg C-CH_4 m^-^2 d^-^1)');
hYLabel = ylabel('Obs (mg C-CH_4 m^-^2 d^-^1)');
box off
% xlim([0,450])
% ylim([0,450])
set(obs                       , ...
  'LineWidth'       , 1           , ...
  'Marker'          , 'o'         , ...
  'MarkerSize'      , 3           , ...
  'MarkerEdgeColor' , [0.25 0.25 0.25]  , ...
  'MarkerFaceColor' , [0.25 0.25 0.25]  );
h=lsline;
p2 = polyfit(get(h,'xdata'),get(h,'ydata'),1);
theString = sprintf('y = %.2f x + %.2f', p2(1), p2(2));
place_x=min(ch4_min_EE*0.0120);
place_y=max(daily_wm_sum_umol_gf_EE*0.0120);
t=text(place_x,place_y,theString);%;t=text(2,-7,'p2');%puts letter in upper L corner
theString = sprintf('r^2 = %.2f', rsq3);
t=text(1,400,theString);%puts letter in upper L corner
refline(1,0)

subplot(3,3,3)
obs3=errorbar(date1_plot,cum_obs_valid_MB_plot,cum_obs_valid_MB_plot_error,'o');
hold on
ciplot(cum_mod_max_valid_upper_MB*10^-6*16.04*0.75,cum_mod_valid_lower_MB*10^-6*16.04*0.75,xData1)
datetick('x','mm/yyyy')
legend('mod','obs')
xlabel('DOY')
ylabel({'Cummulative CH_4','(g C-CH_4 m^-^2)'})
 box off
 subplot(3,3,6)
ciplot(cum_mod_max_valid_upper_WP*10^-6*16.04*0.75,cum_mod_valid_lower_WP*10^-6*16.04*0.75,xData1)
datetick('x','mm/yyyy')
hold on
obs6=errorbar(date1_plot,cum_obs_valid_WP_plot,cum_obs_valid_WP_plot_error,'o');
datetick('x','mm/yyyy')
hold on
xlabel('DOY')
ylabel({'Cummulative CH_4','(g C-CH_4 m^-^2)'})
 box off
  subplot(3,3,9)
ciplot(cum_mod_max_valid_upper_EE*10^-6*16.04*0.75,cum_mod_valid_lower_EE*10^-6*16.04*0.75,xData1)
datetick('x','mm/yyyy')
hold on
obs9=errorbar(date1_plot,cum_obs_valid_EE_plot,cum_obs_valid_EE_plot_error,'o');
datetick('x','mm/yyyy')
hold on
xlabel('DOY')
ylabel({'Cummulative CH_4','(g C-CH_4 m^-^2)'})
 box off


set( gca                       , ...
    'FontName'   , 'Helvetica' );
set([hXLabel, hYLabel, text]  , ...
    'FontSize'   , 12         );
 
set(gcf, 'PaperPositionMode', 'auto');
print('CH4_validation_MB_WP_EE','-dpng','-r600');

%% met plot
startDate = datenum('01-01-2011');
endDate = datenum('12-31-2014');
xData1=linspace(startDate,endDate,1461);

startDate2 = datenum('07-12-2012');
endDate2 = datenum('12-31-2014');
xData2=linspace(startDate2,endDate2,902);

startDate3 = datenum('01-01-2015');
endDate3 = datenum('12-31-2015');
xData3=linspace(startDate3,endDate3,365);

figure
subplot(3,1,1)
plot(xData1,daily_TA_param(1:1461),'.k')%MB param TA
hold on
plot(xData2,daily_TA_param(1462:end),'.b')%WP param TA
hold on
plot(xData3,daily_TA_EE,'.g')%EE data
hold on
plot(xData3,daily_TA_valid(1:365),'.k')%MB valid TA
hold on
plot(xData3,daily_TA_valid(366:end),'.b')%WP valid TA
datetick('x','mm/yyyy')
ylabel({'Air Temperature','(?C)'})
ylim([0 30])
subplot(3,1,2)
plot(xData1,daily_PAR_gf_param(1:1461),'.r')%MB param PAR
hold on
plot(xData2,daily_PAR_gf_param(1462:end),'.b')%WP param TA
hold on
plot(xData3,daily_PAR_gf_valid(1:365),'.r')%MB valid TA
hold on
plot(xData3,daily_PAR_gf_valid(366:end),'.b')%WP valid TA
hold on
plot(xData3,daily_PAR_gf_EE,'.g')%EE data
datetick('x','mm/yyyy')
ylabel({'PAR', '(\mumol m^-^2 s^-^1)'})
subplot(3,1,3)
plot(xData1,daily_WT_gf_param(1:1461),'.r')%MB param PAR
hold on
plot(xData2,daily_WT_gf_param(1462:end),'.b')%WP param TA
hold on
plot(xData3,daily_WT_gf_EE,'.g')%EE data
hold on
plot(xData3,daily_WT_gf_valid(1:365),'.r')%MB valid TA
hold on
plot(xData3,daily_WT_gf_valid(366:end),'.b')%WP valid TA
datetick('x','mm/yyyy')
ylabel({'Water table height', '(m)'})
ylim([-50 150])
legend('MB','WP','EE')

set(gcf, 'PaperPositionMode', 'auto');
print('Met_Variables__MB_WP_EE','-dpng','-r600');

%% inhibition figure

figure
subplot(3,1,1)
plot(DOY_disc_2,M_percent_reduction_2,'.')
subplot(3,1,2)
plot(WT_2,M_percent_reduction,'.')
subplot(3,1,3)
plot(

 %% extra plotting OLD CODE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 residual=daily_ch4_obs-daily_ch4_mod_min;
 
 figure
 obs=plot(daily_ch4_mod_min,residual,'.');
 xlabel('DAMM model (mg C-CH_4 m^-^2 d^-^1)');
 ylabel('Residual (obs-mod)')
 box off
 set(obs                       , ...
  'LineWidth'       , 1           , ...
  'Marker'          , 'o'         , ...
  'MarkerSize'      , 3          , ...
  'MarkerEdgeColor' , [.4 .4 .4]  , ...
  'MarkerFaceColor' , [1 1 1]  );

set(gcf, 'PaperPositionMode', 'auto');
print('PEPRMT_CH4_residual_20150831','-dpng','-r600');

 
 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure
subplot(2,1,1)
plot(date1,((1-M_percent_reduction)*100),'ko',...
    'LineWidth',0.5,...
    'MarkerSize',3,...
    'MarkerEdgeColor','k',...
    'MarkerFaceColor',[0,0,0]);
box off
ylabel('Inhibition of CH_4 production (%)');
set(gca,'FontSize',12)
x2 =1;
y2 = 2;
str2 = '(a)';
text(x2,y2,str2)

subplot(2,1,2)
plot(date1,daily_WT_gf,'ko',...
     'LineWidth',0.5,...
    'MarkerSize',3,...
    'MarkerEdgeColor','k',...
    'MarkerFaceColor',[0,0,0]);
box off
ylabel('Water table (cm)');
set(gca,'FontSize',12)

set(gcf, 'PaperPositionMode', 'auto');
print('WT_CH4_inhibition_20150622','-dpng','-r600');
%print -depsc2 finalPlot1_inset.eps
close;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
WT_2_adj=(WT_gf/100)+1;%when wt=1 at soil surface
for t=1:length(WT_gf)
   if WT_gf(t)<0
    M_percent_reduction(t)=(0.48*WT_2_adj(t).^2)+(-0.18*WT_2_adj(t))+0.0042;%more sensitive
   else
    M_percent_reduction(t)=1;
   end
end
figure
plot(WT_gf, M_percent_reduction,'.')

Sel=percent_reduction>1;
percent_reduction(Sel)=1;

figure ('Units', 'pixels',...
    'Position', [100 100 500 375]);
subplot(2,1,1)
plot(WT_gf,((1-percent_reduction)*100),'.')
ylabel('Inhibition of R_e_c_o (%)');
subplot(2,1,2)
plot(WT_gf, ((M_percent_reduction*100)-100)*-1,'.')
ylabel('Inhibition of CH_4 production (%)');
xlabel('Water table (cm)');


set(obs                           , ...
  'LineStyle'       , 'none'      , ...
  'Marker'          , '.'         , ...
  'Color'           , [0 0 0]  );
set(obs                       , ...
  'LineWidth'       , 1           , ...
  'Marker'          , '.'         , ...
  'MarkerSize'      , 15          , ...
  'MarkerEdgeColor' , [.2 .2 .2]  , ...
  'MarkerFaceColor' , [.7 .7 .7]  );
set(gcf, 'PaperPositionMode', 'auto');
print('Reco_AND_CH4_inhibition_WT_gf_20150828','-dpng','-r600');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure ('Units', 'pixels',...
    'Position', [100 100 500 375]);
hold on
obs = plot(days_plot,daily_WT_gf,'.');
%axis([0 350 0 200])
set(obs                           , ...
  'LineStyle'       , 'none'      , ...
  'Marker'          , '.'         , ...
  'Color'           , [0 0 0]  );
set(obs                       , ...
  'LineWidth'       , 1           , ...
  'Marker'          , '.'         , ...
  'MarkerSize'      , 15          , ...
  'MarkerEdgeColor' , [.2 .2 .2]  , ...
  'MarkerFaceColor' , [.7 .7 .7]  );
hXLabel = xlabel('Day of year');
hYLabel = ylabel('Water table height (cm)');
 
%legend('y = 0.78*x + 6.6')
% 
% set([legend, gca]             , ...
%     'FontSize'   , 14           );
set([hXLabel, hYLabel, text]  , ...
    'FontSize'   , 14         );
 
set(gca, ...
  'Box'         , 'off'     , ...
  'TickDir'     , 'out'     , ...
  'TickLength'  , [.02 .02] , ...
  'XMinorTick'  , 'off'      , ...
  'YMinorTick'  , 'off'      , ...
  'YGrid'       , 'off'      , ...
  'XColor'      , [0 0 0], ...
  'YColor'      , [0 0 0], ...
  'Xtick'       , 0:100:1100,...
  'YTick'       , -50:10:50, ...
  'LineWidth'   , 1         );
 
set(gcf, 'PaperPositionMode', 'auto');
print('WT_20150617','-dpng','-r600');
%print -depsc2 finalPlot1_inset.eps
close;
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure
plot(date1,Plant_flux_net*10^-3*16.04*0.75,'.',date1,Hydro_flux*10^-3*16.04*0.75,'.')
legend('plant','hydro')
ylabel('CH_4 flux (mg C-CH_4 m^-^2 d^-^1)')
box off
set('FontSize'   , 12         );
set(gcf, 'PaperPositionMode', 'auto');
print('PEPRMT_CH4_plant_hydro_20150624','-dpng','-r600');

plant_total=cumsum(Plant_flux_net*10^-3*16.04*0.75);
plant_total=plant_total(end);
hydro_total=cumsum(Hydro_flux*10^-3*16.04*0.75);
hydro_total=hydro_total(end);

(plant_total-hydro_total)/plant_total

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure
plot(date1,M1*1e-3*16.04*0.75*60*60*24,'.',date1,M2*1e-3*16.04*0.75*60*60*24,'.')
legend('SOM','labile')
ylabel('CH_4 flux (mg C-CH_4 m^-^2 d^-^1)')
box off
set('FontSize'   , 12         );
set(gcf, 'PaperPositionMode', 'auto');
print('PEPRMT_CH4_SOM_labile_20150624','-dpng','-r600');

SOM_total=cumsum(M1*10^-3*16.04*0.75*60*60*24);
SOM_total=SOM_total(end);
labile_total=cumsum(M2*10^-3*16.04*0.75*60*60*24);
labile_total=labile_total(end);

(labile_total-SOM_total)/labile_total

