%New code for parameterizing PAMM CO2 Reco and GPP models separately
%5.17.2015
%Data you need to run all codes
data_master.daily=[daily_wm daily_wc daily_gapfilling_error_wm daily_gapfilling_error_wc daily_random_error_wm daily_random_error_wc daily_er_ANNnight daily_gpp_ANNnight];
data_master.halfhr=[DOY DOY_disc TA WT_gf PAR_gf LAI_gf GPP_min decday Season_drop];
data_master.halfhr_CI_gapfill=[wc_90CI wc_90CI_lower wc_90CI_upper wm_90CI wm_90CI_lower wm_90CI_upper];
data_master.halfhr_CI_random=[random_error_wc_prop_lower random_error_wc_prop_upper random_error_wm_prop_lower random_error_wm_prop_upper];
data_master.halfhr_obs=[wc wc_gf wm wm_gf gpp_ANNnight er_ANNnight];

%growing season mean temp 2013 and 2014
TA_ave_2013=nanmean(TA(8256:25776));
TA_ave_2014=nanmean(TA(25777:end));
GPP_cum_2013=(gpp_ANNnight(8256:25776))*60*30;
GPP_cum_2014=(gpp_ANNnight(25777:end))*60*30;
GPP_cum_2013=cumsum(GPP_cum_2013);
GPP_cum_2014=cumsum(GPP_cum_2014);
GPP_cum_2013=GPP_cum_2013(end)*10^-6*44.01*0.27;%g C m-2 yr-1
GPP_cum_2014=GPP_cum_2014(end)*10^-6*44.01*0.27;

ch4_cum_2013=(wm_gf(8256:25776))*60*30;
ch4_cum_2014=(wm_gf(25777:end))*60*30;
ch4_cum_2013=cumsum(ch4_cum_2013);
ch4_cum_2014=cumsum(ch4_cum_2014);
ch4_cum_2013=ch4_cum_2013(end)*10^-9*16.04*0.75;%g C m-2 yr-1
ch4_cum_2014=ch4_cum_2014(end)*10^-9*16.04*0.75;


%save chain results in simple format
chaindata.findmin=[find_min find_min_2];
chaindata.long=[chain_long sschain_long];
chaindata.short=[sschain chain];

find_min =chaindata.findmin(:,1);
find_min_2 =chaindata.findmin(:,2);
chain_long  =chaindata.long(:,1:4);
sschain_long=chaindata.long(:,5);
sschain =chaindata.short(:,1);
chain=chaindata.short(:,2:5);
 
%paramerterization data
%run using modeled GPP
NEE_fake=NEE_fake';
GPP_fake=GPP_fake';
daily_Reco_fake=daily_Reco_fake';
daily_GPP_fake=daily_GPP_fake';
xdata=[DOY(1:25776) DOY_disc(1:25776) TA(1:25776) WT_gf(1:25776) PAR_gf(1:25776) LAI_gf(1:25776) GPP_fake(1:25776) decday(1:25776) Season_drop(1:25776)];
ydata=[DOY(1:25776) NEE_fake(1:25776)];%this has NEE umol m-2 s-1 and CH4 umol m-2 30min-1
zdata=[gf_error_fake_wc(1:538) re_error_fake_wc(1:538) daily_Reco_fake(1:538) daily_GPP_fake(1:538)];

theta=[0 1 0 1];
theta=theta';
[NEE_mod, S1, S2, Reco] = PAMM_final_sys_CO2_Reco(theta,xdata);
figure
plot(Reco)
hold on
plot(Reco_fake(1:25776))

 

 
%% start MDF

%use for running PAMM_final_sys_CO2
model.ssfun = @PAMM_final_ss_CO2_Reco;
model.modelfun = @PAMM_final_sys_CO2_Reco;

data.xdata = xdata;
data.ydata = ydata;
data.zdata = zdata;
 
 Rsum_obs = data.ydata(:,2);
 nan_obs = sum(isnan(Rsum_obs));
 n_obs = length(Rsum_obs) - nan_obs;
model.N     = n_obs;%total number of non nan obs
%model.S20 = [1 1 2];%has to do with error variance of prior
%model.N0  = [4 4 4];%has to do with error variance of prior
 
params = {
%     {'ea1',           0,     -5,         5}%eas allowed to go 10-80 , chain 1=1 , chain 2=30, chain 3=60 or 19, then range here is -10 to 60
%     {'km1',           1,     1e-3,        1e3}%chain1=1, chain 2=5, chain 3=1e2
%     {'ea2',           0,     -5,         5}
%     {'km2',           1,     1e-3,        1e3}
    {'C_allocation',  0.5,     0.1,        1}
%     name      start/theta/par  lBound   uBound    mean_prior   std_prior
    };
 
options.method = 'dram';
options.verbosity  = 0;%level of info printed
options.burnintime = 50000;% burn in before adaptation starts
%[results, chain, s2chain] = mcmcrun_FINAL_FINAL(model,data,params,options,results);
% [RESULTS,CHAIN,S2CHAIN,SSCHAIN] = mcmcrun_FINAL_FINAL(MODEL,DATA,PARAMS,OPTIONS)
%First generate an initial chain
options.nsimu = 50000;
[results, chain, s2chain, sschain]= mcmcrun_FINAL(model,data,params,options);

%find the minimum
figure
plot(sschain,'.')%sum-of-squares chains
nanmin(sschain)
Sel=sschain<nanmin(sschain)+0.000000000000000001735;%442
figure
plot(Sel)
% cumsum(Sel)
find_min=chain(Sel,:);
find_min=find_min(end,:)';
theta=find_min;
figure
%plot(chain(:,1))
hist(chain(:,1))

%Then re-run starting from the results of the previous run, this will take about 10 minutes.
params = {
%     {'ea1',          find_min(1),     -5,         5}
%     {'km1',          find_min(2),     1e-3,        1e3}
%     {'ea2',          find_min(3),     -5,         5}
%     {'km2',          find_min(4),     1e-3,        1e3}
    {'C_allocation',  find_min(1),     0.1,        1}
%     name      start/theta/par  lBound   uBound    mean_prior   std_prior
    };
 
options.method = 'dram';
options.verbosity  = 0;%level of info printed
options.burnintime = 0;% burn in before adaptation starts
 
options.nsimu = 150000;
[results, chain, s2chain, sschain] = mcmcrun_FINAL(model,data,params,options, results);
 
figure
mcmcplot(chain,[],results,'chainpanel');%plots chain for ea parameter and all the values it accepted
figure
mcmcplot(chain,[],results,'pairs');%tells you if parameters are correlated
%  figure
%  mcmcplot(sqrt(s2chain),[],[],'hist')%take square root of the s2chain to get the chain for error standard deviation
% title('Error std posterior')
figure
mcmcplot(chain,[],results,'denspanel',2);

sschain_long=sschain;%save before you move on
chain_long=chain;
results_long=results;

%find the minimum
figure
plot(sschain_long,'.')%sum-of-squares chains
nanmin(sschain_long)
Sel=sschain_long<nanmin(sschain_long)+0.000001;%442
figure
plot(Sel)
% cumsum(Sel)
find_min=chain_long(Sel,:);
find_min=find_min(end,:)';

[~, ~, ~, Reco_mod_min] = PAMM_final_sys_CO2_Reco(find_min,xdata);
figure
plot(decday(1:25776),Reco_fake(1:25776)','.',decday(1:25776),Reco_mod_min,'.')
legend('obs','mod')

%% Choose posterior

%First establish if chain stabilized
chainstats(chain,results)
%Geweke test should be high ideally >0.9 for all parameters

%rejection rate of chain needs to be less than 50%
results_long.rejected*100

%Determine where means and sd of parameters stabilize in the chain
%this tells you what part of chain is stable for selection of posterior

%see where means and sd of parameters stabilize in the chain
%this tells you what part of chain is stable for selection of posterior
window_size = 5000;
simple = tsmovavg(chain_long(:,1),'s',window_size,1);
simple_std = movingstd(chain_long(:,1),5000);
figure
subplot(2,1,1)
plot(simple,'.')
title('5000 iteration moving average EA SOM')
subplot(2,1,2)
plot(simple_std,'.')
title('5000 iteration moving SD EA SOM')

window_size = 5000;
simple = tsmovavg(chain_long(:,2),'s',window_size,1);
simple_std = movingstd(chain_long(:,2),5000);
figure
subplot(2,1,1)
plot(simple,'.')
title('5000 iteration moving average km SOM')
subplot(2,1,2)
plot(simple_std,'.')
title('5000 iteration moving SD km SOM')

window_size = 5000;
simple = tsmovavg(chain_long(:,3),'s',window_size,1);
simple_std = movingstd(chain_long(:,3),5000);
figure
subplot(2,1,1)
plot(simple,'.')
title('5000 iteration moving average EA labile')
subplot(2,1,2)
plot(simple_std,'.')
title('5000 iteration moving SD EA labile')

window_size = 5000;
simple = tsmovavg(chain_long(:,4),'s',window_size,1);
simple_std = movingstd(chain_long(:,4),5000);
figure
subplot(2,1,1)
plot(simple,'.')
title('5000 iteration moving average km labile')
subplot(2,1,2)
plot(simple_std,'.')
title('5000 iteration moving SD km labile')

%identified visually that chain is good around 100,000
%choose parammeters 10,000 parameter sets from that area
%e.g. 100,000-101,000

find_min_10000=chain(140001:150000,:);%update everytime
%now select 1000 unique parameter sets from these 10,000, that are
%distributed across the range
figure
hist(find_min_10000(:,1))
%bin data and sample evenly across bins


%run 10000 parameter sets through all data+ and test chi square
xdata=[DOY DOY_disc TA WT_gf PAR_gf LAI_gf GPP_min decday Season_drop wc_90CI];
ydata=[DOY wc_gf];%this has NEE umol m-2 s-1 and CH4 umol m-2 30min-1
zdata=[daily_gapfilling_error_wc' daily_random_error_wc' daily_er_ANNnight' daily_gpp_ANNnight'];

%make 1000 theta sets that will be used to estimate CI 
model_pop_Reco=zeros(length(1:43296));%length of validation data
model_pop_Reco=model_pop_Reco(:,1:10000);%update as needed 
 
for i=1:length(find_min_10000)
    [~, ~, ~, Reco] = PAMM_final_sys_CO2_Reco(find_min_10000(i,:)',xdata);
    model_pop_Reco(:,i)=Reco;
    
end

model_std_Reco=std(model_pop_Reco');
model_mean_Reco=mean(model_pop_Reco');
mean_theta_final=nanmean(find_min_10000);
mean_theta_final=mean_theta_final';

%% according to Zobitz 2008, model mean==model min(best para set)
[NEE_mod_min, S1, S2, Reco_min]  = PAMM_final_sys_CO2_Reco(find_min,xdata);
[NEE_mod_mean, S1_mean, S2_mean, Reco_mean]  = PAMM_final_sys_CO2_Reco(mean_theta_final,xdata);

figure
plot(decday,er_ANNnight,'.',decday,Reco_min,'.')
legend('obs','mod min')
figure
plot(decday,er_ANNnight,'.',decday,Reco_mean,'.')
legend('obs','mod mean')

figure
plot(decday,wc_gf,'.',decday,NEE_mod_min,'.')
legend('obs','mod min')
figure
plot(decday,wc_gf,'.',decday,NEE_mod_mean,'.')
legend('obs','mod mean')
 
figure
plot(model_std_Reco,'.')
figure
plot(model_std,'.')

% 90% confidence intervals (if you want 95% use 1.96)
%b/c the 10,000 parameters are not independent or may even be redundant, I
%reduce the sample size to 1000
lower_Reco=Reco_min-(1.645*(model_std_Reco./sqrt(50)));%use 1.645 for 90% CI
upper_Reco=Reco_min+(1.645*(model_std_Reco./sqrt(50)));
 
figure
plot(lower_Reco,'.')
hold on
plot(upper_Reco,'.')

%RMSE is a better test of model performance than r2
RMSE_reco_min=nansum((Reco_min'-er_ANNnight).^2);
RMSE_reco_min=sqrt(RMSE_reco_min/43296);
 
X = ones(length(Reco_min'),1);
X = [X Reco_min'];
[~,~,~,~,STATS] = regress(er_ANNnight,X);
 
%% annual sum test

 %Validation stats
reco_dailysum=er_ANNnight(25777:end)*60*30;%convert from nmol m-2 s-1 to nmol m-2 day-1
cum_obs_valid=cumsum(reco_dailysum);%umol m-2 yr-1
cum_obs_valid_sum=cum_obs_valid(end)*10^-6;%mol CO2 m-2 yr-1
cum_obs_valid_sum=cum_obs_valid_sum*44.01*0.27;%g C-CO2 m-2 yr-1

mod_dailysum=Reco_min(25777:end)*60*30;%convert from nmol m-2 s-1 to nmol m-2 day-1
cum_mod_valid=cumsum(mod_dailysum);%nmol m-2 yr-1
cum_mod_valid_sum=cum_mod_valid(end)*10^-6;%mol m-2 yr-1
cum_mod_valid_sum=cum_mod_valid_sum*44.01*0.27;%g C-CO2 m-2 yr-1

mod_dailysum_min=lower_Reco(25777:end)*60*30;%convert from nmol m-2 s-1 to nmol m-2 day-1
cum_mod_min_valid=cumsum(mod_dailysum_min);%nmol m-2 yr-1
cum_mod_min_valid_sum=cum_mod_min_valid(end)*10^-6;%mol m-2 yr-1
cum_mod_min_valid_sum=cum_mod_min_valid_sum*44.01*0.27;%g C-CO2 m-2 yr-1
 
mod_dailysum_max=upper_Reco(25777:end)*60*30;%convert from nmol m-2 s-1 to nmol m-2 day-1
cum_mod_max_valid=cumsum(mod_dailysum_max);%nmol m-2 yr-1
cum_mod_max_valid_sum=cum_mod_max_valid(end)*10^-6;%mol m-2 yr-1
cum_mod_max_valid_sum=cum_mod_max_valid_sum*44.01*0.27;%g C-CO2 m-2 yr-1

cum_mod_max_valid_sum-cum_mod_valid_sum
%Param stats
reco_dailysum=er_ANNnight(1:25776)*60*30;%convert from nmol m-2 s-1 to nmol m-2 day-1
cum_obs_param=cumsum(reco_dailysum);%umol m-2 yr-1
cum_obs_param_sum=cum_obs_param(end)*10^-6;%mol CO2 m-2 yr-1
cum_obs_param_sum=cum_obs_param_sum*44.01*0.27;%g C-CO2 m-2 yr-1

mod_dailysum=Reco_min(1:25776)*60*30;%convert from nmol m-2 s-1 to nmol m-2 day-1
cum_mod_param=cumsum(mod_dailysum);%nmol m-2 yr-1
cum_mod_param_sum=cum_mod_param(end)*10^-6;%mol m-2 yr-1
cum_mod_param_sum=cum_mod_param_sum*44.01*0.27;%g C-CO2 m-2 yr-1

mod_dailysum_min=lower_Reco(1:25776)*60*30;%convert from nmol m-2 s-1 to nmol m-2 day-1
cum_mod_min_param=cumsum(mod_dailysum_min);%nmol m-2 yr-1
cum_mod_min_param_sum=cum_mod_min_param(end)*10^-6;%mol m-2 yr-1
cum_mod_min_param_sum=cum_mod_min_param_sum*44.01*0.27;%g C-CO2 m-2 yr-1
 
mod_dailysum_max=upper_Reco(1:25776)*60*30;%convert from nmol m-2 s-1 to nmol m-2 day-1
cum_mod_max_param=cumsum(mod_dailysum_max);%nmol m-2 yr-1
cum_mod_max_param_sum=cum_mod_max_param(end)*10^-6;%mol m-2 yr-1
cum_mod_max_param_sum=cum_mod_max_param_sum*44.01*0.27;%g C-CO2 m-2 yr-1

cum_mod_max_param_sum-cum_mod_param_sum

figure
plot(decday(25777:end),cum_obs_valid,'.')
hold on
plot(decday(25777:end),cum_mod_min_valid,'.',decday(25777:end),cum_mod_max_valid,'.')
legend('obs','modmin','modmax')

%% add together Reco and GPP modeled to compare to NEE obs
NEE_mod=Reco_min+GPP_min;
upper_NEE=upper_Reco+upper_GPP;
lower_NEE=lower_Reco+lower_GPP;

%Validation stats
nee_dailysum=wc_gf(25777:end)*60*30;%convert from nmol m-2 s-1 to nmol m-2 day-1
cum_obs_valid=cumsum(nee_dailysum);%umol m-2 yr-1
cum_obs_valid_sum=cum_obs_valid(end)*10^-6;%mol CO2 m-2 yr-1
cum_obs_valid_sum=cum_obs_valid_sum*44.01*0.27;%g C-CO2 m-2 yr-1

NEE_obs_lower=wc_90CI_lower+random_error_wc_prop_lower;
NEE_obs_upper=wc_90CI_upper+random_error_wc_prop_upper;
figure
plot(decday,wc_gf,'.',decday,NEE_obs_lower,'.',decday,NEE_obs_upper,'.')
legend('obs','total error low','total error high')

nee_dailysum_lower=NEE_obs_lower(25777:end)*60*30;%convert from nmol m-2 s-1 to nmol m-2 day-1
cum_obs_valid_lower=cumsum(nee_dailysum_lower);%umol m-2 yr-1
cum_obs_valid_sum_lower=cum_obs_valid_lower(end)*10^-6;%mol CO2 m-2 yr-1
cum_obs_valid_sum_lower=cum_obs_valid_sum_lower*44.01*0.27;%g C-CO2 m-2 yr-1

nee_dailysum_upper=NEE_obs_upper(25777:end)*60*30;%convert from nmol m-2 s-1 to nmol m-2 day-1
cum_obs_valid_upper=cumsum(nee_dailysum_upper);%umol m-2 yr-1
cum_obs_valid_sum_upper=cum_obs_valid_upper(end)*10^-6;%mol CO2 m-2 yr-1
cum_obs_valid_sum_upper=cum_obs_valid_sum_upper*44.01*0.27;%g C-CO2 m-2 yr-1

cum_obs_valid_sum_lower-cum_obs_valid_sum

mod_dailysum=NEE_mod(25777:end)*60*30;%convert from nmol m-2 s-1 to nmol m-2 day-1
cum_mod_valid=cumsum(mod_dailysum);%nmol m-2 yr-1
cum_mod_valid_sum=cum_mod_valid(end)*10^-6;%mol m-2 yr-1
cum_mod_valid_sum=cum_mod_valid_sum*44.01*0.27;%g C-CO2 m-2 yr-1

mod_dailysum_min=lower_NEE(25777:end)*60*30;%convert from nmol m-2 s-1 to nmol m-2 day-1
cum_mod_min_valid=cumsum(mod_dailysum_min);%nmol m-2 yr-1
cum_mod_min_valid_sum=cum_mod_min_valid(end)*10^-6;%mol m-2 yr-1
cum_mod_min_valid_sum=cum_mod_min_valid_sum*44.01*0.27;%g C-CO2 m-2 yr-1
 
mod_dailysum_max=upper_NEE(25777:end)*60*30;%convert from nmol m-2 s-1 to nmol m-2 day-1
cum_mod_max_valid=cumsum(mod_dailysum_max);%nmol m-2 yr-1
cum_mod_max_valid_sum=cum_mod_max_valid(end)*10^-6;%mol m-2 yr-1
cum_mod_max_valid_sum=cum_mod_max_valid_sum*44.01*0.27;%g C-CO2 m-2 yr-1

cum_mod_max_valid_sum-cum_mod_valid_sum

figure
plot(decday(25777:end),cum_obs_valid,decday(25777:end),cum_obs_valid_upper,decday(25777:end),cum_obs_valid_lower)
hold on
plot(decday(25777:end),cum_mod_valid,decday(25777:end),cum_mod_max_valid,decday(25777:end),cum_mod_min_valid)
legend('obs','obs max','obs min','mod','mod max','mod min')

%Param stats
nee_dailysum=wc_gf(1:25776)*60*30;%convert from nmol m-2 s-1 to nmol m-2 day-1
cum_obs_param=cumsum(nee_dailysum);%umol m-2 yr-1
cum_obs_param_sum=cum_obs_param(end)*10^-6;%mol CO2 m-2 yr-1
cum_obs_param_sum=cum_obs_param_sum*44.01*0.27;%g C-CO2 m-2 yr-1

nee_dailysum_lower=NEE_obs_lower(1:25776)*60*30;%convert from nmol m-2 s-1 to nmol m-2 day-1
cum_obs_param_lower=cumsum(nee_dailysum_lower);%umol m-2 yr-1
cum_obs_param_sum_lower=cum_obs_param_lower(end)*10^-6;%mol CO2 m-2 yr-1
cum_obs_param_sum_lower=cum_obs_param_sum_lower*44.01*0.27;%g C-CO2 m-2 yr-1

nee_dailysum_upper=NEE_obs_upper(1:25776)*60*30;%convert from nmol m-2 s-1 to nmol m-2 day-1
cum_obs_param_upper=cumsum(nee_dailysum_upper);%umol m-2 yr-1
cum_obs_param_sum_upper=cum_obs_param_upper(end)*10^-6;%mol CO2 m-2 yr-1
cum_obs_param_sum_upper=cum_obs_param_sum_upper*44.01*0.27;%g C-CO2 m-2 yr-1

cum_obs_param_sum_lower-cum_obs_param_sum

mod_dailysum=NEE_mod(1:25776)*60*30;
cum_mod_param=cumsum(mod_dailysum);
cum_mod_param_sum=cum_mod_param(end)*10^-6;
cum_mod_param_sum=cum_mod_param_sum*44.01*0.27;

mod_dailysum_min=lower_NEE(1:25776)*60*30;%convert from umol m-2 s-1 to nmol m-2 30min
cum_mod_min_param=cumsum(mod_dailysum_min);%cum
cum_mod_min_param_sum=cum_mod_min_param(end)*10^-6;%mol m-2 yr-1
cum_mod_min_param_sum=cum_mod_min_param_sum*44.01*0.27;%g C-CO2 m-2 yr-1
 
mod_dailysum_max=upper_NEE(1:25776)*60*30;
cum_mod_max_param=cumsum(mod_dailysum_max);
cum_mod_max_param_sum=cum_mod_max_param(end)*10^-6;
cum_mod_max_param_sum=cum_mod_max_param_sum*44.01*0.27;

cum_mod_max_param_sum-cum_mod_param_sum
%plot the cum sums

%all time
nee_dailysum=wc_gf*60*30;%convert from nmol m-2 s-1 to nmol m-2 day-1
cum_obs_=cumsum(nee_dailysum);%umol m-2 yr-1
cum_obs__sum=cum_obs_(end)*10^-6;%mol CO2 m-2 yr-1
cum_obs__sum=cum_obs__sum*44.01*0.27;%g C-CO2 m-2 yr-1

nee_dailysum_lower=NEE_obs_lower*60*30;%convert from nmol m-2 s-1 to nmol m-2 day-1
cum_obs__lower=cumsum(nee_dailysum_lower);%umol m-2 yr-1
cum_obs__sum_lower=cum_obs__lower(end)*10^-6;%mol CO2 m-2 yr-1
cum_obs__sum_lower=cum_obs__sum_lower*44.01*0.27;%g C-CO2 m-2 yr-1

nee_dailysum_upper=NEE_obs_upper*60*30;%convert from nmol m-2 s-1 to nmol m-2 day-1
cum_obs__upper=cumsum(nee_dailysum_upper);%umol m-2 yr-1
cum_obs__sum_upper=cum_obs__upper(end)*10^-6;%mol CO2 m-2 yr-1
cum_obs__sum_upper=cum_obs__sum_upper*44.01*0.27;%g C-CO2 m-2 yr-1

cum_obs__sum_lower-cum_obs__sum

mod_dailysum=NEE_mod*60*30;
cum_mod_=cumsum(mod_dailysum);
cum_mod__sum=cum_mod_(end)*10^-6;
cum_mod__sum=cum_mod__sum*44.01*0.27;

mod_dailysum_min=lower_NEE*60*30;%convert from umol m-2 s-1 to nmol m-2 30min
cum_mod_min_=cumsum(mod_dailysum_min);%cum
cum_mod_min__sum=cum_mod_min_(end)*10^-6;%mol m-2 yr-1
cum_mod_min__sum=cum_mod_min__sum*44.01*0.27;%g C-CO2 m-2 yr-1
 
mod_dailysum_max=upper_NEE*60*30;
cum_mod_max_=cumsum(mod_dailysum_max);
cum_mod_max__sum=cum_mod_max_(end)*10^-6;
cum_mod_max__sum=cum_mod_max__sum*44.01*0.27;

cum_mod_max__sum-cum_mod__sum
% 
%figure
% subplot(1,2,1)
% plot(decday(1:25776),cum_obs_param*10^-6*44.01*0.27,'k',decday(1:25776),cum_obs_param_upper*10^-6*44.01*0.27,'k--',decday(1:25776),cum_obs_param_lower*10^-6*44.01*0.27,'k--')
% hold on
% plot(decday(1:25776),cum_mod_param*10^-6*44.01*0.27,'r',decday(1:25776),cum_mod_max_param*10^-6*44.01*0.27,'r--',decday(1:25776),cum_mod_min_param*10^-6*44.01*0.27,'r--')
% legend('obs','obs max','obs min','mod','mod max','mod min')
% xlabel('DOY')
% ylabel('Cummulative NEE (g C-CO_2 m^-2)')
% title('Parameterization period')
% subplot(1,2,2)
% plot(decday(25777:end),cum_obs_valid*10^-6*44.01*0.27,'k',decday(25777:end),cum_obs_valid_upper*10^-6*44.01*0.27,'k--',decday(25777:end),cum_obs_valid_lower*10^-6*44.01*0.27,'k--')
% hold on
% plot(decday(25777:end),cum_mod_valid*10^-6*44.01*0.27,'r',decday(25777:end),cum_mod_max_valid*10^-6*44.01*0.27,'r--',decday(25777:end),cum_mod_min_valid*10^-6*44.01*0.27,'r--')
% legend('obs','obs max','obs min','mod','mod max','mod min')
% xlabel('DOY')
% ylabel('Cummulative NEE (g C-CO_2 m^-2)')
% title('Validation period')
figure
plot(decday,cum_obs_*10^-6*44.01*0.27,'k',decday,cum_obs__upper*10^-6*44.01*0.27,'k--',decday,cum_obs__lower*10^-6*44.01*0.27,'k--')
hold on
plot(decday,cum_mod_*10^-6*44.01*0.27,'r',decday,cum_mod_max_*10^-6*44.01*0.27,'r--',decday,cum_mod_min_*10^-6*44.01*0.27,'r--')
legend('obs','+90% CI','-90% CI','mod','+90% CI','-90% CI')
xlabel('DOY')
ylabel('Cummulative NEE (g C-CO_2 m^-^2)')

set(gcf, 'PaperPositionMode', 'auto');
print('PAMM_cumm_NEE_20150618','-dpng','-r600');

cum_obs_plot=cum_obs_*10^-6*44.01*0.27;
cum_obs_plot_upper=cum_obs__upper*10^-6*44.01*0.27;
cum_obs_plot_lower=cum_obs__lower*10^-6*44.01*0.27;

cum_obs_plot=[cum_obs_plot(8255,1) cum_obs_plot(25775,1) cum_obs_plot(43296,1)];
cum_obs_plot_upper=[cum_obs_plot_upper(8255,1) cum_obs_plot_upper(25775,1) cum_obs_plot_upper(43296,1)];
cum_obs_plot_lower=[cum_obs_plot_lower(8255,1) cum_obs_plot_lower(25775,1) cum_obs_plot_lower(43296,1)];
date1_plot=[date1(173) date1(538) date1(903)];
cum_obs_plot_error=cum_obs_plot_upper-cum_obs_plot;

startDate = datenum('07-12-2012');
endDate = datenum('12-31-2014');
xData2=linspace(startDate,endDate,43296);
date2_plot=[xData2(8255) xData2(25775) xData2(43296)];

figure
ciplot(cum_mod_min_*10^-6*44.01*0.27,cum_mod_max_*10^-6*44.01*0.27,xData2,'k');
datetick('x','mm/yyyy')
hold on
errorbar(date2_plot,cum_obs_plot,cum_obs_plot_error)
datetick('x','mm/yyyy')

set(obs                       , ...
  'LineWidth'       , 1           , ...
  'Marker'          , 'o'         , ...
  'MarkerSize'      , 3           , ...
  'MarkerEdgeColor' , [0.5 0.5 0.5]  , ...
  'MarkerFaceColor' , [0.5 0.5 0.5]  );
 
%hXLabel = xlabel('Day of year'                     );
hYLabel = ylabel('NEE (g C-CO_2 m^-^2 d^-^1)' );
 box off
 legend('model','obs')

set(gcf, 'PaperPositionMode', 'auto');
print('PAMM_cumm_NEE_20150623','-dpng','-r600');
%% Daily integrals
days = 195:1:1097;%for validation
days=days';
%b/c my code likes to start from 1, shift data temporarily so they start at
%1
days=days-194;
DOY_daily=DOY-194;
 
daily_NEE_0=wc_gf*60*30;%first turn gpp from umol m-2 s-1 into umol m-2 30min-1 
[daily_NEE_obs]=daily_sum(DOY_daily,daily_NEE_0,days);
daily_NEE_0=NEE_obs_lower*60*30;%first turn gpp from umol m-2 s-1 into umol m-2 30min-1 
[daily_NEE_obs_lower]=daily_sum(DOY_daily,daily_NEE_0,days);
daily_NEE_0=NEE_obs_upper*60*30;%first turn gpp from umol m-2 s-1 into umol m-2 30min-1 
[daily_NEE_obs_upper]=daily_sum(DOY_daily,daily_NEE_0,days);

daily_NEE_mod_0=NEE_mod'*60*30;%first turn gpp from umol m-2 s-1 into umol m-2 30min-1 
[daily_NEE_mod_min]=daily_sum(DOY_daily,daily_NEE_mod_0,days);
daily_NEE_mod_lower_0=lower_NEE'*60*30;%first turn gpp from umol m-2 s-1 into umol m-2 30min-1 
[daily_NEE_mod_min_lower]=daily_sum(DOY_daily,daily_NEE_mod_lower_0,days);
daily_NEE_mod_upper_0=upper_NEE'*60*30;%first turn gpp from umol m-2 s-1 into umol m-2 30min-1 
[daily_NEE_mod_min_upper]=daily_sum(DOY_daily,daily_NEE_mod_upper_0,days);

daily_NEE_obs=daily_NEE_obs*10^-6*11.88;%g CO_2-C m^-^2 d^-^1
daily_NEE_obs_lower=daily_NEE_obs_lower*10^-6*11.88;%g CO_2-C m^-^2 d^-^1
daily_NEE_obs_upper=daily_NEE_obs_upper*10^-6*11.88;%g CO_2-C m^-^2 d^-^1
daily_NEE_mod_min=daily_NEE_mod_min*10^-6*11.88;%g CO_2-C m^-^2 d^-^1
daily_NEE_mod_min_lower=daily_NEE_mod_min_lower*10^-6*11.88;%g CO_2-C m^-^2 d^-^1
daily_NEE_mod_min_upper=daily_NEE_mod_min_upper*10^-6*11.88;%g CO_2-C m^-^2 d^-^1
%RMSE is a better test of model performance than r2
RMSE_nee_min_daily=nansum((daily_NEE_mod_min-daily_NEE_obs).^2);
RMSE_nee_min_daily=sqrt(RMSE_nee_min_daily/903);

X = ones(length(daily_NEE_mod_min'),1);
X = [X daily_NEE_mod_min'];
[~,~,~,~,STATS] = regress(daily_NEE_obs',X);
 
figure
plot(days,daily_NEE_obs,'.',days,daily_NEE_obs_upper,'.',days,daily_NEE_obs_lower,'.',days,daily_NEE_mod_min,'.',days,daily_NEE_mod_min_upper,'.',days,daily_NEE_mod_min_lower,'.')%,days,daily_NEE_mod_mean,'.')
ylabel('NEE daily sum (g CO_2-C m^-^2 d^-^1)')
legend('obs','obs upper','obs lower','mod','mod upper','mod lower')


daily_Reco_0=Reco_min*60*30;%first turn gpp from umol m-2 s-1 into umol m-2 30min-1
daily_Reco_0=daily_Reco_0';
[daily_Reco_mod_min]=daily_sum(DOY_daily,daily_Reco_0,days);
daily_lower_Reco_0=lower_Reco*60*30;%first turn gpp from umol m-2 s-1 into umol m-2 30min-1
daily_lower_Reco_0=daily_lower_Reco_0';
[daily_lower_Reco]=daily_sum(DOY_daily,daily_lower_Reco_0,days);
daily_upper_0=upper_Reco*60*30;%first turn gpp from umol m-2 s-1 into umol m-2 30min-1
daily_upper_0=daily_upper_0';
[daily_upper_Reco]=daily_sum(DOY_daily,daily_upper_0,days);

  
daily_Reco_0=er_ANNnight*60*30;%first turn gpp from umol m-2 s-1 into umol m-2 30min-1 
[daily_Reco_obs]=daily_sum(DOY_daily,daily_Reco_0,days);

daily_Reco_mod_min=daily_Reco_mod_min*10^-6*11.88;%g CO_2-C m^-^2 d^-^1
daily_lower_Reco=daily_lower_Reco*10^-6*11.88;%g CO_2-C m^-^2 d^-^1
daily_upper_Reco=daily_upper_Reco*10^-6*11.88;%g CO_2-C m^-^2 d^-^1
daily_Reco_obs=daily_Reco_obs*10^-6*11.88;%g CO_2-C m^-^2 d^-^1

figure
plot(days,daily_Reco_obs,'.',days,daily_Reco_mod_min,'.')
ylabel('Reco daily sum umol m-2 d-1')
legend('obs','mod')
 
 %RMSE is a better test of model performance than r2
RMSE_reco_min_daily=nansum((daily_Reco_mod_min-daily_Reco_obs).^2);
RMSE_reco_min_daily=sqrt(RMSE_reco_min_daily/903);

 
X = ones(length(daily_Reco_mod_min'),1);
X = [X daily_Reco_mod_min'];
[~,~,~,~,STATS] = regress(daily_Reco_obs',X);
  
 
daily_upper_Reco=daily_upper_Reco';
daily_lower_Reco=daily_lower_Reco';
daily_Reco_obs=daily_Reco_obs';
daily_Reco_mod_mean=daily_Reco_mod_mean';
daily_NEE_obs=daily_NEE_obs';
 
%% plot posterior 
Ea_SOM_posterior=find_min_10000(:,1)+18;%kJ mol-1
km_SOM_posterior=find_min_10000(:,2)*1e-6;%umol m-2
Ea_labile_posterior=find_min_10000(:,3)+17.5;
km_labile_posterior=find_min_10000(:,4)*1e-6;
figure
subplot(4,1,1)
hist(Ea_SOM_posterior);%pdf for alpha 1
xlabel('Ea SOM (kJ mol-1)')
subplot(4,1,2)
hist(km_SOM_posterior);%pdf for ea 1
xlabel('km SOM (umol C m-3 soil)')
subplot(4,1,3)
hist(Ea_labile_posterior);%pdf for km 1
xlabel('Ea labile (kJ mol-1)')
subplot(4,1,4)
hist(km_labile_posterior);%pdf for alpha 2
xlabel('km labile (umol C m-3 soil)')
 
set(gcf, 'PaperPositionMode', 'auto');
print('PAMM_posterior_20150616','-dpng','-r600');

%Final parameter estimates
Ea_SOM_FINAL=find_min(1)+18;%kJ mol-1
km_SOM_FINAL=find_min(2)*1e-6*12/100/15;%g C cm-3
Ea_labile_FINAL=find_min(3)+17.5;
km_labile_FINAL=find_min(4)*1e-6*12/100/15;%g C cm-3
%% plot posteriors from all models
%load file with all CO2 posteriors 20150623
figure
subplot(3,4,1)
hist(Ea_SOM_posterior);%pdf for alpha 1
xlabel('Ea_S_O_M (kJ mol^-^1)')
subplot(3,4,2)
hist(km_SOM_posterior*12/100/15);%pdf for ea 1
xlabel('km_S_O_M (g C cm^-^3 soil)')
subplot(3,4,3)
hist(Ea_labile_posterior);%pdf for km 1
xlabel('Ea_l_a_b_i_l_e (kJ mol^-^1)')
subplot(3,4,4)
hist(km_labile_posterior*12/100/15);%pdf for alpha 2
xlabel('km_l_a_b_i_l_e (g C cm^-^3 soil)')

subplot(3,4,5)
hist(Rref_posterior_TPGPP*12*1e-6*60*60*24);%pdf for alpha 1
xlabel('R_r_e_f (g C m^-^2 d^-^1)')
subplot(3,4,6)
hist(Eo_posterior_TPGPP);%pdf for ea 1
xlabel('E_o (k)')
subplot(3,4,7)
hist(k2_posterior_TPGPP*60*30);%pdf for km 1
xlabel('k2')

subplot(3,4,9)
hist(Rref_posterior_TP*12*1e-6*60*60*24);%pdf for alpha 1
xlabel('R_r_e_f (g C m^-^2 d^-^1)')
subplot(3,4,10)
hist(Eo_posterior_TP);%pdf for ea 1
xlabel('E_o (k)')

set(gcf, 'PaperPositionMode', 'auto');
print('ALL_CO2_posterior_20150623','-dpng','-r600');


%% plotting C pools
%run find_min through Reco code so it outputs S1 and S2
[NEE_mod, S1, S2, Reco] = PAMM_final_sys_CO2_Reco(find_min,xdata);
%convert umol C m-3 to g C m-3
S1=(S1+(3e9*0.8))*1e-6*12/1000;%add in total C for SOM pool which initially starts out as SOMlabile only
S2=S2*1e-6*12/1000;
figure
plot(decday,S1,'.',decday, S2,'.')
legend('SOM','labile')
ylabel('Carbon pools (kg C m^-3)')
xlabel('Day of year')


figure
plot(days_plot,daily_Reco_mod_min,'.',days_plot,daily_Reco_obs','.')
legend('mod','obs')


 %% plotting

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%daily 
days_plot=days+194;
%import date1 from excel file called datetick_WP
 

figure
ciplot(daily_lower_Reco,daily_upper_Reco,xData1,'k');
datetick('x','mm/yyyy')
hold on
obs=plot(date1,daily_Reco_obs','.');
set(obs                       , ...
  'LineWidth'       , 1           , ...
  'Marker'          , 'o'         , ...
  'MarkerSize'      , 3           , ...
  'MarkerEdgeColor' , [0.5 0.5 0.5]  , ...
  'MarkerFaceColor' , [0.5 0.5 0.5]  );
 
%hXLabel = xlabel('Day of year'                     );
hYLabel = ylabel('Ecosystem respiration (g C-CO_2 m^-^2 d^-^1)' );
 box off
 legend('model','obs')
set( gca                       , ...
    'FontName'   , 'Helvetica' );
set([hXLabel, hYLabel, text]  , ...
    'FontSize'   , 12         );
 
set(gcf, 'PaperPositionMode', 'auto');
print('PAMM_Reco_alldata_20150622','-dpng','-r600');
%print -depsc2 finalPlot1.eps
close;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

figure ('Units', 'pixels',...
    'Position', [100 100 500 375])
obs=plot(daily_Reco_mod_min,daily_Reco_obs,'.');
hXLabel = xlabel('PAMM Model(g CO_2-C m^-^2 d^-^1)');
hYLabel=ylabel('Obs (g CO_2-C m^-^2 d^-^1)') ;
box off
set(obs                       , ...
  'LineWidth'       , 1           , ...
  'Marker'          , 'o'         , ...
  'MarkerSize'      , 3           , ...
  'MarkerEdgeColor' , [0.25 0.25 0.25]  , ...
  'MarkerFaceColor' , [0.25 0.25 0.25]  );
 set( gca                       , ...
    'FontName'   , 'Helvetica' );
set([hXLabel, hYLabel, text]  , ...
    'FontSize'   , 12         );
 
set(gcf, 'PaperPositionMode', 'auto');
print('Reco_PAMM_onetoone_20150622','-dpng','-r600');
%print -depsc2 finalPlot1.eps
close;
%%%%%%%%%%%%%%%%
%% calculate final model NEE with model uncertainty
daily_NEE_mod_min_lower(49)=daily_NEE_mod_min_lower(48);
daily_NEE_mod_min_upper(49)=daily_NEE_mod_min_upper(48);
daily_NEE_mod_min(49)=daily_NEE_mod_min(48);

startDate = datenum('07-12-2012');
endDate = datenum('12-31-2014');
xData1=linspace(startDate,endDate,903);
figure ('Units', 'pixels',...
    'Position', [100 100 500 375]);
ciplot(daily_NEE_mod_min_lower,daily_NEE_mod_min_upper,xData1,'k')
datetick('x','mm/yyyy')
hold on
obs = plot(date1,daily_NEE_obs','.');
 
 box off
set(obs                       , ...
  'LineWidth'       , 1           , ...
  'Marker'          , 'o'         , ...
  'MarkerSize'      , 3           , ...
  'MarkerEdgeColor' , [0 1 0]  , ...
  'MarkerFaceColor' , [0 1 0]  );
 
hXLabel = xlabel('Day of year'                     );
hYLabel = ylabel('Net ecosystem exchange (g C-CO_2 m^-^2 d^-^1)' );
 
 legend('model','obs') 
set( gca                       , ...
    'FontName'   , 'Helvetica' );
set([hXLabel, hYLabel, text]  , ...
    'FontSize'   , 12         );
 
set(gcf, 'PaperPositionMode', 'auto');
print('NEE_alldata_20150622','-dpng','-r600');
%print -depsc2 finalPlot1.eps
close;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure ('Units', 'pixels',...
    'Position', [100 100 500 375]);
hold on
obs = plot(daily_NEE_mod_min',daily_NEE_obs,'.');
box off
set(obs                       , ...
  'LineWidth'       , 1           , ...
  'Marker'          , 'o'         , ...
  'MarkerSize'      , 6          , ...
  'MarkerEdgeColor' , [.25 .25 .25]  , ...
  'MarkerFaceColor' , [.25 .25 .25]  );
hXLabel = xlabel('Model (g C m^-^2 d^-^1)');
hYLabel = ylabel('Obs (g C m^-^2 d^-^1)');
box off
set([hXLabel, hYLabel, text]  , ...
    'FontSize'   , 12         );
 
set(gcf, 'PaperPositionMode', 'auto');
print('NEE_alldata_onetoone_20150622','-dpng','-r600');
 
%print -depsc2 finalPlot1_inset.eps
close;
 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Sel=percent_reduction>1;
percent_reduction(Sel)=1;

figure ('Units', 'pixels',...
    'Position', [100 100 500 375]);
plot(WT_gf,((1-percent_reduction)*100),'.')
ylabel('Inhibition of R_e_c_o (%)');
xlabel('Water table (cm)');


set(obs                           , ...
  'LineStyle'       , 'none'      , ...
  'Marker'          , '.'         , ...
  'Color'           , [0 0 0]  );
set(obs                       , ...
  'LineWidth'       , 1           , ...
  'Marker'          , '.'         , ...
  'MarkerSize'      , 15          , ...
  'MarkerEdgeColor' , [.2 .2 .2]  , ...
  'MarkerFaceColor' , [.7 .7 .7]  );
set(gcf, 'PaperPositionMode', 'auto');
print('Reco_inhibition_WT_gf_20150622','-dpng','-r600');
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

figure
subplot(2,1,1)
plot(date1,((1-M_percent_reduction)*100),'ko',...
    'LineWidth',0.5,...
    'MarkerSize',3,...
    'MarkerEdgeColor','k',...
    'MarkerFaceColor',[0,0,0]);
box off
ylabel('Inhibition of CH_4 production (%)');
set(gca,'FontSize',12)
x2 =1;
y2 = 2;
str2 = '(a)';
text(x2,y2,str2)

subplot(2,1,2)
plot(date1,daily_WT_gf,'ko',...
     'LineWidth',0.5,...
    'MarkerSize',3,...
    'MarkerEdgeColor','k',...
    'MarkerFaceColor',[0,0,0]);
box off
ylabel('Water table (cm)');
set(gca,'FontSize',12)

set(gcf, 'PaperPositionMode', 'auto');
print('WT_CH4_inhibition_20150622','-dpng','-r600');
set(gcf, 'PaperPositionMode', 'auto');
print('WT_ht_20150730','-dpng','-r600');

%print -depsc2 finalPlot1_inset.eps
close;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%import date tick excel file

figure
subplot(3,1,1)
plot(date1,daily_TA,'.')
ylabel('Air temperature (?C)')
subplot(3,1,2)
plot(date1,daily_PAR_gf,'.')
ylabel('PAR (�mol m^-^2 s^-^1)')
subplot(3,1,3)
plot(date1,daily_WT_gf,'.')
ylabel('Water table height (cm)')

set(obs                           , ...
  'LineStyle'       , 'none'      , ...
  'Marker'          , '.'         , ...
  'Color'           , [0 0 0]  );
set(obs                       , ...
  'LineWidth'       , 1           , ...
  'Marker'          , '.'         , ...
  'MarkerSize'      , 15          , ...
  'MarkerEdgeColor' , [.2 .2 .2]  , ...
  'MarkerFaceColor' , [.7 .7 .7]  );
set(gcf, 'PaperPositionMode', 'auto');
print('Met_variables_20150622','-dpng','-r600');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
days = 195:1:1097;%NEEDS TO BE UPDATED!!
days=days';
days=days-194;
DOY_daily=DOY-194;
%first do daily averages
[daily_DOY,daily_R1]=daily_ave(DOY_daily,R1',days);%time is 30min time scale, days is DOY
[daily_DOY,daily_R2]=daily_ave(DOY_daily,R2',days);%time is 30min time scale, days is DOY

figure
plot(daily_DOY,daily_R1,'.')

figure
plot(date1,daily_R1,'.',date1, daily_R2,'.')
ylabel('Respiration (�mol m^-^2 s^-^1)')
