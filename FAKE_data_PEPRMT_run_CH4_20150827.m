%use for running DAMMCH4 example

%First take output from CO2 model and convert to daily averages
days = 195:1:1097;%NEEDS TO BE UPDATED!!--all data
%days = 1:1:538;%NEEDS TO BE UPDATED!!--param data

days=days';
%b/c my code likes to start from 1, shift data temporarily so they start at
%1
days=days-194;
DOY_daily=DOY-194;
%DOY_daily=DOY(1:25776)-194;
%first do daily averages
[daily_DOY,daily_DOY_disc]=daily_ave(DOY_daily,DOY_disc,days);%time is 30min time scale, days is DOY
figure
plot(daily_DOY,daily_DOY_disc,'.')

[daily_DOY,daily_TA]=daily_ave(DOY_daily,TA,days);%time is 30min time scale, days is DOY
figure
plot(DOY_daily,TA,'.r')
hold on
plot(daily_DOY,daily_TA,'.')

[daily_DOY,daily_WT_gf]=daily_ave(DOY_daily,WT_gf,days);%time is 30min time scale, days is DOY
figure
plot(DOY_daily,WT_gf,'.')
hold on
plot(daily_DOY,daily_WT_gf,'.r')

[daily_DOY,daily_PAR_gf]=daily_ave(DOY_daily,PAR_gf,days);%time is 30min time scale, days is DOY
figure
plot(DOY_daily,PAR_gf,'.')
hold on
plot(daily_DOY,daily_PAR_gf,'.r')

daily_S1_0=S1_fake';%already in cummulative umol m-2
[daily_S1_fake]=daily_sum(DOY_daily,daily_S1_0,days);%time is 30min time scale, days is DOY
figure
plot(DOY_daily,S1_fake,'.')
hold on
plot(daily_DOY,daily_S1_fake,'.r')

test=cumsum(S1);
test(end)
test2=cumsum(daily_S1);
test2(end)

daily_S2_0=S2_fake';%already in cummulative umol m-2 
[daily_S2_fake]=daily_sum(DOY_daily,daily_S2_0,days);%time is 30min time scale, days is DOY
figure
plot(DOY_daily,S2_fake,'.')
hold on
plot(daily_DOY,daily_S2_fake,'.r')


%now do daily integrals
daily_gpp_min_0=GPP_fake'*-1*60*30;%first turn gpp from umol m-2 s-1 into umol m-2 30min-1
[daily_gpp_fake]=daily_sum(DOY_daily,daily_gpp_min_0,days);
figure
plot(daily_gpp_fake,'.')

daily_reco_min_0=Reco_fake'*60*30;%first turn gpp from umol m-2 s-1 into umol m-2 30min-1
[daily_Reco_fake]=daily_sum(DOY_daily,daily_reco_min_0,days);
figure
plot(daily_Reco_fake,'.')

%don't need to convert CH4 to daily b/c CH4_fake is already a daily
%variable
figure
plot(daily_DOY,CH4_fake,'.',daily_DOY,daily_wm_sum_umol_gf,'.')
legend('fake','obs')

%you do nee NEE_fake daily sum

daily_nee_min_0=NEE_fake'*60*30;%first turn gpp from umol m-2 s-1 into umol m-2 30min-1
[daily_nee_fake]=daily_sum(DOY_daily,daily_nee_min_0,days);
figure
plot(daily_nee_fake,'.')


%prep GPP mod min for input to CH4 model
daily_GPP_0=GPP_fake'*60*30;%first turn gpp from umol m-2 s-1 into umol m-2 30min-1
[daily_GPP_fake]=daily_sum(DOY_daily,daily_GPP_0,days);
%daily_GPP=daily_GPP';
figure
plot(daily_DOY+193,daily_GPP_fake,'.',daily_DOY+193,daily_gpp_ANNnight,'.')
ylabel('GPP daily sum umol m-2 d-1')
legend('mod','obs')


%% 

model.ssfun = @PAMM_final_ss_CH4;
model.modelfun = @PAMM_final_sys_CH4;

%paramerterization data
daily_DOY=daily_DOY';
daily_DOY_disc =daily_DOY_disc';
daily_TA =daily_TA';
daily_WT_gf =daily_WT_gf';
daily_PAR_gf =daily_PAR_gf';
daily_gpp_fake =daily_gpp_fake';
daily_S1_fake =daily_S1_fake';
daily_S2_fake=daily_S2_fake';
CH4_fake=CH4_fake';
daily_gapfilling_error_wm=daily_gapfilling_error_wm';
daily_random_error_wm=daily_random_error_wm';

daily_gapfilling_error_wc=daily_gapfilling_error_wc';
daily_random_error_wc=daily_random_error_wc';

xdata=[daily_DOY(1:538) daily_DOY_disc(1:538) daily_TA(1:538) daily_WT_gf(1:538) daily_PAR_gf(1:538) daily_gpp_fake(1:538) daily_S1_fake(1:538) daily_S2_fake(1:538)];
ydata=[daily_DOY(1:538) CH4_fake(1:538) gf_error_fake_wm(1:538) re_error_fake_wm(1:538)];%this has CH4 umol m-2 d-1
% theta=[0 1 0 1 0 1];
%theta=[0.1];
theta=[-7 900 3 500 10 500 0.24];
theta=theta';
mod_ch4 = PAMM_final_sys_CH4(theta,xdata);
 figure
 plot(daily_DOY(1:538),mod_ch4,'.')
 hold on
 plot(daily_DOY(1:538),CH4_fake(1:538),'.r')
mod=cumsum(mod_ch4(173:537));
mod=mod(end);
fake=cumsum(CH4_fake(173:537));
fake=fake(end);
fake/mod

%% START MDF
data.xdata =xdata;
data.ydata =ydata;

Rsum_obs = data.ydata(:,2);
nan_obs = sum(isnan(Rsum_obs));
n_obs = length(Rsum_obs) - nan_obs;
model.N     = n_obs;%total number of non nan obs
%model.S20 = [1 1 2];%has to do with error variance of prior
%model.N0  = [4 4 4];%has to do with error variance of prior


params = {
    {'M_ea1',        0,     -10,         10}
    {'M_km1',        1,     1e-3,        1e3}
    {'M_ea2',        0,     -10,         10}
    {'M_km2',        1,     1e-3,        1e3}
    {'M_ea3',        0,     -10,         12}%moved to 12 b/c set as 10 in fake data
    {'M_km3',        1,     1e-3,        1e3}
   {'Vtrans_effect',       0,     -0.24,       0.76}
%     name      start/theta/par  lBound   uBound    mean_prior   std_prior
    };

% params = {
%     {'M_ea1',        0,     -10,         10}
%     {'M_ea2',        0,     -10,         10}
%     {'M_ea3',        0,     -10,         10}
%     {'V_trans',      0.24,   0.01,       0.9}
%     {'Oxi_Factor',   0.72,   0.1,        0.95}
% %     name      start/theta/par  lBound   uBound    mean_prior   std_prior
%     };


options.method = 'dram';
options.verbosity  = 0;%level of info printed
options.burnintime = 50000;% burn in before adaptation starts
%[results, chain, s2chain] = mcmcrun(model,data,params,options,results);
% [RESULTS,CHAIN,S2CHAIN,SSCHAIN] = MCMCRUN(MODEL,DATA,PARAMS,OPTIONS)
%First generate an initial chain
options.nsimu = 50000;
[results, chain, s2chain, sschain]= mcmcrun_FINAL(model,data,params,options);

%find the minimum
figure
plot(sschain,'.')%sum-of-squares chains
nanmin(sschain)
Sel=sschain<nanmin(sschain)+0.000000000001;%442
figure
plot(Sel)
% cumsum(Sel)
find_min=chain(Sel,:);
find_min=find_min(end,:)';
figure
hist(chain(:,5))

mod_ch4 = PAMM_final_sys_CH4(find_min,xdata);
figure
plot(daily_DOY(1:538),mod_ch4,'.',daily_DOY(1:538),CH4_fake(1:538),'.')
legend('mod','obs')

%Then re-run starting from the results of the previous run, this will take about 10 minutes.
params = {
%     {'M_ea1',        find_min(1),     -10,         10}
%     {'M_km1',        find_min(2),     1e-3,        1e3}
%     {'M_ea2',        find_min(3),     -10,          10}
%     {'M_km2',        find_min(4),     1e-3,        1e3}
%     {'M_ea3',        find_min(5),     -10,         12}
%     {'M_km3',        find_min(6),     1e-3,        1e3}
    {'Oxi_effect',      find_min(1),     -0.24,       0.76}
%     name      start/theta/par  lBound   uBound    mean_prior   std_prior
    };
 
options.method = 'dram';
options.verbosity  = 0;%level of info printed
options.burnintime = 0;% burn in before adaptation starts
 
options.nsimu = 150000;
[results, chain, s2chain, sschain] = mcmcrun_FINAL(model,data,params,options, results);
 
figure
mcmcplot(chain,[],results,'chainpanel');%plots chain for ea parameter and all the values it accepted
figure
mcmcplot(chain,[],results,'pairs');%tells you if parameters are correlated
%  figure
%  mcmcplot(sqrt(s2chain),[],[],'hist')%take square root of the s2chain to get the chain for error standard deviation
% title('Error std posterior')
figure
mcmcplot(chain,[],results,'denspanel',2);
 
 
chainstats(chain,results)
sschain_long=sschain;%save before you move on
chain_long=chain;
results_long=results;

%find the minimum
figure
plot(sschain_long,'.')%sum-of-squares chains
nanmin(sschain_long)
Sel=sschain_long<nanmin(sschain_long)+0.0001;%442
figure
plot(Sel)
% cumsum(Sel)
find_min=chain_long(Sel,:);
find_min=find_min(end,:)';

CH4_mod_min = PAMM_final_sys_CH4(find_min,xdata);
figure
plot(CH4_mod_min)
hold on
plot(CH4_fake(1:538),'r')

%% Choose posterior

%First establish if chain stabilized
chainstats(chain,results)
%Geweke test should be high ideally >0.9 for all parameters

%rejection rate of chain needs to be less than 50%
results_long.rejected*100

%Determine where means and sd of parameters stabilize in the chain
%this tells you what part of chain is stable for selection of posterior

%see where means and sd of parameters stabilize in the chain
%this tells you what part of chain is stable for selection of posterior
window_size = 5000;
simple = tsmovavg(chain_long(:,1),'s',window_size,1);
simple_std = movingstd(chain_long(:,1),5000);
figure
subplot(2,1,1)
plot(simple,'.')
title('5000 iteration moving average EA SOM')
subplot(2,1,2)
plot(simple_std,'.')
title('5000 iteration moving SD EA SOM')

window_size = 5000;
simple = tsmovavg(chain_long(:,2),'s',window_size,1);
simple_std = movingstd(chain_long(:,2),5000);
figure
subplot(2,1,1)
plot(simple,'.')
title('5000 iteration moving average km SOM')
subplot(2,1,2)
plot(simple_std,'.')
title('5000 iteration moving SD km SOM')

window_size = 5000;
simple = tsmovavg(chain_long(:,3),'s',window_size,1);
simple_std = movingstd(chain_long(:,3),5000);
figure
subplot(2,1,1)
plot(simple,'.')
title('5000 iteration moving average EA labile')
subplot(2,1,2)
plot(simple_std,'.')
title('5000 iteration moving SD EA labile')

window_size = 5000;
simple = tsmovavg(chain_long(:,4),'s',window_size,1);
simple_std = movingstd(chain_long(:,4),5000);
figure
subplot(2,1,1)
plot(simple,'.')
title('5000 iteration moving average km labile')
subplot(2,1,2)
plot(simple_std,'.')
title('5000 iteration moving SD km labile')

window_size = 5000;
simple = tsmovavg(chain_long(:,5),'s',window_size,1);
simple_std = movingstd(chain_long(:,5),5000);
figure
subplot(2,1,1)
plot(simple,'.')
title('5000 iteration moving average EA oxi')
subplot(2,1,2)
plot(simple_std,'.')
title('5000 iteration moving SD EA oxi')

window_size = 5000;
simple = tsmovavg(chain_long(:,6),'s',window_size,1);
simple_std = movingstd(chain_long(:,6),5000);
figure
subplot(2,1,1)
plot(simple,'.')
title('5000 iteration moving average km oxi')
subplot(2,1,2)
plot(simple_std,'.')
title('5000 iteration moving SD km oxi')

%identified visually that chain is good around 100,000
%choose parammeters 10,000 parameter sets from that area
%e.g. 100,000-101,000

find_min_10000=chain(140001:150000,:);%update everytime
%now select 1000 unique parameter sets from these 10,000, that are
%distributed across the range
figure
hist(find_min_10000(:,1))
%bin data and sample evenly across bins


%run 10000 parameter sets through all data+ and test chi square
xdata=[daily_DOY daily_DOY_disc daily_TA daily_WT_gf daily_PAR_gf daily_gpp_fake daily_S1_fake daily_S2_fake];
ydata=[daily_DOY CH4_fake gf_error_fake_wm re_error_fake_wm];%this has CH4 umol m-2 d-1

%make 1000 theta sets that will be used to estimate CI 
model_pop_ch4=zeros(length(1:10000));%length of validation data
model_pop_ch4=model_pop_ch4(1:903,:);%update as needed 
 
for i=1:length(find_min_10000)
   ch4mod = PAMM_final_sys_CH4(find_min_10000(i,:)',xdata);
    model_pop_ch4(:,i)=ch4mod;
    
end

model_std_ch4=std(model_pop_ch4');
model_mean_ch4=mean(model_pop_ch4');
mean_theta_final=nanmean(find_min_10000);
mean_theta_final=mean_theta_final';

%% according to Zobitz 2008, model mean==model min(best para set)
ch4_min = PAMM_final_sys_CH4(find_min,xdata);
ch4_mean  = PAMM_final_sys_CH4(mean_theta_final,xdata);

figure
plot(daily_DOY,CH4_fake,'.',daily_DOY,ch4_min,'.')
legend('obs','mod min')
figure
plot(daily_DOY,CH4_fake,'.',daily_DOY,ch4_mean,'.')
legend('obs','mod mean')
 
figure
plot(model_std_ch4,'.')

% 90% confidence intervals (if you want 95% use 1.96)
%b/c the 10,000 parameters are not independent or may even be redundant, I
%reduce the sample size to 1000
lower_ch4=ch4_min-(1.645*(model_std_ch4./sqrt(50)));%use 1.645 for 90% CI
upper_ch4=ch4_min+(1.645*(model_std_ch4./sqrt(50)));
 
figure
plot(lower_ch4,'.')
hold on
plot(upper_ch4,'.')

%RMSE is a better test of model performance than r2
RMSE_ch4_min=nansum((ch4_min'-daily_wm_sum_umol_gf).^2);
RMSE_ch4_min=sqrt(RMSE_ch4_min/903);
 
X = ones(length(ch4_min'),1);
X = [X ch4_min'];
[~,~,~,~,STATS] = regress(daily_wm_sum_umol_gf,X);
 
%% add random and gapfilling error

wm_obs_lower=wm_90CI_lower+random_error_wm_prop_lower;
wm_obs_upper=wm_90CI_upper+random_error_wm_prop_upper;
figure
plot(decday,wm_gf,'.',decday,wm_obs_lower,'.',decday,wm_obs_upper,'.')
legend('obs','total error low','total error high')
%convert these to daily integrals
daily_wm_lower=wm_obs_lower*60*30*0.001;%nmol to umol m-2 30min-1
[daily_wm_lower]=daily_sum(DOY_daily,daily_wm_lower,days);%time is 30min time scale, days is DOY

daily_wm_upper=wm_obs_upper*60*30*0.001;%nmol to umol m-2 30min-1
[daily_wm_upper]=daily_sum(DOY_daily,daily_wm_upper,days);%time is 30min time scale, days is DOY

figure
plot(daily_DOY,daily_wm_sum_umol_gf,'.r',daily_DOY,daily_wm_lower,'.',daily_DOY,daily_wm_upper,'.')
legend('obs','lower','upper')

%% annual sum test
%Validation stats
cum_obs_valid=cumsum(daily_wm_sum_umol_gf(539:end));%umol m-2 yr-1
cum_obs_valid_sum=cum_obs_valid(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_valid_sum=cum_obs_valid_sum*16.04*0.75;%g C-Ch4 m-2 yr-1

cum_obs_valid_lower=cumsum(daily_wm_lower(539:end));%umol m-2 yr-1
cum_obs_valid_sum_lower=cum_obs_valid_lower(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_valid_sum_lower=cum_obs_valid_sum_lower*16.04*0.75;%g C-Ch4 m-2 yr-1

cum_obs_valid_upper=cumsum(daily_wm_upper(539:end));%umol m-2 yr-1
cum_obs_valid_sum_upper=cum_obs_valid_upper(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_valid_sum_upper=cum_obs_valid_sum_upper*16.04*0.75;%g C-Ch4 m-2 yr-1

cum_obs_valid_sum_upper-cum_obs_valid_sum

cum_mod_valid=cumsum(ch4_min(539:end));%nmol m-2 yr-1
cum_mod_valid_sum=cum_mod_valid(end)*10^-6;%mol m-2 yr-1
cum_mod_valid_sum=cum_mod_valid_sum*16.04*0.75;%g C-Ch4 m-2 yr-1

cum_mod_min_valid=cumsum(lower_ch4(539:end));%nmol m-2 yr-1
cum_mod_min_valid_sum=cum_mod_min_valid(end)*10^-6;%mol m-2 yr-1
cum_mod_min_valid_sum=cum_mod_min_valid_sum*16.04*0.75;%g C-Ch4 m-2 yr-1
 
cum_mod_max_valid=cumsum(upper_ch4(539:end));%nmol m-2 yr-1
cum_mod_max_valid_sum=cum_mod_max_valid(end)*10^-6;%mol m-2 yr-1
cum_mod_max_valid_sum=cum_mod_max_valid_sum*16.04*0.75;%g C-Ch4 m-2 yr-1

cum_mod_max_valid_sum-cum_mod_valid_sum
%Param stats
cum_obs_param=cumsum(daily_wm_sum_umol_gf(1:538));%umol m-2 yr-1
cum_obs_param_sum=cum_obs_param(end)*10^-6;%mol CO2 m-2 yr-1
cum_obs_param_sum=cum_obs_param_sum*16.04*0.75;%g C-Ch4 m-2 yr-1

cum_obs_param_lower=cumsum(daily_wm_lower(1:538));%umol m-2 yr-1
cum_obs_param_sum_lower=cum_obs_param_lower(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_param_sum_lower=cum_obs_param_sum_lower*16.04*0.75;%g C-Ch4 m-2 yr-1

cum_obs_param_upper=cumsum(daily_wm_upper(1:538));%umol m-2 yr-1
cum_obs_param_sum_upper=cum_obs_param_upper(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_param_sum_upper=cum_obs_param_sum_upper*16.04*0.75;%g C-Ch4 m-2 yr-1

cum_obs_param_sum_upper-cum_obs_param_sum

cum_mod_param=cumsum(ch4_min(1:538));%nmol m-2 yr-1
cum_mod_param_sum=cum_mod_param(end)*10^-6;%mol m-2 yr-1
cum_mod_param_sum=cum_mod_param_sum*16.04*0.75;%g C-Ch4 m-2 yr-1

cum_mod_min_param=cumsum(lower_ch4(1:538));%nmol m-2 yr-1
cum_mod_min_param_sum=cum_mod_min_param(end)*10^-6;%mol m-2 yr-1
cum_mod_min_param_sum=cum_mod_min_param_sum*16.04*0.75;%g C-Ch4 m-2 yr-1
 
cum_mod_max_param=cumsum(upper_ch4(1:538));%nmol m-2 yr-1
cum_mod_max_param_sum=cum_mod_max_param(end)*10^-6;%mol m-2 yr-1
cum_mod_max_param_sum=cum_mod_max_param_sum*16.04*0.75;%g C-Ch4 m-2 yr-1

cum_mod_max_param_sum-cum_mod_param_sum

figure
subplot(1,2,1)
plot(daily_DOY(1:538),cum_obs_param*10^-6*16.04*0.75,'k',daily_DOY(1:538),cum_obs_param_upper*10^-6*16.04*0.75,'k--',daily_DOY(1:538),cum_obs_param_lower*10^-6*16.04*0.75,'k--')
hold on
plot(daily_DOY(1:538),cum_mod_param*10^-6*16.04*0.75,'r',daily_DOY(1:538),cum_mod_max_param*10^-6*16.04*0.75,'r--',daily_DOY(1:538),cum_mod_min_param*10^-6*16.04*0.75,'r--')
legend('obs','obs max','obs min','mod','mod max','mod min')
xlabel('DOY')
ylabel('Cummulative CH4 (g C-CH_4 m^-2)')
title('Parameterization period')
subplot(1,2,2)
plot(daily_DOY(539:end),cum_obs_valid*10^-6*16.04*0.75,'k',daily_DOY(539:end),cum_obs_valid_upper*10^-6*16.04*0.75,'k--',daily_DOY(539:end),cum_obs_valid_lower*10^-6*16.04*0.75,'k--')
hold on
plot(daily_DOY(539:end),cum_mod_valid*10^-6*16.04*0.75,'r',daily_DOY(539:end),cum_mod_max_valid*10^-6*16.04*0.75,'r--',daily_DOY(539:end),cum_mod_min_valid*10^-6*16.04*0.75,'r--')
legend('obs','obs max','obs min','mod','mod max','mod min')
xlabel('DOY')
ylabel('Cummulative CH4 (g C-CH_4 m^-2)')
title('Validation period')

set(gcf, 'PaperPositionMode', 'auto');
print('PAMM_cumm_CH4_20150617','-dpng','-r600');

%all time
cum_obs_=cumsum(daily_wm_sum_umol_gf);%umol m-2 yr-1
cum_obs__sum=cum_obs_(end)*10^-6;%mol CO2 m-2 yr-1
cum_obs__sum=cum_obs__sum*16.04*0.75;%g C-CO2 m-2 yr-1

cum_obs__lower=cumsum(daily_wm_lower);%umol m-2 yr-1
cum_obs__sum_lower=cum_obs__lower(end)*10^-6;%mol CO2 m-2 yr-1
cum_obs__sum_lower=cum_obs__sum_lower*16.04*0.75;%g C-CO2 m-2 yr-1

cum_obs__upper=cumsum(daily_wm_upper);%umol m-2 yr-1
cum_obs__sum_upper=cum_obs__upper(end)*10^-6;%mol CO2 m-2 yr-1
cum_obs__sum_upper=cum_obs__sum_upper*16.04*0.75;%g C-CO2 m-2 yr-1
cum_obs__sum_upper-cum_obs__sum

cum_mod_=cumsum(ch4_min);
cum_mod__sum=cum_mod_(end)*10^-6;
cum_mod__sum=cum_mod__sum*16.04*0.75;

cum_mod_min_=cumsum(lower_ch4);%cum
cum_mod_min__sum=cum_mod_min_(end)*10^-6;%mol m-2 yr-1
cum_mod_min__sum=cum_mod_min__sum*16.04*0.75;%g C-CO2 m-2 yr-1
 
cum_mod_max_=cumsum(upper_ch4);
cum_mod_max__sum=cum_mod_max_(end)*10^-6;
cum_mod_max__sum=cum_mod_max__sum*16.04*0.75;
cum_mod_max__sum-cum_mod__sum
% 

figure
plot(daily_DOY,cum_obs_*10^-6*16.04*0.75,'k',daily_DOY,cum_obs__upper*10^-6*16.04*0.75,'k--',daily_DOY,cum_obs__lower*10^-6*16.04*0.75,'k--')
hold on
plot(daily_DOY,cum_mod_*10^-6*16.04*0.75,'r',daily_DOY,cum_mod_max_*10^-6*16.04*0.75,'r--',daily_DOY,cum_mod_min_*10^-6*16.04*0.75,'r--')
legend('obs','+90% CI','-90% CI','mod','+90% CI','-90% CI')
xlabel('DOY')
ylabel('Cummulative CH_4 (g C-CO_2 m^-^2)')

set(gcf, 'PaperPositionMode', 'auto');
print('PAMM_cumm_CH4_20150619','-dpng','-r600');



cum_obs_plot=cum_obs_*10^-6*16.04*0.75;
cum_obs_plot_upper=cum_obs__upper*10^-6*16.04*0.75;
cum_obs_plot_lower=cum_obs__lower*10^-6*16.04*0.75;

cum_obs_plot=[cum_obs_plot(173) cum_obs_plot(538) cum_obs_plot(903)];
cum_obs_plot_upper=[cum_obs_plot_upper(173) cum_obs_plot_upper(538) cum_obs_plot_upper(903)];
cum_obs_plot_lower=[cum_obs_plot_lower(173) cum_obs_plot_lower(538) cum_obs_plot_lower(903)];
date1_plot=[date1(173) date1(538) date1(903)];
cum_obs_plot_error=cum_obs_plot_upper-cum_obs_plot;

startDate = datenum('07-12-2012');
endDate = datenum('12-31-2014');
xData2=linspace(startDate,endDate,903);
date2_plot=[xData2(173) xData2(538) xData2(903)];

figure
ciplot(cum_mod_min_*10^-6*16.04*0.75,cum_mod_max_*10^-6*16.04*0.75,xData2,'k');
datetick('x','mm/yyyy')
hold on
errorbar(date2_plot,cum_obs_plot,cum_obs_plot_error)
datetick('x','mm/yyyy')

set(obs                       , ...
  'LineWidth'       , 1           , ...
  'Marker'          , 'o'         , ...
  'MarkerSize'      , 3           , ...
  'MarkerEdgeColor' , [0.5 0.5 0.5]  , ...
  'MarkerFaceColor' , [0.5 0.5 0.5]  );
 
%hXLabel = xlabel('Day of year'                     );
ylabel('Cummulative CH_4 (g C-CO_2 m^-^2)')
 box off
 legend('model','obs')

set(gcf, 'PaperPositionMode', 'auto');
print('PAMM_cumm_CH4_20150623','-dpng','-r600');


%% assessing fit and model performance

daily_lower_ch4_obs=daily_wm_lower'*10^-3*16.04*0.75;
daily_upper_ch4_obs=daily_wm_upper'*10^-3*16.04*0.75;
daily_ch4_obs=daily_wm_sum_umol_gf*10^-3*16.04*0.75;

daily_ch4_mod_min=ch4_min'*10^-3*16.04*0.75;
daily_ch4_mod_lower=lower_ch4'*10^-3*16.04*0.75;
daily_ch4_mod_upper=upper_ch4'*10^-3*16.04*0.75;

X = ones(length(daily_ch4_mod_min),1);
X = [X daily_ch4_mod_min];
[~,~,~,~,STATS] = regress(daily_ch4_obs,X);
 
RMSE_pamm_ch4=nansum((daily_ch4_mod_min-daily_ch4_obs).^2);
RMSE_pamm_ch4=sqrt(RMSE_pamm_ch4/903);
 


%% Plot posteriors
Ea_SOM_posterior=find_min_10000(:,1)+70;%kJ mol-1
km_SOM_posterior=find_min_10000(:,2)*3e3;%umol m-2
Ea_labile_posterior=find_min_10000(:,3)+70;
km_labile_posterior=find_min_10000(:,4)*3e3;
Ea_oxy_posterior=find_min_10000(:,5)+70;
km_oxy_posterior=find_min_10000(:,6)*3e3;

figure
subplot(3,2,1)
hist(Ea_SOM_posterior);%pdf for alpha 1
ylabel('Ea SOM (kJ mol-1)')
subplot(3,2,2)
hist(km_SOM_posterior);%pdf for ea 1
ylabel('km SOM (umol C m-3 soil)')
subplot(3,2,3)
hist(Ea_labile_posterior);%pdf for km 1
ylabel('Ea labile (kJ mol-1)')
subplot(3,2,4)
hist(km_labile_posterior);%pdf for alpha 2
ylabel('km labile (umol C m-3 soil)')
subplot(3,2,5)
hist(Ea_oxy_posterior);%pdf for km 1
ylabel('Ea labile (kJ mol-1)')
subplot(3,2,6)
hist(km_oxy_posterior);%pdf for alpha 2
ylabel('km labile (umol C m-3 soil)')
 
set(gcf, 'PaperPositionMode', 'auto');
print('PAMM_CH4_posterior_20150617','-dpng','-r600');

Ea_SOM_FINAL=find_min(1)+70;%kJ mol-1
km_SOM_FINAL=find_min(2)*3e3*1e-9*12/100/15;%umol m-2
Ea_labile_FINAL=find_min(3)+70;
km_labile_FINAL=find_min(4)*3e3*1e-9*12/100/15;
Ea_oxy_FINAL=find_min(5)+70;
km_oxy_FINAL=find_min(6)*3e3*1e-9*12/100/15;

%% plot posteriors from all models
%load file with all CH4 posteriors 20150623
figure
subplot(3,4,1)
hist(Ea_SOM_posterior);%pdf for alpha 1
xlabel('Ea_S_O_M (kJ mol^-^1)')
subplot(3,4,2)
hist(km_SOM_posterior*1e-9*12/100/15);%pdf for ea 1
xlabel('km_S_O_M (g C cm^-^3 soil)')
subplot(3,4,3)
hist(Ea_labile_posterior);%pdf for km 1
xlabel('Ea_l_a_b_i_l_e (kJ mol^-^1)')
subplot(3,4,4)
hist(km_labile_posterior*1e-9*12/100/15);%pdf for alpha 2
xlabel('km_l_a_b_i_l_e (g C cm^-^3 soil)')
% subplot(3,6,5)
% hist(Ea_oxy_posterior);%pdf for km 1
% xlabel('Ea labile (kJ mol-1)')
% subplot(3,6,6)
% hist(km_oxy_posterior);%pdf for alpha 2
% xlabel('km labile (umol C m-3 soil)')

subplot(3,4,5)
hist(Mref_posterior_TPGPP);%g C m-2 d-1
xlabel('M_r_e_f (g C m^-^2 d^-^1)')
subplot(3,4,6)
hist(Eo_posterior_TPGPP);%pdf for ea 1
xlabel('E_o (K)')
subplot(3,4,7)
hist(k2_posterior_TPGPP*60*60*24);%pdf for km 1
xlabel('k2')
% subplot(3,6,10)
% hist(Ea_oxy_posterior_TPGPP);%pdf for km 1
% xlabel('Ea oxy (kJ mol-1)')
% subplot(3,6,11)
% hist(km_oxy_posterior_TPGPP);%pdf for alpha 2
% xlabel('km oxy (umol C m-3 soil)')

subplot(3,4,9)
hist(Mref_posterior_TP);%pdf for alpha 1
xlabel('M_r_e_f (g C m^-^2 d^-^1)')
subplot(3,4,10)
hist(Eo_posterior_TP);%pdf for ea 1
xlabel('E_o (K)')
% subplot(3,6,15)
% hist(Ea_oxy_posterior_TP);%pdf for km 1
% xlabel('Ea oxy (kJ mol-1)')
% subplot(3,6,16)
% hist(km_oxy_posterior_TP);%pdf for alpha 2
% xlabel('km oxy (umol C m-3 soil)')
set(gcf, 'PaperPositionMode', 'auto');
print('CH4_ALL_posteriors_20150623','-dpng','-r600');


%% plotting different pools in final model simulation
Sel=days>135&days<260;
pw1=nansum(Plant_flux(Sel));
hw1=nansum(Hydro_flux(Sel));

Sel2=days>505&days<620;
pw2=nansum(Plant_flux(Sel2));
hw2=nansum(Hydro_flux(Sel2));

%wintertime fraction of transport by hyrdo
wh=(hw1+hw2)/(hw1+hw2+pw1+pw2);
%wintertime fraction of transport by plants
ph=(pw1+pw2)/(hw1+hw2+pw1+pw2);

 %% plotting

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%daily 
days_plot=days+194;

startDate = datenum('07-12-2012');
endDate = datenum('12-31-2014');
xData1=linspace(startDate,endDate,903);

figure ('Units', 'pixels',...
    'Position', [100 100 500 375]);
ciplot(daily_ch4_mod_lower,daily_ch4_mod_upper,xData1,'k')
hold on
obs = plot(date1,daily_ch4_obs,'.');
box off
 
set(obs                       , ...
  'LineWidth'       , 1           , ...
  'Marker'          , 'o'         , ...
  'MarkerSize'      , 3           , ...
  'MarkerEdgeColor' , [0 1 0]  , ...
  'MarkerFaceColor' , [0 1 0]  );
 
hXLabel = xlabel('Day of year'                     );
hYLabel = ylabel('CH_4 exchange(mg C-CH_4 m^-^2 d^-^1)' );
 
 legend('model','obs')
set( gca                       , ...
    'FontName'   , 'Helvetica' );
set([hXLabel, hYLabel, text]  , ...
    'FontSize'   , 12         );
 
set(gcf, 'PaperPositionMode', 'auto');
print('PAMM_ch4_alldata_20150623','-dpng','-r600');
%print -depsc2 finalPlot1.eps
close;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Plot one to one line

figure ('Units', 'pixels',...
    'Position', [100 100 500 375]);
hold on
obs = plot(daily_ch4_mod_min,daily_ch4_obs,'.');
set(obs                       , ...
  'LineWidth'       , 1           , ...
  'Marker'          , 'o'         , ...
  'MarkerSize'      , 3          , ...
  'MarkerEdgeColor' , [.25 .25 .25]  , ...
  'MarkerFaceColor' , [.25 .25 .25]  );
hXLabel = xlabel('Model (mg C-CH_4 m^-^2 d^-^1)');
hYLabel = ylabel('Obs (mg C-CH_4 m^-^2 d^-^1)');
set([hXLabel, hYLabel, text]  , ...
    'FontSize'   , 12         );
  
set(gcf, 'PaperPositionMode', 'auto');
print('PAMM_CH4_alldata_onetoone_20150623','-dpng','-r600');
 
%print -depsc2 finalPlot1_inset.eps
close;
 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 residual=daily_ch4_obs-daily_ch4_mod_min;
 
 figure
 obs=plot(daily_ch4_mod_min,residual,'.');
 xlabel('PAMM model (mg C-CH_4 m^-^2 d^-^1)');
 ylabel('Residual (obs-mod)')
 box off
 set(obs                       , ...
  'LineWidth'       , 1           , ...
  'Marker'          , 'o'         , ...
  'MarkerSize'      , 3          , ...
  'MarkerEdgeColor' , [.4 .4 .4]  , ...
  'MarkerFaceColor' , [1 1 1]  );

set(gcf, 'PaperPositionMode', 'auto');
print('PAMM_CH4_residual_20150623','-dpng','-r600');

 
 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure
subplot(2,1,1)
plot(date1,((1-M_percent_reduction)*100),'ko',...
    'LineWidth',0.5,...
    'MarkerSize',3,...
    'MarkerEdgeColor','k',...
    'MarkerFaceColor',[0,0,0]);
box off
ylabel('Inhibition of CH_4 production (%)');
set(gca,'FontSize',12)
x2 =1;
y2 = 2;
str2 = '(a)';
text(x2,y2,str2)

subplot(2,1,2)
plot(date1,daily_WT_gf,'ko',...
     'LineWidth',0.5,...
    'MarkerSize',3,...
    'MarkerEdgeColor','k',...
    'MarkerFaceColor',[0,0,0]);
box off
ylabel('Water table (cm)');
set(gca,'FontSize',12)

set(gcf, 'PaperPositionMode', 'auto');
print('WT_CH4_inhibition_20150622','-dpng','-r600');
%print -depsc2 finalPlot1_inset.eps
close;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure ('Units', 'pixels',...
    'Position', [100 100 500 375]);
hold on
obs = plot(days_plot,daily_WT_gf,'.');
%axis([0 350 0 200])
set(obs                           , ...
  'LineStyle'       , 'none'      , ...
  'Marker'          , '.'         , ...
  'Color'           , [0 0 0]  );
set(obs                       , ...
  'LineWidth'       , 1           , ...
  'Marker'          , '.'         , ...
  'MarkerSize'      , 15          , ...
  'MarkerEdgeColor' , [.2 .2 .2]  , ...
  'MarkerFaceColor' , [.7 .7 .7]  );
hXLabel = xlabel('Day of year');
hYLabel = ylabel('Water table height (cm)');
 
%legend('y = 0.78*x + 6.6')
% 
% set([legend, gca]             , ...
%     'FontSize'   , 14           );
set([hXLabel, hYLabel, text]  , ...
    'FontSize'   , 14         );
 
set(gca, ...
  'Box'         , 'off'     , ...
  'TickDir'     , 'out'     , ...
  'TickLength'  , [.02 .02] , ...
  'XMinorTick'  , 'off'      , ...
  'YMinorTick'  , 'off'      , ...
  'YGrid'       , 'off'      , ...
  'XColor'      , [0 0 0], ...
  'YColor'      , [0 0 0], ...
  'Xtick'       , 0:100:1100,...
  'YTick'       , -50:10:50, ...
  'LineWidth'   , 1         );
 
set(gcf, 'PaperPositionMode', 'auto');
print('WT_20150617','-dpng','-r600');
%print -depsc2 finalPlot1_inset.eps
close;
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure
plot(date1,Plant_flux_net*10^-3*16.04*0.75,'.',date1,Hydro_flux*10^-3*16.04*0.75,'.')
legend('plant','hydro')
ylabel('CH_4 flux (mg C-CH_4 m^-^2 d^-^1)')
box off
set('FontSize'   , 12         );
set(gcf, 'PaperPositionMode', 'auto');
print('PAMM_CH4_plant_hydro_20150624','-dpng','-r600');

plant_total=cumsum(Plant_flux_net*10^-3*16.04*0.75);
plant_total=plant_total(end);
hydro_total=cumsum(Hydro_flux*10^-3*16.04*0.75);
hydro_total=hydro_total(end);

(plant_total-hydro_total)/plant_total

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure
plot(date1,M1*1e-3*16.04*0.75*60*60*24,'.',date1,M2*1e-3*16.04*0.75*60*60*24,'.')
legend('SOM','labile')
ylabel('CH_4 flux (mg C-CH_4 m^-^2 d^-^1)')
box off
set('FontSize'   , 12         );
set(gcf, 'PaperPositionMode', 'auto');
print('PAMM_CH4_SOM_labile_20150624','-dpng','-r600');

SOM_total=cumsum(M1*10^-3*16.04*0.75*60*60*24);
SOM_total=SOM_total(end);
labile_total=cumsum(M2*10^-3*16.04*0.75*60*60*24);
labile_total=labile_total(end);

(labile_total-SOM_total)/labile_total

