%testing PEPRMT model in MB
%2016.08.02

%% Prep data
%Load MB L3 data
%going to run model on 2011-2015
figure
plot(data.decday(3793:91440),data.wc_gf(3793:91440),'.')
decday_MB=data.decday(3793:91440);
wc_gf_MB=data.wc_gf(3793:91440);
DOY_disc_MB=data.DOY(3793:91440);

%need a DOY continuous
DOY_MB=DOY_disc_MB;
DOY_MB(17521:end)=DOY_MB(17521:end)+365;
DOY_MB(35089:end)=DOY_MB(35089:end)+366;
DOY_MB(52609:end)=DOY_MB(52609:end)+365;
DOY_MB(70129:end)=DOY_MB(70129:end)+365;
figure
plot(diff(DOY_MB))
% figure
% plot(data.DOY,'.')%2012 had 366 days
leap_year=Season_drop_one_year(17400:17447);
Season_drop=vertcat(Season_drop_one_year ,Season_drop_one_year,leap_year,Season_drop_one_year,...
    Season_drop_one_year, Season_drop_one_year);

WT_gf_MB=Metdata.WT(3793:91440);
figure
plot(WT_gf_MB,'.')

PAR_gf_MB=Metdata.PAR(3793:91440);
figure
plot(PAR_gf_MB,'.')

MB_gpp_ANNnight_cum=data.gpp_ANNnight(3793:91440);
MB_gpp_ANNnight_cum=cumsum(MB_gpp_ANNnight_cum);
figure
plot(MB_gpp_ANNnight_cum,'.')
%compute LAI for MB
GCCsmooth=Metdata.GCCsmooth(3793:91440);
figure
plot(GCCsmooth,'.g')

LAI_MB=0.0037*exp(GCCsmooth*17.06);
LAI_gf_MB=FillSmallGaps(LAI_MB,10000);

figure
plot(LAI_gf_MB,'.b')
hold on
plot(LAI_MB,'.r')

TA_MB=data.TA(3793:91440);
% figure
% plot(TA_MB,'r.')

%% run GPP model
xdata=[DOY_MB DOY_disc_MB TA_MB WT_gf_MB...
    PAR_gf_MB LAI_gf_MB MB_gpp_ANNnight_cum decday_MB...
    Season_drop];
GPP_min_MB  = PAMM_final_sys_CO2_GPP(find_min_GPP,xdata);

figure
plot(data.decday(3793:91440),data.gpp_ANNnight(3793:91440),'.b')
hold on
plot(data.decday(3793:91440),GPP_min_MB,'.r')
legend('obs','mod')
%% run Reco model
xdata=[DOY_MB DOY_disc_MB TA_MB WT_gf_MB...
    PAR_gf_MB LAI_gf_MB GPP_min_MB' decday_MB...
    Season_drop];
[NEE_min_MB, S1_MB, S2_MB, Reco_min_MB]  = PAMM_final_sys_CO2_Reco(find_min_Reco,xdata);

figure
plot(data.decday(3793:91440),data.er_ANNnight(3793:91440),'.b')
hold on
plot(data.decday(3793:91440),Reco_min_MB,'.r')
legend('obs','mod')

figure
plot(data.decday(3793:91440),data.wc_gf(3793:91440),'.b')
hold on
plot(data.decday(3793:91440),NEE_min_MB,'.r')
legend('obs','mod')

figure
plot(S1_MB,'.')
hold on
plot(S2_MB,'.')

%% convert to daily variables
%First take output from CO2 model and convert to daily averages
days = 1:1:1826;%NMBDS TO BE UPDATED!!--all data

days=days';%b/c my code likes to start from 1, shift data temporarily so they start at
%1
%first do daily averages
[daily_DOY,daily_DOY_disc]=daily_ave(DOY_MB,DOY_disc_MB,days);%time is 30min time scale, days is DOY
figure
plot(daily_DOY,daily_DOY_disc,'.')

[daily_DOY,daily_TA]=daily_ave(DOY_MB,TA_MB,days);%time is 30min time scale, days is DOY
figure
plot(DOY_MB,TA_MB,'.r')
hold on
plot(daily_DOY,daily_TA,'.')

[daily_DOY,daily_WT_gf]=daily_ave(DOY_MB,WT_gf_MB,days);%time is 30min time scale, days is DOY
figure
plot(DOY_MB,WT_gf_MB,'.')
hold on
plot(daily_DOY,daily_WT_gf,'.r')

[daily_DOY,daily_PAR_gf]=daily_ave(DOY_MB,PAR_gf_MB,days);%time is 30min time scale, days is DOY
figure
plot(DOY_MB,PAR_gf_MB,'.')
hold on
plot(daily_DOY,daily_PAR_gf,'.r')

daily_S1_0=S1_MB';%already in cummulative umol m-2
[daily_S1]=daily_sum(DOY_MB,daily_S1_0,days);%time is 30min time scale, days is DOY
% figure
% plot(DOY_MB,S1_MB,'.')
% hold on
% plot(daily_DOY,daily_S1,'.r')
% 
% test=cumsum(S1_MB);
% test(end)
% test2=cumsum(daily_S1);
% test2(end)

daily_S2_0=S2_MB';%already in cummulative umol m-2 
[daily_S2]=daily_sum(DOY_MB,daily_S2_0,days);%time is 30min time scale, days is DOY
% figure
% plot(DOY_MB,S2_MB,'.')
% hold on
% plot(daily_DOY,daily_S2,'.r')


%now do daily integrals
daily_gpp_min_0=GPP_min_MB*-1*60*30;%first turn gpp from umol m-2 s-1 into umol m-2 30min-1
[daily_gpp_min]=daily_sum(DOY_MB,daily_gpp_min_0',days);
figure
plot(daily_gpp_min,'.')

wm_gf_MB=data.wm_gf(3793:91440);
daily_wm=wm_gf_MB*60*30*0.001;%nmol to umol m-2 30min-1
[daily_wm_sum_umol_gf]=daily_sum(DOY_MB,daily_wm,days);%time is 30min time scale, days is DOY
% figure
% plot(DOY_MB,daily_wm,'.')
% hold on
% plot(daily_DOY,daily_wm_sum_umol_gf,'.r')
% 

daily_NEE_0=NEE_min_MB*60*30;%first turn NMB from umol m-2 s-1 into umol m-2 30min-1
[daily_NEE_min]=daily_sum(DOY_MB,daily_NEE_0',days);

daily_Reco_0=Reco_min_MB*60*30;%first turn NMB from umol m-2 s-1 into umol m-2 30min-1
[daily_Reco_min]=daily_sum(DOY_MB,daily_Reco_0',days);

daily_NEE_obs=data.wc_gf(3793:91440)*60*30;%first turn NMB from umol m-2 s-1 into umol m-2 30min-1
[daily_NEE_obs]=daily_sum(DOY_MB,daily_NEE_obs,days);

daily_GPP_obs=data.gpp_ANNnight(3793:91440)*60*30;%first turn NMB from umol m-2 s-1 into umol m-2 30min-1
[daily_GPP_obs]=daily_sum(DOY_MB,daily_GPP_obs,days);

daily_Reco_obs=data.er_ANNnight(3793:91440)*60*30;%first turn NMB from umol m-2 s-1 into umol m-2 30min-1
[daily_Reco_obs]=daily_sum(DOY_MB,daily_Reco_obs,days);

figure
plot(daily_DOY,daily_NEE_min,'.',daily_DOY,daily_NEE_obs,'.')
ylabel('NEE daily sum umol m-2 d-1')
legend('mod','obs')
figure
plot(daily_DOY,daily_Reco_min,'.',daily_DOY,daily_Reco_obs,'.')
ylabel('Reco daily sum umol m-2 d-1')
legend('mod','obs')
figure
plot(daily_DOY,daily_gpp_min*-1,'.',daily_DOY,daily_GPP_obs,'.')
ylabel('GPP daily sum umol m-2 d-1')
legend('mod','obs')


%% run CH4 model
%paramerterization data
daily_DOY=daily_DOY';
daily_DOY_disc =daily_DOY_disc';
daily_TA =daily_TA';
daily_WT_gf =daily_WT_gf';
daily_PAR_gf =daily_PAR_gf';
daily_gpp_min =daily_gpp_min';
daily_S1 =daily_S1';
daily_S2=daily_S2';
daily_wm_sum_umol_gf=daily_wm_sum_umol_gf';

xdata=[daily_DOY daily_DOY_disc daily_TA daily_WT_gf daily_PAR_gf daily_gpp_min daily_S1 daily_S2];
ch4_MB = PAMM_final_sys_CH4(find_min_CH4,xdata);
figure
plot(daily_DOY,ch4_MB,'.',daily_DOY,daily_wm_sum_umol_gf,'.')
ylabel('CH4 daily sum umol m-2 d-1')
legend('mod','obs')



%% annual sums
%2015 only
cum_obs_valid=cumsum(daily_wm_sum_umol_gf(1462:end));%umol m-2 yr-1
cum_obs_valid_sum=cum_obs_valid(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_valid_sum=cum_obs_valid_sum*16.04*0.75;%g C-Ch4 m-2 yr-1

cum_mod_valid=cumsum(ch4_MB(1462:end));%nmol m-2 yr-1
cum_mod_valid_sum=cum_mod_valid(end)*10^-6;%mol m-2 yr-1
cum_mod_valid_sum=cum_mod_valid_sum*16.04*0.75;%g C-Ch4 m-2 yr-1

obs_dailysum_NEE=daily_NEE_obs(1462:end)*60*30;%convert from nmol m-2 s-1 to nmol m-2 day-1
cum_obs_valid_NEE=cumsum(obs_dailysum_NEE);%nmol m-2 yr-1
cum_obs_valid_sum_NEE=cum_obs_valid_NEE(end)*10^-6;%mol m-2 yr-1
cum_obs_valid_sum_NEE=cum_obs_valid_sum_NEE*44.01*0.27;%g C-CO2 m-2 yr-1

mod_dailysum_NEE=daily_NEE_min(1462:end)*60*30;%convert from nmol m-2 s-1 to nmol m-2 day-1
cum_mod_valid_NEE=cumsum(mod_dailysum_NEE);%nmol m-2 yr-1
cum_mod_valid_sum_NEE=cum_mod_valid_NEE(end)*10^-6;%mol m-2 yr-1
cum_mod_valid_sum_NEE=cum_mod_valid_sum_NEE*44.01*0.27;%g C-CO2 m-2 yr-1

obs_dailysum_Reco=daily_Reco_obs(1462:end)*60*30;%convert from nmol m-2 s-1 to nmol m-2 day-1
cum_obs_valid_Reco=cumsum(obs_dailysum_Reco);%nmol m-2 yr-1
cum_obs_valid_sum_Reco=cum_obs_valid_Reco(end)*10^-6;%mol m-2 yr-1
cum_obs_valid_sum_Reco=cum_obs_valid_sum_Reco*44.01*0.27;%g C-CO2 m-2 yr-1

mod_dailysum_Reco=daily_Reco_min(1462:end)*60*30;%convert from nmol m-2 s-1 to nmol m-2 day-1
cum_mod_valid_Reco=cumsum(mod_dailysum_Reco);%nmol m-2 yr-1
cum_mod_valid_sum_Reco=cum_mod_valid_Reco(end)*10^-6;%mol m-2 yr-1
cum_mod_valid_sum_Reco=cum_mod_valid_sum_Reco*44.01*0.27;%g C-CO2 m-2 yr-1

obs_dailysum_GPP=daily_GPP_obs(1462:end)*60*30;%convert from nmol m-2 s-1 to nmol m-2 day-1
cum_obs_valid_GPP=cumsum(obs_dailysum_GPP);%nmol m-2 yr-1
cum_obs_valid_sum_GPP=cum_obs_valid_GPP(end)*10^-6;%mol m-2 yr-1
cum_obs_valid_sum_GPP=cum_obs_valid_sum_GPP*44.01*0.27;%g C-CO2 m-2 yr-1

mod_dailysum_GPP=daily_gpp_min(1462:end)*60*30;%convert from nmol m-2 s-1 to nmol m-2 day-1
cum_mod_valid_GPP=cumsum(mod_dailysum_GPP);%nmol m-2 yr-1
cum_mod_valid_sum_GPP=cum_mod_valid_GPP(end)*10^-6;%mol m-2 yr-1
cum_mod_valid_sum_GPP=cum_mod_valid_sum_GPP*44.01*0.27;%g C-CO2 m-2 yr-1










