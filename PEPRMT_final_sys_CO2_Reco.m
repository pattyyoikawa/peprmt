function  [NEE_mod, S1, S2, Reco_1] = PEPRMT_final_sys_CO2_Reco(theta,xdata)

%inputs
Time_2 =xdata(:,1);
%DOY_disc_2=xdata(:,2);
%Exogenous Variables
TA_2 = xdata(:,3);
WT_2 = xdata(:,4);%water table height cm
%PAR_2 = xdata(:,5);
%LAI_2 = xdata(:,6);
GPP_2 = xdata(:,7);%gpp modeled--umol m-2 s-1 negative for uptake
%decday_2=xdata(:,8);
Season_drop_2=xdata(:,9);
%wc_90CI_2=xdata(:,10);
site_2=xdata(:,11);
wetland_age_2=xdata(:,12);

%Static C allocation theme
NPPsum_avail_2 = (GPP_2*-1*60*30);%umol m-2 30min-1 --give Reco access to all GPP

%SET UP Reco%%%%%%%%%%%%%%%%%%%%%%%%%%%
% C_allocation=theta(1);
alpha1 = 3e3;%umol m-2 s-1,--SET AS CONSTANT
ea1 = (theta(1)+18)*1000; % 5/9/15 initial=19 Joule mol-1, 22.5
km1 = theta(2)*1e-6; % umol C m-3 soil = 1.2e-5 g C cm-3 %smaller? Km for SOM C pool
%km1_gCcm3=km1*1e-6*12*1e-6;
alpha2 = 3e3;%--SET AS CONSTANT
ea2 = (theta(3)+17.5)*1000; %5/9/15 initial 17 convert to Joule, 20.7
km2 = theta(4)*1e-6; % *1e-6 umol C m-3 soil = 1.2e-5 g C cm-3 %larger? Km for Ps C pool
%km2_gCcm3=km2*1e-6*12*1e-6;

%initialize C pools
C1_init = 6e8;%in umol m-3 for peat soils BD~0.26g soil cm-3 (Miller 2008 West Pond 0-35+cm depth measured in winter 2006), with 20% C runs around 3e9 umol C m-3--I assume only 20% of that C is labile and avail to microbes so 3e9*0.2=6e8
%Actual C content at West Pond is 25.93%C in soil 0-35+cm as measured by
%Miller 2008
% C1_init_gCcm3=C1_init*1e-6*12*1e-6;
C2_init = 0;%in umol m-3

%Reco inhibited when WT high
  a1=0.00033;
  a2=0.0014;
  a3=0.75;

%Time Invariant
R = 8.314;%J K-1 mol-1
RT = R .* (TA_2 + 274.15);%T in Kelvin-all units cancel out
Vmax1 = alpha1 .* exp(-ea1./RT);%umol m-2 s-1 SOM
Vmax2 = alpha2 .* exp(-ea2./RT);%umol m-2 s-1 labile

%variable letting model know when switching sites
%i.e. restart pools
site_change=diff(site_2);
site_change=vertcat(site_change, 0);

%preallocating space
S1sol = zeros(1,length(Time_2));
S2sol = zeros(1,length(Time_2));
R1 = zeros(1,length(Time_2));
R2 = zeros(1,length(Time_2));
S1=zeros(1,length(Time_2));
S2=zeros(1,length(Time_2));
percent_reduction=zeros(1,length(Time_2));
percent_enhancement=zeros(1,length(Time_2));
Reco_1=zeros(1,length(Time_2));
Reco_full=zeros(1,length(Time_2));
%NPP_1=zeros(1,length(Time_2));
%NPP_full=zeros(1,length(Time_2));
Ps=zeros(1,length(Time_2));
C2in=zeros(1,length(Time_2));



for t = 1:length(Time_2);%at t=1
    
%C allocation
    C2in(t) = NPPsum_avail_2(t);
%     GPPsum_avail(t) = NPPsum_avail_2(t)*0.5;%only 50% of GPP is available to methanogens
%     C2in_ch4(t) = GPPsum_avail(t);

    if t == 1 || site_change(t)>0 %if beginning of model or switch sites, start C1 pool over
        S1(t) = C1_init ;%substrate avail NOT affected by water avail-- SOM pool
        S2(t) = C2_init + C2in(t) ; % Ps C pool-- some initial Ps C lingering in soil + day 1 GPPavail
    else
        S1(t) = S1sol(t-1);
        S2(t) = S2sol(t-1);%substrate availability based on Ps on time step previous
    end
% if S1(t)<0
%     S1(t)=0;
% end
%  if S2(t)<0
%    S2(t)=0;
% end
 %following Davidson and using multiple eq for diff substrate pools
    R1(t) = Vmax1(t) .* S1(t) ./(km1 + S1(t)); %umol m2 sec Reaction velocity
    R2(t) = Vmax2(t) .* S2(t) ./(km2 + S2(t)); %umol m2 sec   
    if R1(t)<0
        R1(t)=0;
    end
    if R2(t)<0
        R2(t)=0;
    end
 
%Reco is reduced by 25% when WT is at or above soil surface
%--McNicol Silver 2015
%   a1=0.00033;
%   a2=0.0014;
%   a3=0.75;
%   WT_ex=[-30 -20 -10 0];
%   percent_red_ex=[1 0.85 0.77 0.75];
%   figure
%   plot(WT_ex,percent_red_ex,'.')

    percent_reduction(t)=(a1*WT_2(t).^2)-(a2*WT_2(t))+a3;
if WT_2(t)>5
    percent_reduction(t)=0.75;
end
    
if percent_reduction(t)>1.25
   percent_reduction(t)=1.25;
end
if percent_reduction(t)<0.75
   percent_reduction(t)=0.75;
end

    
    R1(t) = R1(t)*percent_reduction(t) ; %umol m2 sec Reaction velocity
    R2(t) = R2(t)*percent_reduction(t); %umol m2 sec
    
%Empirical factor for elevated Reco during the first 3 yrs following restoration
   if wetland_age_2(t)<4
    percent_enhancement(t)=1.2;
   else
    percent_enhancement(t)=1;
   end
    R1(t) = R1(t)*percent_enhancement(t) ; %umol m2 sec Reaction velocity
    R2(t) = R2(t)*percent_enhancement(t); %umol m2 sec
   
    if (t==1)
        S1sol(t) = C1_init - (R1(t)*60*30);%accounts for depletion of C sources in soil due to Reco and methane production
        S2sol(t) = (C2_init+C2in(t)) - (R2(t)*60*30);
    else
        S1sol(t) = S1sol(t-1) - (R1(t)*60*30);
        S2sol(t) = (S2sol(t-1)+C2in(t))- (R2(t)*60*30);
    end
    if S1sol(t)<0
        S1sol(t)=0;
    end
    if S2sol(t)<0
        S2sol(t)=0;
    end
    
if Season_drop_2(t)>5%in autumn time or season 6, labile PS C pool empties into SOM
    
        S1sol(t) = S1sol(t)+(0.2*S2sol(t));%move part of labile C into SOM pool--mimicing plant matter dying
        S2sol(t) = S2sol(t)-(0.2*S2sol(t));
        %S1sol(t) = S1sol(t)+(S2sol(t));%move entire labile C into SOM pool--mimicing plant matter dying
end

if Season_drop_2(t)<2%in winter time or season 1, labile PS C pool empties into SOM
    
        %S1sol(t) = S1sol(t)+(0.2*S2sol(t));%move part of labile C into SOM pool--mimicing plant matter dying
        S1sol(t) = S1sol(t)+(S2sol(t));%move entire labile C into SOM pool--mimicing plant matter dying
        S2sol(t) = 0;
end
    
   Reco_1(t) = R1(t) + R2(t); 
   Reco_full(t) = (R1(t)*60*30) + (R2(t)*60*30); %umol m2 30min
end
NEE_mod=GPP_2'+Reco_1;%umol m-2 s-1

end

  














