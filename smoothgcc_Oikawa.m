function [gcc,Yearsmooth,DOYsmooth,gccsmooth] = smoothgcc_Oikawa(gcc,decday,Year,DOYrunning,threshold,windowsize,method,fig)

% This function was written to calculate and smooth gcc data as in Sonnentag et al.
% (2012) afm ('Digital repeat photography for phenological research in
% forest ecosystems') and matches the options available from the 'PhenoCam
% GUI Tool' (for more information refer to 'PhenoCamGUI_UserGuide'
% available on the phenocam website
% http://phenocam.sr.unh.edu/webcam/tools/)

% ------------------- Inputs ------------------

% grn = green brightness level from region of interest (ROI)

% red = red brightness level from ROI

% blu = blue brightness level

% decday = decimal day or year

% DOYrunning = DOY associated associated with each image increasing continuously from DOY(1) 

% Year = vector associated with year

% Threshold = A darkness threshold.  This number dictates the minimum
% digital number value (pixel brightness).A value 15% is recommended

% windowsize = window size (odd numbered so that window is centered at mid-point). 
% A window of 3 days is recommended based on prior research.

% Method = choose either '90percentile', 'mean' or 'median' filtering.
% This smoothing will produce a time series of the 90th percentile, mean or median value at the interval 
% (window) chosen above. 'Percentile' is recommended based on Sonnentag et
% al. (2012) afm
% plot outputs = 1 for yes and 0 for no

% ------------------ Outputs ------------------
% gcc = green chromatic coordinates calculated as in Sonnentag et al. (2012) afm
% DOIsmooth = DOY associated with smoothed gcc value and increasing
% continuously from DOY(1)
% gccsmooth = smoothed gcc value

% -----------------------------------------------
% Sara Knox
% March 2015

% Calculate gcc
%gcc = grn./(grn+red+blu);
gcc=gcc;

% smooth
% set window size
ws = windowsize;

% Define method
method = method; %mean, median, or quantile

% set upper bound of smoothed data
l = floor(max(DOYrunning)./ws)-floor(DOYrunning(1)./ws);

% make matrix to dump smoothed results in
gccsmooth = NaN(l,1);
DOYsmooth = NaN(l,1);
Yearsmooth = NaN(l,1);

% set threshold (dark images)
threshold = threshold; %Suggested value of 15%
threshold = 255*(threshold/100); 

datebase = DOYrunning(1)-1;

% smooth time series    
for n=1:l;
    
    DOYsmooth(n) = datebase+(n+(ws-1)*(n-1)+floor(ws-ws/2));
    ind  = find(floor(decday) == DOYsmooth(n));
    Yearsmooth(n) = Year(ind(1));
    
    subset = find(DOYrunning >= datebase+(n+(ws-1)*(n-1)) & DOYrunning <= datebase+(n+(ws-1)*(n-1))+(ws-1) &...
        gcc > threshold );
    
    if strcmp(method,'median') == 1
        gccsmooth(n)=nanmedian(gcc(subset));
    end
    
    if strcmp(method,'90percentile') == 1
        data = gcc(subset);
        ind2 = find(~isnan(data));
        gccsmooth(n)=quantile(data(ind2),0.9);
    end
    
    if strcmp(method,'mean') == 1
        gccsmooth(n)=nanmean(gcc(subset));
    end
    
end

if fig == 1
    plot(decday, gcc,'.','MarkerSize',14);
    hold on
    plot(DOYsmooth, gccsmooth,'.','MarkerSize',20);
    legend('gcc','gccsmooth')
end


