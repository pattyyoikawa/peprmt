%code for parameterizing PEPRMT CO2 Reco model
%8.8.2016

%save chain results in simple format
chaindata.findmin=[find_min find_min_2];
chaindata.long=[chain_long sschain_long];
chaindata.short=[sschain chain];

find_min =chaindata.findmin(:,1);
find_min_2 =chaindata.findmin(:,2);
chain_long  =chaindata.long(:,1:4);
sschain_long=chaindata.long(:,5);
sschain =chaindata.short(:,1);
chain=chaindata.short(:,2:5);
 
%paramerterization data
%run using modeled GPP
xdata=[DOY_param DOY_disc_param TA_param WT_gf_param PAR_gf_param LAI_gf_param GPP_min_param decday_param Season_drop_param wc_90CI_param site_param wetland_age_param];
ydata=[DOY_param wc_gf_param];%this has NEE umol m-2 s-1 and CH4 umol m-2 30min-1
zdata=[daily_gapfilling_error_wc_param daily_random_error_wc_param daily_Reco_obs_param' daily_GPP_obs_param'];

theta=[0 1 0 1];
theta=theta';
[NEE_mod_param, S1_mod_param, S2_mod_param, Reco_mod_param] = PEPRMT_final_sys_CO2_Reco(find_min_Reco_new,xdata);
figure
plot(Reco_mod_param)
hold on
plot(er_ANNnight_param)
legend('mod','obs')

figure
plot(wc_gf_param)
hold on
plot(NEE_mod_param)
legend('obs','mod')

figure
plot(cumsum(wc_gf_param),'r')
hold on
plot(cumsum(NEE_mod_param))
 
figure
plot(decday_param,S1,'.')
hold on
plot(decday_param,S2,'.')
legend('SOM','labile')
%Reco
figure
plot(decday_param,er_ANNnight_param,'.',decday_param,Reco,'.')
legend('obs','mod')
figure
plot(Reco,er_ANNnight_param,'.')
xlabel('mod')
ylabel('obs')
 
figure
plot(TA_param,er_ANNnight_param,'.')
figure
plot(WT_gf_param,percent_reduction,'.')
figure
plot(decday_param,Vmax1,'.',decday_param,Vmax2,'.')
legend('som','ps')
figure
plot(decday_param,R1,'.')
hold on
plot(decday_param,R2,'.')
legend('SOM','labile')
 
X = ones(length(Reco'),1);
X = [X Reco'];
[~,~,~,~,STATS] = regress(er_ANNnight_param,X);
%  
%  
% %annual sum test
% wc_dailysum=wc_gf_param*60*30;%convert from nmol m-2 s-1 to nmol m-2 day-1
% cum_obs=cumsum(wc_dailysum);%umol m-2 yr-1
% cum_obs=cum_obs(end)*10^-6;%mol CO2 m-2 yr-1
% cum_obs=cum_obs*44.01*0.27;%g C-CO2 m-2 yr-1
% %model_mean=model_mean';
% mod_dailysum=NEE_mod*60*30;%convert from nmol m-2 s-1 to nmol m-2 day-1
% cum_mod=cumsum(mod_dailysum);%nmol m-2 yr-1
% cum_mod=cum_mod(end)*10^-6;%mol m-2 yr-1
% cum_mod=cum_mod*44.01*0.27;%g C-CO2 m-2 yr-1
 
figure
plot(WT_gf_param)
figure
plot(WT_gf,Reco,'.',WT_gf,er_ANNnight,'.')
legend('mod','obs')
  a1=0.00033;
  a2=0.0014;
  a3=0.75;
 
percent_reduction=(a1*WT_gf_param.^2)-(a2*WT_gf_param)+a3;
figure
plot(WT_gf_param,percent_reduction,'.')
 
%all data
xdata=[DOY DOY_disc TA WT_gf PAR_gf LAI_gf GPP_mod_min decday Season_drop wc_90CI site wetland_age];
[NEE_mod_min, S1, S2, Reco_mod_min] = PEPRMT_final_sys_CO2_Reco(find_min,xdata);
figure
plot(decday,er_ANNnight,'.',decday,Reco_mod_min,'.')
legend('obs','mod')


 
%% start MDF

%use for running PEPRMT_final_sys_CO2
model.ssfun = @PEPRMT_final_ss_CO2_Reco;
model.modelfun = @PEPRMT_final_sys_CO2_Reco;

data.xdata = xdata;
data.ydata = ydata;
data.zdata = zdata;
 
 Rsum_obs = data.ydata(:,2);
 nan_obs = sum(isnan(Rsum_obs));
 n_obs = length(Rsum_obs) - nan_obs;
model.N     = n_obs;%total number of non nan obs
%model.S20 = [1 1 2];%has to do with error variance of prior
%model.N0  = [4 4 4];%has to do with error variance of prior
 
params = {
    {'ea1',           0,     -5,         5}%eas allowed to go 10-80 , chain 1=1 , chain 2=30, chain 3=60 or 19, then range here is -10 to 60
    {'km1',           1,     1e-10,        1e10}%chain1=1, chain 2=5, chain 3=1e2
    {'ea2',           0,     -5,         5}
    {'km2',           1,     1e-10,        1e10}
%     name      start/theta/par  lBound   uBound    mean_prior   std_prior
    };
 
options.method = 'dram';
options.verbosity  = 0;%level of info printed
options.burnintime = 50000;% burn in before adaptation starts
%[results, chain, s2chain] = mcmcrun_FINAL_FINAL(model,data,params,options,results);
% [RESULTS,CHAIN,S2CHAIN,SSCHAIN] = mcmcrun_FINAL_FINAL(MODEL,DATA,PARAMS,OPTIONS)
%First generate an initial chain
options.nsimu = 50000;
[results, chain, s2chain, sschain]= mcmcrun_FINAL(model,data,params,options);

%find the minimum
figure
plot(sschain,'.')%sum-of-squares chains
nanmin(sschain)
Sel=sschain<nanmin(sschain)+0.0001;%442
figure
plot(Sel)
% cumsum(Sel)
find_min=chain(Sel,:);
find_min=find_min(end,:)';
theta=find_min;
figure
plot(chain(:,3))

[NEE_mod, S1, S2, Reco] = PEPRMT_final_sys_CO2_Reco(find_min,xdata);
figure
plot(Reco)
hold on
plot(er_ANNnight_param)
legend('mod','obs')


%Then re-run starting from the results of the previous run, this will take about 10 minutes.
params = {
    {'ea1',          find_min(1),     -5,         -0.1}
    {'km1',          find_min(2),     1e-3,        1e3}
    {'ea2',          find_min(3),     -5,         -0.1}
    {'km2',          find_min(4),     1e-3,        1e3}
%     name      start/theta/par  lBound   uBound    mean_prior   std_prior
    };
 
options.method = 'dram';
options.verbosity  = 0;%level of info printed
options.burnintime = 0;% burn in before adaptation starts
 
options.nsimu = 150000;
[results, chain, s2chain, sschain] = mcmcrun_FINAL(model,data,params,options, results);
 
figure
mcmcplot(chain,[],results,'chainpanel');%plots chain for ea parameter and all the values it accepted
figure
mcmcplot(chain,[],results,'pairs');%tells you if parameters are correlated
%  figure
%  mcmcplot(sqrt(s2chain),[],[],'hist')%take square root of the s2chain to get the chain for error standard deviation
% title('Error std posterior')
figure
mcmcplot(chain,[],results,'denspanel',2);

sschain_long=sschain;%save before you move on
chain_long=chain;
results_long=results;

%find the minimum
figure
plot(sschain_long,'.')%sum-of-squares chains
nanmin(sschain_long)
Sel=sschain_long<nanmin(sschain_long)+0.000001;%442
figure
plot(Sel)
% cumsum(Sel)
find_min=chain_long(Sel,:);
find_min=find_min(end,:)';

[~, ~, ~, Reco_mod_min] = PEPRMT_final_sys_CO2_Reco(find_min,xdata);
figure
plot(decday_param,er_ANNnight_param,'.',decday_param,Reco_mod_min,'.')
legend('obs','mod')

%% Choose posterior

%First establish if chain stabilized
chainstats(chain,results)
%Geweke test should be high ideally >0.9 for all parameters

%rejection rate of chain needs to be less than 50%
results_long.rejected*100

%Determine where means and sd of parameters stabilize in the chain
%this tells you what part of chain is stable for selection of posterior

%see where means and sd of parameters stabilize in the chain
%this tells you what part of chain is stable for selection of posterior
window_size = 5000;
simple = tsmovavg(chain_long(:,1),'s',window_size,1);
simple_std = movingstd(chain_long(:,1),5000);
figure
subplot(2,1,1)
plot(simple,'.')
title('5000 iteration moving average EA SOM')
subplot(2,1,2)
plot(simple_std,'.')
title('5000 iteration moving SD EA SOM')

window_size = 5000;
simple = tsmovavg(chain_long(:,2),'s',window_size,1);
simple_std = movingstd(chain_long(:,2),5000);
figure
subplot(2,1,1)
plot(simple,'.')
title('5000 iteration moving average km SOM')
subplot(2,1,2)
plot(simple_std,'.')
title('5000 iteration moving SD km SOM')

window_size = 5000;
simple = tsmovavg(chain_long(:,3),'s',window_size,1);
simple_std = movingstd(chain_long(:,3),5000);
figure
subplot(2,1,1)
plot(simple,'.')
title('5000 iteration moving average EA labile')
subplot(2,1,2)
plot(simple_std,'.')
title('5000 iteration moving SD EA labile')

window_size = 5000;
simple = tsmovavg(chain_long(:,4),'s',window_size,1);
simple_std = movingstd(chain_long(:,4),5000);
figure
subplot(2,1,1)
plot(simple,'.')
title('5000 iteration moving average km labile')
subplot(2,1,2)
plot(simple_std,'.')
title('5000 iteration moving SD km labile')

%identified visually that chain is good around 100,000
%choose parammeters 10,000 parameter sets from that area
%e.g. 100,000-101,000

find_min_10000=chain(140001:150000,:);%update everytime
%now select 1000 unique parameter sets from these 10,000, that are
%distributed across the range
figure
hist(find_min_10000(:,1))
%bin data and sample evenly across bins


%run 10000 parameter sets through validation data + and test chi square
xdata=[DOY_valid DOY_disc_valid TA_valid WT_gf_valid PAR_gf_valid LAI_gf_valid GPP_min_valid decday_valid Season_drop_valid wc_90CI_valid site_valid wetland_age_valid];
ydata=[DOY_valid wc_gf_valid];%this has NEE umol m-2 s-1 and CH4 umol m-2 30min-1
zdata=[daily_gapfilling_error_wc_valid daily_random_error_wc_valid daily_Reco_obs_valid daily_GPP_obs_valid];

%make 1000 theta sets that will be used to estimate CI 
 model_pop_Reco=zeros(length(1:35040));%length of validation data
 model_pop_Reco=model_pop_Reco(:,1:10000);%update as needed 
 
for i=1:length(find_min_10000)
    [~, ~, ~, Reco] = PEPRMT_final_sys_CO2_Reco(find_min_10000(i,:)',xdata);
    model_pop_Reco(:,i)=Reco;
    
end

model_std_Reco=std(model_pop_Reco');
model_mean_Reco=mean(model_pop_Reco');
mean_theta_final=nanmean(find_min_10000);
mean_theta_final=mean_theta_final';

%% Run validation data through MB WP
% [NEE_mod_mean, S1_mean, S2_mean, Reco_mean]  = PEPRMT_final_sys_CO2_Reco(mean_theta_final,xdata);
% figure
% plot(decday_valid,er_ANNnight_valid,'.',decday_valid,Reco_mean,'.')
% legend('obs','mod mean')
% figure
% plot(decday_valid,wc_gf_valid,'.',decday_valid,NEE_mod_mean,'.')
% legend('obs','mod mean')

%find_min_Reco_new=find_min_Reco;

[NEE_mod_min, S1, S2, Reco_min]  = PEPRMT_final_sys_CO2_Reco(find_min_Reco_new,xdata);

figure
plot(decday_valid,er_ANNnight_valid,'.',decday_valid,Reco_min,'.')
legend('obs','mod min')
figure
plot(decday_valid,wc_gf_valid,'.',decday_valid,NEE_mod_min,'.')
legend('obs','mod min')
figure
plot(cumsum(wc_gf_valid))
hold on
plot(cumsum(NEE_mod_min))
legend('obs','mod min')
figure
plot(decday_valid,GPP_min_valid,'.',decday_valid,gpp_ANNnight_valid,'.')
legend('mod','obs')

figure
plot(decday_valid,S1,'.',decday_valid,S2,'.')
legend('SOC','labile C')

S1_totalC=S1*5*1e-6*12*1e-3;%S1 is in umol C m-3 available, multiply by 5 to get total C, then convert to g C m-3
S2_totalC=S2*5*1e-6*12*1e-3;%S1 is in umol C m-3 available, multiply by 5 to get total C, then convert to g C m-3
 
figure
plot(decday_valid,S1_totalC,'.',decday_valid,S2_totalC,'.')
xlabel('Decday')
ylabel('Total C (kg C m-2)')
legend('SOC','labile C')

%Get average annual increase in SOC
SOC_increase_MB_2015=S1_totalC()-S1_totalC();
SOC_increase_WP_2015=S1_totalC()-S1_totalC();


figure
plot(model_std_Reco,'.')
figure
plot(model_std,'.')

% 90% confidence intervals (if you want 95% use 1.96)
%b/c the 10,000 parameters are not independent or may even be redundant, I
%reduce the sample size to 1000
lower_Reco=Reco_min-(1.645*(model_std_Reco./sqrt(50)));%use 1.645 for 90% CI
upper_Reco=Reco_min+(1.645*(model_std_Reco./sqrt(50)));
%     lower_Reco=quantile(model_pop_Reco',0.05);% 90% quantile
%     upper_Reco=quantile(model_pop_Reco',0.95);% 90% quantile
% 

figure
plot(lower_Reco,'.')
hold on
plot(upper_Reco,'.')


lower_GPP_mod_min=GPP_min_mod-(1.645*(model_std_Reco./sqrt(50)));
upper_GPP_mod_min=GPP_min_mod+(1.645*(model_std_Reco./sqrt(50)));
figure
plot(lower_GPP_mod_min)%using Reco uncertainty
hold on
plot(lower_GPP_mod)%using GPP uncertainty quantile
hold on
plot(upper_GPP_mod_min)
hold on
plot(upper_GPP_mod)
legend('lower new','lower old','upper new','upper old')

%lower and upper NEE
lower_NEE=lower_GPP_mod_min+lower_Reco;
upper_NEE=upper_GPP_mod_min+upper_Reco;
% lower_NEE=NEE_mod_min'-(1.645*(model_std_Reco./sqrt(5)));
% upper_NEE=NEE_mod_min'+(1.645*(model_std_Reco./sqrt(5)));

figure
plot(decday_valid,lower_NEE,'.',decday_valid,upper_NEE,'.',decday_valid,NEE_mod_min,'.',decday_valid,wc_gf_valid,'.')
legend('low','upper','min','obs')

 
%CAIC
p=4;
residual_SS_MB=sum((NEE_mod_min(1:17520)-wc_gf_valid(1:17520))^2);
CAIC_valid_MB=-2*log(residual_SS_MB) + p*(log(17520)+1);
residual_SS_WP=sum((NEE_mod_min(17521:end)-wc_gf_valid(17521:end))^2);
CAIC_valid_WP=-2*log(residual_SS_WP) + p*(log(17520)+1);

%% East End data
xdata_EE=[DOY_valid(1:17520) DOY_disc_valid(1:17520) TA_gf_EE_2015 WT_gf_EE_2015 PAR_gf_EE_2015 LAI_gf_EE_2015 GPP_min_EE_2015' decday_EE_2015 Season_drop_valid(1:17520) wc_90CI_valid(1:17520) site_EE wetland_age_EE];
[NEE_mod_min_EE, S1_EE, S2_EE, Reco_min_EE]  = PEPRMT_final_sys_CO2_Reco(find_min_Reco_new,xdata_EE);

figure
plot(decday_EE_2015,er_ANNnight_EE,'.',decday_EE_2015,Reco_min_EE,'.')
legend('obs','mod min')
figure
plot(decday_EE_2015,wc_gf_EE_2015,'.',decday_EE_2015,NEE_mod_min_EE,'.')
legend('obs','mod min')
figure
plot(cumsum(wc_gf_EE_2015))
hold on
plot(cumsum(NEE_mod_min_EE))
legend('obs','mod min')
figure
plot(decday_EE_2015,GPP_min_EE_2015,'.',decday_EE_2015,gpp_ANNnight_EE,'.')
legend('mod','obs')

figure
plot(decday_EE_2015,S1_EE,'.',decday_EE_2015,S2_EE,'.')
legend('SOC','labile C')

S1_totalC_EE=S1_EE*5*1e-6*12*1e-3;%S1 is in umol C m-3 available, multiply by 5 to get total C, then convert to g C m-3
S2_totalC_EE=S2_EE*5*1e-6*12*1e-3;%S1 is in umol C m-3 available, multiply by 5 to get total C, then convert to g C m-3
 
figure
plot(decday_EE_2015,S1_totalC_EE,'.',decday_EE_2015,S2_totalC_EE,'.')
xlabel('Decday')
ylabel('Total C (kg C m-2)')
legend('SOC','labile C')

%Get average annual increase in SOC
SOC_increase_EE_2015=S1_totalC(end)-S1_totalC(1);


figure
plot(model_std_Reco,'.')
figure
plot(model_std,'.')

% 90% confidence intervals (if you want 95% use 1.96)
%b/c the 10,000 parameters are not independent or may even be redundant, I
%reduce the sample size to 1000
lower_Reco_EE=Reco_min_EE-(1.645*(model_std_Reco(1:17520)./sqrt(50)));%use 1.645 for 90% CI
upper_Reco_EE=Reco_min_EE+(1.645*(model_std_Reco(1:17520)./sqrt(50)));
%     lower_Reco=quantile(model_pop_Reco',0.05);% 90% quantile
%     upper_Reco=quantile(model_pop_Reco',0.95);% 90% quantile
% 

figure
plot(lower_Reco,'.')
hold on
plot(upper_Reco,'.')


lower_GPP_mod_min_EE=GPP_min_EE_2015-(1.645*(model_std_Reco(1:17520)./sqrt(50)));
upper_GPP_mod_min_EE=GPP_min_EE_2015+(1.645*(model_std_Reco(1:17520)./sqrt(50)));
figure
plot(lower_GPP_mod_min_EE)%using Reco uncertainty
hold on
plot(upper_GPP_mod_min_EE)

%lower and upper NEE
lower_NEE_EE=lower_GPP_mod_min_EE+lower_Reco_EE;
upper_NEE_EE=upper_GPP_mod_min_EE+upper_Reco_EE;
% lower_NEE=NEE_mod_min'-(1.645*(model_std_Reco./sqrt(5)));
% upper_NEE=NEE_mod_min'+(1.645*(model_std_Reco./sqrt(5)));

figure
plot(decday_EE_2015,lower_NEE_EE,'.',decday_EE_2015,upper_NEE_EE,'.',decday_EE_2015,NEE_mod_min_EE,'.',decday_EE_2015,wc_gf_EE_2015,'.')
legend('low','upper','min','obs')

%CAIC
p=4;
residual_SS_EE=sum((NEE_mod_min_EE-wc_gf_EE_2015)^2);
CAIC_valid_EE=-2*log(residual_SS_EE) + p*(log(17520)+1);


%% annual sums
%Validation MB stats
NEE_mod_min=NEE_mod_min';
lower_NEE=lower_NEE';
upper_NEE=upper_NEE';

wc_gf_valid_cum=wc_gf_valid*60*30;
wc_gf_lower_valid_cum=wc_gf_lower_valid*60*30;
wc_gf_upper_valid_cum=wc_gf_upper_valid*60*30;
NEE_mod_min_cum=NEE_mod_min*60*30;
lower_NEE_cum=lower_NEE*60*30;
upper_NEE_cum=upper_NEE*60*30;

cum_obs_NEE_valid_MB=cumsum(wc_gf_valid_cum(1:17520));%umol m-2 yr-1
cum_obs_NEE_valid_sum_MB=cum_obs_NEE_valid_MB(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_NEE_valid_sum_MB=cum_obs_NEE_valid_sum_MB*44.01*0.27;%g C-Ch4 m-2 yr-1

cum_obs_NEE_valid_lower_MB=cumsum(wc_gf_lower_valid_cum(1:17520));%umol m-2 yr-1
cum_obs_NEE_valid_sum_lower_MB=cum_obs_NEE_valid_lower_MB(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_NEE_valid_sum_lower_MB=cum_obs_NEE_valid_sum_lower_MB*44.01*0.27;%g C-Ch4 m-2 yr-1

cum_obs_NEE_valid_upper_MB=cumsum(wc_gf_upper_valid_cum(1:17520));%umol m-2 yr-1
cum_obs_NEE_valid_sum_upper_MB=cum_obs_NEE_valid_upper_MB(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_NEE_valid_sum_upper_MB=cum_obs_NEE_valid_sum_upper_MB*44.01*0.27;%g C-Ch4 m-2 yr-1

(cum_obs_NEE_valid_sum_lower_MB-cum_obs_NEE_valid_sum_MB)/3

cum_mod_NEE_valid_MB=cumsum(NEE_mod_min_cum(1:17520));%nmol m-2 yr-1
cum_mod_NEE_valid_sum_MB=cum_mod_NEE_valid_MB(end)*10^-6;%mol m-2 yr-1
cum_mod_NEE_valid_sum_MB=cum_mod_NEE_valid_sum_MB*44.01*0.27;%g C-Ch4 m-2 yr-1

cum_mod_NEE_valid_lower_MB=cumsum(lower_NEE_cum(1:17520));%nmol m-2 yr-1
cum_mod_NEE_valid_sum_lower_MB=cum_mod_NEE_valid_lower_MB(end)*10^-6;%mol m-2 yr-1
cum_mod_NEE_valid_sum_lower_MB=cum_mod_NEE_valid_sum_lower_MB*44.01*0.27;%g C-Ch4 m-2 yr-1
 
cum_mod_NEE_max_valid_upper_MB=cumsum(upper_NEE_cum(1:17520));%nmol m-2 yr-1
cum_mod_NEE_valid_sum_upper_MB=cum_mod_NEE_max_valid_upper_MB(end)*10^-6;%mol m-2 yr-1
cum_mod_NEE_valid_sum_upper_MB=cum_mod_NEE_valid_sum_upper_MB*44.01*0.27;%g C-Ch4 m-2 yr-1

cum_mod_NEE_valid_sum_upper_MB-cum_mod_NEE_valid_sum_MB
(cum_mod_NEE_valid_sum_MB*100)/cum_obs_NEE_valid_sum_MB

%% Validation WP 
cum_obs_NEE_valid_WP=cumsum(wc_gf_valid_cum(17521:end));%umol m-2 yr-1
cum_obs_NEE_valid_sum_WP=cum_obs_NEE_valid_WP(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_NEE_valid_sum_WP=cum_obs_NEE_valid_sum_WP*44.01*0.27;%g C-Ch4 m-2 yr-1

cum_obs_NEE_valid_lower_WP=cumsum(wc_gf_lower_valid_cum(17521:end));%umol m-2 yr-1
cum_obs_NEE_valid_sum_lower_WP=cum_obs_NEE_valid_lower_WP(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_NEE_valid_sum_lower_WP=cum_obs_NEE_valid_sum_lower_WP*44.01*0.27;%g C-Ch4 m-2 yr-1

cum_obs_NEE_valid_upper_WP=cumsum(wc_gf_upper_valid_cum(17521:end));%umol m-2 yr-1
cum_obs_NEE_valid_sum_upper_WP=cum_obs_NEE_valid_upper_WP(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_NEE_valid_sum_upper_WP=cum_obs_NEE_valid_sum_upper_WP*44.01*0.27;%g C-Ch4 m-2 yr-1

(cum_obs_NEE_valid_sum_upper_WP-cum_obs_NEE_valid_sum_WP)/2

cum_mod_NEE_valid_WP=cumsum(NEE_mod_min_cum(17521:end));%nmol m-2 yr-1
cum_mod_NEE_valid_sum_WP=cum_mod_NEE_valid_WP(end)*10^-6;%mol m-2 yr-1
cum_mod_NEE_valid_sum_WP=cum_mod_NEE_valid_sum_WP*44.01*0.27;%g C-Ch4 m-2 yr-1

cum_mod_NEE_valid_lower_WP=cumsum(lower_NEE_cum(17521:end));%nmol m-2 yr-1
cum_mod_NEE_valid_sum_lower_WP=cum_mod_NEE_valid_lower_WP(end)*10^-6;%mol m-2 yr-1
cum_mod_NEE_valid_sum_lower_WP=cum_mod_NEE_valid_sum_lower_WP*44.01*0.27;%g C-Ch4 m-2 yr-1
 
cum_mod_NEE_max_valid_upper_WP=cumsum(upper_NEE_cum(17521:end));%nmol m-2 yr-1
cum_mod_NEE_valid_sum_upper_WP=cum_mod_NEE_max_valid_upper_WP(end)*10^-6;%mol m-2 yr-1
cum_mod_NEE_valid_sum_upper_WP=cum_mod_NEE_valid_sum_upper_WP*44.01*0.27;%g C-Ch4 m-2 yr-1

cum_mod_NEE_valid_sum_upper_WP-cum_mod_NEE_valid_sum_WP
(cum_mod_NEE_valid_sum_WP*100)/cum_obs_NEE_valid_sum_WP

%RMSE is a better test of model performance than r2
RMSE_ch4_min_WP_valid=nansum((CH4_mod_min_valid(17521:end)-daily_wm_sum_umol_gf_valid(17521:end)).^2);
RMSE_ch4_min_WP_valid=sqrt(RMSE_ch4_min_WP_valid/365);
RMSE_ch4_min_WP_valid=RMSE_ch4_min_WP_valid*10^-3*44.01*0.27;%mg C-Ch4 m-2 yr-1

%% Validation EE 
NEE_mod_min_EE=NEE_mod_min_EE';
lower_NEE_EE=lower_NEE_EE';
upper_NEE_EE=upper_NEE_EE';

wc_gf_EE_2015_cum=wc_gf_EE_2015*60*30;
wc_gf_EE_lower_cum=wc_gf_EE_lower*60*30;
wc_gf_EE_upper_cum=wc_gf_EE_upper*60*30;

NEE_mod_min_EE_cum=NEE_mod_min_EE*60*30;
lower_NEE_EE_cum=lower_NEE_EE*60*30;
upper_NEE_EE_cum=upper_NEE_EE*60*30;

cum_obs_NEE_valid_EE=cumsum(wc_gf_EE_2015_cum);%umol m-2 yr-1
cum_obs_NEE_valid_sum_EE=cum_obs_NEE_valid_EE(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_NEE_valid_sum_EE=cum_obs_NEE_valid_sum_EE*44.01*0.27;%g C-Ch4 m-2 yr-1

cum_obs_NEE_valid_lower_EE=cumsum(wc_gf_EE_lower_cum);%umol m-2 yr-1
cum_obs_NEE_valid_sum_lower_EE=cum_obs_NEE_valid_lower_EE(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_NEE_valid_sum_lower_EE=cum_obs_NEE_valid_sum_lower_EE*44.01*0.27;%g C-Ch4 m-2 yr-1

cum_obs_NEE_valid_upper_EE=cumsum(wc_gf_EE_upper_cum);%umol m-2 yr-1
cum_obs_NEE_valid_sum_upper_EE=cum_obs_NEE_valid_upper_EE(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_NEE_valid_sum_upper_EE=cum_obs_NEE_valid_sum_upper_EE*44.01*0.27;%g C-Ch4 m-2 yr-1

cum_obs_NEE_valid_sum_upper_EE-cum_obs_NEE_valid_sum_EE/2

cum_mod_NEE_valid_EE=cumsum(NEE_mod_min_EE_cum);%nmol m-2 yr-1
cum_mod_NEE_valid_sum_EE=cum_mod_NEE_valid_EE(end)*10^-6;%mol m-2 yr-1
cum_mod_NEE_valid_sum_EE=cum_mod_NEE_valid_sum_EE*44.01*0.27;%g C-Ch4 m-2 yr-1

cum_mod_NEE_valid_lower_EE=cumsum(lower_NEE_EE_cum);%nmol m-2 yr-1
cum_mod_NEE_valid_sum_lower_EE=cum_mod_NEE_valid_lower_EE(end)*10^-6;%mol m-2 yr-1
cum_mod_NEE_valid_sum_lower_EE=cum_mod_NEE_valid_sum_lower_EE*44.01*0.27;%g C-Ch4 m-2 yr-1
 
cum_mod_NEE_max_valid_upper_EE=cumsum(upper_NEE_EE_cum);%nmol m-2 yr-1
cum_mod_NEE_valid_sum_upper_EE=cum_mod_NEE_max_valid_upper_EE(end)*10^-6;%mol m-2 yr-1
cum_mod_NEE_valid_sum_upper_EE=cum_mod_NEE_valid_sum_upper_EE*44.01*0.27;%g C-Ch4 m-2 yr-1

cum_mod_NEE_valid_sum_upper_EE-cum_mod_NEE_valid_sum_EE

(cum_mod_NEE_valid_sum_EE*100)/cum_obs_NEE_valid_sum_EE
%RMSE is a better test of model performance than r2
RMSE_NEE_min_EE_valid=nansum((NEE_mod_min_EE_cum-wc_gf_EE_2015_cum).^2);
RMSE_NEE_min_EE_valid=sqrt(RMSE_NEE_min_EE_valid/365);
RMSE_NEE_min_EE_valid=RMSE_NEE_min_EE_valid*10^-3*44.01*0.27;%mg C-Ch4 m-2 yr-1

%% Looking at Reco to GPP ratio in MB
%Not re-naming cum variables so run and look at value but it won't be saved
gpp_ANNnight_valid_cum=gpp_ANNnight_valid*60*30;
er_ANNnight_valid_cum=er_ANNnight_valid*60*30;
GPP_min_mod_cum=GPP_min_mod*60*30;
Reco_min_cum=Reco_min*60*30;

%2015
cum_obs_GPP_valid_MB=cumsum(gpp_ANNnight_valid_cum(1:17520));%umol m-2 yr-1
cum_obs_GPP_valid_sum_MB=cum_obs_GPP_valid_MB(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_GPP_valid_sum_MB=cum_obs_GPP_valid_sum_MB*44.01*0.27;%g C-Ch4 m-2 yr-1

cum_obs_ER_valid_MB=cumsum(er_ANNnight_valid_cum(1:17520));%umol m-2 yr-1
cum_obs_ER_valid_sum_MB=cum_obs_ER_valid_MB(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_ER_valid_sum_MB=cum_obs_ER_valid_sum_MB*44.01*0.27;%g C-Ch4 m-2 yr-1

ratio_MB_obs_2015=cum_obs_ER_valid_sum_MB/cum_obs_GPP_valid_sum_MB;

cum_mod_GPP_valid_MB=cumsum(GPP_min_mod_cum(1:17520));%umol m-2 yr-1
cum_mod_GPP_valid_sum_MB=cum_mod_GPP_valid_MB(end)*10^-6;%mol ch4 m-2 yr-1
cum_mod_GPP_valid_sum_MB=cum_mod_GPP_valid_sum_MB*44.01*0.27;%g C-Ch4 m-2 yr-1

cum_mod_ER_valid_MB=cumsum(Reco_min_cum(1:17520));%umol m-2 yr-1
cum_mod_ER_valid_sum_MB=cum_mod_ER_valid_MB(end)*10^-6;%mol ch4 m-2 yr-1
cum_mod_ER_valid_sum_MB=cum_mod_ER_valid_sum_MB*44.01*0.27;%g C-Ch4 m-2 yr-1

ratio_MB_mod_2015=cum_mod_ER_valid_sum_MB/cum_mod_GPP_valid_sum_MB;

%% Looking at Reco to GPP ratio in WP
%Not re-naming cum variables so run and look at value but it won't be saved

%2015
cum_obs_GPP_valid_WP=cumsum(gpp_ANNnight_valid_cum(17521:end));%umol m-2 yr-1
cum_obs_GPP_valid_sum_WP=cum_obs_GPP_valid_WP(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_GPP_valid_sum_WP=cum_obs_GPP_valid_sum_WP*44.01*0.27;%g C-Ch4 m-2 yr-1

cum_obs_ER_valid_WP=cumsum(er_ANNnight_valid_cum(17521:end));%umol m-2 yr-1
cum_obs_ER_valid_sum_WP=cum_obs_ER_valid_WP(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_ER_valid_sum_WP=cum_obs_ER_valid_sum_WP*44.01*0.27;%g C-Ch4 m-2 yr-1

ratio_WP_obs_2015=cum_obs_ER_valid_sum_WP/cum_obs_GPP_valid_sum_WP;

cum_mod_GPP_valid_WP=cumsum(GPP_min_mod_cum(17521:end));%umol m-2 yr-1
cum_mod_GPP_valid_sum_WP=cum_mod_GPP_valid_WP(end)*10^-6;%mol ch4 m-2 yr-1
cum_mod_GPP_valid_sum_WP=cum_mod_GPP_valid_sum_WP*44.01*0.27;%g C-Ch4 m-2 yr-1

cum_mod_ER_valid_WP=cumsum(Reco_min_cum(17521:end));%umol m-2 yr-1
cum_mod_ER_valid_sum_WP=cum_mod_ER_valid_WP(end)*10^-6;%mol ch4 m-2 yr-1
cum_mod_ER_valid_sum_WP=cum_mod_ER_valid_sum_WP*44.01*0.27;%g C-Ch4 m-2 yr-1

ratio_WP_mod_2015=cum_mod_ER_valid_sum_WP/cum_mod_GPP_valid_sum_WP;

%% Looking at Reco to GPP ratio in EE
%Not re-naming cum variables so run and look at value but it won't be saved
gpp_ANNnight_EE_cum=gpp_ANNnight_EE*60*30;
er_ANNnight_EE_cum=er_ANNnight_EE*60*30;
GPP_min_EE_2015_cum=GPP_min_EE_2015*60*30;
Reco_min_EE_cum=Reco_min_EE*60*30;

%2015
cum_obs_GPP_valid_EE=cumsum(gpp_ANNnight_EE_cum);%umol m-2 yr-1
cum_obs_GPP_valid_sum_EE=cum_obs_GPP_valid_EE(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_GPP_valid_sum_EE=cum_obs_GPP_valid_sum_EE*44.01*0.27;%g C-Ch4 m-2 yr-1

cum_obs_ER_valid_EE=cumsum(er_ANNnight_EE_cum);%umol m-2 yr-1
cum_obs_ER_valid_sum_EE=cum_obs_ER_valid_EE(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_ER_valid_sum_EE=cum_obs_ER_valid_sum_EE*44.01*0.27;%g C-Ch4 m-2 yr-1

ratio_EE_obs_2015=cum_obs_ER_valid_sum_EE/cum_obs_GPP_valid_sum_EE;

cum_mod_GPP_valid_EE=cumsum(GPP_min_EE_2015_cum);%umol m-2 yr-1
cum_mod_GPP_valid_sum_EE=cum_mod_GPP_valid_EE(end)*10^-6;%mol ch4 m-2 yr-1
cum_mod_GPP_valid_sum_EE=cum_mod_GPP_valid_sum_EE*44.01*0.27;%g C-Ch4 m-2 yr-1

cum_mod_ER_valid_EE=cumsum(Reco_min_EE_cum);%umol m-2 yr-1
cum_mod_ER_valid_sum_EE=cum_mod_ER_valid_EE(end)*10^-6;%mol ch4 m-2 yr-1
cum_mod_ER_valid_sum_EE=cum_mod_ER_valid_sum_EE*44.01*0.27;%g C-Ch4 m-2 yr-1

ratio_EE_mod_2015=cum_mod_ER_valid_sum_EE/cum_mod_GPP_valid_sum_EE;

figure
plot(decday_EE_2015,gpp_ANNnight_EE,decday_EE_2015,er_ANNnight_EE)
hold on
plot(decday_EE_2015,gpp_ANNnight_MB(70129:end),decday_EE_2015,er_ANNnight_MB(70129:end))

%% Get uncertainty in modeled param data MB WP

%First, I have to run param data in 3 parts b/c the dataset is too long
xdata_param_1=[DOY_param(1:37808) DOY_disc_param(1:37808) TA_param(1:37808)...
    WT_gf_param(1:37808) PAR_gf_param(1:37808) LAI_gf_param(1:37808)...
    GPP_min_param(1:37808) decday_param(1:37808) Season_drop_param(1:37808)...
    wc_90CI_param(1:37808) site_param(1:37808) wetland_age_param(1:37808)];
%make 1000 theta sets that will be used to estimate CI 
 model_pop_Reco=zeros(length(1:37808));%length of validation data
 model_pop_Reco=model_pop_Reco(:,1:10000);%update as needed 
 
for i=1:length(find_min_10000)
    [~, ~, ~, Reco] = PEPRMT_final_sys_CO2_Reco(find_min_10000(i,:)',xdata_param_1);
    model_pop_Reco(:,i)=Reco;
end

model_std_Reco_1=std(model_pop_Reco');
model_mean_Reco_1=mean(model_pop_Reco');
mean_theta_final_1=nanmean(find_min_10000);
mean_theta_final_1=mean_theta_final_1';

xdata_param_2=[DOY_param(37809:75616) DOY_disc_param(37809:75616) TA_param(37809:75616)...
    WT_gf_param(37809:75616) PAR_gf_param(37809:75616) LAI_gf_param(37809:75616)...
    GPP_min_param(37809:75616) decday_param(37809:75616) Season_drop_param(37809:75616)...
    wc_90CI_param(37809:75616) site_param(37809:75616) wetland_age_param(37809:75616)];
%make 1000 theta sets that will be used to estimate CI 
 model_pop_Reco=zeros(length(1:37808));%length of validation data
 model_pop_Reco=model_pop_Reco(:,1:10000);%update as needed 
 
for i=1:length(find_min_10000)
    [~, ~, ~, Reco] = PEPRMT_final_sys_CO2_Reco(find_min_10000(i,:)',xdata_param_2);
    model_pop_Reco(:,i)=Reco;
end

model_std_Reco_2=std(model_pop_Reco');
model_mean_Reco_2=mean(model_pop_Reco');
mean_theta_final_2=nanmean(find_min_10000);
mean_theta_final_2=mean_theta_final_2';

xdata_param_3=[DOY_param(75617:end) DOY_disc_param(75617:end) TA_param(75617:end)...
    WT_gf_param(75617:end) PAR_gf_param(75617:end) LAI_gf_param(75617:end)...
    GPP_min_param(75617:end) decday_param(75617:end) Season_drop_param(75617:end)...
    wc_90CI_param(75617:end) site_param(75617:end) wetland_age_param(75617:end)];
%make 1000 theta sets that will be used to estimate CI 
 model_pop_Reco=zeros(length(1:37808));%length of validation data
 model_pop_Reco=model_pop_Reco(:,1:10000);%update as needed 
 
for i=1:length(find_min_10000)
    [~, ~, ~, Reco] = PEPRMT_final_sys_CO2_Reco(find_min_10000(i,:)',xdata_param_3);
    model_pop_Reco(:,i)=Reco;
end

model_std_Reco_3=std(model_pop_Reco');
model_mean_Reco_3=mean(model_pop_Reco');
mean_theta_final_3=nanmean(find_min_10000);
mean_theta_final_3=mean_theta_final_3';

%put 3 part together to get uncertainty
model_std_Reco_param=vertcat(model_std_Reco_1',model_std_Reco_2',model_std_Reco_3');
%% run param data MB WP
xdata_param=[DOY_param DOY_disc_param TA_param WT_gf_param PAR_gf_param LAI_gf_param GPP_min_param decday_param Season_drop_param wc_90CI_param site_param wetland_age_param];
[NEE_mod_min_param, S1_param, S2_param, Reco_min_param]  = PEPRMT_final_sys_CO2_Reco(find_min_Reco_new,xdata_param);

figure
plot(decday_param,er_ANNnight_param,'.',decday_param,Reco_min_param,'.')
legend('obs','mod min')
figure
plot(decday_param,wc_gf_param,'.',decday_param,NEE_mod_min_param,'.')
legend('obs','mod min')
figure
plot(cumsum(wc_gf_param))
hold on
plot(cumsum(NEE_mod_min_param))
legend('obs','mod min')
figure
plot(decday_param,gpp_ANNnight_param,'.',decday_param,GPP_min_param,'.')
legend('obs','mod min')

figure
plot(decday_param,S1_param,'.',decday_param,S2_param,'.')
legend('SOC','labile C')
S1_totalC_param=S1_param*5*1e-6*12*1e-3;%S1 is in umol C m-3 available, multiply by 5 to get total C, then convert to kg C m-3
S2_totalC_param=S2_param*5*1e-6*12*1e-3;%S1 is in umol C m-3 available, multiply by 5 to get total C, then convert to kg C m-3
figure
plot(decday_param,S1_totalC_param,'.',decday_param,S2_totalC_param,'.')
xlabel('Decday')
ylabel('Total C (kg C m-2)')
legend('SOC','labile C')

%Get average annual increase in SOC
SOC_increase_MB_2011=S1_totalC_param()-S1_totalC_param();
SOC_increase_MB_2012=S1_totalC_param()-S1_totalC_param();
SOC_increase_MB_2013=S1_totalC_param()-S1_totalC_param();
SOC_increase_MB_2014=S1_totalC_param()-S1_totalC_param();
SOC_increase_WP_2013=S1_totalC_param()-S1_totalC_param();
SOC_increase_WP_2014=S1_totalC_param()-S1_totalC_param();
(SOC_increase_MB_2011+SOC_increase_MB_2012+SOC_increase_MB_2013+SOC_increase_MB_2014)/4
(SOC_increase_WP_2013+SOC_increase_WP_2014)/2

% 90% confidence intervals (if you want 95% use 1.96)
%b/c the 10,000 parameters are not independent or may even be redundant, I
%reduce the sample size to 50
lower_Reco_param=Reco_min_param'-(1.645*(model_std_Reco_param./sqrt(50)));%use 1.645 for 90% CI
upper_Reco_param=Reco_min_param'+(1.645*(model_std_Reco_param./sqrt(50)));
figure
plot(lower_Reco_param,'.')
hold on
plot(upper_Reco_param,'.')
%assume GPP has same uncertainty as Reco--not sure if this is right
lower_GPP_param=GPP_min_param-(1.645*(model_std_Reco_param./sqrt(50)));
upper_GPP_param=GPP_min_param+(1.645*(model_std_Reco_param./sqrt(50)));
figure
plot(lower_GPP_param)%using GPP uncertainty quantile
hold on
plot(upper_GPP_param)

%lower and upper NEE
lower_NEE_param=lower_GPP_param+lower_Reco_param;
upper_NEE_param=upper_GPP_param+upper_Reco_param;

figure
plot(decday_param,lower_NEE_param,'.',decday_param,upper_NEE_param,'.',decday_param,NEE_mod_min_param,'.',decday_param,wc_gf_param,'.')
legend('low','upper','min','obs') 

%% Param stats MB 
NEE_mod_min_param=NEE_mod_min_param';
lower_NEE_param=lower_NEE_param';
upper_NEE_param=upper_NEE_param';

wc_gf_param_cum=wc_gf_param*60*30;
wc_gf_lower_param_cum=wc_gf_lower_param*60*30;
wc_gf_upper_param_cum=wc_gf_upper_param*60*30;
NEE_mod_min_param_cum=NEE_mod_min_param*60*30;
lower_NEE_param_cum=lower_NEE_param*60*30;
upper_NEE_param_cum=upper_NEE_param*60*30;

cum_obs_NEE_param_MB=cumsum(wc_gf_param_cum(1:70128));%umol m-2 yr-1
cum_obs_NEE_param_sum_MB=cum_obs_NEE_param_MB(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_NEE_param_sum_MB=cum_obs_NEE_param_sum_MB*44.01*0.27;%g C-Ch4 m-2 yr-1

cum_obs_NEE_param_lower_MB=cumsum(wc_gf_lower_param_cum(1:70128));%umol m-2 yr-1
cum_obs_NEE_param_sum_lower_MB=cum_obs_NEE_param_lower_MB(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_NEE_param_sum_lower_MB=cum_obs_NEE_param_sum_lower_MB*44.01*0.27;%g C-Ch4 m-2 yr-1

cum_obs_NEE_param_upper_MB=cumsum(wc_gf_upper_param_cum(1:70128));%umol m-2 yr-1
cum_obs_NEE_param_sum_upper_MB=cum_obs_NEE_param_upper_MB(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_NEE_param_sum_upper_MB=cum_obs_NEE_param_sum_upper_MB*44.01*0.27;%g C-Ch4 m-2 yr-1

(cum_obs_NEE_param_sum_lower_MB-cum_obs_NEE_param_sum_MB)/5

cum_mod_NEE_param_MB=cumsum(NEE_mod_min_param_cum(1:70128));%nmol m-2 yr-1
cum_mod_NEE_param_sum_MB=cum_mod_NEE_param_MB(end)*10^-6;%mol m-2 yr-1
cum_mod_NEE_param_sum_MB=cum_mod_NEE_param_sum_MB*44.01*0.27;%g C-Ch4 m-2 yr-1

cum_mod_NEE_param_lower_MB=cumsum(lower_NEE_param_cum(1:70128));%nmol m-2 yr-1
cum_mod_NEE_param_sum_lower_MB=cum_mod_NEE_param_lower_MB(end)*10^-6;%mol m-2 yr-1
cum_mod_NEE_param_sum_lower_MB=cum_mod_NEE_param_sum_lower_MB*44.01*0.27;%g C-Ch4 m-2 yr-1
 
cum_mod_NEE_max_param_upper_MB=cumsum(upper_NEE_param_cum(1:70128));%nmol m-2 yr-1
cum_mod_NEE_param_sum_upper_MB=cum_mod_NEE_max_param_upper_MB(end)*10^-6;%mol m-2 yr-1
cum_mod_NEE_param_sum_upper_MB=cum_mod_NEE_param_sum_upper_MB*44.01*0.27;%g C-Ch4 m-2 yr-1

cum_mod_NEE_param_sum_upper_MB-cum_mod_NEE_param_sum_MB
(cum_mod_NEE_param_sum_MB*100)/cum_obs_NEE_param_sum_MB

%% Annual sums for each param year MB n=4
%Not remaning cum variables so run and look at value
%2011
cum_obs_NEE_param_MB=cumsum(wc_gf_param_cum(1:17520));%umol m-2 yr-1
cum_obs_NEE_param_sum_MB=cum_obs_NEE_param_MB(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_NEE_param_sum_MB=cum_obs_NEE_param_sum_MB*44.01*0.27;%g C-Ch4 m-2 yr-1

cum_obs_NEE_param_lower_MB=cumsum(wc_gf_lower_param_cum(1:17520));%umol m-2 yr-1
cum_obs_NEE_param_sum_lower_MB=cum_obs_NEE_param_lower_MB(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_NEE_param_sum_lower_MB=cum_obs_NEE_param_sum_lower_MB*44.01*0.27;%g C-Ch4 m-2 yr-1

cum_obs_NEE_param_upper_MB=cumsum(wc_gf_upper_param_cum(1:17520));%umol m-2 yr-1
cum_obs_NEE_param_sum_upper_MB=cum_obs_NEE_param_upper_MB(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_NEE_param_sum_upper_MB=cum_obs_NEE_param_sum_upper_MB*44.01*0.27;%g C-Ch4 m-2 yr-1

(cum_obs_NEE_param_sum_lower_MB-cum_obs_NEE_param_sum_MB)/4

cum_mod_NEE_param_MB=cumsum(NEE_mod_min_param_cum(1:17520));%nmol m-2 yr-1
cum_mod_NEE_param_sum_MB=cum_mod_NEE_param_MB(end)*10^-6;%mol m-2 yr-1
cum_mod_NEE_param_sum_MB=cum_mod_NEE_param_sum_MB*44.01*0.27;%g C-Ch4 m-2 yr-1

cum_mod_NEE_param_lower_MB=cumsum(lower_NEE_param_cum(1:17520));%nmol m-2 yr-1
cum_mod_NEE_param_sum_lower_MB=cum_mod_NEE_param_lower_MB(end)*10^-6;%mol m-2 yr-1
cum_mod_NEE_param_sum_lower_MB=cum_mod_NEE_param_sum_lower_MB*44.01*0.27;%g C-Ch4 m-2 yr-1
 
cum_mod_NEE_max_param_upper_MB=cumsum(upper_NEE_param_cum(1:17520));%nmol m-2 yr-1
cum_mod_NEE_param_sum_upper_MB=cum_mod_NEE_max_param_upper_MB(end)*10^-6;%mol m-2 yr-1
cum_mod_NEE_param_sum_upper_MB=cum_mod_NEE_param_sum_upper_MB*44.01*0.27;%g C-Ch4 m-2 yr-1

(cum_mod_NEE_param_sum_upper_MB-cum_mod_NEE_param_sum_MB)*2
(cum_mod_NEE_param_sum_MB*100)/cum_obs_NEE_param_sum_MB

%2012
cum_obs_NEE_param_MB=cumsum(wc_gf_param_cum(17521:35088));%umol m-2 yr-1
cum_obs_NEE_param_sum_MB=cum_obs_NEE_param_MB(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_NEE_param_sum_MB=cum_obs_NEE_param_sum_MB*44.01*0.27;%g C-Ch4 m-2 yr-1

cum_obs_NEE_param_lower_MB=cumsum(wc_gf_lower_param_cum(17521:35088));%umol m-2 yr-1
cum_obs_NEE_param_sum_lower_MB=cum_obs_NEE_param_lower_MB(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_NEE_param_sum_lower_MB=cum_obs_NEE_param_sum_lower_MB*44.01*0.27;%g C-Ch4 m-2 yr-1

cum_obs_NEE_param_upper_MB=cumsum(wc_gf_upper_param_cum(17521:35088));%umol m-2 yr-1
cum_obs_NEE_param_sum_upper_MB=cum_obs_NEE_param_upper_MB(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_NEE_param_sum_upper_MB=cum_obs_NEE_param_sum_upper_MB*44.01*0.27;%g C-Ch4 m-2 yr-1

(cum_obs_NEE_param_sum_lower_MB-cum_obs_NEE_param_sum_MB)/4

cum_mod_NEE_param_MB=cumsum(NEE_mod_min_param_cum(17521:35088));%nmol m-2 yr-1
cum_mod_NEE_param_sum_MB=cum_mod_NEE_param_MB(end)*10^-6;%mol m-2 yr-1
cum_mod_NEE_param_sum_MB=cum_mod_NEE_param_sum_MB*44.01*0.27;%g C-Ch4 m-2 yr-1

cum_mod_NEE_param_lower_MB=cumsum(lower_NEE_param_cum(17521:35088));%nmol m-2 yr-1
cum_mod_NEE_param_sum_lower_MB=cum_mod_NEE_param_lower_MB(end)*10^-6;%mol m-2 yr-1
cum_mod_NEE_param_sum_lower_MB=cum_mod_NEE_param_sum_lower_MB*44.01*0.27;%g C-Ch4 m-2 yr-1
 
cum_mod_NEE_max_param_upper_MB=cumsum(upper_NEE_param_cum(17521:35088));%nmol m-2 yr-1
cum_mod_NEE_param_sum_upper_MB=cum_mod_NEE_max_param_upper_MB(end)*10^-6;%mol m-2 yr-1
cum_mod_NEE_param_sum_upper_MB=cum_mod_NEE_param_sum_upper_MB*44.01*0.27;%g C-Ch4 m-2 yr-1

(cum_mod_NEE_param_sum_upper_MB-cum_mod_NEE_param_sum_MB)*2
(cum_mod_NEE_param_sum_MB*100)/cum_obs_NEE_param_sum_MB

%2013
cum_obs_NEE_param_MB=cumsum(wc_gf_param_cum(35089:52608));%umol m-2 yr-1
cum_obs_NEE_param_sum_MB=cum_obs_NEE_param_MB(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_NEE_param_sum_MB=cum_obs_NEE_param_sum_MB*44.01*0.27;%g C-Ch4 m-2 yr-1

cum_obs_NEE_param_lower_MB=cumsum(wc_gf_lower_param_cum(35089:52608));%umol m-2 yr-1
cum_obs_NEE_param_sum_lower_MB=cum_obs_NEE_param_lower_MB(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_NEE_param_sum_lower_MB=cum_obs_NEE_param_sum_lower_MB*44.01*0.27;%g C-Ch4 m-2 yr-1

cum_obs_NEE_param_upper_MB=cumsum(wc_gf_upper_param_cum(35089:52608));%umol m-2 yr-1
cum_obs_NEE_param_sum_upper_MB=cum_obs_NEE_param_upper_MB(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_NEE_param_sum_upper_MB=cum_obs_NEE_param_sum_upper_MB*44.01*0.27;%g C-Ch4 m-2 yr-1

(cum_obs_NEE_param_sum_lower_MB-cum_obs_NEE_param_sum_MB)/4

cum_mod_NEE_param_MB=cumsum(NEE_mod_min_param_cum(35089:52608));%nmol m-2 yr-1
cum_mod_NEE_param_sum_MB=cum_mod_NEE_param_MB(end)*10^-6;%mol m-2 yr-1
cum_mod_NEE_param_sum_MB=cum_mod_NEE_param_sum_MB*44.01*0.27;%g C-Ch4 m-2 yr-1

cum_mod_NEE_param_lower_MB=cumsum(lower_NEE_param_cum(35089:52608));%nmol m-2 yr-1
cum_mod_NEE_param_sum_lower_MB=cum_mod_NEE_param_lower_MB(end)*10^-6;%mol m-2 yr-1
cum_mod_NEE_param_sum_lower_MB=cum_mod_NEE_param_sum_lower_MB*44.01*0.27;%g C-Ch4 m-2 yr-1
 
cum_mod_NEE_max_param_upper_MB=cumsum(upper_NEE_param_cum(35089:52608));%nmol m-2 yr-1
cum_mod_NEE_param_sum_upper_MB=cum_mod_NEE_max_param_upper_MB(end)*10^-6;%mol m-2 yr-1
cum_mod_NEE_param_sum_upper_MB=cum_mod_NEE_param_sum_upper_MB*44.01*0.27;%g C-Ch4 m-2 yr-1

(cum_mod_NEE_param_sum_upper_MB-cum_mod_NEE_param_sum_MB)*2
(cum_mod_NEE_param_sum_MB*100)/cum_obs_NEE_param_sum_MB

%2014
cum_obs_NEE_param_MB=cumsum(wc_gf_param_cum(52609:70128));%umol m-2 yr-1
cum_obs_NEE_param_sum_MB=cum_obs_NEE_param_MB(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_NEE_param_sum_MB=cum_obs_NEE_param_sum_MB*44.01*0.27;%g C-Ch4 m-2 yr-1

cum_obs_NEE_param_lower_MB=cumsum(wc_gf_lower_param_cum(52609:70128));%umol m-2 yr-1
cum_obs_NEE_param_sum_lower_MB=cum_obs_NEE_param_lower_MB(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_NEE_param_sum_lower_MB=cum_obs_NEE_param_sum_lower_MB*44.01*0.27;%g C-Ch4 m-2 yr-1

cum_obs_NEE_param_upper_MB=cumsum(wc_gf_upper_param_cum(52609:70128));%umol m-2 yr-1
cum_obs_NEE_param_sum_upper_MB=cum_obs_NEE_param_upper_MB(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_NEE_param_sum_upper_MB=cum_obs_NEE_param_sum_upper_MB*44.01*0.27;%g C-Ch4 m-2 yr-1

(cum_obs_NEE_param_sum_lower_MB-cum_obs_NEE_param_sum_MB)/4

cum_mod_NEE_param_MB=cumsum(NEE_mod_min_param_cum(52609:70128));%nmol m-2 yr-1
cum_mod_NEE_param_sum_MB=cum_mod_NEE_param_MB(end)*10^-6;%mol m-2 yr-1
cum_mod_NEE_param_sum_MB=cum_mod_NEE_param_sum_MB*44.01*0.27;%g C-Ch4 m-2 yr-1

cum_mod_NEE_param_lower_MB=cumsum(lower_NEE_param_cum(52609:70128));%nmol m-2 yr-1
cum_mod_NEE_param_sum_lower_MB=cum_mod_NEE_param_lower_MB(end)*10^-6;%mol m-2 yr-1
cum_mod_NEE_param_sum_lower_MB=cum_mod_NEE_param_sum_lower_MB*44.01*0.27;%g C-Ch4 m-2 yr-1
 
cum_mod_NEE_max_param_upper_MB=cumsum(upper_NEE_param_cum(52609:70128));%nmol m-2 yr-1
cum_mod_NEE_param_sum_upper_MB=cum_mod_NEE_max_param_upper_MB(end)*10^-6;%mol m-2 yr-1
cum_mod_NEE_param_sum_upper_MB=cum_mod_NEE_param_sum_upper_MB*44.01*0.27;%g C-Ch4 m-2 yr-1

(cum_mod_NEE_param_sum_upper_MB-cum_mod_NEE_param_sum_MB)*2
(cum_mod_NEE_param_sum_MB*100)/cum_obs_NEE_param_sum_MB

%% Annual sums for each param year MB n=4--Looking at Reco to GPP ratio
%Not re-naming cum variables so run and look at value but it won't be saved
gpp_ANNnight_MB_cum=gpp_ANNnight_MB*60*30;
er_ANNnight_MB_cum=er_ANNnight_MB*60*30;
GPP_min_param_cum=GPP_min_param*60*30;
Reco_min_param_cum=Reco_min_param*60*30;

%2011
cum_obs_GPP_param_MB=cumsum(gpp_ANNnight_MB_cum(1:17520));%umol m-2 yr-1
cum_obs_GPP_param_sum_MB=cum_obs_GPP_param_MB(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_GPP_param_sum_MB=cum_obs_GPP_param_sum_MB*44.01*0.27;%g C-Ch4 m-2 yr-1

cum_obs_ER_param_MB=cumsum(er_ANNnight_MB_cum(1:17520));%umol m-2 yr-1
cum_obs_ER_param_sum_MB=cum_obs_ER_param_MB(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_ER_param_sum_MB=cum_obs_ER_param_sum_MB*44.01*0.27;%g C-Ch4 m-2 yr-1

ratio_MB_2011=cum_obs_ER_param_sum_MB/cum_obs_GPP_param_sum_MB;

cum_mod_GPP_param_MB=cumsum(GPP_min_param_cum(1:17520));%umol m-2 yr-1
cum_mod_GPP_param_sum_MB=cum_mod_GPP_param_MB(end)*10^-6;%mol ch4 m-2 yr-1
cum_mod_GPP_param_sum_MB=cum_mod_GPP_param_sum_MB*44.01*0.27;%g C-Ch4 m-2 yr-1

cum_mod_ER_param_MB=cumsum(Reco_min_param_cum(1:17520));%umol m-2 yr-1
cum_mod_ER_param_sum_MB=cum_mod_ER_param_MB(end)*10^-6;%mol ch4 m-2 yr-1
cum_mod_ER_param_sum_MB=cum_mod_ER_param_sum_MB*44.01*0.27;%g C-Ch4 m-2 yr-1

ratio_MB_mod_2011=cum_mod_ER_param_sum_MB/cum_mod_GPP_param_sum_MB;

%2012
cum_obs_GPP_param_MB=cumsum(gpp_ANNnight_MB_cum(17521:35088));%umol m-2 yr-1
cum_obs_GPP_param_sum_MB=cum_obs_GPP_param_MB(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_GPP_param_sum_MB=cum_obs_GPP_param_sum_MB*44.01*0.27;%g C-Ch4 m-2 yr-1

cum_obs_ER_param_MB=cumsum(er_ANNnight_MB_cum(17521:35088));%umol m-2 yr-1
cum_obs_ER_param_sum_MB=cum_obs_ER_param_MB(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_ER_param_sum_MB=cum_obs_ER_param_sum_MB*44.01*0.27;%g C-Ch4 m-2 yr-1

ratio_MB_2012=cum_obs_ER_param_sum_MB/cum_obs_GPP_param_sum_MB;

cum_mod_GPP_param_MB=cumsum(GPP_min_param_cum(17521:35088));%umol m-2 yr-1
cum_mod_GPP_param_sum_MB=cum_mod_GPP_param_MB(end)*10^-6;%mol ch4 m-2 yr-1
cum_mod_GPP_param_sum_MB=cum_mod_GPP_param_sum_MB*44.01*0.27;%g C-Ch4 m-2 yr-1

cum_mod_ER_param_MB=cumsum(Reco_min_param_cum(17521:35088));%umol m-2 yr-1
cum_mod_ER_param_sum_MB=cum_mod_ER_param_MB(end)*10^-6;%mol ch4 m-2 yr-1
cum_mod_ER_param_sum_MB=cum_mod_ER_param_sum_MB*44.01*0.27;%g C-Ch4 m-2 yr-1

ratio_MB_mod_2012=cum_mod_ER_param_sum_MB/cum_mod_GPP_param_sum_MB;

%2013
cum_obs_GPP_param_MB=cumsum(gpp_ANNnight_MB_cum(35089:52608));%umol m-2 yr-1
cum_obs_GPP_param_sum_MB=cum_obs_GPP_param_MB(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_GPP_param_sum_MB=cum_obs_GPP_param_sum_MB*44.01*0.27;%g C-Ch4 m-2 yr-1

cum_obs_ER_param_MB=cumsum(er_ANNnight_MB_cum(35089:52608));%umol m-2 yr-1
cum_obs_ER_param_sum_MB=cum_obs_ER_param_MB(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_ER_param_sum_MB=cum_obs_ER_param_sum_MB*44.01*0.27;%g C-Ch4 m-2 yr-1

ratio_MB_2013=cum_obs_ER_param_sum_MB/cum_obs_GPP_param_sum_MB;

cum_mod_GPP_param_MB=cumsum(GPP_min_param_cum(35089:52608));%umol m-2 yr-1
cum_mod_GPP_param_sum_MB=cum_mod_GPP_param_MB(end)*10^-6;%mol ch4 m-2 yr-1
cum_mod_GPP_param_sum_MB=cum_mod_GPP_param_sum_MB*44.01*0.27;%g C-Ch4 m-2 yr-1

cum_mod_ER_param_MB=cumsum(Reco_min_param_cum(35089:52608));%umol m-2 yr-1
cum_mod_ER_param_sum_MB=cum_mod_ER_param_MB(end)*10^-6;%mol ch4 m-2 yr-1
cum_mod_ER_param_sum_MB=cum_mod_ER_param_sum_MB*44.01*0.27;%g C-Ch4 m-2 yr-1

ratio_MB_mod_2013=cum_mod_ER_param_sum_MB/cum_mod_GPP_param_sum_MB;

%2014
cum_obs_GPP_param_MB=cumsum(gpp_ANNnight_MB_cum(52609:70128));%umol m-2 yr-1
cum_obs_GPP_param_sum_MB=cum_obs_GPP_param_MB(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_GPP_param_sum_MB=cum_obs_GPP_param_sum_MB*44.01*0.27;%g C-Ch4 m-2 yr-1

cum_obs_ER_param_MB=cumsum(er_ANNnight_MB_cum(52609:70128));%umol m-2 yr-1
cum_obs_ER_param_sum_MB=cum_obs_ER_param_MB(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_ER_param_sum_MB=cum_obs_ER_param_sum_MB*44.01*0.27;%g C-Ch4 m-2 yr-1

ratio_MB_2014=cum_obs_ER_param_sum_MB/cum_obs_GPP_param_sum_MB;

cum_mod_GPP_param_MB=cumsum(GPP_min_param_cum(52609:70128));%umol m-2 yr-1
cum_mod_GPP_param_sum_MB=cum_mod_GPP_param_MB(end)*10^-6;%mol ch4 m-2 yr-1
cum_mod_GPP_param_sum_MB=cum_mod_GPP_param_sum_MB*44.01*0.27;%g C-Ch4 m-2 yr-1

cum_mod_ER_param_MB=cumsum(Reco_min_param_cum(52609:70128));%umol m-2 yr-1
cum_mod_ER_param_sum_MB=cum_mod_ER_param_MB(end)*10^-6;%mol ch4 m-2 yr-1
cum_mod_ER_param_sum_MB=cum_mod_ER_param_sum_MB*44.01*0.27;%g C-Ch4 m-2 yr-1

ratio_MB_mod_2014=cum_mod_ER_param_sum_MB/cum_mod_GPP_param_sum_MB;


%% Param WP --all years cum
cum_obs_NEE_param_WP=cumsum(wc_gf_valid_cum(70129:end));%umol m-2 yr-1
cum_obs_NEE_param_sum_WP=cum_obs_NEE_param_WP(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_NEE_param_sum_WP=cum_obs_NEE_param_sum_WP*44.01*0.27;%g C-Ch4 m-2 yr-1

cum_obs_NEE_param_lower_WP=cumsum(wc_gf_lower_param_cum(70129:end));%umol m-2 yr-1
cum_obs_NEE_param_sum_lower_WP=cum_obs_NEE_param_lower_WP(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_NEE_param_sum_lower_WP=cum_obs_NEE_param_sum_lower_WP*44.01*0.27;%g C-Ch4 m-2 yr-1

cum_obs_NEE_param_upper_WP=cumsum(wc_gf_upper_param_cum(70129:end));%umol m-2 yr-1
cum_obs_NEE_param_sum_upper_WP=cum_obs_NEE_param_upper_WP(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_NEE_param_sum_upper_WP=cum_obs_NEE_param_sum_upper_WP*44.01*0.27;%g C-Ch4 m-2 yr-1

(cum_obs_NEE_param_sum_upper_WP-cum_obs_NEE_param_sum_WP)/3

cum_mod_NEE_param_WP=cumsum(NEE_mod_min_param_cum(70129:end));%nmol m-2 yr-1
cum_mod_NEE_param_sum_WP=cum_mod_NEE_param_WP(end)*10^-6;%mol m-2 yr-1
cum_mod_NEE_param_sum_WP=cum_mod_NEE_param_sum_WP*44.01*0.27;%g C-Ch4 m-2 yr-1

cum_mod_NEE_param_lower_WP=cumsum(lower_NEE_param_cum(70129:end));%nmol m-2 yr-1
cum_mod_NEE_param_sum_lower_WP=cum_mod_NEE_param_lower_WP(end)*10^-6;%mol m-2 yr-1
cum_mod_NEE_param_sum_lower_WP=cum_mod_NEE_param_sum_lower_WP*44.01*0.27;%g C-Ch4 m-2 yr-1
 
cum_mod_NEE_max_param_upper_WP=cumsum(upper_NEE_param_cum(70129:end));%nmol m-2 yr-1
cum_mod_NEE_param_sum_upper_WP=cum_mod_NEE_max_param_upper_WP(end)*10^-6;%mol m-2 yr-1
cum_mod_NEE_param_sum_upper_WP=cum_mod_NEE_param_sum_upper_WP*44.01*0.27;%g C-Ch4 m-2 yr-1

cum_mod_NEE_param_sum_upper_WP-cum_mod_NEE_param_sum_WP
(cum_mod_NEE_param_sum_WP*100)/cum_obs_NEE_param_sum_WP

%% By year Param WP 
%2012--Not a complete year!
cum_obs_NEE_param_WP=cumsum(wc_gf_valid_cum(70129:78384));%umol m-2 yr-1
cum_obs_NEE_param_sum_WP=cum_obs_NEE_param_WP(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_NEE_param_sum_WP=cum_obs_NEE_param_sum_WP*44.01*0.27;%g C-Ch4 m-2 yr-1

cum_obs_NEE_param_lower_WP=cumsum(wc_gf_lower_param_cum(70129:78384));%umol m-2 yr-1
cum_obs_NEE_param_sum_lower_WP=cum_obs_NEE_param_lower_WP(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_NEE_param_sum_lower_WP=cum_obs_NEE_param_sum_lower_WP*44.01*0.27;%g C-Ch4 m-2 yr-1

cum_obs_NEE_param_upper_WP=cumsum(wc_gf_upper_param_cum(70129:78384));%umol m-2 yr-1
cum_obs_NEE_param_sum_upper_WP=cum_obs_NEE_param_upper_WP(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_NEE_param_sum_upper_WP=cum_obs_NEE_param_sum_upper_WP*44.01*0.27;%g C-Ch4 m-2 yr-1

(cum_obs_NEE_param_sum_upper_WP-cum_obs_NEE_param_sum_WP)/2

cum_mod_NEE_param_WP=cumsum(NEE_mod_min_param_cum(70129:78384));%nmol m-2 yr-1
cum_mod_NEE_param_sum_WP=cum_mod_NEE_param_WP(end)*10^-6;%mol m-2 yr-1
cum_mod_NEE_param_sum_WP=cum_mod_NEE_param_sum_WP*44.01*0.27;%g C-Ch4 m-2 yr-1

cum_mod_NEE_param_lower_WP=cumsum(lower_NEE_param_cum(70129:78384));%nmol m-2 yr-1
cum_mod_NEE_param_sum_lower_WP=cum_mod_NEE_param_lower_WP(end)*10^-6;%mol m-2 yr-1
cum_mod_NEE_param_sum_lower_WP=cum_mod_NEE_param_sum_lower_WP*44.01*0.27;%g C-Ch4 m-2 yr-1
 
cum_mod_NEE_max_param_upper_WP=cumsum(upper_NEE_param_cum(70129:78384));%nmol m-2 yr-1
cum_mod_NEE_param_sum_upper_WP=cum_mod_NEE_max_param_upper_WP(end)*10^-6;%mol m-2 yr-1
cum_mod_NEE_param_sum_upper_WP=cum_mod_NEE_param_sum_upper_WP*44.01*0.27;%g C-Ch4 m-2 yr-1

cum_mod_NEE_param_sum_upper_WP-cum_mod_NEE_param_sum_WP
(cum_mod_NEE_param_sum_WP*100)/cum_obs_NEE_param_sum_WP
%2013
cum_obs_NEE_param_WP=cumsum(wc_gf_valid_cum(78385:95904));%umol m-2 yr-1
cum_obs_NEE_param_sum_WP=cum_obs_NEE_param_WP(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_NEE_param_sum_WP=cum_obs_NEE_param_sum_WP*44.01*0.27;%g C-Ch4 m-2 yr-1

cum_obs_NEE_param_lower_WP=cumsum(wc_gf_lower_param_cum(78385:95904));%umol m-2 yr-1
cum_obs_NEE_param_sum_lower_WP=cum_obs_NEE_param_lower_WP(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_NEE_param_sum_lower_WP=cum_obs_NEE_param_sum_lower_WP*44.01*0.27;%g C-Ch4 m-2 yr-1

cum_obs_NEE_param_upper_WP=cumsum(wc_gf_upper_param_cum(78385:95904));%umol m-2 yr-1
cum_obs_NEE_param_sum_upper_WP=cum_obs_NEE_param_upper_WP(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_NEE_param_sum_upper_WP=cum_obs_NEE_param_sum_upper_WP*44.01*0.27;%g C-Ch4 m-2 yr-1

(cum_obs_NEE_param_sum_upper_WP-cum_obs_NEE_param_sum_WP)/2

cum_mod_NEE_param_WP=cumsum(NEE_mod_min_param_cum(78385:95904));%nmol m-2 yr-1
cum_mod_NEE_param_sum_WP=cum_mod_NEE_param_WP(end)*10^-6;%mol m-2 yr-1
cum_mod_NEE_param_sum_WP=cum_mod_NEE_param_sum_WP*44.01*0.27;%g C-Ch4 m-2 yr-1

cum_mod_NEE_param_lower_WP=cumsum(lower_NEE_param_cum(78385:95904));%nmol m-2 yr-1
cum_mod_NEE_param_sum_lower_WP=cum_mod_NEE_param_lower_WP(end)*10^-6;%mol m-2 yr-1
cum_mod_NEE_param_sum_lower_WP=cum_mod_NEE_param_sum_lower_WP*44.01*0.27;%g C-Ch4 m-2 yr-1
 
cum_mod_NEE_max_param_upper_WP=cumsum(upper_NEE_param_cum(78385:95904));%nmol m-2 yr-1
cum_mod_NEE_param_sum_upper_WP=cum_mod_NEE_max_param_upper_WP(end)*10^-6;%mol m-2 yr-1
cum_mod_NEE_param_sum_upper_WP=cum_mod_NEE_param_sum_upper_WP*44.01*0.27;%g C-Ch4 m-2 yr-1

cum_mod_NEE_param_sum_upper_WP-cum_mod_NEE_param_sum_WP
(cum_mod_NEE_param_sum_WP*100)/cum_obs_NEE_param_sum_WP

%2014
cum_obs_NEE_param_WP=cumsum(wc_gf_valid_cum(95905:end));%umol m-2 yr-1
cum_obs_NEE_param_sum_WP=cum_obs_NEE_param_WP(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_NEE_param_sum_WP=cum_obs_NEE_param_sum_WP*44.01*0.27;%g C-Ch4 m-2 yr-1

cum_obs_NEE_param_lower_WP=cumsum(wc_gf_lower_param_cum(95905:end));%umol m-2 yr-1
cum_obs_NEE_param_sum_lower_WP=cum_obs_NEE_param_lower_WP(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_NEE_param_sum_lower_WP=cum_obs_NEE_param_sum_lower_WP*44.01*0.27;%g C-Ch4 m-2 yr-1

cum_obs_NEE_param_upper_WP=cumsum(wc_gf_upper_param_cum(95905:end));%umol m-2 yr-1
cum_obs_NEE_param_sum_upper_WP=cum_obs_NEE_param_upper_WP(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_NEE_param_sum_upper_WP=cum_obs_NEE_param_sum_upper_WP*44.01*0.27;%g C-Ch4 m-2 yr-1

(cum_obs_NEE_param_sum_upper_WP-cum_obs_NEE_param_sum_WP)/2

cum_mod_NEE_param_WP=cumsum(NEE_mod_min_param_cum(95905:end));%nmol m-2 yr-1
cum_mod_NEE_param_sum_WP=cum_mod_NEE_param_WP(end)*10^-6;%mol m-2 yr-1
cum_mod_NEE_param_sum_WP=cum_mod_NEE_param_sum_WP*44.01*0.27;%g C-Ch4 m-2 yr-1

cum_mod_NEE_param_lower_WP=cumsum(lower_NEE_param_cum(95905:end));%nmol m-2 yr-1
cum_mod_NEE_param_sum_lower_WP=cum_mod_NEE_param_lower_WP(end)*10^-6;%mol m-2 yr-1
cum_mod_NEE_param_sum_lower_WP=cum_mod_NEE_param_sum_lower_WP*44.01*0.27;%g C-Ch4 m-2 yr-1
 
cum_mod_NEE_max_param_upper_WP=cumsum(upper_NEE_param_cum(95905:end));%nmol m-2 yr-1
cum_mod_NEE_param_sum_upper_WP=cum_mod_NEE_max_param_upper_WP(end)*10^-6;%mol m-2 yr-1
cum_mod_NEE_param_sum_upper_WP=cum_mod_NEE_param_sum_upper_WP*44.01*0.27;%g C-Ch4 m-2 yr-1

cum_mod_NEE_param_sum_upper_WP-cum_mod_NEE_param_sum_WP
(cum_mod_NEE_param_sum_WP*100)/cum_obs_NEE_param_sum_WP

%% Annual sums for each param year WP n=2--Looking at Reco to GPP ratio
%Not re-naming cum variables so run and look at value but it won't be saved
gpp_ANNnight_WP_cum=gpp_ANNnight_WP*60*30;
er_ANNnight_WP_cum=er_ANNnight_WP*60*30;
GPP_min_param_cum=GPP_min_param*60*30;
Reco_min_param_cum=Reco_min_param*60*30;

%2012-NOT A FULL YEAR!!
cum_obs_GPP_param_WP=cumsum(gpp_ANNnight_WP_cum(1:8256));%umol m-2 yr-1
cum_obs_GPP_param_sum_WP=cum_obs_GPP_param_WP(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_GPP_param_sum_WP=cum_obs_GPP_param_sum_WP*44.01*0.27;%g C-Ch4 m-2 yr-1

cum_obs_ER_param_WP=cumsum(er_ANNnight_WP_cum(1:8256));%umol m-2 yr-1
cum_obs_ER_param_sum_WP=cum_obs_ER_param_WP(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_ER_param_sum_WP=cum_obs_ER_param_sum_WP*44.01*0.27;%g C-Ch4 m-2 yr-1

ratio_WP_2012=cum_obs_ER_param_sum_WP/cum_obs_GPP_param_sum_WP;

cum_mod_GPP_param_WP=cumsum(GPP_min_param_cum(70129:78384));%umol m-2 yr-1
cum_mod_GPP_param_sum_WP=cum_mod_GPP_param_WP(end)*10^-6;%mol ch4 m-2 yr-1
cum_mod_GPP_param_sum_WP=cum_mod_GPP_param_sum_WP*44.01*0.27;%g C-Ch4 m-2 yr-1

cum_mod_ER_param_WP=cumsum(Reco_min_param_cum(70129:78384));%umol m-2 yr-1
cum_mod_ER_param_sum_WP=cum_mod_ER_param_WP(end)*10^-6;%mol ch4 m-2 yr-1
cum_mod_ER_param_sum_WP=cum_mod_ER_param_sum_WP*44.01*0.27;%g C-Ch4 m-2 yr-1

ratio_WP_mod_2012=cum_mod_ER_param_sum_WP/cum_mod_GPP_param_sum_WP;

%2013
cum_obs_GPP_param_WP=cumsum(gpp_ANNnight_WP_cum(8257:25776));%umol m-2 yr-1
cum_obs_GPP_param_sum_WP=cum_obs_GPP_param_WP(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_GPP_param_sum_WP=cum_obs_GPP_param_sum_WP*44.01*0.27;%g C-Ch4 m-2 yr-1

cum_obs_ER_param_WP=cumsum(er_ANNnight_WP_cum(8257:25776));%umol m-2 yr-1
cum_obs_ER_param_sum_WP=cum_obs_ER_param_WP(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_ER_param_sum_WP=cum_obs_ER_param_sum_WP*44.01*0.27;%g C-Ch4 m-2 yr-1

ratio_WP_2013=cum_obs_ER_param_sum_WP/cum_obs_GPP_param_sum_WP;

cum_mod_GPP_param_WP=cumsum(GPP_min_param_cum(78385:95904));%umol m-2 yr-1
cum_mod_GPP_param_sum_WP=cum_mod_GPP_param_WP(end)*10^-6;%mol ch4 m-2 yr-1
cum_mod_GPP_param_sum_WP=cum_mod_GPP_param_sum_WP*44.01*0.27;%g C-Ch4 m-2 yr-1

cum_mod_ER_param_WP=cumsum(Reco_min_param_cum(78385:95904));%umol m-2 yr-1
cum_mod_ER_param_sum_WP=cum_mod_ER_param_WP(end)*10^-6;%mol ch4 m-2 yr-1
cum_mod_ER_param_sum_WP=cum_mod_ER_param_sum_WP*44.01*0.27;%g C-Ch4 m-2 yr-1

ratio_WP_mod_2013=cum_mod_ER_param_sum_WP/cum_mod_GPP_param_sum_WP;

%2014
cum_obs_GPP_param_WP=cumsum(gpp_ANNnight_WP_cum(25777:43296));%umol m-2 yr-1
cum_obs_GPP_param_sum_WP=cum_obs_GPP_param_WP(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_GPP_param_sum_WP=cum_obs_GPP_param_sum_WP*44.01*0.27;%g C-Ch4 m-2 yr-1

cum_obs_ER_param_WP=cumsum(er_ANNnight_WP_cum(25777:43296));%umol m-2 yr-1
cum_obs_ER_param_sum_WP=cum_obs_ER_param_WP(end)*10^-6;%mol ch4 m-2 yr-1
cum_obs_ER_param_sum_WP=cum_obs_ER_param_sum_WP*44.01*0.27;%g C-Ch4 m-2 yr-1

ratio_WP_2014=cum_obs_ER_param_sum_WP/cum_obs_GPP_param_sum_WP;

cum_mod_GPP_param_WP=cumsum(GPP_min_param_cum(95905:end));%umol m-2 yr-1
cum_mod_GPP_param_sum_WP=cum_mod_GPP_param_WP(end)*10^-6;%mol ch4 m-2 yr-1
cum_mod_GPP_param_sum_WP=cum_mod_GPP_param_sum_WP*44.01*0.27;%g C-Ch4 m-2 yr-1

cum_mod_ER_param_WP=cumsum(Reco_min_param_cum(95905:end));%umol m-2 yr-1
cum_mod_ER_param_sum_WP=cum_mod_ER_param_WP(end)*10^-6;%mol ch4 m-2 yr-1
cum_mod_ER_param_sum_WP=cum_mod_ER_param_sum_WP*44.01*0.27;%g C-Ch4 m-2 yr-1

ratio_WP_mod_2014=cum_mod_ER_param_sum_WP/cum_mod_GPP_param_sum_WP;

%% Daily integrals
%First do param results
days=1:1:2363;

daily_NEE_0=NEE_mod_min_param*60*30;%first turn NMB from umol m-2 s-1 into umol m-2 30min-1
[daily_NEE_min_param]=daily_sum(DOY_param,daily_NEE_0,days);
daily_NEE_0=lower_NEE_param*60*30;%first turn NMB from umol m-2 s-1 into umol m-2 30min-1
[daily_NEE_min_param_lower]=daily_sum(DOY_param,daily_NEE_0',days);
daily_NEE_0=upper_NEE_param*60*30;%first turn NMB from umol m-2 s-1 into umol m-2 30min-1
[daily_NEE_min_param_upper]=daily_sum(DOY_param,daily_NEE_0',days);

daily_Reco_0=Reco_min_param*60*30;%first turn NMB from umol m-2 s-1 into umol m-2 30min-1
[daily_Reco_min_param]=daily_sum(DOY_param,daily_Reco_0',days);
daily_Reco_0=lower_Reco_param*60*30;%first turn NMB from umol m-2 s-1 into umol m-2 30min-1
[daily_Reco_min_param_lower]=daily_sum(DOY_param,daily_Reco_0,days);
daily_Reco_0=upper_Reco_param*60*30;%first turn NMB from umol m-2 s-1 into umol m-2 30min-1
[daily_Reco_min_param_upper]=daily_sum(DOY_param,daily_Reco_0,days);

daily_GPP_0=GPP_min_param*60*30;%first turn NMB from umol m-2 s-1 into umol m-2 30min-1
[daily_GPP_min_param]=daily_sum(DOY_param,daily_GPP_0,days);
daily_GPP_0=lower_GPP_param*60*30;%first turn NMB from umol m-2 s-1 into umol m-2 30min-1
[daily_GPP_min_param_lower]=daily_sum(DOY_param,daily_GPP_0,days);
daily_GPP_0=upper_GPP_param*60*30;%first turn NMB from umol m-2 s-1 into umol m-2 30min-1
[daily_GPP_min_param_upper]=daily_sum(DOY_param,daily_GPP_0,days);

[daily_S1_totalC_param]=daily_sum(DOY_param,S1_totalC_param',days);%g C m-3
[daily_S2_totalC_param]=daily_sum(DOY_param,S2_totalC_param',days);%g C m-3

daily_NEE_min_param=daily_NEE_min_param*10^-6*11.88;%g CO_2-C m^-^2 d^-^1
daily_NEE_min_param_lower=daily_NEE_min_param_lower*10^-6*11.88;%g CO_2-C m^-^2 d^-^1
daily_NEE_min_param_upper=daily_NEE_min_param_upper*10^-6*11.88;%g CO_2-C m^-^2 d^-^1
daily_Reco_min_param=daily_Reco_min_param*10^-6*11.88;%g CO_2-C m^-^2 d^-^1
daily_Reco_min_param_lower=daily_Reco_min_param_lower*10^-6*11.88;%g CO_2-C m^-^2 d^-^1
daily_Reco_min_param_upper=daily_Reco_min_param_upper*10^-6*11.88;%g CO_2-C m^-^2 d^-^1
daily_GPP_min_param=daily_GPP_min_param*10^-6*11.88;%g CO_2-C m^-^2 d^-^1
daily_GPP_min_param_lower=daily_GPP_min_param_lower*10^-6*11.88;%g CO_2-C m^-^2 d^-^1
daily_GPP_min_param_upper=daily_GPP_min_param_upper*10^-6*11.88;%g CO_2-C m^-^2 d^-^1

%now do obs param integrals
days=1:1:2363;

daily_NEE_0=wc_gf_param*60*30;%first turn NMB from umol m-2 s-1 into umol m-2 30min-1
[daily_NEE_obs_param]=daily_sum(DOY_param,daily_NEE_0,days);
daily_NEE_0=wc_gf_lower_param*60*30;%first turn NMB from umol m-2 s-1 into umol m-2 30min-1
[daily_NEE_obs_param_lower]=daily_sum(DOY_param,daily_NEE_0,days);
daily_NEE_0=wc_gf_upper_param*60*30;%first turn NMB from umol m-2 s-1 into umol m-2 30min-1
[daily_NEE_obs_param_upper]=daily_sum(DOY_param,daily_NEE_0,days);

%no uncertainty for partitioned obs fluxes
daily_Reco_0=er_ANNnight_param*60*30;%first turn NMB from umol m-2 s-1 into umol m-2 30min-1
[daily_Reco_obs_param]=daily_sum(DOY_param,daily_Reco_0,days);
daily_GPP_0=gpp_ANNnight_param*60*30;%first turn NMB from umol m-2 s-1 into umol m-2 30min-1
[daily_GPP_obs_param]=daily_sum(DOY_param,daily_GPP_0,days);

daily_NEE_obs_param=daily_NEE_obs_param*10^-6*11.88;%g CO_2-C m^-^2 d^-^1
daily_NEE_obs_param_lower=daily_NEE_obs_param_lower*10^-6*11.88;%g CO_2-C m^-^2 d^-^1
daily_NEE_obs_param_upper=daily_NEE_obs_param_upper*10^-6*11.88;%g CO_2-C m^-^2 d^-^1
daily_Reco_obs_param=daily_Reco_obs_param*10^-6*11.88;%g CO_2-C m^-^2 d^-^1
daily_GPP_obs_param=daily_GPP_obs_param*10^-6*11.88;%g CO_2-C m^-^2 d^-^1

%Now do validation results
days=1:1:730;
daily_NEE_0=NEE_mod_min*60*30;%first turn NMB from umol m-2 s-1 into umol m-2 30min-1
[daily_NEE_min_valid]=daily_sum(DOY_param(1:35040),daily_NEE_0',days);
daily_NEE_0=lower_NEE*60*30;%first turn NMB from umol m-2 s-1 into umol m-2 30min-1
[daily_NEE_min_valid_lower]=daily_sum(DOY_param(1:35040),daily_NEE_0,days);
daily_NEE_0=upper_NEE*60*30;%first turn NMB from umol m-2 s-1 into umol m-2 30min-1
[daily_NEE_min_valid_upper]=daily_sum(DOY_param(1:35040),daily_NEE_0,days);

daily_Reco_0=Reco_min*60*30;%first turn NMB from umol m-2 s-1 into umol m-2 30min-1
[daily_Reco_min_valid]=daily_sum(DOY_param(1:35040),daily_Reco_0',days);
daily_Reco_0=lower_Reco*60*30;%first turn NMB from umol m-2 s-1 into umol m-2 30min-1
[daily_Reco_min_valid_lower]=daily_sum(DOY_param(1:35040),daily_Reco_0',days);
daily_Reco_0=upper_Reco*60*30;%first turn NMB from umol m-2 s-1 into umol m-2 30min-1
[daily_Reco_min_valid_upper]=daily_sum(DOY_param(1:35040),daily_Reco_0',days);

daily_GPP_0=GPP_min_mod*60*30;%first turn NMB from umol m-2 s-1 into umol m-2 30min-1
[daily_GPP_min_valid]=daily_sum(DOY_param(1:35040),daily_GPP_0',days);
daily_GPP_0=lower_GPP_mod_min*60*30;%first turn NMB from umol m-2 s-1 into umol m-2 30min-1
[daily_GPP_min_valid_lower]=daily_sum(DOY_param(1:35040),daily_GPP_0',days);
daily_GPP_0=upper_GPP_mod_min*60*30;%first turn NMB from umol m-2 s-1 into umol m-2 30min-1
[daily_GPP_min_valid_upper]=daily_sum(DOY_param(1:35040),daily_GPP_0',days);

[daily_S1_totalC_valid]=daily_sum(DOY_param(1:35040),S1_totalC',days);%g C m-3
[daily_S2_totalC_valid]=daily_sum(DOY_param(1:35040),S2_totalC',days);%g C m-3

daily_NEE_min_valid=daily_NEE_min_valid*10^-6*11.88;%g CO_2-C m^-^2 d^-^1
daily_NEE_min_valid_lower=daily_NEE_min_valid_lower*10^-6*11.88;%g CO_2-C m^-^2 d^-^1
daily_NEE_min_valid_upper=daily_NEE_min_valid_upper*10^-6*11.88;%g CO_2-C m^-^2 d^-^1
daily_Reco_min_valid=daily_Reco_min_valid*10^-6*11.88;%g CO_2-C m^-^2 d^-^1
daily_Reco_min_valid_lower=daily_Reco_min_valid_lower*10^-6*11.88;%g CO_2-C m^-^2 d^-^1
daily_Reco_min_valid_upper=daily_Reco_min_valid_upper*10^-6*11.88;%g CO_2-C m^-^2 d^-^1
daily_GPP_min_valid=daily_GPP_min_valid*10^-6*11.88;%g CO_2-C m^-^2 d^-^1
daily_GPP_min_valid_lower=daily_GPP_min_valid_lower*10^-6*11.88;%g CO_2-C m^-^2 d^-^1
daily_GPP_min_valid_upper=daily_GPP_min_valid_upper*10^-6*11.88;%g CO_2-C m^-^2 d^-^1

%Validation obs results
days=1:1:730;
daily_NEE_0=wc_gf_valid*60*30;%first turn NMB from umol m-2 s-1 into umol m-2 30min-1
[daily_NEE_obs_valid]=daily_sum(DOY_param(1:35040),daily_NEE_0,days);
daily_NEE_0=wc_gf_lower_valid*60*30;%first turn NMB from umol m-2 s-1 into umol m-2 30min-1
[daily_NEE_obs_valid_lower]=daily_sum(DOY_param(1:35040),daily_NEE_0,days);
daily_NEE_0=wc_gf_upper_valid*60*30;%first turn NMB from umol m-2 s-1 into umol m-2 30min-1
[daily_NEE_obs_valid_upper]=daily_sum(DOY_param(1:35040),daily_NEE_0,days);
%no uncertainty for partitioned obs fluxes

daily_Reco_0=er_ANNnight_valid*60*30;%first turn NMB from umol m-2 s-1 into umol m-2 30min-1
[daily_Reco_obs_valid]=daily_sum(DOY_param(1:35040),daily_Reco_0,days);
daily_GPP_0=gpp_ANNnight_valid*60*30;%first turn NMB from umol m-2 s-1 into umol m-2 30min-1
[daily_GPP_obs_valid]=daily_sum(DOY_param(1:35040),daily_GPP_0,days);

daily_NEE_obs_valid=daily_NEE_obs_valid*10^-6*11.88;%g CO_2-C m^-^2 d^-^1
daily_NEE_obs_valid_lower=daily_NEE_obs_valid_lower*10^-6*11.88;%g CO_2-C m^-^2 d^-^1
daily_NEE_obs_valid_upper=daily_NEE_obs_valid_upper*10^-6*11.88;%g CO_2-C m^-^2 d^-^1
daily_Reco_obs_valid=daily_Reco_obs_valid*10^-6*11.88;%g CO_2-C m^-^2 d^-^1
daily_GPP_obs_valid=daily_GPP_obs_valid*10^-6*11.88;%g CO_2-C m^-^2 d^-^1

%EE data
days=1:1:365;

daily_NEE_0=NEE_mod_min_EE*60*30;%first turn NMB from umol m-2 s-1 into umol m-2 30min-1
[daily_NEE_min_EE]=daily_sum(DOY_EE_2015,daily_NEE_0,days);
daily_NEE_0=lower_NEE_EE*60*30;%first turn NMB from umol m-2 s-1 into umol m-2 30min-1
[daily_NEE_min_EE_lower]=daily_sum(DOY_EE_2015,daily_NEE_0,days);
daily_NEE_0=upper_NEE_EE*60*30;%first turn NMB from umol m-2 s-1 into umol m-2 30min-1
[daily_NEE_min_EE_upper]=daily_sum(DOY_EE_2015,daily_NEE_0,days);

daily_Reco_0=Reco_min_EE*60*30;%first turn NMB from umol m-2 s-1 into umol m-2 30min-1
[daily_Reco_min_EE]=daily_sum(DOY_EE_2015,daily_Reco_0',days);
daily_Reco_0=lower_Reco_EE*60*30;%first turn NMB from umol m-2 s-1 into umol m-2 30min-1
[daily_Reco_min_EE_lower]=daily_sum(DOY_EE_2015,daily_Reco_0',days);
daily_Reco_0=upper_Reco_EE*60*30;%first turn NMB from umol m-2 s-1 into umol m-2 30min-1
[daily_Reco_min_EE_upper]=daily_sum(DOY_EE_2015,daily_Reco_0',days);

daily_GPP_0=GPP_min_EE_2015*60*30;%first turn NMB from umol m-2 s-1 into umol m-2 30min-1
[daily_GPP_min_EE]=daily_sum(DOY_EE_2015,daily_GPP_0',days);
daily_GPP_0=lower_GPP_mod_min_EE*60*30;%first turn NMB from umol m-2 s-1 into umol m-2 30min-1
[daily_GPP_min_EE_lower]=daily_sum(DOY_EE_2015,daily_GPP_0',days);
daily_GPP_0=upper_GPP_mod_min_EE*60*30;%first turn NMB from umol m-2 s-1 into umol m-2 30min-1
[daily_GPP_min_EE_upper]=daily_sum(DOY_EE_2015,daily_GPP_0',days);

[daily_S1_totalC_EE]=daily_sum(DOY_EE_2015,S1_totalC_EE',days);%g C m-3
[daily_S2_totalC_EE]=daily_sum(DOY_EE_2015,S2_totalC_EE',days);%g C m-3

daily_NEE_min_EE=daily_NEE_min_EE*10^-6*11.88;%g CO_2-C m^-^2 d^-^1
daily_NEE_min_EE_lower=daily_NEE_min_EE_lower*10^-6*11.88;%g CO_2-C m^-^2 d^-^1
daily_NEE_min_EE_upper=daily_NEE_min_EE_upper*10^-6*11.88;%g CO_2-C m^-^2 d^-^1
daily_Reco_min_EE=daily_Reco_min_EE*10^-6*11.88;%g CO_2-C m^-^2 d^-^1
daily_Reco_min_EE_lower=daily_Reco_min_EE_lower*10^-6*11.88;%g CO_2-C m^-^2 d^-^1
daily_Reco_min_EE_upper=daily_Reco_min_EE_upper*10^-6*11.88;%g CO_2-C m^-^2 d^-^1
daily_GPP_min_EE=daily_GPP_min_EE*10^-6*11.88;%g CO_2-C m^-^2 d^-^1
daily_GPP_min_EE_lower=daily_GPP_min_EE_lower*10^-6*11.88;%g CO_2-C m^-^2 d^-^1
daily_GPP_min_EE_upper=daily_GPP_min_EE_upper*10^-6*11.88;%g CO_2-C m^-^2 d^-^1

%EE data obs
daily_NEE_0=wc_gf_EE_2015*60*30;%first turn NMB from umol m-2 s-1 into umol m-2 30min-1
[daily_NEE_obs_EE]=daily_sum(DOY_EE_2015,daily_NEE_0,days);
daily_NEE_0=wc_gf_EE_lower*60*30;%first turn NMB from umol m-2 s-1 into umol m-2 30min-1
[daily_NEE_obs_EE_lower]=daily_sum(DOY_EE_2015,daily_NEE_0,days);
daily_NEE_0=wc_gf_EE_upper*60*30;%first turn NMB from umol m-2 s-1 into umol m-2 30min-1
[daily_NEE_obs_EE_upper]=daily_sum(DOY_EE_2015,daily_NEE_0,days);

daily_Reco_0=er_ANNnight_EE*60*30;%first turn NMB from umol m-2 s-1 into umol m-2 30min-1
[daily_Reco_obs_EE]=daily_sum(DOY_EE_2015,daily_Reco_0,days);
daily_GPP_0=gpp_ANNnight_EE*60*30;%first turn NMB from umol m-2 s-1 into umol m-2 30min-1
[daily_GPP_obs_EE]=daily_sum(DOY_EE_2015,daily_GPP_0,days);

daily_NEE_obs_EE=daily_NEE_obs_EE*10^-6*11.88;%g CO_2-C m^-^2 d^-^1
daily_NEE_obs_EE_lower=daily_NEE_obs_EE_lower*10^-6*11.88;%g CO_2-C m^-^2 d^-^1
daily_NEE_obs_EE_upper=daily_NEE_obs_EE_upper*10^-6*11.88;%g CO_2-C m^-^2 d^-^1
daily_Reco_obs_EE=daily_Reco_obs_EE*10^-6*11.88;%g CO_2-C m^-^2 d^-^1
daily_GPP_obs_EE=daily_GPP_obs_EE*10^-6*11.88;%g CO_2-C m^-^2 d^-^1

days_EE=1:1:365;
days_valid=1:1:730;
days_param=1:1:2363;

figure
plot(days_EE,daily_NEE_obs_EE,'.',days_EE,daily_NEE_obs_EE_lower,'.',...
    days_EE,daily_NEE_obs_valid_upper,'.',days_EE,daily_NEE_min_EE,'.',...
    days_EE,daily_NEE_min_EE_lower,'.',days_EE,daily_NEE_min_EE_upper,'.')%,days,daily_NEE_mod_mean,'.')
title('EE 2015')
ylabel('NEE daily sum (g CO_2-C m^-^2 d^-^1)')
legend('obs','obs lower','obs upper','mod','mod lower','mod upper')

figure
plot(days_valid,daily_NEE_obs_valid,'.',days_valid,daily_NEE_obs_valid_lower,'.',...
    days_valid,daily_NEE_obs_valid_upper,'.',days_valid,daily_NEE_min_valid,'.',...
    days_valid,daily_NEE_min_valid_lower,'.',days_valid,daily_NEE_min_valid_upper,'.')%,days,daily_NEE_mod_mean,'.')
title('MB WP 2015')
ylabel('NEE daily sum (g CO_2-C m^-^2 d^-^1)')
legend('obs','obs lower','obs upper','mod','mod lower','mod upper')

figure
plot(days_param,daily_NEE_obs_param,'.',days_param,daily_NEE_obs_param_lower,'.',...
    days_param,daily_NEE_obs_param_upper,'.',days_param,daily_NEE_min_param,'.',...
    days_param,daily_NEE_min_param_lower,'.',days_param,daily_NEE_min_param_upper,'.')%,days,daily_NEE_mod_mean,'.')
title('MB WP PAram')
ylabel('NEE daily sum (g CO_2-C m^-^2 d^-^1)')
legend('obs','obs lower','obs upper','mod','mod lower','mod upper')


%% Statistics Updated 10/10/2016
%RMSE is a better test of model performance than r2
%NEE
RMSE_nee_min_daily_MB_param=nansum((daily_NEE_min_param(1:1461)-daily_NEE_obs_param(1:1461)).^2);
RMSE_nee_min_daily_MB_param=sqrt(RMSE_nee_min_daily_MB_param/1461);
RMSE_nee_min_daily_WP_param=nansum((daily_NEE_min_param(1462:end)-daily_NEE_obs_param(1462:end)).^2);
RMSE_nee_min_daily_WP_param=sqrt(RMSE_nee_min_daily_WP_param/902);
 
RMSE_nee_min_daily_MB_valid=nansum((daily_NEE_min_valid(1:365)-daily_NEE_obs_valid(1:365)).^2);
RMSE_nee_min_daily_MB_valid=sqrt(RMSE_nee_min_daily_MB_valid/365);
RMSE_nee_min_daily_WP_valid=nansum((daily_NEE_min_valid(366:end)-daily_NEE_obs_valid(366:end)).^2);
RMSE_nee_min_daily_WP_valid=sqrt(RMSE_nee_min_daily_WP_valid/365);
RMSE_nee_min_daily_EE_valid=nansum((daily_NEE_min_EE(1:end)-daily_NEE_obs_EE(1:end)).^2);
RMSE_nee_min_daily_EE_valid=sqrt(RMSE_nee_min_daily_EE_valid/365);

%Mean absolute error (MAE)
%NEE
MAE_nee_min_daily_MB_param=nansum((daily_NEE_min_param(1:1461)-daily_NEE_obs_param(1:1461)));
MAE_nee_min_daily_MB_param=(MAE_nee_min_daily_MB_param/1461);
MAE_nee_min_daily_WP_param=nansum((daily_NEE_min_param(1462:end)-daily_NEE_obs_param(1462:end)));
MAE_nee_min_daily_WP_param=(MAE_nee_min_daily_WP_param/902);
 
MAE_nee_min_daily_MB_valid=nansum((daily_NEE_min_valid(1:365)-daily_NEE_obs_valid(1:365)));
MAE_nee_min_daily_MB_valid=(MAE_nee_min_daily_MB_valid/365);
MAE_nee_min_daily_WP_valid=nansum((daily_NEE_min_valid(366:end)-daily_NEE_obs_valid(366:end)));
MAE_nee_min_daily_WP_valid=(MAE_nee_min_daily_WP_valid/365);
MAE_nee_min_daily_EE_valid=nansum((daily_NEE_min_EE(1:end)-daily_NEE_obs_EE(1:end)));
MAE_nee_min_daily_EE_valid=(MAE_nee_min_daily_EE_valid/365);

%REco
% MAE_reco_min_daily=nansum(daily_Reco_mod_min(539:end)'-daily_Reco_obs(539:end));
% MAE_reco_min_daily=(MAE_reco_min_daily/365);

%D-index sensitive to systematic model bias
%NEE
D_index_NEE_min_daily_top_MB_param=nansum((daily_NEE_min_param(1:1461)-daily_NEE_obs_param(1:1461)).^2);
O_avg_MB_param=nanmean(daily_NEE_obs_param(1:1461));
D_index_NEE_min_daily_bottom_MB_param=nansum(((daily_NEE_min_param(1:1461)-O_avg_MB_param)+(daily_NEE_obs_param(1:1461)-O_avg_MB_param)).^2);
D_index_NEE_min_daily_MB_param=1-(D_index_NEE_min_daily_top_MB_param/D_index_NEE_min_daily_bottom_MB_param);

D_index_NEE_min_daily_top_WP_param=nansum((daily_NEE_min_param(1462:end)-daily_NEE_obs_param(1462:end)).^2);
O_avg_WP_param=nanmean(daily_NEE_obs_param(1462:end));
D_index_NEE_min_daily_bottom_WP_param=nansum(((daily_NEE_min_param(1462:end)-O_avg_WP_param)+(daily_NEE_obs_param(1462:end)-O_avg_WP_param)).^2);
D_index_NEE_min_daily_WP_param=1-(D_index_NEE_min_daily_top_WP_param/D_index_NEE_min_daily_bottom_WP_param);

D_index_NEE_min_daily_top_MB_valid=nansum((daily_NEE_min_valid(1:365)-daily_NEE_obs_valid(1:365)).^2);
O_avg_MB_valid=nanmean(daily_NEE_obs_valid(1:365));
D_index_NEE_min_daily_bottom_MB_valid=nansum(((daily_NEE_min_valid(1:365)-O_avg_MB_valid)+(daily_NEE_obs_valid(1:365)-O_avg_MB_valid)).^2);
D_index_NEE_min_daily_MB_valid=1-(D_index_NEE_min_daily_top_MB_valid/D_index_NEE_min_daily_bottom_MB_valid);

D_index_NEE_min_daily_top_WP_valid=nansum((daily_NEE_min_valid(366:end)-daily_NEE_obs_valid(366:end)).^2);
O_avg_WP_valid=nanmean(daily_NEE_obs_valid(366:end));
D_index_NEE_min_daily_bottom_WP_valid=nansum(((daily_NEE_min_valid(366:end)-O_avg_WP_valid)+(daily_NEE_obs_valid(366:end)-O_avg_WP_valid)).^2);
D_index_NEE_min_daily_WP_valid=1-(D_index_NEE_min_daily_top_WP_valid/D_index_NEE_min_daily_bottom_WP_valid);

D_index_NEE_min_daily_top_EE_valid=nansum((daily_NEE_min_EE(1:365)-daily_NEE_obs_EE(1:365)).^2);
O_avg_EE_valid=nanmean(daily_NEE_obs_EE(1:365));
D_index_NEE_min_daily_bottom_EE_valid=nansum(((daily_NEE_min_EE(1:365)-O_avg_EE_valid)+(daily_NEE_obs_EE(1:365)-O_avg_EE_valid)).^2);
D_index_NEE_min_daily_EE_valid=1-(D_index_NEE_min_daily_top_EE_valid/D_index_NEE_min_daily_bottom_EE_valid);

%REco
% D_index_reco_min_daily_top=nansum((daily_Reco_mod_min(539:end)'-daily_Reco_obs(539:end)).^2);
% O_avg=nanmean(daily_Reco_obs(539:end));
% D_index_reco_min_daily_bottom=nansum(((daily_Reco_mod_min(539:end)'-O_avg)+(daily_Reco_obs(539:end)-O_avg)).^2);
% D_index_reco_min_daily=1-(D_index_reco_min_daily_top/D_index_reco_min_daily_bottom);
 
  

 %% plotting NEE param data for MB AND WP
days_plot_MB=daily_DOY_param(1:1461);
days_plot_WP=daily_DOY_param(1462:end);

startDate = datenum('01-01-2011');
endDate = datenum('12-31-2014');
xData1=linspace(startDate,endDate,1461);

startDate2 = datenum('07-12-2012');
endDate2 = datenum('12-31-2014');
xData2=linspace(startDate2,endDate2,902);

figure ('Units', 'pixels',...
    'Position', [100 100 500 375]);
subplot(2,1,1)
ciplot(daily_NEE_min_param_lower(1:1461),daily_NEE_min_param_upper(1:1461),xData1,'k')
hold on
obs = plot(xData1,daily_NEE_obs_param(1:1461),'.');
ylim([-12 10])
%set(gca, 'XTick', [startDate:150:endDate] );
datetick('x','mm/yyyy','keeplimits')
box off
hYLabel = ylabel({'CO_2 exchange','(g CO_2-C m^-^2 d^-^1)' });
 legend('model','obs')
t=text(xData1(20),10,'(a)');%;t=text(2,-7,'p2');%puts letter in upper L corner

subplot(2,1,2)
ciplot(daily_NEE_min_param_lower(1462:end),daily_NEE_min_param_upper(1462:end),xData2,'k')
hold on
obs = plot(xData2,daily_NEE_obs_param(1462:end),'.');
%set(gca, 'XTick', [735062:150:735964] );
%set(gca, 'XTick', [startDate2:150:endDate2] );
datetick('x','mm/yyyy','keeplimits')
box off
hXLabel = xlabel('Day of year'                     );
hYLabel = ylabel({'CO_2 exchange','(g CO_2-C m^-^2 d^-^1)'} );
ylim([-12 5])
t=text(xData2(5),10,'(d)');%;t=text(2,-7,'p2');%puts letter in upper L corner

set( gca                       , ...
    'FontName'   , 'Helvetica' );
set([hXLabel, hYLabel, text]  , ...
    'FontSize'   , 12         );
 
set(gcf, 'PaperPositionMode', 'auto');
print('PEPRMT_NEE_param_MB_WP','-dpng','-r600');

%% one to one's for param data MB and WP

X = ones(length(daily_NEE_min_param(1:1461)),1);
X = [X daily_NEE_min_param(1:1461)'];
[~,~,~,~,STATS] = regress(daily_NEE_obs_param(1:1461)',X);
rsq=STATS(1);

X = ones(length(daily_NEE_min_param(1462:end)),1);
X = [X daily_NEE_min_param(1462:end)'];
[~,~,~,~,STATS2] = regress(daily_NEE_obs_param(1462:end)',X);
rsq2=STATS2(1);


figure ('Units', 'pixels',...
    'Position', [100 100 500 375])
obs=plot(daily_NEE_obs_param(1:1461),daily_NEE_min_param(1:1461),'.');
hXLabel = xlabel('Model (g CO_2-C m^-^2 d^-^1)');
hYLabel = ylabel('Obs (g CO_2-C m^-^2 d^-^1)');
box off
xlim([-12,7])
ylim([-12,7])
set(obs                       , ...
  'LineWidth'       , 1           , ...
  'Marker'          , 'o'         , ...
  'MarkerSize'      , 3           , ...
  'MarkerEdgeColor' , [0.25 0.25 0.25]  , ...
  'MarkerFaceColor' , [0.25 0.25 0.25]  );
h=lsline;
p2 = polyfit(get(h,'xdata'),get(h,'ydata'),1);
theString = sprintf('y = %.2f x + %.2f', p2(1), p2(2));
place_x=min(daily_NEE_min_param(1:1461));
place_y=max(daily_NEE_obs_param(1:1461));
t=text(place_x,place_y,theString);%;t=text(2,-7,'p2');%puts letter in upper L corner
theString = sprintf('r^2 = %.2f', rsq);
t=text(1,4,theString);%puts letter in upper L corner
refline(1,0)
 set( gca                       , ...
    'FontName'   , 'Helvetica' );
set([hXLabel, hYLabel, text]  , ...
    'FontSize'   , 12         );
 
set(gcf, 'PaperPositionMode', 'auto');
print('NEE_PEPRMT_onetoone_param_MB','-dpng','-r600');


figure ('Units', 'pixels',...
    'Position', [100 100 500 375])
obs=plot(daily_NEE_obs_param(1462:end),daily_NEE_min_param(1462:end),'.');
hXLabel = xlabel('Model (g CO_2-C m^-^2 d^-^1)');
hYLabel = ylabel('Obs (g CO_2-C m^-^2 d^-^1)');
box off
xlim([-10,5])
ylim([-10,5])
set(obs                       , ...
  'LineWidth'       , 1           , ...
  'Marker'          , 'o'         , ...
  'MarkerSize'      , 3           , ...
  'MarkerEdgeColor' , [0.25 0.25 0.25]  , ...
  'MarkerFaceColor' , [0.25 0.25 0.25]  );
h=lsline;
p2 = polyfit(get(h,'xdata'),get(h,'ydata'),1);
theString = sprintf('y = %.2f x + %.2f', p2(1), p2(2));
place_x=min(daily_NEE_min_param(1462:end));
place_y=max(daily_NEE_obs_param(1462:end));
t=text(place_x,place_y,theString);%;t=text(2,-7,'p2');%puts letter in upper L corner
theString = sprintf('r^2 = %.2f', rsq2);
t=text(1,4,theString);%puts letter in upper L corner
refline(1,0)
 set( gca                       , ...
    'FontName'   , 'Helvetica' );
set([hXLabel, hYLabel, text]  , ...
    'FontSize'   , 12         );
 
set(gcf, 'PaperPositionMode', 'auto');
print('NEE_PEPRMT_onetoone_param_WP','-dpng','-r600');

%% Cumm plots for param data MB and WP
%working with daily integrals b/c units are already correct g C m-2 
cum_obs_NEE_param_MB_plot=cumsum(daily_NEE_obs_param(1:1461));
cum_obs_NEE_param_upper_MB_plot=cumsum(daily_NEE_obs_param_upper(1:1461));
cum_obs_NEE_param_lower_MB_plot=cumsum(daily_NEE_obs_param_lower(1:1461));

cum_obs_NEE_param_MB_plot=[cum_obs_NEE_param_MB_plot(365) cum_obs_NEE_param_MB_plot(731) cum_obs_NEE_param_MB_plot(1096) cum_obs_NEE_param_MB_plot(1461)];
cum_obs_NEE_param_upper_MB_plot=[cum_obs_NEE_param_upper_MB_plot(365) cum_obs_NEE_param_upper_MB_plot(731) cum_obs_NEE_param_upper_MB_plot(1096) cum_obs_NEE_param_upper_MB_plot(1461)];
cum_obs_NEE_param_lower_MB_plot=[cum_obs_NEE_param_lower_MB_plot(365) cum_obs_NEE_param_lower_MB_plot(731) cum_obs_NEE_param_lower_MB_plot(1096) cum_obs_NEE_param_lower_MB_plot(1461)];

startDate = datenum('01-01-2011');
endDate = datenum('12-31-2014');
xData1=linspace(startDate,endDate,1461);
date1_plot=[xData1(365) xData1(731) xData1(1096) xData1(1461)];

cum_obs_NEE_param_MB_plot_error=cum_obs_NEE_param_upper_MB_plot-cum_obs_NEE_param_MB_plot;
cum_obs_NEE_param_MB_plot_error=cum_obs_NEE_param_MB_plot_error/4;

cum_min_NEE_param_MB_plot=cumsum(daily_NEE_min_param(1:1461));
cum_min_NEE_param_upper_MB_plot=cumsum(daily_NEE_min_param_upper(1:1461));
cum_min_NEE_param_lower_MB_plot=cumsum(daily_NEE_min_param_lower(1:1461));

figure
ciplot(cum_min_NEE_param_upper_MB_plot,cum_min_NEE_param_lower_MB_plot,xData1)
datetick('x','mm/yyyy')
hold on
errorbar(date1_plot,cum_obs_NEE_param_MB_plot,cum_obs_NEE_param_MB_plot_error)
datetick('x','mm/yyyy')
hold on
ylabel({'Cummulative CO_2', '(g C-CO_2 m^-^2)'})
 box off

set(gcf, 'PaperPositionMode', 'auto');
print('NEE_PEPRMT_cumm_param_MB','-dpng','-r600');

cum_obs_NEE_param_WP_plot=cumsum(daily_NEE_obs_param(1462:end));
cum_obs_NEE_param_upper_WP_plot=cumsum(daily_NEE_obs_param_upper(1462:end));
cum_obs_NEE_param_lower_WP_plot=cumsum(daily_NEE_obs_param_lower(1462:end));

cum_obs_NEE_param_WP_plot=[cum_obs_NEE_param_WP_plot(172) cum_obs_NEE_param_WP_plot(537) cum_obs_NEE_param_WP_plot(902) ];
cum_obs_NEE_param_upper_WP_plot=[cum_obs_NEE_param_upper_WP_plot(172) cum_obs_NEE_param_upper_WP_plot(537) cum_obs_NEE_param_upper_WP_plot(902)];
cum_obs_NEE_param_lower_WP_plot=[cum_obs_NEE_param_lower_WP_plot(172) cum_obs_NEE_param_lower_WP_plot(537) cum_obs_NEE_param_lower_WP_plot(902)];

startDate2 = datenum('07-12-2012');
endDate2 = datenum('12-31-2014');
xData2=linspace(startDate2,endDate2,902);
date2_plot=[xData2(172) xData2(537) xData2(902)];

cum_obs_NEE_param_WP_plot_error=cum_obs_NEE_param_upper_WP_plot-cum_obs_NEE_param_WP_plot;
cum_obs_NEE_param_WP_plot_error=cum_obs_NEE_param_WP_plot_error/2;

cum_min_NEE_param_WP_plot=cumsum(daily_NEE_min_param(1462:end));
cum_min_NEE_param_upper_WP_plot=cumsum(daily_NEE_min_param_upper(1462:end));
cum_min_NEE_param_lower_WP_plot=cumsum(daily_NEE_min_param_lower(1462:end));

figure
ciplot(cum_min_NEE_param_upper_WP_plot,cum_min_NEE_param_lower_WP_plot,xData2)
hold on
errorbar(date2_plot,cum_obs_NEE_param_WP_plot,cum_obs_NEE_param_WP_plot_error)
datetick('x','mm/yyyy','keeplimits')
hold on
ylabel({'Cummulative CO_2', '(g C-CO_2 m^-^2)'})
 box off

set(gcf, 'PaperPositionMode', 'auto');
print('NEE_PEPRMT_cumm_param_WP','-dpng','-r600');

 %% plotting NEE Validation data for MB AND WP and EE
 %9 panel figure
days_plot_MB_valid=daily_DOY_valid(1:365);
days_plot_WP_valid=daily_DOY_valid(366:end);

startDate = datenum('01-01-2015');
endDate = datenum('12-31-2015');
xData1=linspace(startDate,endDate,365);
%for 1:1's
X = ones(length(daily_NEE_min_valid(1:365)),1);
X = [X daily_NEE_min_valid(1:365)'];
[~,~,~,~,STATS] = regress(daily_NEE_obs_valid(1:365)',X);
rsq=STATS(1);

X = ones(length(daily_NEE_min_valid(366:end)),1);
X = [X daily_NEE_min_valid(366:end)'];
[~,~,~,~,STATS2] = regress(daily_NEE_obs_valid(366:end)',X);
rsq2=STATS2(1);

X = ones(length(daily_NEE_min_EE),1);
X = [X daily_NEE_min_EE'];
[~,~,~,~,STATS3] = regress(daily_NEE_obs_EE',X);
rsq3=STATS3(1);

%For cum plots: working with daily integrals b/c units are already correct g C m-2 
%MB
cum_obs_NEE_valid_MB_plot=cumsum(daily_NEE_obs_valid(1:365));
cum_obs_NEE_valid_upper_MB_plot=cumsum(daily_NEE_obs_valid_upper(1:365));
cum_obs_NEE_valid_lower_MB_plot=cumsum(daily_NEE_obs_valid_lower(1:365));

cum_obs_NEE_valid_MB_plot=cum_obs_NEE_valid_MB_plot(365);
cum_obs_NEE_valid_upper_MB_plot=cum_obs_NEE_valid_upper_MB_plot(365);
cum_obs_NEE_valid_lower_MB_plot=cum_obs_NEE_valid_lower_MB_plot(365);

cum_obs_NEE_valid_MB_plot_error=cum_obs_NEE_valid_upper_MB_plot-cum_obs_NEE_valid_MB_plot;
cum_obs_NEE_valid_MB_plot_error=cum_obs_NEE_valid_MB_plot_error/4;

cum_min_NEE_valid_MB_plot=cumsum(daily_NEE_min_valid(1:365));
cum_min_NEE_valid_upper_MB_plot=cumsum(daily_NEE_min_valid_upper(1:365));
cum_min_NEE_valid_lower_MB_plot=cumsum(daily_NEE_min_valid_lower(1:365));
%WP
cum_obs_NEE_valid_WP_plot=cumsum(daily_NEE_obs_valid(366:end));
cum_obs_NEE_valid_upper_WP_plot=cumsum(daily_NEE_obs_valid_upper(366:end));
cum_obs_NEE_valid_lower_WP_plot=cumsum(daily_NEE_obs_valid_lower(366:end));

cum_obs_NEE_valid_WP_plot=cum_obs_NEE_valid_WP_plot(365);
cum_obs_NEE_valid_upper_WP_plot=cum_obs_NEE_valid_upper_WP_plot(365);
cum_obs_NEE_valid_lower_WP_plot=cum_obs_NEE_valid_lower_WP_plot(365);

cum_obs_NEE_valid_WP_plot_error=cum_obs_NEE_valid_upper_WP_plot-cum_obs_NEE_valid_WP_plot;
cum_obs_NEE_valid_WP_plot_error=cum_obs_NEE_valid_WP_plot_error/2;

cum_min_NEE_valid_WP_plot=cumsum(daily_NEE_min_valid(366:end));
cum_min_NEE_valid_upper_WP_plot=cumsum(daily_NEE_min_valid_upper(366:end));
cum_min_NEE_valid_lower_WP_plot=cumsum(daily_NEE_min_valid_lower(366:end));
%EE
cum_obs_NEE_valid_EE_plot=cumsum(daily_NEE_obs_EE);
cum_obs_NEE_valid_upper_EE_plot=cumsum(daily_NEE_obs_EE_upper);
cum_obs_NEE_valid_lower_EE_plot=cumsum(daily_NEE_obs_EE_lower);

cum_obs_NEE_valid_EE_plot=cum_obs_NEE_valid_EE_plot(365);
cum_obs_NEE_valid_upper_EE_plot=cum_obs_NEE_valid_upper_EE_plot(365);
cum_obs_NEE_valid_lower_EE_plot=cum_obs_NEE_valid_lower_EE_plot(365);

cum_obs_NEE_valid_EE_plot_error=cum_obs_NEE_valid_upper_EE_plot-cum_obs_NEE_valid_EE_plot;
cum_obs_NEE_valid_EE_plot_error=cum_obs_NEE_valid_EE_plot_error/2;

cum_min_NEE_valid_EE_plot=cumsum(daily_NEE_min_EE);
cum_min_NEE_valid_upper_EE_plot=cumsum(daily_NEE_min_EE_upper);
cum_min_NEE_valid_lower_EE_plot=cumsum(daily_NEE_min_EE_lower);
%for cum plots
date1_plot=xData1(365);

%% plot validation figure
figure ('Units', 'pixels',...
    'Position', [100 100 500 375]);
subplot(3,3,1)
ciplot(daily_NEE_min_valid_lower(1:365),daily_NEE_min_valid_upper(1:365),xData1,'k')
hold on
obs = plot(xData1,daily_NEE_obs_valid(1:365),'.');
ylim([-12 10])
datetick('x','mm/yyyy','keeplimits')
box off
hYLabel = ylabel({'CO_2 exchange','(g CO_2-C m^-^2 d^-^1)' });
 legend('model','obs')

subplot(3,3,4)
ciplot(daily_NEE_min_valid_lower(366:end),daily_NEE_min_valid_upper(366:end),xData1,'k')
hold on
obs = plot(xData1,daily_NEE_obs_valid(366:end),'.');
datetick('x','mm/yyyy','keeplimits')
box off
hYLabel = ylabel({'CO_2 exchange','(g CO_2-C m^-^2 d^-^1)'} );
ylim([-12 5])

subplot(3,3,7)
ciplot(daily_NEE_min_EE_lower,daily_NEE_min_EE_upper,xData1,'k')
hold on
obs = plot(xData1,daily_NEE_obs_EE,'.');
datetick('x','mm/yyyy','keeplimits')
box off
hYLabel = ylabel({'CO_2 exchange','(g CO_2-C m^-^2 d^-^1)'} );
ylim([-12 5])

subplot(3,3,2)
obs=plot(daily_NEE_obs_valid(1:365),daily_NEE_min_valid(1:365),'.');
hXLabel = xlabel('Model (g CO_2-C m^-^2 d^-^1)');
hYLabel = ylabel('Obs (g CO_2-C m^-^2 d^-^1)');
box off
xlim([-12,7])
ylim([-12,7])
set(obs                       , ...
  'LineWidth'       , 1           , ...
  'Marker'          , 'o'         , ...
  'MarkerSize'      , 3           , ...
  'MarkerEdgeColor' , [0.25 0.25 0.25]  , ...
  'MarkerFaceColor' , [0.25 0.25 0.25]  );
h=lsline;
p2 = polyfit(get(h,'xdata'),get(h,'ydata'),1);
theString = sprintf('y = %.2f x + %.2f', p2(1), p2(2));
place_x=min(daily_NEE_min_valid(1:365));
place_y=max(daily_NEE_obs_valid(1:365));
t=text(place_x,place_y,theString);%;t=text(2,-7,'p2');%puts letter in upper L corner
theString = sprintf('r^2 = %.2f', rsq);
t=text(1,4,theString);%puts letter in upper L corner
refline(1,0)

subplot(3,3,5)
obs=plot(daily_NEE_obs_valid(366:end),daily_NEE_min_valid(366:end),'.');
hXLabel = xlabel('Model (g CO_2-C m^-^2 d^-^1)');
hYLabel = ylabel('Obs (g CO_2-C m^-^2 d^-^1)');
box off
xlim([-10,5])
ylim([-10,5])
set(obs                       , ...
  'LineWidth'       , 1           , ...
  'Marker'          , 'o'         , ...
  'MarkerSize'      , 3           , ...
  'MarkerEdgeColor' , [0.25 0.25 0.25]  , ...
  'MarkerFaceColor' , [0.25 0.25 0.25]  );
h=lsline;
p2 = polyfit(get(h,'xdata'),get(h,'ydata'),1);
theString = sprintf('y = %.2f x + %.2f', p2(1), p2(2));
place_x=min(daily_NEE_min_valid(366:end));
place_y=max(daily_NEE_obs_valid(366:end));
t=text(place_x,place_y,theString);%;t=text(2,-7,'p2');%puts letter in upper L corner
theString = sprintf('r^2 = %.2f', rsq2);
t=text(1,4,theString);%puts letter in upper L corner
refline(1,0)

subplot(3,3,8)
obs=plot(daily_NEE_obs_EE,daily_NEE_min_EE,'.');
hXLabel = xlabel('Model (g CO_2-C m^-^2 d^-^1)');
hYLabel = ylabel('Obs (g CO_2-C m^-^2 d^-^1)');
box off
xlim([-10,5])
ylim([-10,5])
set(obs                       , ...
  'LineWidth'       , 1           , ...
  'Marker'          , 'o'         , ...
  'MarkerSize'      , 3           , ...
  'MarkerEdgeColor' , [0.25 0.25 0.25]  , ...
  'MarkerFaceColor' , [0.25 0.25 0.25]  );
h=lsline;
p2 = polyfit(get(h,'xdata'),get(h,'ydata'),1);
theString = sprintf('y = %.2f x + %.2f', p2(1), p2(2));
place_x=min(daily_NEE_min_EE);
place_y=max(daily_NEE_obs_EE);
t=text(place_x,place_y,theString);%;t=text(2,-7,'p2');%puts letter in upper L corner
theString = sprintf('r^2 = %.2f', rsq3);
t=text(1,4,theString);%puts letter in upper L corner
refline(1,0)

subplot(3,3,3)
ciplot(cum_min_NEE_valid_upper_MB_plot,cum_min_NEE_valid_lower_MB_plot,xData1)
datetick('x','mm/yyyy')
hold on
errorbar(date1_plot,cum_obs_NEE_valid_MB_plot,cum_obs_NEE_valid_MB_plot_error)
datetick('x','mm/yyyy')
hold on
ylabel({'Cummulative CO_2', '(g C-CO_2 m^-^2)'})
 box off

subplot(3,3,6)
ciplot(cum_min_NEE_valid_upper_WP_plot,cum_min_NEE_valid_lower_WP_plot,xData1)
datetick('x','mm/yyyy')
hold on
errorbar(date1_plot,cum_obs_NEE_valid_WP_plot,cum_obs_NEE_valid_WP_plot_error)
datetick('x','mm/yyyy')
hold on
ylabel({'Cummulative CO_2', '(g C-CO_2 m^-^2)'})
 box off
 
subplot(3,3,9)
ciplot(cum_min_NEE_valid_upper_EE_plot,cum_min_NEE_valid_lower_EE_plot,xData1)
datetick('x','mm/yyyy')
hold on
errorbar(date1_plot,cum_obs_NEE_valid_EE_plot,cum_obs_NEE_valid_EE_plot_error)
datetick('x','mm/yyyy')
hold on
ylabel({'Cummulative CO_2', '(g C-CO_2 m^-^2)'})
 box off


set( gca                       , ...
    'FontName'   , 'Helvetica' );
set([hXLabel, hYLabel, text]  , ...
    'FontSize'   , 12         );
 
set(gcf, 'PaperPositionMode', 'auto');
print('NEE_validation_MB_WP_EE','-dpng','-r600');

%*******************************************************************
%% plotting Reco and GPP param and valid data for MB AND WP AND EE 
%UPDATED ON 2016.10.10
days_plot_MB_partition=vertcat(daily_DOY_param(1:1461), daily_DOY_EE+1461);
days_plot_WP_partition=vertcat(daily_DOY_param(1462:end),daily_DOY_EE+2363);
days_plot_EE_partition=daily_DOY_EE;

daily_plot_MB_Reco_lower=vertcat(daily_Reco_min_param_lower(1:1461)', daily_Reco_min_valid_lower(1:365)');
daily_plot_WP_Reco_lower=vertcat(daily_Reco_min_param_lower(1462:end)', daily_Reco_min_valid_lower(366:end)');
daily_plot_MB_GPP_lower=vertcat(daily_GPP_min_param_lower(1:1461)', daily_GPP_min_valid_lower(1:365)');
daily_plot_WP_GPP_lower=vertcat(daily_GPP_min_param_lower(1462:end)', daily_GPP_min_valid_lower(366:end)');
daily_plot_MB_Reco_upper=vertcat(daily_Reco_min_param_upper(1:1461)', daily_Reco_min_valid_upper(1:365)');
daily_plot_WP_Reco_upper=vertcat(daily_Reco_min_param_upper(1462:end)', daily_Reco_min_valid_upper(366:end)');
daily_plot_MB_GPP_upper=vertcat(daily_GPP_min_param_upper(1:1461)', daily_GPP_min_valid_upper(1:365)');
daily_plot_WP_GPP_upper=vertcat(daily_GPP_min_param_upper(1462:end)', daily_GPP_min_valid_upper(366:end)');

daily_plot_MB_Reco_obs=vertcat(daily_Reco_obs_param(1:1461)', daily_Reco_obs_valid(1:365)');
daily_plot_WP_Reco_obs=vertcat(daily_Reco_obs_param(1462:end)', daily_Reco_obs_valid(366:end)');
daily_plot_MB_GPP_obs=vertcat(daily_GPP_obs_param(1:1461)', daily_GPP_obs_valid(1:365)');
daily_plot_WP_GPP_obs=vertcat(daily_GPP_obs_param(1462:end)', daily_GPP_obs_valid(366:end)');

startDate = datenum('01-01-2011');
endDate = datenum('12-31-2015');
xData1=linspace(startDate,endDate,1826);

startDate2 = datenum('07-12-2012');
endDate2 = datenum('12-31-2015');
xData2=linspace(startDate2,endDate2,1267);

startDate3 = datenum('01-01-2015');
endDate3 = datenum('12-31-2015');
xData3=linspace(startDate3,endDate3,365);

%for 1:1's
%MB
X = ones(length(daily_Reco_min_param(1:1461)),1);
X = [X daily_Reco_min_param(1:1461)'];
[~,~,~,~,STATS1] = regress(daily_Reco_obs_param(1:1461)',X);
rsq1=STATS1(1);
X = ones(length(daily_GPP_min_param(1:1461)),1);
X = [X daily_GPP_min_param(1:1461)'];
[~,~,~,~,STATS2] = regress(daily_GPP_obs_param(1:1461)',X);
rsq2=STATS2(1);
X = ones(length(daily_Reco_min_valid(1:365)),1);
X = [X daily_Reco_min_valid(1:365)'];
[~,~,~,~,STATS3] = regress(daily_Reco_obs_valid(1:365)',X);
rsq3=STATS3(1);
X = ones(length(daily_GPP_min_valid(1:365)),1);
X = [X daily_GPP_min_valid(1:365)'];
[~,~,~,~,STATS4] = regress(daily_GPP_obs_valid(1:365)',X);
rsq4=STATS4(1);

%WP
X = ones(length(daily_Reco_min_param(1462:end)),1);
X = [X daily_Reco_min_param(1462:end)'];
[~,~,~,~,STATS5] = regress(daily_Reco_obs_param(1462:end)',X);
rsq5=STATS5(1);
X = ones(length(daily_GPP_min_param(1462:end)),1);
X = [X daily_GPP_min_param(1462:end)'];
[~,~,~,~,STATS6] = regress(daily_GPP_obs_param(1462:end)',X);
rsq6=STATS6(1);
X = ones(length(daily_Reco_min_valid(366:end)),1);
X = [X daily_Reco_min_valid(366:end)'];
[~,~,~,~,STATS7] = regress(daily_Reco_obs_valid(366:end)',X);
rsq7=STATS7(1);
X = ones(length(daily_GPP_min_valid(366:end)),1);
X = [X daily_GPP_min_valid(366:end)'];
[~,~,~,~,STATS8] = regress(daily_GPP_obs_valid(366:end)',X);
rsq8=STATS8(1);

%EE
X = ones(length(daily_Reco_min_EE(1:365)),1);
X = [X daily_Reco_min_EE(1:365)'];
[~,~,~,~,STATS9] = regress(daily_Reco_obs_EE(1:365)',X);
rsq9=STATS9(1);
X = ones(length(daily_GPP_min_EE(1:365)),1);
X = [X daily_GPP_min_EE(1:365)'];
[~,~,~,~,STATS10] = regress(daily_GPP_obs_EE(1:365)',X);
rsq10=STATS10(1);


%% plot Reco GPP figure all sites all years
figure ('Units', 'pixels',...
    'Position', [100 100 500 375]);
%MB time series GPP and Reco
subplot(3,1,1)
ciplot(daily_plot_MB_Reco_lower,daily_plot_MB_Reco_upper,xData1,'k')
hold on
ciplot(daily_plot_MB_GPP_lower,daily_plot_MB_GPP_upper,xData1,'k')
hold on
obs = plot(xData1,daily_plot_MB_Reco_obs,'.r');
hold on
obs = plot(xData1,daily_plot_MB_GPP_obs,'.g');
ylim([-20 15])
datetick('x','mm/yyyy')
box off
hYLabel = ylabel({'CO_2 exchange','(g CO_2-C m^-^2 d^-^1)' });
% legend('model R_e_c_o','model GPP','partitioned Reco','partitioned GPP')

 %WP time series GPP and Reco
 subplot(3,1,2)
ciplot(daily_plot_WP_Reco_lower,daily_plot_WP_Reco_upper,xData2,'k')
hold on
ciplot(daily_plot_WP_GPP_lower,daily_plot_WP_GPP_upper,xData2,'k')
hold on
obs = plot(xData2,daily_plot_WP_Reco_obs,'.r');
hold on
obs = plot(xData2,daily_plot_WP_GPP_obs,'.g');
ylim([-17 10])
datetick('x','mm/yyyy','keeplimits')
box off
hYLabel = ylabel({'CO_2 exchange','(g CO_2-C m^-^2 d^-^1)' });
% legend('model R_e_c_o','model GPP','partitioned Reco','partitioned GPP')

 %EE time series GPP and Reco
 subplot(3,1,3)
ciplot(daily_Reco_min_EE_lower,daily_Reco_min_EE_upper,xData3,'k')
hold on
ciplot(daily_GPP_min_EE_lower,daily_GPP_min_EE_upper,xData3,'k')
hold on
obs = plot(xData3,daily_GPP_obs_EE,'.g');
hold on
obs = plot(xData3,daily_Reco_obs_EE,'.r');
ylim([-17 10])
datetick('x','mm/yyyy','keeplimits')
box off
hYLabel = ylabel({'CO_2 exchange','(g CO_2-C m^-^2 d^-^1)' });
 legend('R_e_c_o _m_o_d_e_l','GPP_m_o_d_e_l','R_e_c_o _p_a_r_t_i_t_i_o_n_e_d','GPP_p_a_r_t_i_t_i_o_n_e_d')
set( gca                       , ...
    'FontName'   , 'Helvetica' );
set([hXLabel, hYLabel, text]  , ...
    'FontSize'   , 12         );
 
set(gcf, 'PaperPositionMode', 'auto');
print('Reco_GPP_all_data_MB_WP_EE','-dpng','-r600');

%% one to one's added in via powerpoint file
%MB 1:1 Reco
p1 = polyfit(daily_Reco_min_param(1:1461),daily_Reco_obs_param(1:1461),1);
p2 = polyfit(daily_Reco_min_valid(1:365),daily_Reco_obs_valid(1:365),1);
p3 = polyfit(daily_GPP_min_param(1:1461),daily_GPP_obs_param(1:1461),1);
p4 = polyfit(daily_GPP_min_valid(1:365),daily_GPP_obs_valid(1:365),1);
%WP
p5 = polyfit(daily_Reco_min_param(1462:end),daily_Reco_obs_param(1462:end),1);
p6 = polyfit(daily_Reco_min_valid(366:end),daily_Reco_obs_valid(366:end),1);
p7 = polyfit(daily_GPP_min_param(1462:end),daily_GPP_obs_param(1462:end),1);
p8 = polyfit(daily_GPP_min_valid(366:end),daily_GPP_obs_valid(366:end),1);
%EE
p9 = polyfit(daily_Reco_min_EE,daily_Reco_obs_EE,1);
p10 = polyfit(daily_GPP_min_EE,daily_GPP_obs_EE,1);

figure
plot(daily_Reco_min_param(1:1461),daily_Reco_obs_param(1:1461),'.r');
h=lsline;
% theString = sprintf('y = %.2f x + %.2f', p1(1), p1(2));
% place_x=min(daily_Reco_min_param(1:1461));
% place_y=max(daily_Reco_obs_param(1:1461));
% t=text(place_x,place_y,theString);%;t=text(2,-7,'p2');%puts letter in upper L corner
% theString = sprintf('r^2 = %.2f', rsq1);
% t=text(1,4,theString);%puts letter in upper L corner
xlim([0,14])
ylim([0,14])
hold on
plot(daily_Reco_min_valid(1:365),daily_Reco_obs_valid(1:365),'.k');
m=lsline;
% theString = sprintf('y = %.2f x + %.2f', p2(1), p2(2));
% place_x=min(daily_Reco_min_valid(1:365));
% place_y=max(daily_Reco_obs_valid(1:365));
% t=text(place_x,place_y,theString);%;t=text(2,-7,'p2');%puts letter in upper L corner
% theString = sprintf('r^2 = %.2f', rsq3);
% t=text(1,5,theString);%puts letter in upper L corner
xlim([0,14])
ylim([0,14])
refline(1,0)
xlabel('R_e_c_o_ _m_o_d_e_l')
ylabel('R_e_c_o_ _p_a_r_t_i_t_i_o_n_e_d')
% legend('R_e_c_o_ _p_a_r_a_m','R_e_c_o_ _p_a_r_a_m','R_e_c_o_ _v_a_l_i_d','R_e_c_o_ _v_a_l_i_d')
box off
set(gcf, 'PaperPositionMode', 'auto');
print('Reco_onetoone_param_validation_MB','-dpng','-r600');

figure
plot(daily_GPP_min_param(1:1461),daily_GPP_obs_param(1:1461),'.r');
h=lsline;
% theString = sprintf('y = %.2f x + %.2f', p3(1), p3(2));
% place_x=min(daily_GPP_min_param(1:1461));
% place_y=max(daily_GPP_obs_param(1:1461));
% t=text(place_x,place_y,theString);%;t=text(2,-7,'p2');%puts letter in upper L corner
% theString = sprintf('r^2 = %.2f', rsq2);
% t=text(1,4,theString);%puts letter in upper L corner
xlim([-21,3])
ylim([-21,3])
hold on
plot(daily_GPP_min_valid(1:365),daily_GPP_obs_valid(1:365),'.k');
m=lsline;
% theString = sprintf('y = %.2f x + %.2f', p4(1), p4(2));
% place_x=min(daily_GPP_min_valid(1:365));
% place_y=max(daily_GPP_obs_valid(1:365));
% t=text(place_x,place_y,theString);%;t=text(2,-7,'p2');%puts letter in upper L corner
% theString = sprintf('r^2 = %.2f', rsq4);
% t=text(1,5,theString);%puts letter in upper L corner
xlim([-21,3])
ylim([-21,3])
refline(1,0)
xlabel('GPP_m_o_d_e_l')
ylabel('GPP_p_a_r_t_i_t_i_o_n_e_d')
%legend('GPP_p_a_r_a_m','GPP_p_a_r_a_m','GPP_v_a_l_i_d','GPP_v_a_l_i_d')
%legend('Param','Param','Valid','Valid')
box off
set(gcf, 'PaperPositionMode', 'auto');
print('GPP_onetoone_param_validation_MB','-dpng','-r600');

figure
plot(daily_Reco_min_param(1462:end),daily_Reco_obs_param(1462:end),'.r');
h=lsline;
xlim([0,9])
ylim([0,9])
hold on
plot(daily_Reco_min_valid(366:end),daily_Reco_obs_valid(366:end),'.k');
m=lsline;
xlim([0,9])
ylim([0,9])
refline(1,0)
xlabel('R_e_c_o_ _m_o_d_e_l')
ylabel('R_e_c_o_ _p_a_r_t_i_t_i_o_n_e_d')
%legend('R_e_c_o_ _p_a_r_a_m','R_e_c_o_ _p_a_r_a_m','R_e_c_o_ _v_a_l_i_d','R_e_c_o_ _v_a_l_i_d')
box off
set(gcf, 'PaperPositionMode', 'auto');
print('Reco_onetoone_param_validation_WP','-dpng','-r600');

figure
plot(daily_GPP_min_param(1462:end),daily_GPP_obs_param(1462:end),'.r');
h=lsline;
xlim([-21,3])
ylim([-21,3])
hold on
plot(daily_GPP_min_valid(366:end),daily_GPP_obs_valid(366:end),'.k');
m=lsline;
xlim([-21,3])
ylim([-21,3])
refline(1,0)
xlabel('GPP_m_o_d_e_l')
ylabel('GPP_p_a_r_t_i_t_i_o_n_e_d')
%legend('GPP_p_a_r_a_m','GPP_p_a_r_a_m','GPP_v_a_l_i_d','GPP_v_a_l_i_d')
% legend('Param','Param','Valid','Valid')
box off
set(gcf, 'PaperPositionMode', 'auto');
print('GPP_onetoone_param_validation_WP','-dpng','-r600');

figure
plot(daily_Reco_min_EE,daily_Reco_obs_EE,'.k');
h=lsline;
xlim([0,6])
ylim([0,6])
refline(1,0)
xlabel('R_e_c_o_ _m_o_d_e_l')
ylabel('R_e_c_o_ _p_a_r_t_i_t_i_o_n_e_d')
%legend('R_e_c_o_ _p_a_r_a_m','R_e_c_o_ _p_a_r_a_m','R_e_c_o_ _v_a_l_i_d','R_e_c_o_ _v_a_l_i_d')
box off
set(gcf, 'PaperPositionMode', 'auto');
print('Reco_onetoone_EE','-dpng','-r600');

figure
plot(daily_GPP_min_EE,daily_GPP_obs_EE,'.k');
h=lsline;
xlim([-12,3])
ylim([-12,3])
refline(1,0)
xlabel('GPP_m_o_d_e_l')
ylabel('GPP_p_a_r_t_i_t_i_o_n_e_d')
%legend('GPP_p_a_r_a_m','GPP_p_a_r_a_m','GPP_v_a_l_i_d','GPP_v_a_l_i_d')
% legend('Param','Param','Valid','Valid')
box off
set(gcf, 'PaperPositionMode', 'auto');
print('GPP_onetoone_EE','-dpng','-r600');


%% plot posterior  2016.10.10
Ea_SOM_posterior=find_min_10000_Reco(:,1)+18+1.3;%kJ mol-1
km_SOM_posterior=find_min_10000_Reco(:,2)*1e-6*12*1e-6*1e2;%g C  cm-3
Ea_labile_posterior=find_min_10000_Reco(:,3)+17.5;
km_labile_posterior=find_min_10000_Reco(:,4)*1e-6*12*1e-6*1e2;%g C  cm-3
figure
subplot(4,1,1)
hist(Ea_SOM_posterior);%pdf for alpha 1
xlabel('Ea SOM (kJ mol^-^1)')
subplot(4,1,2)
hist(km_SOM_posterior);%pdf for ea 1
xlabel('km SOM (g C cm^-^3 soil)')
subplot(4,1,3)
hist(Ea_labile_posterior);%pdf for km 1
xlabel('Ea labile (kJ mol^-^1)')
subplot(4,1,4)
hist(km_labile_posterior);%pdf for alpha 2
xlabel('km labile (g C cm^-^3 soil)')
 
set(gcf, 'PaperPositionMode', 'auto');
print('PEPRMT_Rec_posterior_20161010,'-dpng','-r600');

%Final parameter estimates
Ea_SOM_FINAL=find_min(1)+18;%kJ mol-1
km_SOM_FINAL=find_min(2)*1e-6*12/100/15;%g C cm-3
Ea_labile_FINAL=find_min(3)+17.5;
km_labile_FINAL=find_min(4)*1e-6*12/100/15;%g C cm-3
%% Correlation matrix Reco 2016.10.10

figure
subplot(4,4,1)
plot(Ea_SOM_posterior,Ea_SOM_posterior,'.');%pdf for alpha 1
subplot(4,4,5)
plot(km_SOM_posterior,Ea_SOM_posterior,'.');%pdf for ea 1
subplot(4,4,6)
plot(km_SOM_posterior,km_SOM_posterior,'.');%pdf for ea 1
subplot(4,4,9)
plot(Ea_labile_posterior,Ea_SOM_posterior,'.');%pdf for ea 1
subplot(4,4,10)
plot(Ea_labile_posterior,km_SOM_posterior,'.');%pdf for ea 1
subplot(4,4,11)
plot(Ea_labile_posterior,Ea_labile_posterior,'.');%pdf for ea 1
subplot(4,4,13)
plot(km_labile_posterior,Ea_SOM_posterior,'.');%pdf for ea 1
subplot(4,4,14)
plot(km_labile_posterior,km_SOM_posterior,'.');%pdf for ea 1
subplot(4,4,15)
plot(km_labile_posterior,Ea_labile_posterior,'.');%pdf for ea 1
subplot(4,4,16)
plot(km_labile_posterior,km_labile_posterior,'.');%pdf for ea 1
 
set(gcf, 'PaperPositionMode', 'auto');
print('PEPRMT_Reco_correlation_matrix_20161010','-dpng','-r600');

%% plot posteriors from all models UPDATED 10/2016
%load file with all CO2 posteriors 20150623
figure
subplot(3,4,1)
hist(Ea_SOM_posterior);%pdf for alpha 1
xlabel('Ea_S_O_M (kJ mol^-^1)')
subplot(3,4,2)
hist(km_SOM_posterior*12/100/15);%pdf for ea 1
xlabel('km_S_O_M (g C cm^-^3 soil)')
subplot(3,4,3)
hist(Ea_labile_posterior);%pdf for km 1
xlabel('Ea_l_a_b_i_l_e (kJ mol^-^1)')
subplot(3,4,4)
hist(km_labile_posterior*12/100/15);%pdf for alpha 2
xlabel('km_l_a_b_i_l_e (g C cm^-^3 soil)')

subplot(3,4,5)
hist(Rref_posterior_TPGPP*12*1e-6*60*60*24);%pdf for alpha 1
xlabel('R_r_e_f (g C m^-^2 d^-^1)')
subplot(3,4,6)
hist(Eo_posterior_TPGPP);%pdf for ea 1
xlabel('E_o (k)')
subplot(3,4,7)
hist(k2_posterior_TPGPP*60*30);%pdf for km 1
xlabel('k2')

subplot(3,4,9)
hist(Rref_posterior_TP*12*1e-6*60*60*24);%pdf for alpha 1
xlabel('R_r_e_f (g C m^-^2 d^-^1)')
subplot(3,4,10)
hist(Eo_posterior_TP);%pdf for ea 1
xlabel('E_o (k)')

set(gcf, 'PaperPositionMode', 'auto');
print('ALL_CO2_posterior_20161017','-dpng','-r600');


%% plotting C pools 
%NEEDS UPDATING
%run find_min through Reco code so it outputs S1 and S2
[NEE_mod, S1, S2, Reco] = PEPRMT_final_sys_CO2_Reco(find_min,xdata);
%convert umol C m-3 to g C m-3
S1=(S1+(3e9*0.8))*1e-6*12/1000;%add in total C for SOM pool which initially starts out as SOMlabile only
S2=S2*1e-6*12/1000;
figure
plot(decday,S1,'.',decday, S2,'.')
legend('SOM','labile')
ylabel('Carbon pools (kg C m^-3)')
xlabel('Day of year')


figure
plot(days_plot,daily_Reco_mod_min,'.',days_plot,daily_Reco_obs','.')
legend('mod','obs')


%% Extra plotting
%NEEDS UPDATING
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Sel=percent_reduction>1;
percent_reduction(Sel)=1;

figure ('Units', 'pixels',...
    'Position', [100 100 500 375]);
plot(WT_gf,((1-percent_reduction)*100),'.')
ylabel('Inhibition of R_e_c_o (%)');
xlabel('Water table (cm)');


set(obs                           , ...
  'LineStyle'       , 'none'      , ...
  'Marker'          , '.'         , ...
  'Color'           , [0 0 0]  );
set(obs                       , ...
  'LineWidth'       , 1           , ...
  'Marker'          , '.'         , ...
  'MarkerSize'      , 15          , ...
  'MarkerEdgeColor' , [.2 .2 .2]  , ...
  'MarkerFaceColor' , [.7 .7 .7]  );
set(gcf, 'PaperPositionMode', 'auto');
print('Reco_inhibition_WT_gf_20150622','-dpng','-r600');
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

figure
subplot(2,1,1)
plot(date1,((1-M_percent_reduction)*100),'ko',...
    'LineWidth',0.5,...
    'MarkerSize',3,...
    'MarkerEdgeColor','k',...
    'MarkerFaceColor',[0,0,0]);
box off
ylabel('Inhibition of CH_4 production (%)');
set(gca,'FontSize',12)
x2 =1;
y2 = 2;
str2 = '(a)';
text(x2,y2,str2)

subplot(2,1,2)
plot(date1,daily_WT_gf,'ko',...
     'LineWidth',0.5,...
    'MarkerSize',3,...
    'MarkerEdgeColor','k',...
    'MarkerFaceColor',[0,0,0]);
box off
ylabel('Water table (cm)');
set(gca,'FontSize',12)

set(gcf, 'PaperPositionMode', 'auto');
print('WT_CH4_inhibition_20150622','-dpng','-r600');
set(gcf, 'PaperPositionMode', 'auto');
print('WT_ht_20150730','-dpng','-r600');

%print -depsc2 finalPlot1_inset.eps
close;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%import date tick excel file

figure
subplot(3,1,1)
plot(date1,daily_TA,'.')
ylabel('Air temperature (?C)')
subplot(3,1,2)
plot(date1,daily_PAR_gf,'.')
ylabel('PAR (�mol m^-^2 s^-^1)')
subplot(3,1,3)
plot(date1,daily_WT_gf,'.')
ylabel('Water table height (cm)')

set(obs                           , ...
  'LineStyle'       , 'none'      , ...
  'Marker'          , '.'         , ...
  'Color'           , [0 0 0]  );
set(obs                       , ...
  'LineWidth'       , 1           , ...
  'Marker'          , '.'         , ...
  'MarkerSize'      , 15          , ...
  'MarkerEdgeColor' , [.2 .2 .2]  , ...
  'MarkerFaceColor' , [.7 .7 .7]  );
set(gcf, 'PaperPositionMode', 'auto');
print('Met_variables_20150622','-dpng','-r600');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
days = 195:1:1097;%NEEDS TO BE UPDATED!!
days=days';
days=days-194;
DOY_daily=DOY-194;
%first do daily averages
[daily_DOY,daily_R1]=daily_ave(DOY_daily,R1',days);%time is 30min time scale, days is DOY
[daily_DOY,daily_R2]=daily_ave(DOY_daily,R2',days);%time is 30min time scale, days is DOY

figure
plot(daily_DOY,daily_R1,'.')

figure
plot(date1,daily_R1,'.',date1, daily_R2,'.')
ylabel('Respiration (�mol m^-^2 s^-^1)')
