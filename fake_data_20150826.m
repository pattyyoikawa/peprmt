%Test model against data with known parameter values and error structure
%similar to observations
%try with data similar to observed and then vary initial pool sizes
%and LAI

%% first test how the models vary if input variables are changed substantially

%Parameters for GPP model
GPP = PAMM_final_sys_CO2_GPP(theta,xdata);
figure
plot(decday(1:25776),GPP,'.',decday(1:25776),gpp_ANNnight(1:25776),'.')
legend('mod','obs')

%make different temp and LAI data, 
%simulating a hot scenario with inibition of LAI
%LAI is 35%lower than original data
%temp is on average 2.5 degree C hotter
TA_fake=TA*1.12;
nanmean(TA_fake)-nanmean(TA)

LAI_fake=LAI_gf*0.2;
figure
subplot(2,1,1)
plot(decday,LAI_gf,'.',decday,LAI_fake,'.')
legend('obs','fake')
subplot(2,1,2)
plot(decday,TA,'.',decday,TA_fake,'.')
%load that into xdata
xdata(:,6)=LAI_fake(1:25776);
xdata(:,3)=TA_fake(1:25776);

%use original parameters for GPP model
theta=[0 0 0];
theta=theta';
GPP_fake = PAMM_final_sys_CO2_GPP(theta,xdata);
figure
plot(decday(1:25776),GPP_fake,'.',decday(1:25776),gpp_ANNnight(1:25776),'.')
legend('fake','obs')

%same for Reco model
theta=[0 1 0 1];
xdata(:,7)=GPP_fake;
[NEE_fake, S1, S2, Reco] = PAMM_final_sys_CO2_Reco(theta,xdata);

figure
plot(decday(1:25776),Reco,'.',decday(1:25776),er_ANNnight(1:25776),'.')
legend('fake','obs')

figure
plot(decday(1:25776),NEE_fake,'.',decday(1:25776),wc_gf(1:25776),'.')
legend('fake','obs')

%Now run fake data through other models
Reco_2 = TPGPP_LAI_final_sys_CO2(find_min_TPGPP,xdata);
figure
plot(decday(1:25776),Reco,'.',decday(1:25776),Reco_2(1:25776),'.')
legend('DAMM','TPGPP')

NEE_fake_2=Reco_2+GPP_fake;
figure
plot(decday(1:25776),NEE_fake,'.',decday(1:25776),NEE_fake_2(1:25776),'.')
legend('DAMM','TPGPP')

Reco_3 = TP_final_sys_CO2(find_min_TP,xdata);
figure
plot(decday(1:25776),Reco,'.',decday(1:25776),Reco_2(1:25776),'.',decday(1:25776),Reco_3(1:25776),'.')
legend('DAMM','TPGPP','TP')

NEE_fake_3=Reco_3+GPP_fake;
figure
plot(decday(1:25776),NEE_fake,'.',decday(1:25776),NEE_fake_2(1:25776),'.',decday(1:25776),NEE_fake_3(1:25776),'.')
legend('DAMM','TPGPP','TP')

%Now look at CH4
CH4_DAMM = PAMM_final_sys_CH4(find_min_DAMM_CH4,xdata);
figure
plot(days,CH4_DAMM,'.',days,daily_wm_sum_umol_gf,'.')
legend('fake','obs')

CH4_TPGPP = TPGPP_LAI_final_sys_CH4(find_min_TPGPP_CH4,xdata);
figure
plot(days,CH4_DAMM,'.',days,CH4_TPGPP,'.')
legend('DAMM','TPGPP')

CH4_TP = TP_final_sys_CH4(find_min_TP_CH4,xdata);
figure
plot(days,CH4_DAMM,'.',days,CH4_TPGPP,'.',days,CH4_TP,'.')
legend('DAMM','TPGPP','TP')

%Conclusion, changing input variables didn't lead to large differences
%between the models--won't put this in the paper
%% Next create fake data using PEPRMT-DAMM only with known parameters
%add error to these data
%solve for the parameters and see if MDF works well

% k=theta(1)+0.8;%0.8 range=0-1
% Ha =theta(2)+30;%30;%activation energy for general crop plant
% Hd=theta(3)+100;%100;
theta_fake_GPP=[-0.6 10 -50];
%Parameters for GPP model
GPP_fake = PAMM_final_sys_CO2_GPP(theta_fake_GPP,xdata);
figure
plot(decday,GPP_fake,'.',decday,gpp_ANNnight,'.')
legend('fake','obs')

theta_fake_Reco=[-2 900 0.5 500];
xdata(:,7)=GPP_fake;
[NEE_fake, S1_fake, S2_fake, Reco_fake] = PAMM_final_sys_CO2_Reco(theta_fake_Reco,xdata);

figure
plot(decday,Reco_fake,'.',decday,er_ANNnight,'.')
legend('fake','obs')

figure
plot(decday,NEE_fake,'.',decday,wc_gf,'.')
legend('fake','obs')

%convert all to daily integrals and calculate ch4 fake
%use PAMM_run_CH4
%put daily fake data into xdata for CH4 model
xdata_ch4=[daily_DOY daily_DOY_disc daily_TA daily_WT_gf daily_PAR_gf daily_gpp_min_fake daily_S1_fake daily_S2_fake];

% M_ea1 = (theta(1)+70)*1000;% parameter in kJ mol-1; multiplied by 1000 = J mol-1
% M_km1 =theta(2)*3e3;%umol m-3 RECAL/OLD
% M_ea2 = (theta(3)+70)*1000;%76.25 only 1 Ea
% M_km2 =theta(4)*3e3;%1e3 ; LABILE/NEW
% M_ea3 = (theta(5)+70)*1000;%72 only 1 Ea
% M_km3 =theta(6)*3e3;%
theta_fake_CH4=[-7 900 3 500 10 500];

CH4_fake = PAMM_final_sys_CH4(theta_fake_CH4,xdata_ch4);
figure
plot(days,CH4_fake,'.',days,daily_wm_sum_umol_gf,'.')
legend('fake','obs')

%%%%%%%%%%%%%%%%
%now need error data at daily time step
%take relationship between original error data and data (Daily sums)
%and use that to calculate fake error for fake daily sums
figure
plot(daily_wc,'.')
hold on
plot(daily_nee_fake,'r.')
figure
plot(daily_wm_sum_umol_gf,'.')
hold on
plot(CH4_fake,'r.')

figure
plot(daily_gapfilling_error_wc,'.')
hold on
plot(daily_random_error_wc,'.')
hold on
plot(daily_wc,'.')
%add to nee's so they are positive numbers
gf_error_fake_wc=(daily_gapfilling_error_wc.*(daily_nee_fake'+8e5))./(daily_wc+8e5);
re_error_fake_wc=(daily_random_error_wc.*(daily_nee_fake'+8e5))./(daily_wc+8e5);

gf_error_fake_wm=(daily_gapfilling_error_wm.*CH4_fake)./daily_wm_sum_umol_gf;
re_error_fake_wm=(daily_random_error_wm.*CH4_fake)./daily_wm_sum_umol_gf;

figure
plot(gf_error_fake_wc,'.')
hold on
plot(daily_gapfilling_error_wc,'.r')
figure
plot(re_error_fake_wc,'.')
hold on
plot(daily_random_error_wc,'.r')

figure
plot(gf_error_fake_wm,'.')
hold on
plot(daily_gapfilling_error_wm,'.r')
figure
plot(re_error_fake_wm,'.')
hold on
plot(daily_random_error_wm,'.r')



%gapfill=[wc_90CI wc_90CI_lower wc_90CI_upper wm_90CI wm_90CI_lower wm_90CI_upper];
%random=[random_error_wc_prop_lower random_error_wc_prop_upper random_error_wm_prop_lower random_error_wm_prop_upper];

NEE_fake_no_gf=NEE_fake';
Sel=isnan(wc);
NEE_fake_no_gf(Sel)=NaN;

figure
plot(decday(1:25776),NEE_fake_no_gf(1:25776),'.')

random_error_fake_wc=1.05-(0.043*NEE_fake_no_gf);%for NEE at West Pond
Sel=isnan(random_error_fake_wc);
random_error_fake_wc(Sel)=0;
figure
plot(decday,random_error_fake_wc,'.')

%For GF error, I don't have 20ANN to determine gf error
%although I could run the gf code on the fake data and get the ANN...


%make daily variables
DOY_daily=DOY-193;

daily_GPP_fake_0=GPP_fake'*60*30;%convert error from umol m-2 s-1 into umol m-2 30min-1
daily_GPP_fake=daily_sum(DOY_daily,daily_GPP_fake_0,days);
daily_Reco_fake_0=Reco_fake'*60*30;%convert error from umol m-2 s-1 into umol m-2 30min-1
daily_Reco_fake=daily_sum(DOY_daily,daily_Reco_fake_0,days);

daily_NEE_fake_0=NEE_fake'*60*30;%convert error from umol m-2 s-1 into umol m-2 30min-1
daily_NEE_fake=daily_sum(DOY_daily,daily_NEE_fake_0,days);
 figure
 plot(daily_NEE_fake,'.')

daily_REerror_fake_0=RE_fake'*60*30;%convert error from umol m-2 s-1 into umol m-2 30min-1
daily_REerror_fake=daily_sum(DOY_daily,daily_REerror_fake_0,days);

daily_GFerror_fake_0=GF_fake'*60*30;%convert error from umol m-2 s-1 into umol m-2 30min-1
daily_GFerror_fake=daily_sum(DOY_daily,daily_GFerror_fake_0,days);

daily_TA_fake=daily_ave(DOY_daily,TA_fake,days);
daily_LAI_fake=daily_ave(DOY_daily,LAI_fake,days);

%Use new fake input data with thetas and errors and run models, get data and then run
%MDF starting theta's from 0 and see if you can derive the correct
%parameters

%Same for CH4 model
xdata(:,3)=TA_fake;
xdata(:,6)=GPP_fake;%output from CO2 PAMM model umol m-2 day-1
ch4_fake = PAMM_final_sys_CH4(theta,xdata);

figure
plot(days,ch4_fake,'.',days,wm_sum_umol,'.')
legend('fake','obs')










